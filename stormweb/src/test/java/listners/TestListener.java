package listners;

import java.awt.AWTException;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import com.aventstack.extentreports.Status;

import atu.testrecorder.ATUTestRecorder;
import atu.testrecorder.exceptions.ATUTestRecorderException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import testBase.*;

public class TestListener extends LoadProp implements ITestListener {
	String filePath = "Screenshots/";
	public String Video_methodName;
	Date today = new Date();
	String DateFormat = new SimpleDateFormat("yyyy-MM-dd").format(today);
	SimpleDateFormat sdf = new SimpleDateFormat("EEE");
	String dayOfTheWeek = sdf.format(today);
	public File videoPath;
	// String videopath=direc+direcname;

	private static ExtentReports extent = ExtentManager.createInstance("ZoyloQA-Report.html");
	private static ThreadLocal<ExtentTest> parentTest = new ThreadLocal<ExtentTest>();
	private static ThreadLocal<ExtentTest> test = new ThreadLocal<ExtentTest>();
	// Counter to keep track of retry attempts
	public int retryAttemptsCounter = 0;
	public ATUTestRecorder recorder;

	// The max limit to retry running of failed test cases
	// Set the value to the number of times we want to retry
	public int maxRetryLimit = 2;

	public synchronized void onStart(ITestContext context) {
		String ZoyloQA = "Regression Report On ZoyloQA";
		ExtentTest parent = extent.createTest(ZoyloQA);
		parentTest.set(parent);
	}

	public synchronized void onFinish(ITestContext context) {

		extent.flush();
	}

	public synchronized void onTestStart(ITestResult result) {
		ExtentTest child = parentTest.get().createNode(result.getMethod().getMethodName());
		test.set(child);
		System.out.println(result.getMethod().getMethodName());
		String methodName = result.getName().toString().trim();
		String className = result.getTestClass().toString().trim();
		System.out.println("***********Testing Class Name =" + className + "*****************");
		System.out.println("***********Testing Method Name =" + methodName + "***************");
		try {
			videoPath = new File(System.getProperty("user.dir") + "/Recordings/" + Environment + "/" + dayOfTheWeek
					+ "-" + DateFormat + "/");
			videoPath.mkdir();
			Video_methodName = result.getName().toString().trim() + "-";
			recorder = new ATUTestRecorder(videoPath.toString(), Video_methodName + today, false);
			recorder.start();
		} catch (ATUTestRecorderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized void onTestSuccess(ITestResult result) {
		test.get().pass("Test passed");
	}

	public synchronized void onTestFailure(ITestResult result) {

		// adding screenshots to log
		if (result.getStatus() == ITestResult.FAILURE) {
			String ScreenShot_methodName = result.getName().toString().trim() + "-";
			String screenShotPath = null;
			try {
				screenShotPath = capture(driver, ScreenShot_methodName + today);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String os = System.getProperty("os.name").toLowerCase(); // Added to verify the OS
			System.out.println("OS=" + os);
			test.get().log(Status.FAIL, result.getThrowable());
			try {
				if (os.equalsIgnoreCase("linux")) {
					test.get().log(Status.FAIL,
							"Snapshot below: " + test.get().addScreenCaptureFromPath(
									linux_imagepath + "/Screenshots/" + Environment + "/" + dayOfTheWeek + "-"
											+ DateFormat + "/" + ScreenShot_methodName + today + ".png"));
				} else {
					test.get().log(Status.FAIL,
							"Snapshot below: " + test.get().addScreenCaptureFromPath(screenShotPath));

				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				recorder.stop();

				if (os.equalsIgnoreCase("linux")) {
					String videodisplay = linux_imagepath + "/Recordings/" + Environment + "/" + dayOfTheWeek + "-"
							+ DateFormat + File.separator + Video_methodName + today + ".mov";
					String link = "<a href=\"" + videodisplay + "\"> " + Video_methodName + " </a>";
					test.get().log(Status.FAIL, "Click : " + link + " to download video file.");
				} else {
					String direc = videoPath + File.separator + Video_methodName + today + ".mov";
					String link = "<a href=\"" + direc + "\"> " + Video_methodName + " </a>";
					test.get().log(Status.FAIL, "Click : " + link + " to download video file.");
				}

			} catch (ATUTestRecorderException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// test.get().fail(result.getThrowable());
	}

	public String capture(WebDriver driver, String screenShotName) throws IOException {
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);

		String dest = System.getProperty("user.dir") + "/Screenshots/" + Environment + "/" + dayOfTheWeek + "-"
				+ DateFormat + "/" + screenShotName + ".png";
		File destination = new File(dest);
		FileUtils.copyFile(source, destination);

		return dest;
	}

	public synchronized void onTestSkipped(ITestResult result) {
		test.get().skip(result.getThrowable());
	}

	public synchronized void onTestFailedButWithinSuccessPercentage(ITestResult result) {

	}

	// Method to attempt retries for failure tests
//	public boolean retry(ITestResult result) {
//			if (result.getStatus() == ITestResult.FAILURE) {
//				if(retryAttemptsCounter < maxRetryLimit){
//					retryAttemptsCounter++;
//					return true;
//				}
//			}
//			return false;
//		}
}
