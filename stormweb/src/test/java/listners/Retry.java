package listners;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class Retry implements IRetryAnalyzer {
	private int retryCount = 0;
    private int maxRetryCount = 1;

// Below method returns 'true' if the test method has to be retried else 'false' 
//and it takes the 'Result' as parameter of the test method that just ran
    public boolean retry(ITestResult result) {
        if (retryCount < maxRetryCount) {
//            System.out.println("Retrying test " + result.getName() + " with status "
//                    + getResultStatusName(result.getStatus()) + " for the " + (retryCount+1) + " time(s).");
            retryCount++;
            return true;
        }
        return false;
    }
    
//    public String getResultStatusName(int status) {
//    	String resultName = null;
//    	if(status==1)
//    		resultName = "SUCCESS";
//    	if(status==2)
//    		resultName = "FAILURE";
//    	if(status==3)
//    		resultName = "SKIP";
//		return resultName;
//    }
//    
    
//    public boolean retry(ITestResult result) {
//		if (!result.isSuccess()) {
//			if(retryAttemptsCounter < maxRetryLimit){
//				retryAttemptsCounter++;
//				return true;
//			}
//		}
//		return false;
//	}	
    
//	//Counter to keep track of retry attempts
//	public int retryAttemptsCounter = 0;
//	
//	//The max limit to retry running of failed test cases
//	//Set the value to the number of times we want to retry
//	public int maxRetryLimit = 1;
	
}
