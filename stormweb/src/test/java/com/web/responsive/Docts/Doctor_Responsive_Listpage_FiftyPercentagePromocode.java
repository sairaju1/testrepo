
//@author:Ch.LakshmiKanth

package com.web.responsive.Docts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Doctor_Responsive_Listpage_FiftyPercentagePromocode  extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public String fiftyPer = "50.0";
	public String hundredPer = "100.0";
	public String resertPer = "15.0";
	public int OC_ZfcPerc;
	
	@BeforeClass
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Thread.sleep(2000);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", "+919553456665");
	 	Browser.update_MongoAttribute(Environment, "zoyloServicePromotion", "promotionCode", "DOC_PER","promotionDetailInfo.promotionalValue", fiftyPer);
	 	Dimension dm= new Dimension(360,640);
	 	driver.manage().window().setSize(dm);
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		
		Double value= Double.valueOf(fiftyPer);
		OC_ZfcPerc=(int)Math.round(value);
							
	}
	
	@Test
	public void DC_Responsive_BookAppointment_FiftyPercentage_Promocode() throws Exception{
		
		RecipientPage.Responsive_GenericSearch(Doctor_Location, "Doctors", "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor(Doctor_Name);
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		Thread.sleep(2000);
		int PromoCodeValue=RecipientPage.Calculate_PromoCodeamount(OC_ZfcPerc);
		System.out.println("Amount :"+PromoCodeValue);
		RecipientPage.EnterPromoCode(OC_Promocode);
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_PaymentToProceedButton);
		Thread.sleep(4000);
		Browser.waitFortheElementXpath(Elements_Recipients.PaymentSelection_ToPay);
		String Amount=Browser.getTextByXpath(Elements_Recipients.PaymentSelection_ToPay).replaceAll("[^0-9]", "");
		int Amount_afterPromocode=Integer.parseInt(Amount);
		Assert.assertEquals(Amount_afterPromocode, PromoCodeValue);
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(3000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		
	}
	
	@AfterClass
	public void CloseBrowser() throws Exception{
		
		Browser.update_MongoAttribute(Environment, "zoyloServicePromotion", "promotionCode", "DOC_PER","promotionDetailInfo.promotionalValue", resertPer);
		Browser.quit();
		
	}

}
