
//@author:Ch.LakshmiKanth

package com.web.responsive.Docts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Doctor_Responsive_BookAppointment_WithoutLogin extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	@BeforeClass
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Thread.sleep(2000);
	 	Dimension dm= new Dimension(360,640);
	 	driver.manage().window().setSize(dm);
	 	Browser.openUrl(EnvironmentURL);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
							
	}
	
	@Test
	public void DC_Responsive_BookAppointment_WithoutLogin() throws Exception{
		
		RecipientPage.Responsive_GenericSearch(Doctor_Location, "Doctors", "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor(Doctor_Name);
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		Thread.sleep(1000);
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(2000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Thread.sleep(2000);
		Assert.assertEquals(msg, "Booking Successful!");
	}
	
	@AfterClass
		public void CloseBrowser() throws Exception{
		Browser.quit();
		
	}

}
