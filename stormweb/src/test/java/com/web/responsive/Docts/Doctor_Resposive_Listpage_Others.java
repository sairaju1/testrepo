
//@author: Ch.LakshmiKanth

package com.web.responsive.Docts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Doctor_Resposive_Listpage_Others extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	@BeforeClass
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Thread.sleep(2000);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", "+919553456665");
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "zoylodoctor@gmail.com");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "zoylorecclinic@gmail.com");
	 	Dimension dm= new Dimension(360,640);
	 	driver.manage().window().setSize(dm);
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		
						
	}
	
	@Test
	public void DC_Responsive_BookAppointment_Others() throws Exception{
		
		RecipientPage.Responsive_GenericSearch(Doctor_Location, "Doctors", "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor(Doctor_Name);
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		RecipientPage.BookingDetails_ProceedToPay("Others", "yaswanth", "9985249990","No_Promocode");
		Thread.sleep(2000);
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		String otheruser=Browser.getTextByXpath(Elements_Recipients.bookingSucesssfull_AppointmentBookedFor);
		Assert.assertEquals(otheruser, "yaswanth");
	}
	
	@AfterClass
	public void CloseBrowser() throws Exception{
		Browser.quit();
	}

}
