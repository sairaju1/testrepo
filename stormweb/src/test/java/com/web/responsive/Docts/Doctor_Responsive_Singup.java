

//@author:Ch.LakshmiKanth

package com.web.responsive.Docts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Doctor_Responsive_Singup extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public String firstName;
	public String lastName;
	public String mobileNumber;
	public String email;
	
	@BeforeClass
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Thread.sleep(2000);
	 	Dimension dm= new Dimension(360,640);
	 	driver.manage().window().setSize(dm);
		Browser.openUrl(EnvironmentURL+"/login");
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		firstName="A"+Browser.generateRandomString(5).toLowerCase();
		lastName="B"+Browser.generateRandomString(5).toLowerCase();
		mobileNumber="9"+Browser.generateRandomNumber(9);
		email=Browser.generateRandomString(5).toLowerCase()+Browser.generateRandomNumber(4)+"@"+Browser.generateRandomString(3).toLowerCase()+".com";
							
	}
	
	@Test
	public void DC_Responsive_Singup_BookAppointment() throws Exception{
		
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_link);
		RecipientPage.SignUp_Details(firstName, lastName, mobileNumber, email, "Zoylo@123");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_signupButton);
		Thread.sleep(5000);
		String OTP=Browser.getOtp(email,Environment);
		System.out.println("OTP Is :"+OTP);
		Thread.sleep(3000);
		Browser.waitFortheElementXpath(Elements_Recipients.signUp_otp);
		Browser.enterTextByXpath(Elements_Recipients.signUp_otp, OTP);
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_OTP_submitButton);
		Thread.sleep(3000);
		RecipientPage.Responsive_GenericSearch(Doctor_Location, "Doctors", "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor(Doctor_Name);
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		//Select Self/Others and Promocode
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		Thread.sleep(1000);
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(2000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Thread.sleep(2000);
		Assert.assertEquals(msg, "Booking Successful!");
	}
	
	@AfterClass
	public void CloseBrowser() throws Exception{
		String id= Browser.get_Document_ID(Environment, "zoyloUser", "emailInfo.emailAddress", email);
		System.out.println("ID:"+id);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", email);
		Browser.remove_Document(Environment, "zoyloUserProfile", "zoyloId", id);
		Browser.quit();
	}
}
