
//@author:Ch.LakshmiKanth

package com.web.responsive.Docts;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Doctor_Responsive_ListPage  extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public String ModifiedFormatDate;
	
	@BeforeClass
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Thread.sleep(2000);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", "+919553456665");
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "zoylodoctor@gmail.com");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "zoylorecclinic@gmail.com");
	 	Dimension dm= new Dimension(360,640);
	 	driver.manage().window().setSize(dm);
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		ModifiedFormatDate=Browser.getDate_format("EEEE, dd MMM yyyy",true, 1);
		System.out.println("Modified Formated Date :"+ModifiedFormatDate);
						
	}
	 
	@Test(priority=1)
	public void DC_Responsive_BookAppointment_ListPage() throws Exception{
		
		RecipientPage.Responsive_GenericSearch(Doctor_Location, "Doctors", "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor(Doctor_Name);
		//Change Slot :- Select Night slot and in Nightslot select 2nd time slot
		Browser.clickOnTheElementByXpath("//span[@class='icon night-icon']");
		Browser.clickOnTheElementByXpath("(//ul[@class='d-flex align-items-center filter-list mt-0 flex-wrap']/li[2])[1]");
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		//Select Self/Others and Promocode
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		Thread.sleep(1000);
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(2000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Thread.sleep(2000);
		Assert.assertEquals(msg, "Booking Successful!");
	}	
		
	@Test(priority=2)
	public void DC_Responsive_RescheduleAppointment() throws Exception{
		
		Browser.clickOnTheElementByXpath("//div[@class='user-loggedin d-inline-flex align-items-center justify-content-end']");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments);
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(Elements_Recipients.header_myAppointments_Reschedule));
		action.moveToElement(we).click().build().perform();
		Thread.sleep(2000);
		String TodayDate=Browser.getCurrentDate();
		String MonthLastDate=Browser.GetCurrentMonthLastDate();
		String Rescheduledate=Browser.getModifiedDate(1);
		if(TodayDate.equals(MonthLastDate)){
			
			Browser.clickOnTheElementByXpath("(//span[@class='next'])[7]");
			Browser.clickOnTheElementByXpath("(//span[text()='"+Rescheduledate+"'])[3]");
			
		}else{
			
			Browser.clickOnTheElementByXpath("(//span[text()='"+Rescheduledate+"'])[3]");
			
		}
		
		Browser.clickOnTheElementByXpath("//span[@class='icon night-icon']");
		Browser.clickOnTheElementByXpath("(//ul[@class='d-flex align-items-center filter-list mt-0 flex-wrap']/li[2])[1]");
		Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_ConfirmAppointment);
		String alert=Browser.getTextByXpath(Elements_Recipients.header_myAppointments_RescheduleMsg);
		Assert.assertEquals(alert, Reschedule_Message);
		Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_RescheduleMsg_Close);
		Thread.sleep(2000);
		String GetRescheduledate=Browser.getTextByXpath(Elements_Recipients.header_myAppointments_AppointmentDate);
		Thread.sleep(2000);
		Assert.assertEquals(GetRescheduledate, ModifiedFormatDate);
		
	}
	
	@Test(priority=3)
	public void DC_Responsive_CancelAppointment() throws Exception{
		
		Browser.clickOnTheElementByXpath("//a[@class='mb-4 secondary-color font14']");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_CancelAppointment_Confirm);
		Thread.sleep(2000);
		Browser.ScrollUp();
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_CancelTab);
		String canceldate=Browser.getTextByXpath(Elements_Recipients.header_myAppointments_CancelTab_AppointmentDate);
		Assert.assertEquals(canceldate, ModifiedFormatDate);
	}
		
	
	@AfterClass
	public void CloseBrowser() throws Exception{
		Browser.quit();
	}
}
