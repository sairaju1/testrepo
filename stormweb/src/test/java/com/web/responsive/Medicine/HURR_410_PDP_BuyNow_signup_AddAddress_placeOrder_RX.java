// @Author: Sagar Sen

package com.web.responsive.Medicine;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Address;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Medicines;
import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class HURR_410_PDP_BuyNow_signup_AddAddress_placeOrder_RX extends LoadProp {
	
	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName, paymode, firstname, lastname, mobile, email, Password;
	public Address Address;
	public int qty = 1;
	public Double searchListPrice, productSubTotal_item, checkout_orderTotal, shippingAmt;
	public Person p;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		paymode = cod;
		productName = productName_rx;
		p = Browser.personData();
		firstname = p.getFirstName();
		lastname = p.getLastName();
		email = p.getEmail();
		mobile = "7" + Browser.generateRandomNumber(9);
		Password = "Zoylo@123";
		Address = p.getAddress();
		Browser.setScreen_dimentions(width, height);
	}

	@Test(groups = { "MED", "High" })
	public void buyNow_SignUp_addAddress_RX_COD() throws Exception {
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		searchListPrice = ecomPage.medicineHomePage_search(productName, "");
		ecomPage.PDP_itemQtyInfo(productName, qty, "Buy");
		productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, searchListPrice, qty);
		ecomPage.checkout_coupon_summary_newSignUp(false, "", productSubTotal_item);

		// SignUP
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_link);
		RecipientPage.SignUp_Details(firstname, lastname, mobile, email, Password);
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_signupButton);
		Thread.sleep(2000);
		String OTP = Browser.getOtp(email, Environment);
		Browser.waitFortheElementXpath(Elements_Recipients.signUp_otp);
		Browser.enterTextByXpath(Elements_Recipients.signUp_otp, OTP);
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_OTP_submitButton);
		checkout_orderTotal = ecomPage.checkout_coupon_summary_newSignUp(false, "", productSubTotal_item);
		ecomPage.checkout_prescriptionPopUp(false);
		ecomPage.shippingPage_addAddress("home", Address, "Telangana", "500085", mobile, "Hyderabad");
		Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_placeOrderBtn);
		
		if (productSubTotal_item < 500.00) {
			Browser.waitFortheElementXpath(Elements_Medicines.checkout_shippingAmtWait);
		}
		shippingAmt = Double
				.parseDouble(Browser.getTextByXpath(Elements_Medicines.checkout_shippingAmt).replaceAll("₹", ""));
		checkout_orderTotal = checkout_orderTotal + shippingAmt;
		ecomPage.shippingPage_orderSummary("", "", checkout_orderTotal, "Healthcare product online order OL.");
		ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
		String orderID = ecomPage.order_confirmationPage(productName, checkout_orderTotal, paymode);
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, orderSuccess_Status);
	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		String id = Browser.get_Document_ID(Environment, "zoyloUser", "emailInfo.emailAddress", email);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", email);
		Browser.remove_Document(Environment, "zoyloUserProfile", "zoyloId", id);
		Browser.quit();
	}

}
