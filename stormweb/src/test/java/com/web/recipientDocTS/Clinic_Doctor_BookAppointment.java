
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Clinic_Doctor_BookAppointment extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone", "+919553456665");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail","Prabhusclinic@gmail.com");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail","Prabhusch@gmail.com");
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(1000);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void ClinicBookAppointment_Doctor() throws Exception{
		
		Actions action = new Actions(driver);
		RecipientPage.GenericSearch("Doctors", Doctor_Location, Clinic_Name);
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.Clinic_SelectDoctorTab);
		
		String DC_Qualfication_One=Browser.getTextByXpath("(//div[@class='doc-details mr-4 flex-grow-1']/p)[2]");
		String DC_Qualification_Two=Browser.getTextByXpath("(//div[@class='doc-details mr-4 flex-grow-1']/p)[3]");
		Assert.assertEquals(DC_Qualification_Two, "PGCD  (14 yrs+)");
		
		WebElement we = driver.findElement(By.xpath("//div[@class='d-flex align-items-start w-100' and contains(., 'Dr Prabhus Chintamaneni')]//following-sibling::div/button"));
		action.moveToElement(we).click().build().perform();
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath("//button[contains(text(),'Confirm')]");
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		
		//Reschedule Clinic associated Doctor
		RecipientPage.header_UserDetails_SelectMenu("My Appointments", "doctor");
		String modifieddate=RecipientPage.Doctor_Reschedule();
		Thread.sleep(2000);
		String Reschduledate=Browser.getTextByXpath(Elements_Recipients.header_myAppointments_AppointmentDate);
		Assert.assertTrue(Reschduledate.contains(modifieddate));
		
		//Cancellation Appointment of Clinic assocaited Doctor
		RecipientPage.Doctor_Cancel_Appointment();
		Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_CancelTab);
		String canceldate=Browser.getTextByXpath(Elements_Recipients.header_myAppointments_CancelTab_AppointmentDate);
		Assert.assertTrue(canceldate.contains(modifieddate));
		
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
		
	}

}
