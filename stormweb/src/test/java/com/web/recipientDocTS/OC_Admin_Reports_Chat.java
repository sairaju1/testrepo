




package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import objectRepository.Elements_Admin;
import objectRepository.Elements_Recipients;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class OC_Admin_Reports_Chat extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public AdminPage AdminPage;
	public String appointmentId;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
		AdminPage=new AdminPage(driver);
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", "9985859990");
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", OC_DoctorEmail);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", OC_ClinicEmail);
	 	Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(OC_Username, OC_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(3000);
	 	
	}
	
	
	
	@Test(priority=1)
	public void BOOK_OC_Appointment() throws Exception{
		
		RecipientPage.HomePage_Select_OnlineConsultation("Cardiology");
		Thread.sleep(2000);
		RecipientPage.OC_ListPage_Search_Doctor(OC_DoctorName);
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.bookingDetails_OC_ProceedToPay);
		RecipientPage.MakePayment("Single", "NetBanking");
		Thread.sleep(10000);
		Browser.waitFortheElementXpath(Elements_Recipients.OC_StartConsultation);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_StartConsultation);
		Thread.sleep(3000);
		Browser.waitFortheElementXpath(Elements_Recipients.OC_Notification);
		String Recp=Browser.getTextByXpath(Elements_Recipients.OC_Notification);
		Assert.assertTrue(Recp.contains("Thank you for choosing ZOYLO."));
		Thread.sleep(3000);
		
		//Appointment Id
		appointmentId=Browser.get_Document_Attribute(Environment, "zoyloAppointment", "patientInfo.patientPhone", "9985859990", "appointmentId");
		Thread.sleep(2000);
		
		//ADMIN
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin("8888899999", "Zoylo@123");
		Thread.sleep(2000);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "True", "Online Consultancy");
		Browser.waitFortheElementXpath(Elements_Admin.Admin_OCReportsTab);
	}
	
	@DataProvider(name="staging")
	public Object[][] statusStage(){
		return new Object[][]{
			{"Scheduled"}, {"Cancelled"}, {"Completed"}, {"Payment Failed"},
		};
	}
		
		@Test(dataProvider="staging",priority=2)
		public void OC_Admin_Reports(String stages) throws Exception{
		//Report screen
		Thread.sleep(10000);
		driver.findElement(By.xpath(Elements_Admin.Admin_OCReports_Search)).sendKeys(appointmentId); //appointmentId
		Thread.sleep(2000);
		driver.findElement(By.xpath(Elements_Admin.Admin_OCReports_Search)).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(By.xpath(Elements_Admin.Admin_OCReports_Search)).sendKeys(Keys.ENTER);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_OCReportSearch_Submit); // Submit search criteria
		Thread.sleep(3000);
		Browser.scrollbyxpath("//td[contains(., '"+appointmentId+"')]/following-sibling::td[contains(., 'Change Status')]");
		String status=Browser.getTextByXpath("//td[contains(., '"+appointmentId+"')]/following-sibling::td[7]");
		Assert.assertEquals(status, "Scheduled");
		Browser.clickOnTheElementByXpath("//td[contains(., '"+appointmentId+"')]/following-sibling::td[contains(., 'Change Status')]");
		Browser.waitFortheElementXpath(Elements_Admin.Admin_OCReport_ViewHeader); //Header
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_OCReport_StatusDropDwn); //Status dropdown
		Browser.clickOnTheElementByXpath("(//li[contains(., '"+stages+"')])[1]");
		Browser.enterTextByXpath(Elements_Admin.Admin_OCSupport_StatusChangeComment, "Changing status to "+stages);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_OCSupport_StatusChangeSubmitBtn);
		Browser.waitFortheElementXpath(Elements_Admin.Admin_OCReport_StatusChangeSuccessMsg);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_OCSupport_StatusCloseBtn);
		String statusChanged=Browser.getTextByXpath("//td[contains(., '"+appointmentId+"')]/following-sibling::td[7]");
		Assert.assertEquals(statusChanged, stages);
		String statusComment=Browser.getTextByXpath("//td[contains(., '"+appointmentId+"')]/following-sibling::td[8]");
		Assert.assertEquals(statusComment, "Changing status to "+stages);
		
		}
	}


