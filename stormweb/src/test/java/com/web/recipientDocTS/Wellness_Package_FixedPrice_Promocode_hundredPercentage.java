
//@author: Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Wellness_Package_FixedPrice_Promocode_hundredPercentage extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public String fiftyPer = "50.0";
	public String hundredPer = "100.0";
	public String resertPer = "15.0";
	public int OC_ZfcPerc;

	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone", "+919553456665");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail","liangtsewellness@gmail.com");
		Browser.update_MongoAttribute(Environment, "zoyloServicePromotion", "promotionCode", Wellness_Promocode,"promotionDetailInfo.promotionalValue", hundredPer);
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(1000);
		
		String hundredPer = "100.0";
		Double value= Double.valueOf(hundredPer);
		OC_ZfcPerc=(int)Math.round(value);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void Wellness_Package_BooKappointment_ZFCFixedPrice_HundredPercentagePromocode() throws Exception{
		
		Actions action = new Actions(driver);
		RecipientPage.GenericSearch("Wellness", Wellness_Location, Wellness_Center);
		Thread.sleep(1000);
		WebElement we =driver.findElement(By.xpath(Elements_Recipients.Wellness_Package_ZFCFixedPrice_Book));
		action.moveToElement(we).click().build().perform();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Wellness_Package_Calendar_Ok);
		Thread.sleep(2000);
		WebElement confirm= driver.findElement(By.xpath(Elements_Recipients.Wellness_Package_Confirm));
		action.moveToElement(confirm).click().build().perform();
		Thread.sleep(3000);
		int PromoCodeValue=RecipientPage.Calculate_PromoCodeamount(OC_ZfcPerc);
		System.out.println("Amount :"+PromoCodeValue);
		Thread.sleep(1000);
		RecipientPage.BookingDetails_ProceedToPay("Self","","",Wellness_Promocode);
		Thread.sleep(2000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		String BS_ToBePaid=Browser.getTextByXpath("(//div[@class='d-flex align-items-center justify-content-between my-4']//h6)[2]").replaceAll("[^0-9]", "");
		int BS_ToBePaid_Amount=Integer.parseInt(BS_ToBePaid);
		Assert.assertEquals(BS_ToBePaid_Amount, PromoCodeValue);
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.update_MongoAttribute(Environment, "zoyloServicePromotion", "promotionCode", Wellness_Promocode,"promotionDetailInfo.promotionalValue", resertPer);
		Browser.quit();
	}

}
