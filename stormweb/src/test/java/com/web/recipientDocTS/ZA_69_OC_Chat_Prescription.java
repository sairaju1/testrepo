

//@author:Ch.LakshmiKanth


package com.web.recipientDocTS;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_69_OC_Chat_Prescription extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public String recipient_Name, formulation;
	public List<String> allFormulations;
	public int numberOfMedic = 6;
	public int viewFormulaSize;
	

	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", OC_User_DB_Phone);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", OC_DoctorEmail);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", OC_ClinicEmail);
	 	Browser.remove_Document(Environment, "zoyloDoctorConsultation", "patientInfo.patientPhone",OC_User_DB_Phone);
	 	allFormulations = new ArrayList<String>(numberOfMedic);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void Book_OC_Prescription_Chat() throws Exception{
		
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(OC_Username, OC_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(3000);
		Set<String> windows = driver.getWindowHandles();
		String Recipient = driver.getWindowHandle();
		((JavascriptExecutor)driver).executeScript("window.open();");
		Set<String> customerWindow = driver.getWindowHandles();
		customerWindow.removeAll(windows);
		String PMS = ((String)customerWindow.toArray()[0]);
		driver.switchTo().window(PMS);
		Browser.openUrl("https://pms-uat.zoylo.com");
		//RecipientPage.PMSLogin("9877777777", "Zoylo@123");
		RecipientPage.PMSLogin(PMS_Username, PMS_Password);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Select_Online);
		//Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Select_Clinic);
		Browser.clickOnTheElementByXpath(Elements_Recipients. PMS_Select_Clinic_SelectButton);
		String Checkdoc=Browser.getTextByXpath(Elements_Recipients.PMS_SignedINName);
		Assert.assertEquals(Checkdoc, PMS_Doctor);
		
		
		//Switching To Recipient
		driver.switchTo().window(Recipient);
		RecipientPage.HomePage_Select_OnlineConsultation("Cardiology");
		Thread.sleep(2000);
		RecipientPage.OC_ListPage_Search_Doctor(OC_DoctorName);
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.bookingDetails_OC_ProceedToPay);
		RecipientPage.MakePayment("Single", "NetBanking");
		Thread.sleep(10000);
		Browser.waitFortheElementXpath(Elements_Recipients.OC_StartConsultation);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_StartConsultation);
		Thread.sleep(3000);
		Browser.waitFortheElementXpath(Elements_Recipients.OC_Notification);
		String Recp=Browser.getTextByXpath(Elements_Recipients.OC_Notification);
		Assert.assertTrue(Recp.contains("Thank you for choosing ZOYLO."));
		Thread.sleep(3000);
		
		//Switching To PMS
		driver.switchTo().window(PMS);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Click_ONlineConsultation);
		Thread.sleep(3000);
//		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_CloseNotification);
//		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_GeneratePrescription);
		
		//Prescription -Complaints
		Browser.enterTextByID(Elements_Recipients.PMS_Prescription_Complaints_Description, "Fever");
		Browser.selectIDbyText(Elements_Recipients.PMS_Prescription_Complaints_SinceValue, "2");
		Browser.selectIDbyText(Elements_Recipients.PMS_Prescription_Complaints_ProblemSince, "Day");
		Browser.selectIDbyText(Elements_Recipients.PMS_Prescription_Complaints_Severity, "2");
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Complaints_Save);
		Browser.waitFortheElementXpath(Elements_Recipients.PMS_Prescription_Complaints_SaveNotification);
		
		//Disease
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Diseases_Description, "Pelvis sindrome");
		Browser.selectIDbyText(Elements_Recipients.PMS_Prescription_Diseases_NatureOFDiagnosis, "Definitive");
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Diseases_Save);
		Browser.waitFortheElementXpath(Elements_Recipients.PMS_Prescription_disease_SaveNotification);
		
		// Investigation
		Browser.enterTextByID(Elements_Recipients.PMS_Prescription_Investigation, "Completed");
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Investigation_Save);
		Browser.waitFortheElementXpath(Elements_Recipients.PMS_Prescription_investigation_SaveNotification);
		
		//Medication
		for (int i =1; i <=numberOfMedic; i++) {
			
			formulation = Browser.generateRandomString(3 + i).toLowerCase();
			Browser.enterTextByXpath("("+Elements_Recipients.PMS_Prescription_Medication_Formulation+")[" + i + "]",formulation);
			Browser.enterTextByXpath("("+Elements_Recipients.PMS_Prescription_Medication_BrandName+")[" + i + "]",Browser.generateRandomString(2 + i).toLowerCase());
			Browser.enterTextByXpath("("+Elements_Recipients.PMS_Prescription_Medication_Dosage+")[" + i + "]","1");
			Browser.selectXpathByText("(//li/label[contains(., 'Frequency')]/following-sibling::select)[" + i + "]","Twice Daily");
			Browser.enterTextByXpath("("+Elements_Recipients.PMS_Prescription_Medication_Duration+")[" + i + "]","2");
			Browser.selectXpathByText("(//li/label[contains(., 'Duration Type')]/following-sibling::select)[" + i + "]","Day");
			if (i != numberOfMedic) {
			Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_AddNewMedication);
			Browser.scrollbyxpath(Elements_Recipients.PMS_Prescription_AddNewMedication);
		
			}
			
			allFormulations.add(formulation);
			
			}
		
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Medication_Save);
		Browser.waitFortheElementXpath(Elements_Recipients.PMS_Prescription_Medication_SaveNotification);
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_EndPrescription);
		Thread.sleep(2000);
		
		//View Prescription
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_ViewPrescription);
		// In View Prescription  Validating Medication No.of rows are available
		viewFormulaSize = driver.findElements(By.xpath("//table[@class='prescriptionDetails' and contains(., 'Medication')]/tr/td[1]/label")).size();
		for (int s = 2; s <= viewFormulaSize - 1; s++) {
			String viewFormula = Browser.getTextByXpath("(//table[@class='prescriptionDetails' and contains(., 'Medication')]/tr/td[1]/label)[" + s + "]");
			Assert.assertTrue(viewFormula.contains(allFormulations.get(s - 2)));
			System.out.println("View Assert of " + allFormulations.get(s - 2));
		}
		
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_ViewPrescription_Close);// Closing View Prescription
		
		//Download
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Download);
		Thread.sleep(3000);
		Browser.removeAttributeNameValue_byXpath(Elements_Recipients.PMS_Prescription_Downlaod_URL, "style","visibility: hidden;");
		String path = Browser.getTextByXpath(Elements_Recipients.PMS_Prescription_Downlaod_URL);
		System.out.println("PDF URL IS : " + path);
		String output = Browser.readPDF(path);
		for (int b = 0; b < numberOfMedic; b++) {
			Assert.assertTrue(output.contains(allFormulations.get(b)));
			System.out.println("PDF Assert of " + allFormulations.get(b));
		}
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
		
	}

}
