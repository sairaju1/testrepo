
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Admin;
import objectRepository.Elements_Recipients;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Doctor_BookAppointment_Wallet extends LoadProp{
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public AdminPage AdminPage;
	public String appointmentID_cashback, appointmentID_transaction,appointmentID_PartialTransaction;
	public int cashback_balance, transaction_balance, partialTransaction_balance;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	AdminPage=new AdminPage(driver);
	 	
		
	}
	
	@Test(priority=1,groups = { "ZoyWeb", "High" })
	public void Doctor_GetAppointmentID_ZoyloWallet() throws Exception{
		
		Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone", "+919985259990");
		Browser.update_MongoAttribute(Environment, "zoyloWalletConfiguration", "walletConfigurationTypeCode",
				"MAX_PAYMENT_PERCENTAGE_LIMIT", "walletConfigurationValue", "100");
		//Recipient Doctor Booking Appointment
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin("9985259990", "zoylo@123");
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(2000);
		RecipientPage.GenericSearch("Doctors", "Lambasingi, Andhra Pradesh 531116", "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor("Dr Promocode Doctor");
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		Thread.sleep(3000);
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		Thread.sleep(2000);
		RecipientPage.MakePayment("Single", "netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		//Getting Appointment ID
		 appointmentID_cashback= Browser.get_Document_Attribute(Environment, "zoyloAppointment", "patientInfo.patientPhone", "+919985259990", "appointmentId");
		System.out.println("Appointment ID for Cashback :"+appointmentID_cashback);
	}
	
	@Test(priority=2,groups = { "ZoyWeb", "High" })
	public void Admin_ZoyloWallet_CashBack() throws Exception{
		//Admin Adding CashBack Amount
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin("8888899999", "Zoylo@123");
		Thread.sleep(3000);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "True", "Wallet");
		Thread.sleep(1000);
		AdminPage.Wallet_CreditWallet("9985259990", "Cashback", appointmentID_cashback, "3", "Hello hi cashback");
		String CashBackNoti=Browser.getTextByXpath(Elements_Admin.Admin_Wallet_CreditWallet_Notification);
		Assert.assertEquals(CashBackNoti, "Credit is added successfully.");
		Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone","+9195585259990");
	}
	
	@Test(priority=3,groups = { "ZoyWeb", "High" })
	public void Check_FullAmount_Dedit_ZoyloWallet() throws Exception {
	
		Browser.openUrl(EnvironmentURL);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(1000);
		RecipientPage.GenericSearch("Doctors", "Lambasingi, Andhra Pradesh 531116", "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor("Dr Promocode Doctor");
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		Thread.sleep(3000);
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		Thread.sleep(2000);
		RecipientPage.MakePayment("Single", "Wallet");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		CloseBrowser();
		appointmentID_transaction=Browser.get_Document_Attribute(Environment, "zoyloAppointment", "patientInfo.patientPhone", "+919985259990", "appointmentId");
		System.out.println("Appointment ID generated Full Wallet: "+appointmentID_transaction);
		Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone", "+919985259990");
	}
	
	@Test(priority=4,groups = { "ZoyWeb", "High" })
	public void Check_PartialPayment_ZoyloWallet() throws Exception{
		LaunchBrowser(); 
		//Browser.openUrl(EnvironmentURL);
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin("9985259990", "zoylo@123");
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(1000);
		RecipientPage.GenericSearch("Doctors", "Lambasingi, Andhra Pradesh 531116", "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor("Dr Promocode Doctor");
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		Thread.sleep(3000);
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		Thread.sleep(2000);
		RecipientPage.MakePayment("Multiple", "Netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		appointmentID_PartialTransaction=Browser.get_Document_Attribute(Environment, "zoyloAppointment", "patientInfo.patientPhone", "+919985259990", "appointmentId");
		System.out.println("Appointment ID generated for Partial Transaction : "+appointmentID_PartialTransaction);
		Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone", "+919985259990");
	}
	
	@Test(priority=5,groups = { "ZoyWeb", "High" })
	public void Check_Transactions() throws Exception {
		
		Browser.openUrl(EnvironmentURL);
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_headerUserIcon);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//a[contains(text(),'Zoylo Wallet')]");
		Thread.sleep(1000);
		cashback_balance=RecipientPage.wallet_transactionVerification("CreditAmount", appointmentID_cashback);
		Thread.sleep(1000);
		transaction_balance=RecipientPage.wallet_transactionVerification("DebitAmount", appointmentID_transaction);
		Thread.sleep(2000);
		partialTransaction_balance=RecipientPage.wallet_transactionVerification("DebitAmount", appointmentID_PartialTransaction);
		Assert.assertEquals(cashback_balance, 3);
		Assert.assertEquals(transaction_balance,2);
		Assert.assertEquals(partialTransaction_balance, 1);
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
	}
}
