

//@author:Ch.LakshmiKanth


package com.web.recipientDocTS;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_72_OC_PMS_AddAppointment extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public String recipient_Name, formulation;
	public String vital_temprature, vital_pulse, vital_respiration, vital_bp, vital_spo2, vital_height, vital_weight;
	public String complaint_description, complaint_since, complaint_problemSince, complaint_severity;
	public String diseases_discription, disease_nature, investigation, notes;
	public String medicine_name, medicine_Brand, medicine_dosage, medicine_frequency, medicine_duration, medicine_durationType;
	
	//PMS PDF Variables
		public String PDF_vital_temprature, PDF_vital_pulse, PDF_vital_respiration, PDF_vital_bp, PDF_vital_spo2, PDF_vital_height, PDF_vital_weight;
		public String PDF_complaint_description, PDF_complaint_since, PDF_complaint_problemSince, PDF_complaint_severity, PDF_diseases_discription, PDF_disease_nature;
		public String PDF_medicine_name, PDF_medicine_Brand, PDF_medicine_dosage, PDF_medicine_frequency, PDF_medicine_duration, PDF_medicine_durationType;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", OC_User_DB_Phone);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "variable@gmail.com");
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "editclinic@gmail.com");
	 	
	 	//Prescription variable signing
	 	// - Vitals
	 	vital_temprature="99.8"; vital_pulse="100"; vital_respiration="77"; vital_bp="120/145";
	 	vital_spo2="98";vital_height="189"; vital_weight="100";
	 	// - complaints
	 	complaint_description="Fever"; complaint_since="2"; complaint_problemSince="Day"; complaint_severity="2";
	 	// - Disease - Investigation
	 	diseases_discription="Viral"; disease_nature="Definitive"; investigation="On going";
	 	// - Medication
	 	medicine_name="Bcomplex"; medicine_Brand="Auro"; medicine_dosage="2"; medicine_frequency="Twice Daily";
	 	medicine_duration="2"; medicine_durationType="Day"; notes="Notes section.";
		
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void BookOC_PMS_AddAppointment() throws Exception{
		
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(OC_Username, OC_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(3000);
		Set<String> windows = driver.getWindowHandles();
		String Recipient = driver.getWindowHandle();
		((JavascriptExecutor)driver).executeScript("window.open();");
		Set<String> customerWindow = driver.getWindowHandles();
		customerWindow.removeAll(windows);
		String PMS = ((String)customerWindow.toArray()[0]);
		driver.switchTo().window(PMS);
		Browser.openUrl("https://pms-uat.zoylo.com");
		RecipientPage.PMSLogin("9990202899", PMS_Password);
		//RecipientPage.PMSLogin(PMS_Username, PMS_Password);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Select_Online);
		//Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Select_Clinic);
		Browser.clickOnTheElementByXpath(Elements_Recipients. PMS_Select_Clinic_SelectButton);
		String Checkdoc=Browser.getTextByXpath(Elements_Recipients.PMS_SignedINName);
		Assert.assertEquals(Checkdoc, "Welcome Varaible");
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath("//button[contains(text(),'Add Appointment')]");
		Browser.waitFortheElementXpath("(//h1[contains(., 'ADD NEW APPOINTMENT')])[1]");
		Browser.selectNameByText("choosedoctor", "Dr Varaible");
		Thread.sleep(2000);
		
		Browser.selectNameByText("timeslot", "21:00");
		Thread.sleep(1000);
		
//		Browser.clickOnTheElementByXpath("//select[@name='timeslot']");
//		Thread.sleep(1000);
//		Browser.clickOnTheElementByXpath("(//select[@name='timeslot']//option)[2]");
		
		Browser.selectNameByText("consultationType", "General");
		Browser.clickOnTheElementByXpath("//a[contains(text(),'NEXT')]");
		Thread.sleep(2000);
		Browser.enterTextByXpath("//input[@name='patientMobile']", "9985259990");
		Browser.clickOnTheElementByXpath("//input[@placeholder='Enter Address']");
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath("//div[@class='appointment-confirm-btn']/button");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath("(//div[@class='appointment-heading']//h3)[2]");
		Assert.assertTrue(msg.contains("Your appointment has been successfully booked ."));
		Browser.clickOnTheElementByXpath("//a[contains(text(),'OK')]");
		Browser.clickOnTheElementByXpath("//div[@class ='fc-title scheduled-status'][contains(., 'Honey')]");
		Browser.clickOnTheElementByXpath("//a[@class='doctor-bottom blue']");
		Thread.sleep(2000);
		
//		//Reschedule
//		Browser.clickOnTheElementByXpath("//a[contains(text(),'reschedule appointment')]");
//		Thread.sleep(3000);
//		Browser.selectXpathByText("//select[@name='timeslot']", "17:10");
//		Browser.clickOnTheElementByXpath("//a[contains(text(),'UPDATE')]");
//		Thread.sleep(3000);
//		Browser.clickOnTheElementByXpath("//div[@class ='fc-title scheduled-status'][contains(., 'Lakshmikanth')]");
//		Thread.sleep(1000);
//		Browser.clickOnTheElementByXpath("//a[@class='doctor-bottom blue']");
		
		//VITALS
		
		Browser.clickOnTheElementByXpath("//span[contains(text(),'Start Consultation')]");
		Browser.enterTextByXpath("//input[@id='temparature']", vital_temprature);
		Browser.enterTextByXpath("//input[@id='pulse']", vital_pulse);
		Browser.enterTextByXpath("//input[@id='respiration']", vital_respiration);
		Browser.enterTextByXpath("//input[@id='bloodpressure']", vital_bp);
		Browser.enterTextByXpath("//input[@id='sp']", vital_spo2);
		Browser.enterTextByXpath("//input[@id='height']", vital_height);
		Browser.enterTextByXpath("//input[@id='Weight']", vital_weight);
		Browser.clickOnTheElementByXpath("//span[contains(text(),'SAVE')]");
		Browser.waitFortheElementXpath("//div[@class='alert alert-success' and contains(., 'Doctor Consultation Vital information saved successfully')]");
		
		//Prescription -Complaints
		Browser.enterTextByID(Elements_Recipients.PMS_Prescription_Complaints_Description, complaint_description);
		Browser.selectIDbyText(Elements_Recipients.PMS_Prescription_Complaints_SinceValue, complaint_since);
		Browser.selectIDbyText(Elements_Recipients.PMS_Prescription_Complaints_ProblemSince, complaint_problemSince);
		Browser.selectIDbyText(Elements_Recipients.PMS_Prescription_Complaints_Severity, complaint_severity);
		Browser.clickOnTheElementByXpath("//button[contains(., 'SAVE')]");
		Browser.waitFortheElementXpath(Elements_Recipients.PMS_Prescription_Complaints_SaveNotification);
		
		//Disease
		Browser.enterTextByID("description", diseases_discription);
		//Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Diseases_Description, diseases_discription);
		Browser.selectIDbyText(Elements_Recipients.PMS_Prescription_Diseases_NatureOFDiagnosis, disease_nature);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Diseases_Save);
		Browser.waitFortheElementXpath(Elements_Recipients.PMS_Prescription_disease_SaveNotification);
		
		// Investigation
		Browser.enterTextByID(Elements_Recipients.PMS_Prescription_Investigation, investigation);
		Browser.clickOnTheElementByXpath("//p//span[contains(., 'SAVE')]");
		Browser.waitFortheElementXpath(Elements_Recipients.PMS_Prescription_investigation_SaveNotification);
		
		//Medication
		
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Medication_Formulation,medicine_name);
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Medication_BrandName, medicine_Brand);
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Medication_Dosage,medicine_dosage);
		Browser.selectXpathByText("(//li/label[contains(., 'Frequency')]/following-sibling::select)",medicine_frequency);
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Medication_Duration,medicine_duration);
		Browser.selectXpathByText("(//li/label[contains(., 'Duration Type')]/following-sibling::select)",medicine_durationType);
		Browser.clickOnTheElementByXpath("//button[contains(., 'SAVE')]");
		Browser.waitFortheElementXpath(Elements_Recipients.PMS_Prescription_Medication_SaveNotification);
		
		//Notes
		Browser.enterTextByID("complaintsNotes", notes);
		Browser.clickOnTheElementByXpath("//button[contains(., 'SAVE')]"); //Notes Save
		Browser.waitFortheElementXpath("//div[@class='alert alert-success' and contains(., 'EPrescription Notes Saved Sucessfully')]"); //Notes saved
		
		
		Thread.sleep(5000);
		Browser.scrollbyxpath("//button[@class='endPrescriptionButton']");
		Browser.clickOnTheElementByXpath("//button[@class='endPrescriptionButton']");
		Thread.sleep(3000);
		
		Browser.clickOnTheElementByXpath("//div[@class='fc-title checkedout-status']");
		Browser.clickOnTheElementByXpath("//a[contains(text(),'Details')]");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//a[contains(text(),'View Prescription')]");
		
		//PMS PDF Container Verification
			Browser.waitFortheElementXpath(Elements_Recipients.pdf_container);
				PDF_vital_temprature=Browser.getTextByXpath(Elements_Recipients.pdf_temperature);
				PDF_vital_pulse=Browser.getTextByXpath(Elements_Recipients.pdf_pulse);
				PDF_vital_respiration=Browser.getTextByXpath(Elements_Recipients.pdf_respiration);
				PDF_vital_bp=Browser.getTextByXpath(Elements_Recipients.pdf_bp1)+"/"+Browser.getTextByXpath(Elements_Recipients.pdf_bp2);
				PDF_vital_spo2=Browser.getTextByXpath(Elements_Recipients.pdf_spo2);
				PDF_vital_height=Browser.getTextByXpath(Elements_Recipients.pdf_height);
				PDF_vital_weight=Browser.getTextByXpath(Elements_Recipients.pdf_weight);
				PDF_complaint_description=Browser.getTextByXpath(Elements_Recipients.pdf_complaintText);
				PDF_complaint_since=Browser.getTextByXpath(Elements_Recipients.pdf_complaintSinceNum);
				PDF_complaint_problemSince=Browser.getTextByXpath(Elements_Recipients.pdf_complaintSinceUnit);
				PDF_complaint_severity=Browser.getTextByXpath(Elements_Recipients.pdf_complaintSeverity);
				PDF_diseases_discription=Browser.getTextByXpath(Elements_Recipients.pdf_diseaseText);
				PDF_disease_nature=Browser.getTextByXpath(Elements_Recipients.pdf_diseaseNature);
				PDF_medicine_name=Browser.getTextByXpath(Elements_Recipients.pdf_medecineName);
				PDF_medicine_Brand=Browser.getTextByXpath(Elements_Recipients.pdf_medecineBrand);
				PDF_medicine_dosage=Browser.getTextByXpath(Elements_Recipients.pdf_medecineDosage);
				PDF_medicine_frequency=Browser.getTextByXpath(Elements_Recipients.pdf_medecineFrequency);
				PDF_medicine_duration=Browser.getTextByXpath(Elements_Recipients.pdf_medecineDuration);
				PDF_medicine_durationType=Browser.getTextByXpath(Elements_Recipients.pdf_medecineDurationType);
				
				//PMS Assertion
				Assert.assertTrue(vital_temprature.contains(PDF_vital_temprature));
				Assert.assertTrue(vital_pulse.contains(PDF_vital_pulse));
				Assert.assertTrue(vital_respiration.contains(PDF_vital_respiration));
				Assert.assertTrue(vital_bp.contains(PDF_vital_bp));
				Assert.assertTrue(vital_spo2.contains(PDF_vital_spo2));
				Assert.assertTrue(vital_height.contains(PDF_vital_height));
				Assert.assertTrue(vital_weight.contains(PDF_vital_weight));
				Assert.assertTrue(complaint_description.toLowerCase().contains(PDF_complaint_description.toLowerCase()));
				Assert.assertTrue(complaint_since.contains(PDF_complaint_since));
				Assert.assertTrue(complaint_problemSince.toLowerCase().contains(PDF_complaint_problemSince.toLowerCase()));
				Assert.assertTrue(complaint_severity.contains(PDF_complaint_severity));
				Assert.assertTrue(diseases_discription.toLowerCase().contains(PDF_diseases_discription.toLowerCase()));
				Assert.assertTrue(disease_nature.toLowerCase().contains(PDF_disease_nature.toLowerCase()));
				Assert.assertTrue(medicine_name.toLowerCase().contains(PDF_medicine_name.toLowerCase()));
				Assert.assertTrue(medicine_Brand.toLowerCase().contains(PDF_medicine_Brand.toLowerCase()));
				Assert.assertTrue(medicine_dosage.contains(PDF_medicine_dosage));
				Assert.assertTrue(medicine_frequency.toLowerCase().contains(PDF_medicine_frequency.toLowerCase()));
				Assert.assertTrue(medicine_duration.contains(PDF_medicine_duration));
				Assert.assertTrue(medicine_durationType.toLowerCase().contains(PDF_medicine_durationType.toLowerCase()));		
		
		
		
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		Browser.quit();
	}

}
