
//author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Doctor_SingUp_BookAppointment extends LoadProp{
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public String firstName;
	public String lastName;
	public String mobileNumber;
	public String email;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.openUrl(EnvironmentURL+"/login");
	 	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 	if(driver.findElements(By.xpath("//button[@class='No thanks']")).size() > 0) {
		Browser.clickOnTheElementByXpath("//button[@class='No thanks']");
		}
	 	firstName="A"+Browser.generateRandomString(5).toLowerCase();
		lastName="B"+Browser.generateRandomString(5).toLowerCase();
		mobileNumber="9"+Browser.generateRandomNumber(9);
		email=Browser.generateRandomString(5).toLowerCase()+Browser.generateRandomNumber(4)+"@"+Browser.generateRandomString(3).toLowerCase()+".com";
	}
	
	
	@Test(groups = { "ZoyWeb", "High" })
	public void Doctors_SingUp_BookAppointment() throws Exception{
		
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_link);
		RecipientPage.SignUp_Details(firstName, lastName, mobileNumber, email, "Zoylo@123");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_signupButton);
		String OTP=Browser.getOtp(email,Environment);
		System.out.println("OTP Is :"+OTP);
		Thread.sleep(10000);
		Browser.waitFortheElementXpath(Elements_Recipients.signUp_otp);
		Browser.enterTextByXpath(Elements_Recipients.signUp_otp, OTP);
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_OTP_submitButton);
		Thread.sleep(1000);
		RecipientPage.GenericSearch("Doctors", Doctor_Location, "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor(Doctor_Name);
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		Thread.sleep(3000);
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		Thread.sleep(2000);
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);	
		Thread.sleep(1000);
		//Edit Profile
		RecipientPage.header_UserDetails_SelectMenu("My Profile", "view-profile");
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.myProfile_editButton);
		Browser.enterTextByXpath(Elements_Recipients.myProfile_EditfirstName, "yaswanth");
		Browser.enterTextByXpath(Elements_Recipients.myProfile_EditlastName,"prasad");
		
		Browser.clickOnTheElementByXpath("//div[@class='vdp-datepicker form-control p-0']//div//input");
		Browser.clickOnTheElementByXpath("//span[@class='day__month_btn up']");
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath("//span[@class='month__year_btn up'][contains(.,'2019')]");
		Browser.clickOnTheElementByXpath("(//span[@class='cell year'])[1]");
		Browser.clickOnTheElementByXpath("//span[contains(text(),'January')]");
		Browser.clickOnTheElementByXpath("(//span[@class='cell day'])[2]");
		
		
		Browser.selectIDbyText(Elements_Recipients.myProfile_Editgender,"Female");
		Browser.selectIDbyText(Elements_Recipients.myProfile_EditbloodGroup, "A+");
		Browser.clickOnTheElementByXpath(Elements_Recipients.myProfile_SaveButton);
		Thread.sleep(10000);
		String fullname=Browser.getTextByXpath(Elements_Recipients.myProfile_FullName);
		System.out.println("Fullname :"+fullname);
		Assert.assertTrue(fullname.contains("yaswanth"));
		Thread.sleep(1000); 
		//Add Address
		Browser.clickOnTheElementByXpath(Elements_Recipients.myprofile_Click_AddAddress);
		Browser.selectXpathByText(Elements_Recipients.myprofile_Address_AddressType, "Primary Address");
		Browser.enterTextByXpath(Elements_Recipients.myprofile_Address_AddressLineone,"Kakatiya Residency");
		Browser.enterTextByXpath(Elements_Recipients.myprofile_Address_AddressLineTwo,"Bandari Layout");
		Browser.enterTextByXpath(Elements_Recipients.myprofile_Address_Locality,"Nizampet");
		Browser.selectXpathByText(Elements_Recipients.myprofile_Address_Country, "India");
		Browser.selectXpathByText(Elements_Recipients.myprofile_Address_State, "Andhra Pradesh");
		Browser.selectXpathByText(Elements_Recipients.myprofile_Address_city, "Kakinada");
		Browser.enterTextByXpath(Elements_Recipients.myprofile_Address_pincode, "5200090");
		Browser.clickOnTheElementByXpath(Elements_Recipients.myprofile_Address_Add);
		Thread.sleep(1000);
		String edit=Browser.getTextByXpath("//a[@class='secondary-color font12']");
		Assert.assertEquals(edit, "Edit Address");
			
		
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		String id= Browser.get_Document_ID(Environment, "zoyloUser", "emailInfo.emailAddress", email);
		System.out.println("ID:"+id);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", email);
		Browser.remove_Document(Environment, "zoyloUserProfile", "zoyloId", id);
		Browser.quit();

		
	}

}
