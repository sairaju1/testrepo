
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_133_New_OC_CheckTheDataInPrescriptionPdf extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public String ChatMsg;
	public String vital_temprature,  vital_bp, vital_spo2, vital_height, vital_weight;
	public String complaint_description;
	public String diseases_discription,notes;
	public String medicine_name, medicine_Brand, medicine_dosage, medicine_frequency, medicine_duration, medicine_durationType,General_Advice;
	public String recipient_Name, formulation;
	public List<String> allFormulations;
	public int numberOfMedic = 6;
	public int viewFormulaSize;
	
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", OC_User_DB_Phone);
	 	//Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail", OC_Doctor_Email);
	 	//Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", OC_Doctor_Clinic);
	 	
	 	allFormulations = new ArrayList<String>(numberOfMedic);
	 	ChatMsg= Browser.generateRandomString(5);
	 	
	 	//Prescription variable signing
	 	// - Vitals
	 	vital_temprature="99.8";  vital_bp="120/145";vital_spo2="98";vital_height="189"; vital_weight="100";
	 	// - complaints
	 	complaint_description="Fever";
	 // - Disease - Investigation
	 	 diseases_discription="Dengue fever"; notes="Suffering With Pelvic Inflammatory Disease";
	 //Medication
	 	medicine_name="DOLO"; medicine_dosage="2";
	 	
	 	//General ADvice
	 	General_Advice="Suffering with Dengue Fever";
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void OC_CheckTheDataInPrescriptionPdf_AfterGeneration() throws Exception{
		
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(OC_Username, OC_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(3000);
		Set<String> windows = driver.getWindowHandles();
		String Recipient = driver.getWindowHandle();
		((JavascriptExecutor)driver).executeScript("window.open();");
		Set<String> customerWindow = driver.getWindowHandles();
		customerWindow.removeAll(windows);
		String PMS = ((String)customerWindow.toArray()[0]);
		driver.switchTo().window(PMS);
		Browser.openUrl(PMS_URL);
		RecipientPage.PMSLogin(OC_Doctor_Mobile, PMS_Password);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Select_Online);
		Browser.clickOnTheElementByXpath(Elements_Recipients. PMS_Select_Clinic_SelectButton);
		String Checkdoc=Browser.getTextByXpath(Elements_Recipients.PMS_SignedINName);
		System.out.println(Checkdoc);
		
		//Switching To Recipient
		driver.switchTo().window(Recipient);
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_GetOnlineConsultation);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Button_StartConsultation);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_SkipToDcoctorConsultation);
		RecipientPage.OC_Promocode("NO_Promocode");
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_BookingSucessfull_StartConsultation);
		Thread.sleep(1000);
		// RecipientPage.Click_CleverTap_Notification();
		Thread.sleep(3000);
		String msg = Browser.getTextByXpath(Elements_Recipients.OC_Notification);
		Assert.assertTrue(msg.contains("Thank you for choosing ZOYLO."));
		Browser.enterTextByXpath(Elements_Recipients.OC_Chat_TextArea, ChatMsg);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Chat_textArea_Send);
		
		//Switching To PMS
		driver.switchTo().window(PMS);
		Thread.sleep(1000);
		System.out.println("Switched To PMS");
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Click_ONlineConsultation);
		String text = Browser.getTextByXpath(Elements_Recipients.PMS_GetChat_Text);
		Assert.assertEquals(text, ChatMsg);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_PrescriptionTab);
		
		
		//Vitals
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Vitals_Height, vital_height);
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Vitals_Weight, vital_weight);
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Vitals_bp, vital_bp);
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Vitals_Temperature, vital_temprature);
		
		//Complaints
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Complaints_Click);
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Complaints_EnterComplaints, complaint_description);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Complaints_Select_Suggession);
		System.out.println("clicked on compaliant suggession");
		
		//Diseases
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Diseases_Click);
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Diseases_EnterDiseases, diseases_discription);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Diseases_Select_Suggession);
		System.out.println("clicked on compaliant suggession");
		
		//Notes
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Notes, notes);
		
		//Investigation
		Browser.clickOnTheElementByXpath("(//div[@class='multiselect__select'])[3]");
		Browser.enterTextByXpath("//input[@id='ajax'][@placeholder='Enter Investigation']", "Lipid");
		Browser.clickOnTheElementByXpath("//span[@data-select='Press enter to select']");
		
		
		//Medication
		for (int i =1; i <=numberOfMedic; i++) {
			
			formulation = Browser.generateRandomString(3 + i).toLowerCase();
			Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Medication, formulation);
			Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Medication_duration, "2");
			Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Medication_TimeofTheDay);
			Browser.selectXpathByText(Elements_Recipients.PMS_Prescription_Medication_ToBeTaken, "before meal");
			//Browser.clickOnTheElementByXpath("//button[@class='btn btn-secondary d-inline-block']");
			Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Medication_ADDDrug);
			
			
			allFormulations.add(formulation);
		}
		
		
		//General Advice
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_GeneralADvice, "Suffering with Dengue Fever");
		
		//Review After
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_ReviewAfter_Number, "1");
		Browser.selectXpathByValue(Elements_Recipients.PMS_Prescription_ReviewAfter_Period, "Week");
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Submit);
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_GeneratePrescription);
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_GeneratePrescription_confirm);
		Thread.sleep(2000);
		
		//Switching to Recipient- To verify Prescription is available
		driver.switchTo().window(Recipient);
		driver.navigate().refresh();
		Thread.sleep(4000);
		String pdfavailable = Browser.getTextByXpath(Elements_Recipients.OC_Download);
		Assert.assertEquals(pdfavailable, "DOWNLOAD");
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_EndSession);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_EndChating);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_NoThanks);
		Thread.sleep(1000);
		String GetCurrentURL = driver.getCurrentUrl();
		System.out.println(GetCurrentURL);
		Assert.assertTrue(GetCurrentURL.contains(EnvironmentURL));
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_headerUserIcon);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_header_MyConsultation);
		Browser.clickOnTheElementByXpath(Elements_Recipients.Myconsultation_ChatConversion_Click_activeUser);
		Thread.sleep(2000);
		//String MyConsultation_Chat_img = driver.findElement(By.xpath(Elements_Recipients.MyConsultation_ChatConversionTab_GetimageName)).getAttribute("src");
		//Assert.assertTrue(MyConsultation_Chat_img.contains("horse.png"));
		
		//Getting Appointment ID
		String AppointmentID=Browser.get_Document_Attribute(Environment, "zoyloAppointment", "patientInfo.patientPhone",OC_User_DB_Phone, "appointmentId");
		System.out.println("Appointment Id :"+AppointmentID);
		
		
		//Using Rest assured To Get the Download pdf URL and Pass to Pdf Read method and Asserting the Medicine 
		String PDFURL=RecipientPage.OC_GetPDF_URL(AppointmentID);
		System.out.println("URL :"+PDFURL);
		String output= Browser.readPDF(PDFURL);
		for (int b = 1; b <= numberOfMedic; b++) {
			Assert.assertTrue(output.contains(allFormulations.get(b-1)));
			System.out.println("PDF Assert of " + allFormulations.get(b-1));
		}
		
		Assert.assertTrue(output.contains(PreConsultation_Username));
		Assert.assertTrue(output.contains(complaint_description));
		Assert.assertTrue(output.contains(diseases_discription));
		Assert.assertTrue(output.contains(notes));
		Assert.assertTrue(output.contains(General_Advice));

	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
		
	}

}
