
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Admin;
import objectRepository.Elements_Recipients;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Clinic_FixedPricePackage_Promocode_FiftyPercentage extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public AdminPage AdminPage;
	public String fiftyPer = "50.0";
	public String hundredPer = "100.0";
	public String resertPer = "15.0";
	public int Clinic_ZfcPerc;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	AdminPage=new AdminPage(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone", "+919553456665");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail","Prabhusclinic@gmail.com");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail","Prabhusch@gmail.com");
		Browser.update_MongoAttribute(Environment, "zoyloServicePromotion", "promotionCode", Clinic_PromoCode,"promotionDetailInfo.promotionalValue", fiftyPer);
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(1000);
		
		String fiftyPer = "50.0";
		Double value= Double.valueOf(fiftyPer);
		Clinic_ZfcPerc=(int)Math.round(value);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void  Clinic_PackageBooKappointment_FixedPrice_FiftyPercentagePromocode() throws Exception{
		
		RecipientPage.GenericSearch("Doctors", Doctor_Location, Clinic_Name);
		Thread.sleep(2000);
		RecipientPage.Clinic_SelectPackage_Book_Confirm("ZFCFixedPrice");
		int PromoCodeValue=RecipientPage.Calculate_PromoCodeamount(Clinic_ZfcPerc);
		System.out.println("Amount :"+PromoCodeValue);
		Thread.sleep(1000);
		RecipientPage.BookingDetails_ProceedToPay("Self","","",Clinic_PromoCode);
		Thread.sleep(2000);
		/*Browser.waitFortheElementXpath("//div[@class='grey-bg border-dashed-grey']//h6");
		String Amount=Browser.getTextByXpath("//div[@class='grey-bg border-dashed-grey']//h6").replaceAll("[^0-9]", "");
		int Amount_afterPromocode=Integer.parseInt(Amount);
		Assert.assertEquals(Amount_afterPromocode, PromoCodeValue);*/
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(2000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		String BS_ToBePaid=Browser.getTextByXpath(Elements_Recipients.bookingSucessfull_AmountToBepaid).replaceAll("[^0-9]", "");
		int BS_ToBePaid_Amount=Integer.parseInt(BS_ToBePaid);
		Assert.assertEquals(BS_ToBePaid_Amount, PromoCodeValue);
		
		//Getting appointment Id
		String appointmentID= Browser.get_Document_Attribute(Environment, "zoyloAppointment", "patientInfo.patientPhone","+919553456665", "appointmentId");
		System.out.println("Appointment ID :"+appointmentID);
		
		//Checking In Admin Same Amount is Replicating or Not
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(Admin_Username, Admin_Password);
		Thread.sleep(1000);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "True", "Package Appointments");
		Thread.sleep(30000);
		Browser.clickOnTheElementByXpath(Elements_Admin.PackageAppointments_Clinic_BookingAppointmentFromDate);
		Browser.clickOnTheElementByXpath(Elements_Admin.PackageAppointments_Clinic_BookingDate_SelectTodayDate);
		Browser.clickOnTheElementByXpath(Elements_Admin.PackageAppointments_Clinic_BookingToDate);
		Browser.clickOnTheElementByXpath(Elements_Admin.PackageAppointments_Clinic_BookingDate_SelectTodayDate);
		Browser.clickOnTheElementByXpath(Elements_Admin.PackageAppointments_Clinic_BookingDate_Apply);
		Thread.sleep(20000);
		int tablesize=driver.findElements(By.xpath("//table[@class='datatable table']/tbody/tr")).size();
		System.out.println("Table size:"+tablesize);
		for(int i=1;i<=tablesize;i++){	
		String CheckAppointmentID =Browser.getTextByXpath("//table[@class='datatable table']/tbody/tr["+i+"]/td[1]");
		if(appointmentID.equals(CheckAppointmentID)){
		System.out.println("Appointment Id Matched At :"+i);
		Browser.clickOnTheElementByXpath("//table[@class='datatable table']/tbody/tr["+i+"]/td[1]");	
		String PaidAmount=Browser.getTextByXpath("//table[@class='datatable table']/tbody/tr["+i+"]/td[8]");
		int Admin_PaidAmount=Integer.parseInt(PaidAmount);
		Assert.assertEquals(PromoCodeValue, Admin_PaidAmount);
		
		
		}
		
		}
		
		
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.update_MongoAttribute(Environment, "zoyloServicePromotion", "promotionCode", Clinic_PromoCode,"promotionDetailInfo.promotionalValue", resertPer);
		Browser.quit();
	}

}
