
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_64_OC_ListPage_Chat extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", OC_User_DB_Phone);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", OC_DoctorEmail);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", OC_ClinicEmail);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void Book_OC_ListPage_Chat() throws Exception{
		
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(OC_Username, OC_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS); 
		Thread.sleep(3000);
		Set<String> windows = driver.getWindowHandles();
		String Recipient = driver.getWindowHandle();
		((JavascriptExecutor)driver).executeScript("window.open();");
		Set<String> customerWindow = driver.getWindowHandles();
		customerWindow.removeAll(windows);
		String PMS = ((String)customerWindow.toArray()[0]);
		driver.switchTo().window(PMS);
		Browser.openUrl("https://pms-uat.zoylo.com");
		RecipientPage.PMSLogin(PMS_Username, PMS_Password);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Select_Online);
		//Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Select_Clinic);
		Browser.clickOnTheElementByXpath(Elements_Recipients. PMS_Select_Clinic_SelectButton);
		String Checkdoc=Browser.getTextByXpath(Elements_Recipients.PMS_SignedINName);
		Assert.assertEquals(Checkdoc, PMS_Doctor);
		
		//Switching To Recipient
		driver.switchTo().window(Recipient);
		System.out.println("Switched To Recipient");
		driver.findElement(By.xpath(Elements_Recipients.home_GetOnlineConsultation)).click();
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_OnlineConsultation_Proceed);
		RecipientPage.OC_ListPage_Search_Doctor(OC_DoctorName);
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.bookingDetails_OC_ProceedToPay);
		Thread.sleep(2000);
		RecipientPage.MakePayment("Single", "NetBanking");
		Thread.sleep(6000);
		Browser.waitFortheElementXpath(Elements_Recipients.OC_StartConsultation);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_StartConsultation);
		Thread.sleep(3000);
		Browser.waitFortheElementXpath(Elements_Recipients.OC_Notification);
		String Recp=Browser.getTextByXpath(Elements_Recipients.OC_Notification);
		Assert.assertTrue(Recp.contains("Thank you for choosing ZOYLO."));
		Thread.sleep(3000);
		Browser.enterTextByXpath(Elements_Recipients.OC_Chat_TextArea, "Hi");
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Chat_textArea_Send);
		Thread.sleep(2000);
		
		
		//Switching To PMS
		driver.switchTo().window(PMS);
		Thread.sleep(1000);
		System.out.println("Switched To PMS");
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Click_ONlineConsultation);
		String text=Browser.getTextByXpath("(//div[@class='container']/div/p)[1]");
		Assert.assertEquals(text, "Hi");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@id='file']")).sendKeys(System.getProperty("user.dir")+"/TestData/horse.png");
		System.out.println("Image Had Sent");
		Thread.sleep(2000);
		
		//Switching To Recipient
		driver.switchTo().window(Recipient);
		Thread.sleep(3000);
		System.out.println("Switched To Recipient");
		String img=driver.findElement(By.xpath("//img[@class='doc-image']")).getAttribute("src");
		System.out.println(img);
		Assert.assertTrue(img.contains("horse.png"));
		
		
		
	}
	
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
	}
	
	

}
