
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_120_New_OC_GuestUser_ExistingNumber_PreConsultationAsement_Self extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public String ChatMsg;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	
	 	Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", OC_User_DB_Phone);
	 	//Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail", OC_Doctor_Email);
	 	//Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", OC_Doctor_Clinic); 	
	 	 ChatMsg= Browser.generateRandomString(5);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void OC_Check_GuestUser_ExistingNumber_PresConsultationAssessement_Self() throws Exception{
		
		Browser.openUrl(EnvironmentURL);
		Set<String> windows = driver.getWindowHandles();
		String Recipient = driver.getWindowHandle();
		((JavascriptExecutor)driver).executeScript("window.open();");
		Set<String> customerWindow = driver.getWindowHandles();
		customerWindow.removeAll(windows);
		String PMS = ((String)customerWindow.toArray()[0]);
		driver.switchTo().window(PMS);
		Browser.openUrl(PMS_URL);
		RecipientPage.PMSLogin(OC_Doctor_Mobile, PMS_Password);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Select_Online);
		Browser.clickOnTheElementByXpath(Elements_Recipients. PMS_Select_Clinic_SelectButton);
		String Checkdoc=Browser.getTextByXpath(Elements_Recipients.PMS_SignedINName);
		System.out.println(Checkdoc);
		
		//Switching To Recipient - New User Enter Name  and OTP authentication and Start Consultation
		driver.switchTo().window(Recipient);
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_GetOnlineConsultation);
		RecipientPage.OC_GuestUser_Login("Kanth", OC_Username);
		Browser.waitFortheElementXpath(Elements_Recipients.OC_Start_PresConsultataion_Assessment);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Start_PresConsultataion_Assessment);
		RecipientPage.OC_LoggedUser_PresAssessment_Questions("MySelf");
		Thread.sleep(2000);
		RecipientPage.OC_Symptoms_Quro();	
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Quro_StartConsultation);
		RecipientPage.Click_CleverTap_Notification(); 
		Thread.sleep(2000);
		RecipientPage.OC_Promocode("NO_Promocode");
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(8000);
		Browser.waitFortheElementXpath(Elements_Recipients.OC_BookingSucessfull_StartConsultation);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_BookingSucessfull_StartConsultation);
		Thread.sleep(3000);
		String msg=Browser.getTextByXpath(Elements_Recipients.OC_Notification);
		Assert.assertTrue(msg.contains("Thank you for choosing ZOYLO."));
		Browser.enterTextByXpath(Elements_Recipients.OC_Chat_TextArea, ChatMsg);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Chat_textArea_Send);
		
		//Switching To PMS
		driver.switchTo().window(PMS);
		Thread.sleep(1000);
		System.out.println("Switched To PMS");
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Click_ONlineConsultation);
		String text=Browser.getTextByXpath(Elements_Recipients.PMS_GetChat_Text);
		Assert.assertEquals(text, ChatMsg);
		Thread.sleep(1000);
		driver.findElement(By.xpath(Elements_Recipients.PMS_Chat_Attachfile)).sendKeys(System.getProperty("user.dir")+"/TestData/horse.png");
		System.out.println("Image Had Sent");
		Thread.sleep(2000);
				
		//Switching To Recipient
		driver.switchTo().window(Recipient);
		Thread.sleep(2000);
		System.out.println("Switched To Recipient");
		String img=driver.findElement(By.xpath("//img[@class='doc-image']")).getAttribute("src");
		System.out.println(img);
		Assert.assertTrue(img.contains("horse.png"));
				
		//Switching To PMS Pre- Assessment
		driver.switchTo().window(PMS);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_PresAssessmentTab);
		String symptom_one=Browser.getTextByXpath(Elements_Recipients.PMS_PreAssessment_GetSymptom);
		System.out.println(symptom_one);
		Assert.assertTrue(symptom_one.contains(PreConsultation_Symptom));
				
		//ADDING Prescription In PMS After Pre- ASSessment
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_PrescriptionTab);
		RecipientPage.PMS_Prescription_Vitals("5.9", "120", "120/80","99.7");
		RecipientPage.PMS_Prescription_Complaints("Fever");
		RecipientPage.PMS_Prescription_Diseases("Dengue fever");
		RecipientPage.PMS_Prescreption_Notes("Suffering With Pelvic Inflammatory Disease");
		RecipientPage.PMS_Prescription_Medication("DOLO", "2");
		RecipientPage.PMS_Prescription_GeneralAdvice("Suffering with Dengue Fever");
		RecipientPage.PMS_Prescription_Review("1", "Week");
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Submit);
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_GeneratePrescription);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_GeneratePrescription_confirm);
				
		//Switching to Recipient- To verify Prescription is available
		driver.switchTo().window(Recipient);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_EndSession);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_EndChating);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_NoThanks);
		Thread.sleep(1000);
		String GetCurrentURL=driver.getCurrentUrl();
		System.out.println(GetCurrentURL);
		Assert.assertTrue(GetCurrentURL.contains(EnvironmentURL));
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_headerUserIcon);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_header_MyConsultation);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.Myconsultation_ChatConversion_Click_activeUser);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.MyConsultation_ChatConversionTab);
		String MyConsultation_Chat_img=driver.findElement(By.xpath(Elements_Recipients.MyConsultation_ChatConversionTab_GetimageName)).getAttribute("src");
		Assert.assertTrue(MyConsultation_Chat_img.contains("horse.png"));
		
		//Clicking on Myconsultation PresConsultation Tab and asserting
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_header_MyConsultation_PreAssessment);
		Thread.sleep(3000);
		String MyCons_Age = Browser.getTextByXpath(Elements_Recipients.MyConsultation_PreConsultation_Age);
		Assert.assertTrue(MyCons_Age.contains(PreConsultation_Age));
		String MyCons_Gender = Browser.getTextByXpath(Elements_Recipients.MyConsultation_PreConsultation_Gender);
		Assert.assertTrue(MyCons_Gender.contains(PreConsultation_Gender));
		String MyCons_Name = Browser.getTextByXpath(Elements_Recipients.MyConsultation_PreConsultation_Name);
		Assert.assertTrue(MyCons_Name.contains(PreConsultation_Username));
		String disease_one = Browser.getTextByXpath(Elements_Recipients.MyConsultation_PreConsultation_DiseaseHeadingOne);
		Assert.assertTrue(disease_one.contains(PreConsultation_diseaseOne));
		String disease_two = Browser.getTextByXpath(Elements_Recipients.MyConsultation_PreConsultation_DiseaseHeadingTwo);
		Assert.assertTrue(disease_two.contains(PreConsultation_diseaseTwo));
		String disease_three = Browser.getTextByXpath(Elements_Recipients.MyConsultation_PreConsultation_DiseaseHeadingThree);
		Assert.assertTrue(disease_three.contains(PreConsultation_diseaseThree));
		String Symptom_Mycons = Browser.getTextByXpath(Elements_Recipients.home_header_MyConsultation_PreAssessment_Symptoms);
		Assert.assertTrue(Symptom_Mycons.contains(PreConsultation_Symptom));
								
		//Getting Appointment ID
		String AppointmentID=Browser.get_Document_Attribute(Environment, "zoyloAppointment", "patientInfo.patientPhone",OC_User_DB_Phone, "appointmentId");
		System.out.println("Appointment Id :"+AppointmentID);
		Thread.sleep(3000);				
						
		//Using Rest assured To Get the Download pdf URL and Pass to Pdf Read method and Asserting the Medicine 
		String PDFURL=RecipientPage.OC_GetPDF_URL(AppointmentID);
		String output= Browser.readPDF(PDFURL);
		Assert.assertTrue(output.contains("DOLO"));
		
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
	}
	

}
