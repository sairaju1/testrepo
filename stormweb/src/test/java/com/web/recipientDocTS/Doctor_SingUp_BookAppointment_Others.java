

//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Doctor_SingUp_BookAppointment_Others extends LoadProp{
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public String firstName;
	public String lastName;
	public String mobileNumber;
	public String email;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.openUrl(EnvironmentURL+"/login");
	 	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 	if(driver.findElements(By.xpath("//button[@class='No thanks']")).size() > 0) {
		Browser.clickOnTheElementByXpath("//button[@class='No thanks']");
		}
	 	firstName="A"+Browser.generateRandomString(5).toLowerCase();
		lastName="B"+Browser.generateRandomString(5).toLowerCase();
		mobileNumber="9"+Browser.generateRandomNumber(9);
		email=Browser.generateRandomString(5).toLowerCase()+Browser.generateRandomNumber(4)+"@"+Browser.generateRandomString(3).toLowerCase()+".com";
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void SingupDoctor_BookAppointment_Others() throws Exception{
		
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_link);
		RecipientPage.SignUp_Details(firstName, lastName, mobileNumber, email, "Zoylo@123");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_signupButton);
		String OTP=Browser.getOtp(email,Environment);
		System.out.println("OTP Is :"+OTP);
		Thread.sleep(2000);
		Browser.enterTextByXpath(Elements_Recipients.signUp_otp, OTP);
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_OTP_submitButton);
		Thread.sleep(1000);
		RecipientPage.GenericSearch("Doctors", Doctor_Location, "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor(Doctor_Name);
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		Thread.sleep(3000);
		RecipientPage.BookAppointment_Others("yaswanth", "9985249990");
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_PaymentToProceedButton);
		Thread.sleep(2000);
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		String otheruser=Browser.getTextByXpath(Elements_Recipients.bookingSucesssfull_AppointmentBookedFor);
		Assert.assertEquals(otheruser, "yaswanth");
		
		
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		String id= Browser.get_Document_ID(Environment, "zoyloUser", "emailInfo.emailAddress", email);
		System.out.println("ID:"+id);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", email);
		Browser.remove_Document(Environment, "zoyloUserProfile", "zoyloId", id);
		Browser.quit();

		
	}

}
