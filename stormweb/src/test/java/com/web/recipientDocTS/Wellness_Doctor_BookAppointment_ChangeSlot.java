package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Wellness_Doctor_BookAppointment_ChangeSlot extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone", "+919553456665");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail","akashwellnessdc@gmail.com");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail","zoylodoctor@gmail.com");
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(1000);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void Wellness_BookAppointment_doctor_ChangeSlot() throws Exception{
		Actions action = new Actions(driver);
		RecipientPage.SelectWellness_Menu();
		RecipientPage.GenericSearch("Wellness", Wellness_Location, Wellness_Center);
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath("//li[@class='nav-item']//a[contains(., 'Doctors')]");
		//Browser.clickOnTheElementByXpath("(//a[contains(., 'Wellness')])[3]");
		WebElement we = driver.findElement(By.xpath(Elements_Recipients.Wellness_doctor_Book));
		action.moveToElement(we).click().build().perform();
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.Wellness_doctor_Confirm);
		Thread.sleep(2000);
		String time=Browser.getTextByXpath("//div[contains(@class,'timings')]/p[2]");
		System.out.println("Time Before Change TimeSlot :"+time);
		Browser.clickOnTheElementByXpath(Elements_Recipients.bookingDetails_Changeslot);
		Browser.clickOnTheElementByXpath(Elements_Recipients.bookingDetails_ChangeSlot_NightTab);
		Browser.clickOnTheElementByXpath("(//ul[@class='d-flex align-items-center filter-list mt-0 flex-wrap']/li[2])[1]");
		Browser.clickOnTheElementByXpath(Elements_Recipients.bookingDetails_ChangeSlot_OkButton);
		Thread.sleep(1000);
		String Change_Time=Browser.getTextByXpath("//div[contains(@class,'timings')]/p[2]");
		System.out.println("Time after Changing Time Slot :"+Change_Time);
		Assert.assertFalse(time.contains(Change_Time));
		Thread.sleep(2000);
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message );
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
		
	}

}
