
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_70_OC_ProfilePage extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	@BeforeClass
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", OC_User_DB_Phone);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", OC_DoctorEmail);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", OC_ClinicEmail);
		
	}
	
	@Test
	public void BookOCAppointment_ProfilePage() throws Exception{
		
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(OC_Username, OC_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(3000);
		Set<String> windows = driver.getWindowHandles();
		String Recipient = driver.getWindowHandle();
		((JavascriptExecutor)driver).executeScript("window.open();");
		Set<String> customerWindow = driver.getWindowHandles();
		customerWindow.removeAll(windows);
		String PMS = ((String)customerWindow.toArray()[0]);
		driver.switchTo().window(PMS);
		Browser.openUrl("https://pms-uat.zoylo.com");
		RecipientPage.PMSLogin(PMS_Username, PMS_Password);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Select_Online);
		//Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Select_Clinic);
		Browser.clickOnTheElementByXpath(Elements_Recipients. PMS_Select_Clinic_SelectButton);
		String Checkdoc=Browser.getTextByXpath(Elements_Recipients.PMS_SignedINName);
		Assert.assertEquals(Checkdoc, PMS_Doctor);
		
		//Switching To Recipient
		driver.switchTo().window(Recipient);
		Thread.sleep(2000);
		RecipientPage.HomePage_Select_OnlineConsultation("Cardiology");
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath("//div[@class='d-flex align-items-start']//h2[contains(., '"+OC_DoctorName+"')]");
		
		
		Browser.clickOnTheElementByXpath("//button[contains(text(),'CONSULT ONLINE')]");
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.bookingDetails_OC_ProceedToPay);
		RecipientPage.MakePayment("Single", "NetBanking");
		Thread.sleep(10000);
		Browser.waitFortheElementXpath(Elements_Recipients.OC_StartConsultation);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_StartConsultation);
		Thread.sleep(3000);
		Browser.waitFortheElementXpath(Elements_Recipients.OC_Notification);
		String Recp=Browser.getTextByXpath(Elements_Recipients.OC_Notification);
		Assert.assertTrue(Recp.contains("Thank you for choosing ZOYLO."));
		Thread.sleep(3000);
		Browser.enterTextByXpath(Elements_Recipients.OC_Chat_TextArea, "Hi");
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Chat_textArea_Send);
		Thread.sleep(1000);
		
		//Switching To PMS
		driver.switchTo().window(PMS);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Click_ONlineConsultation);
		String text=Browser.getTextByXpath("(//div[@class='container']/div/p)[1]");
		Assert.assertEquals(text, "Hi");
			
	}
	
	@AfterClass
	public void CloseBrowser() throws Exception{
		Browser.quit();
	}
	

}
