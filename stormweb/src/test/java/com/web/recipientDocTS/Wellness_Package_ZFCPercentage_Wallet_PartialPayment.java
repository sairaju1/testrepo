
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Wellness_Package_ZFCPercentage_Wallet_PartialPayment extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public String walletAmt="10";
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone", "+919985259990");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail","liangtsewellness@gmail.com");
		Browser.update_MongoAttribute(Environment, "zoyloWalletConfiguration", "walletConfigurationTypeCode",
				"MAX_PAYMENT_PERCENTAGE_LIMIT", "walletConfigurationValue", "100");
	 	Browser.update_MongoAttribute(Environment, "zoyloWallet", "userInfo.phoneNumber", "9985259990", "balanceAmount", "0.0");
	 
	
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void Wellness_Bookappointment_ZFCPercentage_wallet_PartialPayment() throws Exception{
		Browser.update_MongoAttribute(Environment, "zoyloWallet", "userInfo.phoneNumber", "9985259990", "balanceAmount", walletAmt);
		Browser.openUrl(EnvironmentURL+"/login");
	 	RecipientPage.Recipient_Sigin("9985259990", "zoylo@123");
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(2000);
		Actions action = new Actions(driver);
		RecipientPage.GenericSearch("Wellness", Wellness_Location, Wellness_Center);
		Thread.sleep(1000);
		WebElement we =driver.findElement(By.xpath(Elements_Recipients.Wellness_Package_ZFCPercentage_Book));
		action.moveToElement(we).click().build().perform();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Wellness_Package_Calendar_Ok);
		Thread.sleep(2000);
		WebElement confirm= driver.findElement(By.xpath(Elements_Recipients.Wellness_Package_Confirm));
		action.moveToElement(confirm).click().build().perform();
		Thread.sleep(2000);
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		Thread.sleep(2000);
		RecipientPage.MakePayment("Multiple", "netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);		
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
		
	}

}
