
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Recipient_ForgotPassword extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	@BeforeClass
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
		Browser.openUrl(EnvironmentURL+"/login");
		
	}
	
	@Test
	public void Check_ForgotPassword() throws Exception{
		
		Browser.clickOnTheElementByXpath(Elements_Recipients.forgotpasswd_link);
		Browser.enterTextByXpath(Elements_Recipients.SignIn_Mobile, "9553456665");
		Browser.clickOnTheElementByXpath(Elements_Recipients.forgotpasswd_submit);
		Thread.sleep(3000);
		String OTP= Browser.getOtp("lakshmikanth.c@zoylo.com", Environment);
		System.out.println("OTp is :"+OTP);
		Browser.enterTextByXpath(Elements_Recipients.forgotpasswd_getotp,OTP);
		Browser.clickOnTheElementByXpath(Elements_Recipients.forgotpasswd_submitotp);
		Browser.enterTextByXpath(Elements_Recipients.forgotpasswd_resetpwd, "zoylo@123");
		Browser.enterTextByXpath(Elements_Recipients.forgotpasswd_confirmreset, "zoylo@123");
		Browser.clickOnTheElementByXpath(Elements_Recipients.forgotpasswd_submitreset);
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		Thread.sleep(3000);
		Browser.getTextByXpath(Elements_Recipients.home_getLoginUserName);
		
	}
	
	@AfterClass
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
		
	}

}
