
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Doctor_BookAppointement_Promocode_HundredPercentage extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	public String fiftyPer = "50.0";
	public String hundredPer = "100.0";
	public String resertPer = "15.0";
	public int OC_ZfcPerc;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone", "+919553456665");
	 	Browser.update_MongoAttribute(Environment, "zoyloServicePromotion", "promotionCode", "DOC_PER","promotionDetailInfo.promotionalValue", hundredPer);
	 	
		Double value= Double.valueOf(hundredPer);
		OC_ZfcPerc=(int)Math.round(value);
		
		
	}
	@Test(groups = { "ZoyWeb", "High" })
	public void BookDoctorappointment_Promocode_HundredPercentage() throws Exception {
		
	
	Browser.openUrl(EnvironmentURL+"/login");
	RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
	driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	RecipientPage.GenericSearch("Doctors", Doctor_Location, "");
	Thread.sleep(1000);
	RecipientPage.ListPage_Select_Doctor(Doctor_Name);
	Browser.ScrollDown();
	Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
	Thread.sleep(3000);
	int CalculatedAmount_AdvancePayment_AfterApplyingCoupon=RecipientPage.Calculate_PromoCodeamount(OC_ZfcPerc);
	System.out.println("Amount :"+CalculatedAmount_AdvancePayment_AfterApplyingCoupon);
	Thread.sleep(1000);
	RecipientPage.BookingDetails_ProceedToPay("Self","","",OC_Promocode);
	Thread.sleep(2000);
	String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
	Assert.assertEquals(msg, Appointment_Sucessful_Message);
	Thread.sleep(1000);
	String Get_BS_RecipientPage_AdvanceAmount_ToBePaid=Browser.getTextByXpath(Elements_Recipients.bookingSucessfull_AmountToBepaid).replaceAll("[^0-9]", "");
	int BS_ToBePaid_Amount=Integer.parseInt(Get_BS_RecipientPage_AdvanceAmount_ToBePaid);
	Assert.assertEquals(BS_ToBePaid_Amount, CalculatedAmount_AdvancePayment_AfterApplyingCoupon);
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		Browser.update_MongoAttribute(Environment, "zoyloServicePromotion", "promotionCode", "DOC_PER","promotionDetailInfo.promotionalValue", resertPer);
		Browser.quit();
	}
}
