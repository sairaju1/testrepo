
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Doctor_BookAppointment_ChangeOtherClinic extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void BookDoctorAppointment_ChangeOtherClinic() throws Exception{
		
		
		//Dr Changeclinic Doctor
		RecipientPage.GenericSearch("Doctors", Doctor_Location, "Dr Changeclinic Doctor");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.ProfilePage_Click_DefaultClinic);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.ProfilePage_OtherClinicTab);
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.ProfilePage_ConfirmButton);
		Thread.sleep(3000);
		RecipientPage.BookingDetails_ProceedToPay("Self", "", "","No_Promocode");
		Thread.sleep(2000);
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, "Booking Successful!");
		Thread.sleep(2000);
		String clinicname=Browser.getTextByXpath("//div[@class='mt-5']//h2");
		Assert.assertEquals(clinicname, "Zoylo Rec Clinic");
		
		
		
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
		
	}

}
