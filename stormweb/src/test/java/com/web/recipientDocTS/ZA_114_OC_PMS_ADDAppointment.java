
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_114_OC_PMS_ADDAppointment extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public String vital_temprature,  vital_bp, vital_spo2, vital_height, vital_weight;
	public String complaint_description;
	public String diseases_discription,notes;
	public String medicine_name, medicine_Brand, medicine_dosage, medicine_frequency, medicine_duration, medicine_durationType;
	
	//PMS PDF Variables
 	public String PDF_vital_temprature,PDF_vital_bp,PDF_vital_height, PDF_vital_weight;
	public String PDF_complaint_description;
	public String PDF_diseases_discription;
	public String PDF_medicine_name,PDF_medicine_dosage,PDF_medicine_TimeOftheDay,PDF_medicine_toBeTaken;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", OC_User_DB_Phone);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "variable@gmail.com");
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "editclinic@gmail.com");
	 	
	 	//Prescription variable signing
	 	// - Vitals
	 	vital_temprature="99.8";  vital_bp="120/145";vital_spo2="98";vital_height="189"; vital_weight="100";
	 	// - complaints
	 	complaint_description="Fever";
	 // - Disease - Investigation
	 	 diseases_discription="Dengue fever"; notes="Suffering With Pelvic Inflammatory Disease";
	 //Medication
	 	medicine_name="DOLO"; medicine_dosage="2";
	 	
	 	
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void Check_BookOC_PMS_AddAppointment() throws Exception{
		
		Browser.openUrl("https://pms-uat.zoylo.com");
		RecipientPage.PMSLogin("9990202899", PMS_Password);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Select_Online);
		Browser.clickOnTheElementByXpath(Elements_Recipients. PMS_Select_Clinic_SelectButton);
		String Checkdoc=Browser.getTextByXpath(Elements_Recipients.PMS_SignedINName);
		Assert.assertEquals(Checkdoc, "Welcome Varaible");
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_ADDAppointment);
		Browser.waitFortheElementXpath("(//h1[contains(., 'ADD NEW APPOINTMENT')])[1]");
		Browser.selectNameByText("choosedoctor", "Dr Varaible");
		Thread.sleep(2000);
		Browser.selectNameByText("timeslot", "21:00");
		Thread.sleep(1000);
		Browser.selectNameByText("consultationType", "General");
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_ADDAppointment_Next);
		Thread.sleep(2000);
		Browser.enterTextByXpath(Elements_Recipients.PMS_ADDAppointment_Mobile, "9985859990");
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_ADDAppointment_EnterAddress);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_ADDAppointment_Confirm);
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.PMS_ADDAppointment_Sucessfull_Msg);
		Assert.assertTrue(msg.contains("Your appointment has been successfully booked ."));
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_ADDAppointment_OK);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_DashBoard_Scheduled);
		//Browser.clickOnTheElementByXpath("//a[@class='doctor-bottom blue']");
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_DashBoard_Details);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_ADDAppointment_StartConsultation);
		
		//Vitals
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Vitals_Height, vital_height);
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Vitals_Weight, vital_weight);
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Vitals_bp, vital_bp);
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Vitals_Temperature, vital_temprature);
				
		//Complaints
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Complaints_Click);
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Complaints_EnterComplaints, complaint_description);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Complaints_Select_Suggession);
		System.out.println("clicked on compaliant suggession");
		
		//Diseases
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Diseases_Click);
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Diseases_EnterDiseases, diseases_discription);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Diseases_Select_Suggession);
		System.out.println("clicked on Disease suggession");
		
		//Notes
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Notes, notes);
		
		//Medication
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Medication, medicine_name);
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Medication_duration, medicine_dosage);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Medication_TimeofTheDay);
		Browser.selectXpathByText(Elements_Recipients.PMS_Prescription_Medication_ToBeTaken, "before meal");
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Medication_ADDDrug);
		
		//General Advice
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_GeneralADvice, "Suffering with Dengue Fever");
		
		//Review After
		Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_ReviewAfter_Number, "1");
		Browser.selectXpathByValue(Elements_Recipients.PMS_Prescription_ReviewAfter_Period, "Week");
		
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Submit);
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_ADDAppointment_EndPrescription);
		Thread.sleep(4000);
		driver.navigate().refresh();
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath("//div[@class='fc-title checkedout-status'][contains(., 'Kanth')]");
		Browser.clickOnTheElementByXpath("//a[contains(text(),'Details')]");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//a[contains(text(),'View Prescription')]");
		Thread.sleep(2000);
		
		//Pdf Verification
		PDF_vital_temprature=Browser.getTextByXpath(Elements_Recipients.pdf_temperature);
		PDF_vital_bp=Browser.getTextByXpath(Elements_Recipients.pdf_bp1)+"/"+Browser.getTextByXpath(Elements_Recipients.pdf_bp2);
		PDF_vital_height=Browser.getTextByXpath(Elements_Recipients.pdf_height);
		PDF_vital_weight=Browser.getTextByXpath(Elements_Recipients.pdf_weight);
		PDF_complaint_description=Browser.getTextByXpath(Elements_Recipients.pdf_complaintText);
		PDF_diseases_discription=Browser.getTextByXpath(Elements_Recipients.pdf_diseaseText);
		PDF_medicine_name=Browser.getTextByXpath(Elements_Recipients.pdf_medecineName);
		PDF_medicine_dosage=Browser.getTextByXpath(Elements_Recipients.pdf_medecineDosage);
		PDF_medicine_TimeOftheDay=Browser.getTextByXpath(Elements_Recipients.pdf_medicineTimeoftheDay);
		PDF_medicine_toBeTaken=Browser.getTextByXpath(Elements_Recipients.pdf_medicinetoBeTaken);
		
		//PMS PDF Container Assertion
		Assert.assertTrue(vital_temprature.contains(PDF_vital_temprature));
		Assert.assertTrue(vital_bp.contains(PDF_vital_bp));
		Assert.assertTrue(vital_height.contains(PDF_vital_height));
		Assert.assertTrue(vital_weight.contains(PDF_vital_weight));
		Assert.assertTrue(complaint_description.toLowerCase().contains(PDF_complaint_description.toLowerCase()));
		Assert.assertTrue(medicine_name.toLowerCase().contains(PDF_medicine_name.toLowerCase()));
		Assert.assertTrue(medicine_dosage.contains(PDF_medicine_dosage));
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		Browser.quit();
	}


}
