
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Clinic_Package_StartingPriceZFCFixed extends LoadProp{
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone", "+919553456665");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail","Prabhusch@gmail.com");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail","liangtsewellness@gmail.com");
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(1000);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void Clinic_Package_BookAppointment_StartingPriceZFCFixed() throws Exception{
		
		RecipientPage.GenericSearch("Doctors", Doctor_Location, Clinic_Name);
		RecipientPage.Clinic_SelectPackage_Book_Confirm("ClinicPackageStartingPrice");
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		
		//Reschedule Clinic Appointment
		RecipientPage.header_UserDetails_SelectMenu("My Appointments", "package");
		String Rescheduledate=RecipientPage.Clinic_Reschedule_Appointent();
		String Reschedulemsg=Browser.getTextByXpath(Elements_Recipients.header_myAppointments_Package_Reschedule_Confirm_Msg);
		Assert.assertEquals(Reschedulemsg, Reschedule_Message);
		Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_Package_Reschedule_Confirm_PopupClose);
		Thread.sleep(2000);
		
		//Cancelling Clinic Appointment:-My Appointments
		RecipientPage.Clinic_Cancel_Appointment();
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_Package_CancelTab);
		String CancelDate=Browser.getTextByXpath(Elements_Recipients.header_myAppointments_Package_CancelTab_Date);
		Assert.assertTrue(CancelDate.contains(Rescheduledate));
		
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
		
	}


}
