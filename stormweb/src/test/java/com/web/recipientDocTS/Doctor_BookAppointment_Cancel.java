
//@author :Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Doctor_BookAppointment_Cancel extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", "+919553456665");
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "zoylodoctor@gmail.com");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "zoylorecclinic@gmail.com	");
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(1000);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void BookDoctorAppointment_Cancel() throws Exception{
		
		RecipientPage.GenericSearch("Doctors", Doctor_Location, "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor(Doctor_Name);
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		Thread.sleep(3000);
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		Thread.sleep(2000);
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_headerUserIcon);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments);
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath(Elements_Recipients.header_myAppointments_CancelAppointmentLink));
		action.moveToElement(we).click().build().perform();
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_CancelAppointment_Confirm);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_CancelTab);
		String date=Browser.getCurrentDate();
		System.out.println("Date is :"+date);
		String canceldate=Browser.getTextByXpath(Elements_Recipients.header_myAppointments_CancelTab_AppointmentDate);
		Assert.assertTrue(canceldate.contains(date));
		
		
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
	}

}
