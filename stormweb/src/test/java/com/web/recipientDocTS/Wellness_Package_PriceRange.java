
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Wellness_Package_PriceRange extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone", "+919553456665");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail","liangtsewellness@gmail.com");
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(1000);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void Wellness_Package_BooKappointment_PriceRange() throws Exception{
		
		RecipientPage.GenericSearch("Wellness", Wellness_Location, Wellness_Center);
		Thread.sleep(1000);
		RecipientPage.Wellness_SelectPackage_Book("PriceRange");
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		
		//Reschedule Wellness Appointment
		RecipientPage.header_UserDetails_SelectMenu("My Appointments", "package");
		String Rescheduledate= RecipientPage.Wellness_Reschedule();
		String Reschedulemsg=Browser.getTextByXpath(Elements_Recipients.header_myAppointments_Package_Reschedule_Confirm_Msg);
		Assert.assertEquals(Reschedulemsg, Reschedule_Message);
		Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_Package_Reschedule_Confirm_PopupClose);
		Thread.sleep(1000);
		
		//Cancelling Wellness appointment
		RecipientPage.Wellness_Cancel();
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_Package_CancelTab);
		String CancelDate=Browser.getTextByXpath(Elements_Recipients.header_myAppointments_Package_CancelTab_Date);
		Assert.assertTrue(CancelDate.contains(Rescheduledate));
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
		
	}

}
