
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Wellness_Doctor_BookAppointment_Others extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone", "+919553456665");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail","akashwellnessdc@gmail.com");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail","zoylodoctor@gmail.com");
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(1000);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void Wellness_BookAppointment_Doctor_Others() throws Exception{
		
		Actions action = new Actions(driver);
		RecipientPage.SelectWellness_Menu();
		RecipientPage.GenericSearch("Wellness", Wellness_Location, Wellness_Center);
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath("//li[@class='nav-item']//a[contains(., 'Doctors')]");
		//Browser.clickOnTheElementByXpath("(//a[contains(., 'Wellness')])[3]");
		WebElement we = driver.findElement(By.xpath(Elements_Recipients.Wellness_doctor_Book));
		action.moveToElement(we).click().build().perform();
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.Wellness_doctor_Confirm);
		Thread.sleep(2000);
		RecipientPage.BookingDetails_ProceedToPay("Others", "yaswanth", "9985249990","No_Promocode");
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		String otheruser=Browser.getTextByXpath(Elements_Recipients.bookingSucesssfull_AppointmentBookedFor);
		Assert.assertEquals(otheruser, "yaswanth");
		
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
	}
	

}
