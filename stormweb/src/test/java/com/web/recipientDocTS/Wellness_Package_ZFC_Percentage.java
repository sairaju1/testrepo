
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Wellness_Package_ZFC_Percentage extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone", "+919553456665");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail","liangtsewellness@gmail.com");
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(1000);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void Wellness_Bookappointment_ZFCPercentage() throws Exception{
		
		Actions action = new Actions(driver);
		RecipientPage.GenericSearch("Wellness", Wellness_Location, Wellness_Center);
		Thread.sleep(1000);
		WebElement we =driver.findElement(By.xpath(Elements_Recipients.Wellness_Package_ZFCPercentage_Book));
		action.moveToElement(we).click().build().perform();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Wellness_Package_Calendar_Ok);
		Thread.sleep(2000);
		WebElement confirm= driver.findElement(By.xpath(Elements_Recipients.Wellness_Package_Confirm));
		action.moveToElement(confirm).click().build().perform();
		Thread.sleep(2000);
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		Thread.sleep(2000);
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		
		//Reschedule Wellness Appointment
				Browser.clickOnTheElementByXpath(Elements_Recipients.home_headerUserIcon);
				Thread.sleep(1000);
				Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments);
				Thread.sleep(2000);
				Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_PackageTab);
				WebElement Reschedule = driver.findElement(By.xpath(Elements_Recipients.header_myAppointments_Package_Reschedule));
				action.moveToElement(Reschedule).click().build().perform();
				Thread.sleep(2000);
				String TodayDate=Browser.getCurrentDate();
				String MonthLastDate=Browser.GetCurrentMonthLastDate();
				String Rescheduledate=Browser.getModifiedDate(1);
				if(TodayDate.equals(MonthLastDate)){
					
					Browser.clickOnTheElementByXpath("(//span[@class='next'])[7]");
					Browser.clickOnTheElementByXpath("(//span[text()='"+Rescheduledate+"'])[3]");
					
				}else{
					
					Browser.clickOnTheElementByXpath("(//span[text()='"+Rescheduledate+"'])[3]");
					
				}
				
				Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_Package_Reschedule_Confirm);
				String Reschedulemsg=Browser.getTextByXpath(Elements_Recipients.header_myAppointments_Package_Reschedule_Confirm_Msg);
				Assert.assertEquals(Reschedulemsg, Reschedule_Message);
				Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_Package_Reschedule_Confirm_PopupClose);
				Thread.sleep(2000);
				
				//Cancelling Wellness appointment
				WebElement Canceled = driver.findElement(By.xpath(Elements_Recipients.header_myAppointments_Package_Cancel_Link));
				action.moveToElement(Canceled).click().build().perform();
				Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_Package_Cancel_Confirm);
				Thread.sleep(3000);
				Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_Package_CancelTab);
				String CancelDate=Browser.getTextByXpath(Elements_Recipients.header_myAppointments_Package_CancelTab_Date);
				Assert.assertTrue(CancelDate.contains(Rescheduledate));
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
		
	}
	
	}


