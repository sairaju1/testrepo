package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Admin_UpdateDoctor_Records extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public AdminPage AdminPage;
	
	

	@BeforeClass
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	AdminPage=new AdminPage(driver);
	 	Browser.openUrl("https://admin.zoylo.com/");
		AdminPage.adminLogin("8888899999", "Zoylo@123");
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(2000);
	 		
	}
	
	@DataProvider(name="UpdateDoctor")
    public Object[][] createData_DP1() throws Exception{
        Object[][] retObjArr=TestUtils.getTableArray("TestData/DoctorUpdate.xls","AdminDoctor", "TC1");
        return(retObjArr);
    }

	
	@Test(dataProvider="UpdateDoctor")
	public void Admin_Doctors_MoveToApproval(String emailId,String id,String userId) throws Exception{
		
		System.out.println("Email:"+emailId);
		Reporter.log("Validating move For Email:"+emailId);
		Browser.openUrl("https://admin.zoylo.com/admin/doctorRegistration/"+id+"/"+userId);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		Reporter.log("Entered url");
		Thread.sleep(4000);
		Browser.clickOnTheElementByXpath("//b[contains(text(),'Additional Info')]");
		Browser.clickOnTheElementByXpath("//b[contains(text(),'Additional Info')]");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[contains(text(),'Awards & Recognitions')]");
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath("//div[contains(text(),'Validate And Move to Approval Que')]");
		Thread.sleep(500);
		String notification=Browser.getTextByXpath("//div[contains(@class,'alert alert-success')]");
		Assert.assertTrue(notification.contains("Validate and move"));
		Thread.sleep(1000);
		
			
	}
	
	@AfterClass
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
	}

}
