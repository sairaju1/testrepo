
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Admin;
import objectRepository.Elements_Recipients;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Wellness_Package_FixedPrice_Promocode extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public AdminPage AdminPage;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
		Elements_Admin.Admin_PageProperties();
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	AdminPage=new AdminPage(driver);
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone", "+919553456665");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail","liangtsewellness@gmail.com");
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(1000);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void Wellness_Package_BooKappointment_ZFCFixedPrice_Promocode() throws Exception{
		
		Actions action = new Actions(driver);
		RecipientPage.GenericSearch("Wellness", Wellness_Location, Wellness_Center);
		Thread.sleep(1000);
		WebElement we =driver.findElement(By.xpath(Elements_Recipients.Wellness_Package_ZFCFixedPrice_Book));
		action.moveToElement(we).click().build().perform();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Wellness_Package_Calendar_Ok);
		Thread.sleep(2000);
		WebElement confirm= driver.findElement(By.xpath(Elements_Recipients.Wellness_Package_Confirm));
		action.moveToElement(confirm).click().build().perform();
		Thread.sleep(3000);
		int PromoCodeValue=RecipientPage.Calculate_PromoCodeamount(10);
		System.out.println("Amount :"+PromoCodeValue);
		RecipientPage.BookingDetails_ProceedToPay("Self","","",Promo_Code);
		Thread.sleep(2000);
		/*Browser.waitFortheElementXpath("//div[@class='grey-bg border-dashed-grey']//h6");
		String Amount=Browser.getTextByXpath("//div[@class='grey-bg border-dashed-grey']//h6").replaceAll("[^0-9]", "");
		int Amount_afterPromocode=Integer.parseInt(Amount);
		Assert.assertEquals(Amount_afterPromocode, PromoCodeValue);*/
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(2000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		String BS_ToBePaid=Browser.getTextByXpath("(//div[@class='d-flex align-items-center justify-content-between my-4']//h6)[2]").replaceAll("[^0-9]", "");
		int BS_ToBePaid_Amount=Integer.parseInt(BS_ToBePaid);
		Assert.assertEquals(BS_ToBePaid_Amount, PromoCodeValue);
		Thread.sleep(1000);
		
		//Getting appointment Id
		String appointmentID= Browser.get_Document_Attribute(Environment, "zoyloAppointment", "patientInfo.patientPhone","+919553456665", "appointmentId");
		System.out.println("Appointment ID :"+appointmentID);
		
		//Checking In Admin Same Amount is Replicating or Not
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin("8888899999", "Zoylo@123");
		Thread.sleep(2000);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "True", "Package Appointments");
		Thread.sleep(25000);
		Browser.clickOnTheElementByXpath(Elements_Admin.PackageAppointments_Wellnessbutton);
		Thread.sleep(25000);
		//Browser.clickOnTheElementByXpath(Elements_Admin.PackageAppointments_Wellnessbutton);
		Browser.clickOnTheElementByXpath(Elements_Admin.PackageAppointments_BookingFromDate);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("(//button[@class='btn btn--floating btn--small btn--flat btn--active btn--current'])[1]");
		Browser.clickOnTheElementByXpath(Elements_Admin.PackageAppointments_BookingToDate);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("(//button[@class='btn btn--floating btn--small btn--flat btn--active btn--current'])[1]");
		Browser.clickOnTheElementByXpath(Elements_Admin.PackageAppointments_Apply);
		Thread.sleep(3000);
		int tablesize=driver.findElements(By.xpath("//table[@class='datatable table']/tbody/tr")).size();
		System.out.println("Table size:"+tablesize);
		for(int i=1;i<=tablesize;i++){	
		String CheckAppointmentID =Browser.getTextByXpath("//table[@class='datatable table']/tbody/tr["+i+"]/td[1]");
		if(appointmentID.equals(CheckAppointmentID)){
		System.out.println("Appointment Id Matched At :"+i);
		Browser.clickOnTheElementByXpath("//table[@class='datatable table']/tbody/tr["+i+"]/td[1]");	
		String PaidAmount=Browser.getTextByXpath("//table[@class='datatable table']/tbody/tr["+i+"]/td[8]");
		int Admin_PaidAmount=Integer.parseInt(PaidAmount);
		Assert.assertEquals(PromoCodeValue, Admin_PaidAmount);
		
		}	
		
		}
	}
	
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
	}

}
