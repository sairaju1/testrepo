
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_121_New_OC_NonVerifiedUser_SkipToDoctorConsultation extends LoadProp{
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public String firstName;
	public String lastName;
	public String mobileNumber;
	public String email;
	public String ChatMsg;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	firstName="A"+Browser.generateRandomString(5).toLowerCase();
		lastName="B"+Browser.generateRandomString(5).toLowerCase();
		mobileNumber="9"+Browser.generateRandomNumber(9);
		email=Browser.generateRandomString(5).toLowerCase()+Browser.generateRandomNumber(4)+"@"+Browser.generateRandomString(3).toLowerCase()+".com";
	 	
	 	//Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail", OC_Doctor_Email);
	 	//Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", OC_Doctor_Clinic); 	
		ChatMsg= Browser.generateRandomString(5);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void OC_Check_NonVerifiedUser_SkipToDoctorConsultation() throws Exception{
		
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Click_CleverTap_Notification();
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_link);
		RecipientPage.SignUp_Details(firstName, lastName, mobileNumber, email, "Zoylo@123");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_signupButton);
		Thread.sleep(5000);
		Browser.waitFortheElementXpath(Elements_Recipients.signUp_otp);
		Set<String> windows = driver.getWindowHandles();
		String Recipient = driver.getWindowHandle();
		((JavascriptExecutor)driver).executeScript("window.open();");
		Set<String> customerWindow = driver.getWindowHandles();
		customerWindow.removeAll(windows);
		String PMS = ((String)customerWindow.toArray()[0]);
		driver.switchTo().window(PMS);
		Browser.openUrl(PMS_URL);
		RecipientPage.PMSLogin(OC_Doctor_Mobile, PMS_Password);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Select_Online);
		Browser.clickOnTheElementByXpath(Elements_Recipients. PMS_Select_Clinic_SelectButton);
		String Checkdoc=Browser.getTextByXpath(Elements_Recipients.PMS_SignedINName);
		System.out.println(Checkdoc);
		
		//Switching To Recipient - NonVerified User and OTP authentication and Start Consultation
		driver.switchTo().window(Recipient);
		Browser.openUrl(EnvironmentURL);
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_GetOnlineConsultation);
		RecipientPage.OC_GuestUser_Login(lastName, mobileNumber);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_SkipToDcoctorConsultation);
		RecipientPage.OC_Promocode("NO_Promocode");
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_BookingSucessfull_StartConsultation);
		Thread.sleep(3000);
		String msg=Browser.getTextByXpath(Elements_Recipients.OC_Notification);
		Assert.assertTrue(msg.contains("Thank you for choosing ZOYLO."));
		Browser.enterTextByXpath(Elements_Recipients.OC_Chat_TextArea, ChatMsg);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Chat_textArea_Send);
		
		//Switching To PMS
		driver.switchTo().window(PMS);
		Thread.sleep(1000);
		System.out.println("Switched To PMS");
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Click_ONlineConsultation);
		String text=Browser.getTextByXpath(Elements_Recipients.PMS_GetChat_Text);
		Assert.assertEquals(text, ChatMsg);
		Thread.sleep(1000);
		driver.findElement(By.xpath(Elements_Recipients.PMS_Chat_Attachfile)).sendKeys(System.getProperty("user.dir")+"/TestData/horse.png");
		System.out.println("Image Had Sent");
		Thread.sleep(2000);
		
		//Switching To Recipient
		driver.switchTo().window(Recipient);
		Thread.sleep(3000);
		System.out.println("Switched To Recipient");
		String img=driver.findElement(By.xpath("//img[@class='doc-image']")).getAttribute("src");
		System.out.println(img);
		Assert.assertTrue(img.contains("horse.png"));
		
		//Switching To PMS To ADD Prescription
		driver.switchTo().window(PMS);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_PrescriptionTab);
		RecipientPage.PMS_Prescription_Vitals("5.9", "120", "120/80","99.7");
		RecipientPage.PMS_Prescription_Complaints("Fever");
		RecipientPage.PMS_Prescription_Diseases("Dengue fever");
		RecipientPage.PMS_Prescreption_Notes("Suffering With Pelvic Inflammatory Disease");
		RecipientPage.PMS_Prescription_Medication("DOLO", "2");
		RecipientPage.PMS_Prescription_GeneralAdvice("Suffering with Dengue Fever");
		RecipientPage.PMS_Prescription_Review("1", "Week");
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Submit);
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_GeneratePrescription);
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_GeneratePrescription_confirm);

		// Switching to Recipient- To verify Prescription is available
		driver.switchTo().window(Recipient);
		driver.navigate().refresh();
		Thread.sleep(4000);
		String pdfavailable = Browser.getTextByXpath("//a[@class='download-presc secondary-color font10 font-medium text-uppercase']");
		Assert.assertEquals(pdfavailable, "DOWNLOAD");
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_EndSession);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_EndChating);
		Browser.clickOnTheElementByXpath(Elements_Recipients.OC_NoThanks);
		Thread.sleep(1000);
		String GetCurrentURL = driver.getCurrentUrl();
		System.out.println(GetCurrentURL);
		Assert.assertTrue(GetCurrentURL.contains(EnvironmentURL));
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_headerUserIcon);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_header_MyConsultation);

		// Getting Appointment ID
		String AppointmentID = Browser.get_Document_Attribute(Environment, "zoyloAppointment","patientInfo.patientPhone", "+91"+mobileNumber, "appointmentId");
		System.out.println("Appointment Id :" + AppointmentID);

		// Using Rest assured To Get the Download pdf URL and Pass to Pdf Read
		// method and Asserting the Medicine
		String PDFURL = RecipientPage.OC_GetPDF_URL(AppointmentID);
		String output = Browser.readPDF(PDFURL);
		Assert.assertTrue(output.contains("DOLO"));
		
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
	}

}
