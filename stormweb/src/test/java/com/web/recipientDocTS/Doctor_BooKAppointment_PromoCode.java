
//@author :Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Doctor_BooKAppointment_PromoCode extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public AdminPage AdminPage;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	AdminPage=new AdminPage(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", "+919553456665");
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "zoylodoctor@gmail.com");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "zoylorecclinic@gmail.com");
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(1000);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void BookDoctorappointment_Promocode() throws Exception{
		
		RecipientPage.GenericSearch("Doctors", Doctor_Location, "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor(Doctor_Name);
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		Thread.sleep(3000);
		String Get_Amount_AfterConsultation=Browser.getTextByXpath("(//div[@class='fee-details']//div[3]/h5)[2]").replaceAll("[^0-9]", "");
		System.out.println("After Consultaton Amount:"+Get_Amount_AfterConsultation);
		int PromoCode_CalculatedValue=RecipientPage.Calculate_PromoCodeamount(10);
		System.out.println("Promocode Value :"+PromoCode_CalculatedValue);
		RecipientPage.EnterPromoCode(Promo_Code);
		String Promocode_amount=Browser.getTextByXpath("(//div[@class='fee-details']//div[5]/h6)[2]").replaceAll("[^0-9]", "");
		int Amount_After_Promocode=Integer.parseInt(Promocode_amount);
		Assert.assertEquals(Amount_After_Promocode, PromoCode_CalculatedValue);
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_PaymentToProceedButton);
		Thread.sleep(2000);
		/*String PaymentPage=Browser.getTextByXpath(Elements_Recipients.PaymentSelection_ToPay).replaceAll("[^0-9]", "");
		int PaymentPage_Amount=Integer.parseInt(PaymentPage);
		Assert.assertEquals(PaymentPage_Amount, PromoCodeValue);*/
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		String BookingSucessful_ToBePaid=Browser.getTextByXpath(Elements_Recipients.bookingSucessfull_AmountToBepaid).replaceAll("[^0-9]", "");
		int BookingSucessful_ToBePaid_Amount=Integer.parseInt(BookingSucessful_ToBePaid);
		Assert.assertEquals(BookingSucessful_ToBePaid_Amount, PromoCode_CalculatedValue);
		Thread.sleep(1000);
		String BookingSucessful_AfterConsultation=Browser.getTextByXpath("(//div[@class='fee-details']//div[3]/h5)[2]").replaceAll("[^0-9]", "");
		Assert.assertEquals(BookingSucessful_AfterConsultation, Get_Amount_AfterConsultation);
		
		//Getting appointment Id
		String appointmentID= Browser.get_Document_Attribute(Environment, "zoyloAppointment", "patientInfo.patientPhone","+919553456665", "appointmentId");
		System.out.println("Appointment ID :"+appointmentID);
		
		//Checking In Admin Same Amount is Replicating or Not
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin("8888899999", "Zoylo@123");
		Thread.sleep(1000);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "True", "Appointment Logs");
		Thread.sleep(20000);
		Browser.enterTextByXpath("(//input[@name=' enter email id'])[1]", "lakshmikanth.c@zoylo.com");
		Browser.clickOnTheElementByXpath("(//div[@id='doctorappointments']//button[contains(., 'apply')])[3]");
		Thread.sleep(3000);
		int tablesize=driver.findElements(By.xpath("//div[@id='doctorappointments']//table[@class='datatable table']/tbody/tr")).size();
		System.out.println("Table size:"+tablesize);
		for(int i=1;i<=tablesize;i++){	
		String CheckAppointmentID =Browser.getTextByXpath("//div[@id='doctorappointments']//table[@class='datatable table']/tbody/tr["+i+"]/td[1]");
		if(appointmentID.equals(CheckAppointmentID)){
		System.out.println("Appointment Id Matched At :"+i);
		Browser.clickOnTheElementByXpath("//div[@id='doctorappointments']//table[@class='datatable table']/tbody/tr["+i+"]/td[1]");	
		String PaidAmount=Browser.getTextByXpath("//div[@id='doctorappointments']//table[@class='datatable table']/tbody/tr["+i+"]/td[15]");
		int Admin_PaidAmount=Integer.parseInt(PaidAmount);
		Assert.assertEquals(PromoCode_CalculatedValue, Admin_PaidAmount);
		String BalanceAmount=Browser.getTextByXpath("//div[@id='doctorappointments']//table[@class='datatable table']/tbody/tr["+i+"]/td[16]");
		Assert.assertEquals(BalanceAmount, Get_Amount_AfterConsultation);
			
			}	
			
			}
		
		
		
	}
	
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
	}

}
