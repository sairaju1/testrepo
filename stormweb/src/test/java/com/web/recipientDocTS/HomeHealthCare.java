
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import objectRepository.Elements_Admin;
import objectRepository.Elements_Recipients;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class HomeHealthCare extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public AdminPage AdminPage;
	public String name, mobile, email, state, city, packageName, Package_Clinic_PatientName,Package_Clinic_Phone,Package_Clinic_Status ,
	Package_Clinic_URL_PatientName, Package_Clinic_URL_Phone, Package_Clinic_URL_Status,Name_URL,Mobileno,Email_URL;

	
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
		Elements_Admin.Admin_PageProperties();
		AdminPage=new AdminPage(driver);
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "homehealth@gamil.com");
	 	name="A"+Browser.generateRandomString(5).toLowerCase();
		mobile="9"+Browser.generateRandomNumber(9);
		System.out.println("Printing mobile no :"+mobile);
		email=name+"@xyz.com";
		state="Andhra Pradesh";
		city="Vijayawada";
		packageName="Dentists";
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(1000);
	 	
	}
	
	@DataProvider(name="HomeHealthCare")
	 public String[][] createData1() {
			return new String[][] {
					{name, mobile },
					{"Lakshmikanth","9553456665"}
					
			};
	}
	
	@Test(dataProvider="HomeHealthCare",groups = { "ZoyWeb", "High" })
	public void BookHomeHealthCare(String Firstname,String phone) throws Exception{
		
		
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_BookHomeHealthcare);
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.HomeHealthCare_BookNow);
		RecipientPage.HomeHealthCare_Details(Firstname, phone, state, city, email, packageName);
		Thread.sleep(1000);
		if(driver.findElements(By.id(Elements_Recipients.HomeHealthCare_EnterOTP)).size()!=0){
			System.out.println("Enter if Condition");
			String OTP=Browser.getOtp(email, Environment);
			Browser.enterTextByID(Elements_Recipients.HomeHealthCare_EnterOTP, OTP);
			Browser.clickOnTheElementByXpath(Elements_Recipients.HomeHealthCare_VerifyOTP);	
			
		}
		Thread.sleep(2000);
//		Browser.waitFortheElementXpath(Elements_Recipients.HomeHealthCare_Msg);
//		String msg=Browser.getTextByXpath(Elements_Recipients.HomeHealthCare_Msg);
//		System.out.println(msg);
//		Assert.assertEquals(msg, "Thank you for providing the details. Our customer care team will get back to you.");
		
		//GetAppointment ID from DB
		String AppointmentID=Browser.get_Document_Attribute(Environment, "zoyloAppointment", "patientInfo.patientPhone","+91"+phone, "appointmentId");
		System.out.println("Appointment Id Of HomeHealth:"+AppointmentID);
		Thread.sleep(1000);
		
		//Verifying In Admin Package Appointments
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin("8888899999", "Zoylo@123");
		Thread.sleep(2000);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "True", "Package Appointments");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(25000);
		Browser.waitFortheElementXpath(Elements_Admin.PackageAppointments_Clinic_BookingFromDate);
		Browser.clickOnTheElementByXpath(Elements_Admin.PackageAppointments_Clinic_BookingFromDate);
		Browser.clickOnTheElementByXpath("(//button[@class='btn btn--floating btn--small btn--flat btn--active btn--current'])[1]");
		Browser.clickOnTheElementByXpath(Elements_Admin.PackageAppointments_Clinic_BookingToDate);
		Browser.clickOnTheElementByXpath("(//button[@class='btn btn--floating btn--small btn--flat btn--active btn--current'])[1]");
		Browser.clickOnTheElementByXpath(Elements_Admin.PackageAppointments_Apply);
		Thread.sleep(5000);
		Browser.waitFortheElementXpath("//table[@class='datatable table']/tbody/tr");
		int tablesize=driver.findElements(By.xpath("//table[@class='datatable table']/tbody/tr")).size();
		System.out.println("Table size:"+tablesize);
		Thread.sleep(2000);
		for(int i=1;i<=tablesize;i++){	
			String CheckAppointmentID =Browser.getTextByXpath("//table[@class='datatable table']/tbody/tr["+i+"]/td[1]");
			if(AppointmentID.equals(CheckAppointmentID)){
			System.out.println("Appointment Id Matched At :"+i);
			
			 Browser.clickOnTheElementByXpath("//table[@class='datatable table']/tbody/tr["+i+"]/td[1]");
			 Package_Clinic_PatientName= Browser.getTextByXpath("//table[@class='datatable table']/tbody/tr["+i+"]/td[2]");
			 Package_Clinic_Phone= Browser.getTextByXpath("//table[@class='datatable table']/tbody/tr["+i+"]/td[3]");
			 Package_Clinic_Status= Browser.getTextByXpath("//table[@class='datatable table']/tbody/tr["+i+"]/td[10]");
			break;
			}
		
		}	
		
		Assert.assertEquals(Package_Clinic_PatientName, Firstname);
		Assert.assertEquals(Package_Clinic_Phone, "+91"+phone);
		Assert.assertEquals(Package_Clinic_Status, "CREATED");
		Thread.sleep(1000);
		AdminPage.adminLogout();
	
		//Removing the user from Database	
		Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone","+91"+mobile);
		Browser.remove_Document(Environment, "zoyloUser", "phoneInfo.phoneNumber", "+91"+mobile);
		
		Browser.openUrl(EnvironmentURL);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
	}
	
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
	
		Browser.quit();
	}
	
	
}
