
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ProfilePage extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	
	@BeforeClass
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(2000);
	}
	
	
	@DataProvider(name="Profile")
	 public String[][] createData1() {
			return new String[][] {
					{ "Yaswanth","Prasad","1985-08-10","9985249990","Female","A+" },
					{"Lakshmikanth","Cherukuri","1983-08-09","9553456665","Male","O-"}
					
			};
	}
	
	
	@Test(dataProvider="Profile")
	public void ProfileEdit(String firstname,String lastname,String DOB,String Phone, String Gender,String BGroup) throws Exception{
		
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_headerUserIcon);
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_MyProfile);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.myProfile_editButton);
		Browser.enterTextByXpath(Elements_Recipients.myProfile_EditfirstName, firstname);
		Browser.enterTextByXpath(Elements_Recipients.myProfile_EditlastName,lastname);
		Browser.removeReadOnly_byXpath(Elements_Recipients.myProfile_EditDOB);
		driver.findElement(By.xpath(Elements_Recipients.myProfile_EditDOB)).clear();
		driver.findElement(By.xpath(Elements_Recipients.myProfile_EditDOB)).sendKeys(DOB);
		Browser.enterTextByXpath(Elements_Recipients.myProfile_Editphone, Phone);
		Browser.selectIDbyText(Elements_Recipients.myProfile_Editgender,Gender);
		Browser.selectIDbyText(Elements_Recipients.myProfile_EditbloodGroup, BGroup);
		Browser.clickOnTheElementByXpath(Elements_Recipients.myProfile_SaveButton);
		Thread.sleep(5000);
		String fullname=Browser.getTextByXpath(Elements_Recipients.myProfile_FullName);
		System.out.println("Fullname :"+fullname);
		Assert.assertTrue(fullname.contains(firstname));
		String mobile=Browser.getTextByXpath(Elements_Recipients.myProfile_Phone);
		Assert.assertTrue(Phone.contains(mobile));
	}
	
	@AfterClass
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
		
	}

}
