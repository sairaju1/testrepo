
//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Clinic_FixedPrice_Promocode_HundredPercentage extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	
	public String fiftyPer = "50.0";
	public String hundredPer = "100.0";
	public String resertPer = "15.0";
	public int Clinic_ZfcPerc;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 	Browser.remove_Document(Environment, "zoyloAppointment",  "patientInfo.patientPhone", "+919553456665");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail","Prabhusclinic@gmail.com");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail","Prabhusch@gmail.com");
		Browser.update_MongoAttribute(Environment, "zoyloServicePromotion", "promotionCode", Clinic_PromoCode,"promotionDetailInfo.promotionalValue", hundredPer);
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(1000);
		
		String hundredPer = "100.0";
		Double value= Double.valueOf(hundredPer);
		Clinic_ZfcPerc=(int)Math.round(value);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void  Clinic_PackageBooKappointment_FixedPrice_HundredPercentagePromocode() throws Exception{
		
		RecipientPage.GenericSearch("Doctors", Doctor_Location, Clinic_Name);
		RecipientPage.Clinic_SelectPackage_Book_Confirm("FixedPrice");
		int PromoCodeValue=RecipientPage.Calculate_PromoCodeamount(Clinic_ZfcPerc);
		System.out.println("Amount :"+PromoCodeValue);
		Thread.sleep(1000);
		RecipientPage.BookingDetails_ProceedToPay("Self","","",Clinic_PromoCode);
		Thread.sleep(2000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message);
		Thread.sleep(1000);
		String BS_ToBePaid=Browser.getTextByXpath(Elements_Recipients.bookingSucessfull_AmountToBepaid).replaceAll("[^0-9]", "");
		int BS_ToBePaid_Amount=Integer.parseInt(BS_ToBePaid);
		Assert.assertEquals(BS_ToBePaid_Amount, PromoCodeValue);
		
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.update_MongoAttribute(Environment, "zoyloServicePromotion", "promotionCode", Clinic_PromoCode,"promotionDetailInfo.promotionalValue", resertPer);
		Browser.quit();
	}

}
