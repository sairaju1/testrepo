

//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZOYL_578_Verify_PaymentGateways extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	public String transactionstatus;
	public String ObjectID;
	
	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
	 		
	}
	
	
	@DataProvider(name="Payment_Gateways")
	 public String[][] createData1() {
			return new String[][] {
					
					{"Wallet", "MOBIKWIK" },
					{"Netbanking", "" },
					{"Cards", "" },
					{"Paypal", "" },
						
				
			};
	}
	
	@Test(dataProvider="Payment_Gateways",groups = { "ZoyWeb", "High" })
	public void Doctor_Verify_PaymentGateways(String PaymentGateway,String PaymentGateway_Type) throws Exception{
		
		Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", "+919553456665");
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "zoylodoctor@gmail.com");
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		RecipientPage.GenericSearch("Doctors", Doctor_Location, "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor(Doctor_Name);
		Thread.sleep(1000);
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		Thread.sleep(2000);
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		Thread.sleep(3000);
		RecipientPage.MakePayment_Options(PaymentGateway, PaymentGateway_Type);
		Thread.sleep(2000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, "Booking Successful!");
		Thread.sleep(1000);
		
		//Getting Object ID
		ObjectID= Browser.get_Document_Attribute(Environment, "zoyloAppointment", "patientInfo.patientPhone", "+919553456665", "_id");
		System.out.println("Appointment ID for Cashback :"+ObjectID);
		
		//Getting Payment Status
		transactionstatus=Browser.get_Document_Attribute(Environment, "zoyloPayment", "transactionReference", ObjectID, "paymentStatusData");
		System.out.println("transaction :"+transactionstatus);
		String[] transactionstaus_Split=transactionstatus.split("paymentStatusName");
		System.out.println("Split:"+transactionstaus_Split[1]);
		String Final_TransactionStatus=transactionstaus_Split[1].replaceAll("\\p{P}","").trim();
		System.out.println("Final Transaction Status:"+Final_TransactionStatus);
		Assert.assertEquals(Final_TransactionStatus,"Payment Successful");
			
		}
	
	@AfterMethod(groups = { "ZoyWeb", "High" })
	public void LogOut() throws Exception{
		
		RecipientPage.Recipient_Logout();
		
		
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
	}
		
	}


