

//@author:Ch.LakshmiKanth

package com.web.recipientDocTS;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class Doctor_BookAppointment_ChangeTimeSlot  extends LoadProp {
	
	public RecipientPage RecipientPage;
	public TestUtils Browser;
	

	@BeforeClass(groups = { "ZoyWeb", "High" })
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Recipients.Recipients_PageProperties();
	 	RecipientPage = new RecipientPage(driver); 
	 	Browser= new TestUtils(driver);
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
	}
	
	@Test(groups = { "ZoyWeb", "High" })
	public void BookDoctorappointment_ChangeTimeSlot() throws Exception{
		
		RecipientPage.GenericSearch("Doctors", Doctor_Location, "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor(Doctor_Name);
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		Thread.sleep(3000);
		String time=Browser.getTextByXpath(Elements_Recipients.bookingDetails_GetTime);
		System.out.println("Time Before Change TimeSlot :"+time);
		RecipientPage.Doctor_ChangeTimeslot();
		Thread.sleep(1000);
		String Change_Time=Browser.getTextByXpath(Elements_Recipients.bookingDetails_GetTime);
		System.out.println("Time after Changing Time Slot :"+Change_Time);
		Assert.assertFalse(time.contains(Change_Time));
		Thread.sleep(2000);
		RecipientPage.BookingDetails_ProceedToPay("Self", "", "","No_Promocode");
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(1000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, Appointment_Sucessful_Message );
		
	}
	
	@AfterClass(groups = { "ZoyWeb", "High" })
	public void CloseBrowser() throws Exception{
		
		Browser.quit();
		
	}

}
