// @Author: Sagar Sen

package com.web.Medicine;

import java.net.HttpURLConnection;
import java.util.Iterator;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_40_check_brokenLinks extends LoadProp {

	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String url;
	public int respCode = 200;
	public HttpURLConnection conn;
	public java.net.URL urlcon;
	public SoftAssert soft;

	@BeforeClass(groups = { "MED", "Low" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		RecipientPage = new RecipientPage(driver);
		soft = new SoftAssert();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
	}

	@Test(groups = { "MED", "Low" })
	public void verify_brokenLinks() throws Exception {
		
		// Link checker zoylo retro home
		Browser.openUrl(EnvironmentURL);
		Thread.sleep(2000);
		List<WebElement> links = driver.findElements(By.tagName("a"));
		Iterator<WebElement> it = links.iterator();
		int javaVoid=0;
		int validURL=0;
		int otherURL=0;
		int brokenURL=0;
		int nullURL=0;
		while (it.hasNext()) {
			url = it.next().getAttribute("href");
//			System.out.println("All URL: " + url);
			if (url != null) {
				if (url.contains("javascript:void(0)")) {
					javaVoid++;
//					System.out.println(url + " is not configured or has no href.");
					continue;
				} else if (!url.startsWith(EnvironmentURL)) {
//					System.out.println(url + " URL is of different domain.");
					otherURL++;
					continue;
				} else {
					
					Response rep = RestAssured.given().head(url);
					int respCode = rep.getStatusCode();
					
					if (respCode == 200) {
						validURL++;
					} else {
						brokenURL++;
//						System.out.println(url + " is a broken link.");
						String brokenClass = driver.findElement(By.tagName("a")).getAttribute("class");
						System.out.println("Broken href class name is : " + brokenClass);
						soft.fail();
						continue;
					}
				}
			} else {
				nullURL++;
				String nullClass = driver.findElement(By.tagName("a")).getAttribute("class");
				System.out.println("Class name for null href is : " + nullClass);
				continue;
			}
		}
		System.out.println("************************ Zoylo Retro home page report ************************");
		System.out.println("Number of javaScriptvoid url in retro : " + javaVoid);
		System.out.println("Number of valid URL's in retro : " + validURL);
		System.out.println("Number of other domain URL's in retro are : " + otherURL);
		System.out.println("Number of broken URL's in retro are : " + brokenURL);
		System.out.println("Number of null URL's in retro are : " + nullURL);
		int total=nullURL+brokenURL;
		System.out.println("Total non working URL's in retro are : " + total);
		
//		System.out.println("************** Zoylo Retro home page link verification done, switching to MED **************");
		
		// Link checker Ecom home
		Browser.openUrl(EnvironmentURL + "/medicines/");
		Browser.waitFortheElementXpath("//span[@class='counter-number']/span[text()='0']");
		
		int ecomjavaVoid=0;
		int ecomvalidURL=0;
		int ecomotherURL=0;
		int ecombrokenURL=0;
		int ecomnullURL=0;
		
		try {
			List<WebElement> linksEcom = driver.findElements(By.tagName("a"));
			Iterator<WebElement> itEcom = linksEcom.iterator();

			while (itEcom.hasNext()) {
				url = itEcom.next().getAttribute("href");
//				System.out.println("All URL: " + url);
				if (url != null) {
					if (url.contains("javascript:void(0)")) {
						ecomjavaVoid++;
						continue;
					} else if (!url.startsWith(EnvironmentURL)) {
						ecomotherURL++;
						continue;
					} else {
						
						Response rep1 = RestAssured.given().head(url);
						int respCode1 = rep1.getStatusCode();
						
						if (respCode1 == 200) {
							ecomvalidURL++;
						} else {
							ecombrokenURL++;
							soft.fail();
							continue;
						}
					}
				} else {
					ecomnullURL++;
					String nullClassEcom = driver.findElement(By.tagName("a")).getAttribute("class");
					System.out.println("Class name for null href is : " + nullClassEcom);
					continue;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("************************ Zoylo Ecomm home page report ************************");
		System.out.println("Ecomm javaScriptvoid url are : " + ecomjavaVoid);
		System.out.println("Ecomm valid URL's are : " + ecomvalidURL);
		System.out.println("Ecomm other domain URL's are : " + ecomotherURL);
		System.out.println("Ecomm broken URL's are : " + ecombrokenURL);
		System.out.println("Ecomm null URL's are : " + ecomnullURL);
		int totalEcom=ecomnullURL+ecombrokenURL;
		System.out.println("Total non working URL's in Ecomm : " + totalEcom);
		
		soft.assertAll();
	}

	@AfterClass(groups = { "MED", "Low" })
	public void tearDown() throws Exception {
		Browser.quit();
	}
}