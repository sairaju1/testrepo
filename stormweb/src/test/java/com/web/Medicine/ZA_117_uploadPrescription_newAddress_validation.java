// @Author: Sagar Sen

package com.web.Medicine;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Address;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_117_uploadPrescription_newAddress_validation extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public Address Address;
	public Person p;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		p = Browser.personData();
		Address = p.getAddress();
	}

	@Test(groups = { "MED", "High" }, priority = 1)
	public void uploadPrescription_removeExistingAddress() throws Exception {
		String alertText = null;
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(adminuser_id, adminuser_pw);
		// Remove user address's from sql
		String parent_id = Browser.sqlDB_selectFromQuery("customer_entity_varchar", "value='" + adminuser_id + "'",
				"entity_id");
		Browser.sqlDB_deleteQuery("customer_address_entity", "parent_id='" + parent_id + "'");
		// route to Upload presc Page
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		Browser.clickOnTheElementByXpath("//a[@class='upload-pre']"); // HomePageUploadBtn-
		ecomPage.uploadPrescriptionPage(false, "", "");
		ecomPage.uploadPrescription_specifications(false, 0);
		Thread.sleep(3000);
		Browser.waitFortheElementXpath(Elements_Medicines.shippingAddressHeader);
//		ecomPage.shippingPage_addAddress("home", Address, "Telangana", "999888", adminuser_id, "Hyderabad");
		
		Browser.waitFortheElementXpath(Elements_Medicines.shippingAddressHeader);
		Browser.selectXpathByValue(Elements_Medicines.shippingAddress_type, "home");
		Browser.enterTextByXpath(Elements_Medicines.shippingAddress_Street, Address.toString());
		Browser.enterTextByName(Elements_Medicines.shippingAddress_pin, "999888");
		Browser.clickOnTheElementByXpath(Elements_Medicines.shippingAddress_type);
		WebDriverWait w = new WebDriverWait(driver, 5);
		if(w.until(ExpectedConditions.alertIsPresent()) != null) {
			alertText = driver.switchTo().alert().getText();
			driver.switchTo().alert().accept();
		}
		
		
		Browser.clickOnTheElementByXpath("//input[@value='Save Address']"); // SaveAddressBtn
//		Browser.waitFortheElementXpath("//div[contains(text(),'Unfortunately, we do not ship to your PIN Code!')]"); // pinCodeVal
		Assert.assertTrue(alertText.contains("Invalid Pincode"));
		
		ecomPage.shippingPage_addAddress("home", Address, "Telangana", "500085", adminuser_id, "Hyderabad");
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath("//input[@value='Save Address']"); // SaveAddressBtn
		Thread.sleep(3000);
		ecomPage.shippingPage_paymentMethod_placeOrderClick("");
		String orderID = ecomPage.order_confirmationPage("", 0.0, "");
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, orderSuccess_Status);
	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
