// @Author: Sagar Sen

package com.web.Medicine;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_21_PDP_BuyNow_partialPayment_placeOrder_OTC_ONLINE extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public int qty = 1;
	public String productName, paymode, couponName, OrderStatus_APIresponse;
	public Double searchListPrice, productSubTotal_item, checkout_orderTotal, checkout_orderTotalWallet;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		paymode = payLater;
		OrderStatus_APIresponse = orderSuccess_Status;
		productName = productName_nonRX;
	}

	@Test(groups = { "MED", "High" })
	public void partialPayment_OTC_Online() throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		searchListPrice = ecomPage.medicineHomePage_search(productName, "");
		ecomPage.PDP_itemQtyInfo(productName, qty, "Buy");
		productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, searchListPrice, qty);
		checkout_orderTotal = ecomPage.checkout_coupon_summary("", "", productSubTotal_item);
		ecomPage.shippingPage_orderSummary("", "", checkout_orderTotal, "Healthcare product online order OL.");
		checkout_orderTotalWallet = checkout_orderTotal - searchListPrice;
		Browser.update_MongoAttribute(Environment, "zoyloWallet", "userInfo.phoneNumber", "9966775890", "balanceAmount",
				Browser.decimalFormat(checkout_orderTotalWallet));
		ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
//		RecipientPage.MakePayment("Multiple", "netbanking");
		String orderID = ecomPage.order_confirmationPage(productName, checkout_orderTotal, paymode);
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, OrderStatus_APIresponse);
	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.update_MongoAttribute(Environment, "zoyloWallet", "userInfo.phoneNumber", "9966775890", "balanceAmount",
				"0.0");
		Browser.quit();
	}

}
