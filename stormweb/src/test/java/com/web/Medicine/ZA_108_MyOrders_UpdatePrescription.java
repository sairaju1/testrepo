// @Author: Sagar Sen

package com.web.Medicine;

import java.util.ArrayList;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_108_MyOrders_UpdatePrescription extends LoadProp {

	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public MedicinePage ecomPage;
	public String productName, paymode;
	public int qty = 1;
	public Double searchListPrice, productSubTotal_item, checkout_orderTotal;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		RecipientPage = new RecipientPage(driver);
		ecomPage = new MedicinePage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		paymode = cod;
		productName = productName_rx;
	}

	@Test(groups = { "MED", "High" })
	public void myOrders_UpdatePrescription() throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		searchListPrice = ecomPage.medicineHomePage_search(productName, "");
		ecomPage.PDP_itemQtyInfo(productName, qty, "Buy");
		productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, searchListPrice, qty);
		checkout_orderTotal = ecomPage.checkout_coupon_summary("", "", productSubTotal_item);
		ecomPage.checkout_prescriptionPopUp(false);
		ecomPage.shippingPage_orderSummary("", "", checkout_orderTotal, "Healthcare product online order OL.");
		ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
		String orderID = ecomPage.order_confirmationPage(productName, checkout_orderTotal, paymode);
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, orderSuccess_Status);
		Browser.openUrl(EnvironmentURL);

		// My orders
		RecipientPage.header_UserDetails_SelectMenu("My Orders", "");
		Browser.waitFortheElementXpath("//h3[text()='My Orders']");
		Browser.waitFortheElementXpath("//td[text()='##" + orderID + "']"); // OrderNo
		Browser.clickOnTheElementByXpath(
				"//td[text()='##" + orderID + "']/following-sibling::td/a[@class='view-details collapsed']"); // viewBtn
		Browser.waitFortheElementXpath("//table/tr[contains(., '##" + orderID
				+ "')]/following-sibling::tr/td/div/div/div/table/tbody/tr/th[text()='" + productName + "']"); // viewProductName
		Browser.clickOnTheElementByXpath(
				"//tr[contains(., '##" + orderID + "')]/following-sibling::tr/td/div/div/div/button"); // updatePrescBtn
		Thread.sleep(3000);

		// Get number of windows
		ArrayList<String> win = new ArrayList<String>(driver.getWindowHandles());
		System.out.println("Number of active windows are : " + win);
		if (win.size() > 1) {
			// switch window
			driver.switchTo().window(win.get(1));
			String currentUrl = driver.getCurrentUrl();
			Assert.assertTrue(currentUrl.contains(orderID));
			Browser.waitFortheElementXpath("//h6[text()='Drag & Drop your prescription copy here']");
			// Verify validation
			Browser.clickOnTheElementByXpath("//input[@value='Update']"); // updateBtn
			Browser.waitFortheElementXpath("//h1[text()='Please upload Prescription  ']");
			Browser.openUrl(currentUrl);
			Browser.waitFortheElementXpath("//h6[text()='Drag & Drop your prescription copy here']");
			Browser.enterTextByXpath("(//input[@form='orderform'])[1]",
					System.getProperty("user.dir") + "/TestData/horse.png");
			Browser.clickOnTheElementByXpath("//input[@value='Update']"); // updateBtn
			Browser.waitFortheElementXpath("//h1[text()='Your orderid " + orderID + " updated successfully ']");
			
			// Magento
			Browser.openUrl(magentoAdminURL);
			ecomPage.Magento_Login(Magento_user, Magento_pwd);
			ecomPage.Magento_sideMenu_click(false, "Sales", "Orders");
			Browser.waitFortheElementXpath("(//label[@class='data-grid-checkbox-cell-inner'])[1]"); // FirstchkBox
			ecomPage.Magento_Sales_Orders_search(orderID);
			Thread.sleep(3000);
			ecomPage.Magento_Sales_Orders_view(orderID);
			Browser.clickOnTheElementByXpath("//span[text()='Order Prescriptions']"); // prescSideMenu
			Browser.waitFortheElementXpath("(//h1[text()='Prescriptions'])[1]"); // prescHeader
			Browser.waitFortheElementXpath("(//img[@alt='horse'])[1]");
			Browser.waitFortheElementXpath("(//td[contains(., 'horse')])[1]/following-sibling::td/a[text()=' View ']");
		}
	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
