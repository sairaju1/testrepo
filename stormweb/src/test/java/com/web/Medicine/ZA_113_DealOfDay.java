// @Author: Sagar Sen

package com.web.Medicine;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_113_DealOfDay extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName, couponName, paymode, OrderStatus_APIresponse, skuId;
	public Double oldPrice, offerPrice, productSubTotal_item, checkout_orderTotal, shipping_orderTotal;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		paymode = payLater;
		OrderStatus_APIresponse = orderSuccess_Status;
	}

	@Test(groups = { "MED", "High" })
	public void dealOfDay_SaleLimit_couponValidate() throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		Browser.scrollbyxpath("//h3[text()='Deals of the Day']"); // dealOfDayHeader
		if (driver.findElements(By.xpath("//div[@class='deals-block']")).size() > 0) {
			// firstDealBoxPrices
			oldPrice = Double.parseDouble(Browser.decimalFormat(Double.parseDouble(
					Browser.getTextByXpath("(//span[@data-price-type='oldPrice']/span)[1]").replaceAll("₹", ""))));
			offerPrice = Double.parseDouble(Browser.decimalFormat(Double.parseDouble(
					Browser.getTextByXpath("(//span[@data-price-type='finalPrice']/span)[1]").replaceAll("₹", ""))));
			Browser.clickOnTheElementByXpath("(//div[@class='deals-block'])[1]"); // firstDealBox
			productName = Browser.getTextByXpath("//div[@class='pro-name']/h1"); // PDPproductName
			couponName = Browser.getTextByXpath(Elements_Medicines.PDP_discountName); // CouponName
			Browser.waitFortheElementXpath(
					"//span[@data-price-type='finalPrice']/span[text()='₹" + Browser.decimalFormat(offerPrice) + "']");

			// Get SKU ID to update sale limit in SQL
			String productURL = driver.getCurrentUrl();
			String[] a = productURL.split("utm_source=");
			String[] b = a[1].split("&");
			skuId = b[0];
			// Update saleLimit in DB
			String product_ID = Browser.sqlDB_selectFromQuery("catalog_product_entity", "sku = '" + skuId + "'",
					"entity_id"); // Get entity_id as product_id in stockItem
			Browser.sqlDB_updateFieldQuery("cataloginventory_stock_item", "product_id = '" + product_ID + "'",
					"max_sale_qty = '1'");
			System.out.println("Max sale qty updated..");

			// Verify Sale limit validation in PDP
			Browser.enterTextByID("qty", "3");
			Browser.clickOnTheElementByXpath("//div[@class='actions']/button[@title='Add to Cart']"); // PDPaddToCart
			Browser.waitFortheElementXpath("//div[@id='qty-error' and text() = 'The maximum you may purchase is 1.']");
			Browser.enterTextByID("qty", "1");
			Browser.clickOnTheElementByXpath("//div[@class='actions']/button[@title='Add to Cart']"); // PDPaddToCart
			Thread.sleep(2000);

			// Verify Sale limit validation in Mini Cart
			Browser.waitFortheElementXpath(Elements_Medicines.header_miniCartViewBtn); // viewCartBtn
			Browser.waitFortheElementXpath("//strong[@class='product-item-name' and contains(., '" + productName
					+ "')]/following-sibling::div/div[@class='product-item-pricing']/div/span/span/span/span[@class='price' and contains(.,'₹"
					+ Browser.decimalFormat(offerPrice) + "')]");
			Browser.clickOnTheElementByXpath("//td[contains(., '" + productName
					+ "')]/following-sibling::td/div[@class='details-qty qty']/input");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//td[contains(., '" + productName
					+ "')]/following-sibling::td/div[@class='details-qty qty']/input")).sendKeys(Keys.ARROW_RIGHT);
			Thread.sleep(1000);
			driver.findElement(By.xpath("//td[contains(., '" + productName
					+ "')]/following-sibling::td/div[@class='details-qty qty']/input")).sendKeys(Keys.BACK_SPACE);
			driver.findElement(By.xpath("//td[contains(., '" + productName
					+ "')]/following-sibling::td/div[@class='details-qty qty']/input")).sendKeys("2");
			Browser.clickOnTheElementByXpath("//span[text()='Cart Subtotal']"); // focusOutClick
			// Qty Error PopUp
			Browser.waitFortheElementXpath("//div[text()='The most you may purchase is 1.']"); // ErrorMsg
			Browser.clickOnTheElementByXpath("//button/span[text()='OK']"); // popUpOk

			// Navigate to Cart
			Browser.openUrl(EnvironmentURL + "/medicines/checkout/cart/");
			productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, offerPrice, 1);
			// CouponValidation
			Browser.enterTextByXpath(Elements_Medicines.checkout_couponCode, couponName);
			Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_couponApplyBtn);
			Browser.waitFortheElementXpath("//div[@data-bind='html: message.text' and contains(., 'The coupon code \""
					+ couponName + "\" is not valid.')]");
			Browser.ScrollDown();
			checkout_orderTotal = ecomPage.checkout_coupon_summary("", "", productSubTotal_item);
			// Coupon Validation in shipping page
			Browser.waitFortheElementXpath(Elements_Medicines.shippingAddressHeader);
			Thread.sleep(5000);
			Browser.enterTextByXpath(Elements_Medicines.shipping_enterCouponName, couponName);
			Browser.clickOnTheElementByXpath("(" + Elements_Medicines.checkout_couponApplyBtn + ")[2]");
			Browser.waitFortheElementXpath(Elements_Medicines.shipping_invalidCouponMessage);
			shipping_orderTotal = ecomPage.shippingPage_orderSummary("", couponName, checkout_orderTotal,
					"Healthcare product online order OL.");
			ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
			String orderID = ecomPage.order_confirmationPage(productName, shipping_orderTotal, paymode);
			String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
			Assert.assertEquals(orderStatus, OrderStatus_APIresponse);
		} else {
			Assert.fail("No deal of day");
			System.out.println("No deals for the day.....");
		}
	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}