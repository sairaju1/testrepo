// @Author: Sagar Sen

package com.web.Medicine;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_26_ecomHomePage_uploadPrescription_MagentoCheck extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String fileName = "dc.png", orderID, imgName;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {

		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);

	}

	@Test(groups = { "MED", "High" }, priority = 1)
	public void ecomHomePage_uploadPrescription() throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		Browser.clickOnTheElementByXpath("//a[@class='upload-pre']"); // HomePageUploadBtn
		Thread.sleep(5000);
		System.out.println("Selecting prescription...");
		
		Browser.clickOnTheElementByXpath(Elements_Medicines.prescription_First_RecentFile); // firstCheckBox
		
		WebElement imgAlt = driver.findElement(By.xpath("(//img[@class='myImg'])[1]"));
		imgName = imgAlt.getAttribute("alt"); // ImageName
		Browser.waitUntil_InvisiblityByXpath(Elements_Medicines.prescription_continueDisable); // disabledBtn
		Browser.clickOnTheElementByXpath(Elements_Medicines.prescription_continueBtn); // continueBtn
		ecomPage.uploadPrescription_specifications(true, 2);
//		ecomPage.shippingPage_orderSummary(false, "", 0.00, "Prescription order.");
		Thread.sleep(3000);
		ecomPage.shippingPage_paymentMethod_placeOrderClick("");
		orderID = ecomPage.order_confirmationPage("", 0.0, "");
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, orderSuccess_Status);
	}
	

	@Test(groups = { "MED", "High" }, priority = 2)
	public void ZA_98_Magento_viewDownload_prescription() throws Exception {
		Browser.openUrl(magentoAdminURL);
		ecomPage.Magento_Login(Magento_user, Magento_pwd);
		ecomPage.Magento_sideMenu_click(false, "Sales", "Orders");
		Browser.waitFortheElementXpath("(//label[@class='data-grid-checkbox-cell-inner'])[1]"); // FirstchkBox
		ecomPage.Magento_Sales_Orders_search(orderID);
		Thread.sleep(3000);
		ecomPage.Magento_Sales_Orders_view(orderID);
		Browser.clickOnTheElementByXpath("//span[text()='Order Prescriptions']"); // prescSideMenu
		Browser.waitFortheElementXpath("(//h1[text()='Prescriptions'])[1]"); // prescHeader
		Browser.waitFortheElementXpath("(//img[@alt='" + imgName + "'])[1]");
		Browser.waitFortheElementXpath("(//td[contains(., '" + imgName + "')])[1]/following-sibling::td/a[text()=' View ']");
	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}