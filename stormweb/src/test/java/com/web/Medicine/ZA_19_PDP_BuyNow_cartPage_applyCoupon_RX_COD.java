// @Author: Sagar Sen

package com.web.Medicine;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_19_PDP_BuyNow_cartPage_applyCoupon_RX_COD extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName, paymode, couponName, couponName2;
	public int qty = 1;
	public Double searchListPrice, productSubTotal_item, checkout_orderTotal, shipping_orderTotal;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		paymode = cod;
		productName = productName_rx;
		couponName2 = "ZMED20";
	}

	@Test(groups = { "MED", "High" })
	public void cartPage_applyCoupon_RX_COD() throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		searchListPrice = ecomPage.medicineHomePage_search(productName, "");
		couponName = ecomPage.PDP_getCouponName(productName);
		ecomPage.PDP_itemQtyInfo(productName, qty, "Buy");
		productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, searchListPrice, qty);
		checkout_orderTotal = ecomPage.checkout_coupon_summary("Apply", couponName, productSubTotal_item);
		ecomPage.checkout_prescriptionPopUp(false);
		ecomPage.shippingPage_orderSummary("", "", checkout_orderTotal, "Healthcare product online order OL.");
		ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
		Thread.sleep(8000);
		if (driver.findElements(By
				.xpath("//div[@data-bind='html: message.text' and contains(., 'not applicable for Cash on Delivery')]"))
				.size() > 0) {
			Browser.clickOnTheElementByXpath(
					"//div[@data-bind='html: message.text' and contains(., 'not applicable for Cash on Delivery')]");
			Browser.scrollbyxpath("(//button[@value='Cancel']/span)[2]");
			Browser.clickOnTheElementByXpath("(//button[@value='Cancel']/span)[2]");
			Browser.waitFortheElementXpath("(" + Elements_Medicines.shipping_couponApplyBtn + ")[2]");
			shipping_orderTotal = ecomPage.shippingPage_orderSummary("Apply", couponName2, checkout_orderTotal,
					"Healthcare product online order OL.");
		}
		ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
		String orderID = ecomPage.order_confirmationPage(productName, shipping_orderTotal, paymode);
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, orderSuccess_Status);

		// My Orders Verify Amount
		Browser.openUrl(EnvironmentURL);
		RecipientPage.header_UserDetails_SelectMenu("My Orders", "");
		Browser.waitFortheElementXpath("//h3[text()='My Orders']");
		Browser.waitFortheElementXpath("//td[text()='##" + orderID + "']"); // OrderNo
		Browser.waitFortheElementXpath("//td[text()='##" + orderID + "']/preceding-sibling::td[text()='Order Placed']"); // OrderStatus
		Browser.waitFortheElementXpath("//td[text()='##" + orderID + "']/preceding-sibling::td[text()='₹ "
				+ Browser.decimalFormat(shipping_orderTotal) + "']"); // OrderAmt
		Browser.clickOnTheElementByXpath(
				"//td[text()='##" + orderID + "']/following-sibling::td/a[@class='view-details collapsed']"); // expandDetails
		Browser.waitFortheElementXpath("//table/tr[contains(., '##" + orderID
				+ "')]/following-sibling::tr/td/div/div/div[2]/div/p/span[text()='Order Total:']/following-sibling::span[text()=' ₹ "
				+ Browser.decimalFormat(shipping_orderTotal) + "']");
	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
