// @Author: Sagar Sen

package com.web.Medicine;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Address;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Medicines;
import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_75_shippingAddress_signUp_validations extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public Person p;
	public Address AddressM;
	public String productName, paymode, firstname, lastname, mobile, email, Password;
	public Double searchListPrice, productSubTotal_item, checkout_orderTotal, shippingAmt;
	public int qty = 1;

	@BeforeClass(groups = { "MED", "Low" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		paymode = cod;
		productName = productName_rx;
		p = Browser.personData();
		firstname = p.getFirstName();
		lastname = p.getLastName();
		email = p.getEmail();
		mobile = "7" + Browser.generateRandomNumber(9);
		Password = "Zoylo@123";
		AddressM = p.getAddress();
	}

	@Test(groups = { "MED", "Low" }, priority = 1)
	public void newSignUp_navigateToShippingPage() throws Exception {
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		ecomPage.medicineTopMenu_click("Medicine", "");
		searchListPrice = ecomPage.medicineHomePage_search(productName, "");
		ecomPage.PDP_itemQtyInfo(productName, qty, "Buy");
		productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, searchListPrice, qty);
		ecomPage.checkout_coupon_summary_newSignUp(false, "", productSubTotal_item);

		// SignUp
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_link);
		RecipientPage.SignUp_Details(firstname, lastname, mobile, email, Password);
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_signupButton);
		Thread.sleep(2000);
		String OTP = Browser.getOtp(email, Environment);
		Browser.waitFortheElementXpath(Elements_Recipients.signUp_otp);
		Browser.enterTextByXpath(Elements_Recipients.signUp_otp, OTP);
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_OTP_submitButton);
		checkout_orderTotal = ecomPage.checkout_coupon_summary_newSignUp(false, "", productSubTotal_item);
		ecomPage.checkout_prescriptionPopUp(false);
	}

	@DataProvider(name = "ShipAddress")
	public Object[][] shippingAddresss() throws Exception {
		Object[][] shipData = TestUtils.getTableArray("TestData/ecom.xls", "Address", "ZA_75");
		return shipData;
	}

	@Test(groups = { "MED",
			"Low" }, dependsOnMethods = "newSignUp_navigateToShippingPage", dataProvider = "ShipAddress")
	public void shippingAddress_validation(String AddressType, String Address, String state, String city,
			String pinCode, String mobileNumber, String AddressVM, String stateVM, String cityVM, String pinCodeVM,
			String mobileNumberVM) throws Exception {
		Browser.waitFortheElementXpath("//span[contains(., 'Shipping Address')]");
		Browser.selectXpathByValue("(//select[@name='customer_address_type'])[1]", AddressType);
		Browser.enterTextByXpath("(//input[@name='street[0]'])[1]", Address);
		Browser.selectXpathByText("(//select[@name='region_id'])[1]", state);
		Browser.enterTextByName("city", city);
		Browser.enterTextByName("postcode", pinCode);
		Browser.enterTextByName("telephone", mobileNumber);
		Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_placeOrderBtn);
		Thread.sleep(3000);
		Browser.waitFortheElementXpath(
				"(//input[@name='street[0]'])[1]/following-sibling::div/span[text()='" + AddressVM + "']");
		Browser.waitFortheElementXpath(
				"(//select[@name='region_id'])[1]/following-sibling::div/span[text()='" + stateVM + "']");
		Browser.waitFortheElementXpath(
				"(//input[@name='city'])[1]/following-sibling::div/span[text()='" + cityVM + "']");
		Browser.waitFortheElementXpath(
				"(//input[@name='postcode'])[1]/following-sibling::div/span[text()='" + pinCodeVM + "']");
		Browser.waitFortheElementXpath(
				"(//input[@name='telephone'])[1]/following-sibling::div/span[text()='" + mobileNumberVM + "']");
	}

	@AfterClass(groups = { "MED", "Low" })
	public void tearDown() throws Exception {
		String id = Browser.get_Document_ID(Environment, "zoyloUser", "emailInfo.emailAddress", email);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", email);
		Browser.remove_Document(Environment, "zoyloUserProfile", "zoyloId", id);
		Browser.quit();
	}

}
