// @Author: Sagar Sen

package com.web.Medicine;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_96_maxSaleLimit_OTC extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		productName = productName_nonRX;
	}

	@Test(groups = { "MED", "High" })
	public void otc_saleLimit() throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		ecomPage.medicineHomePage_search(productName, "");
		Thread.sleep(5000);
		String product_ID = Browser.sqlDB_selectFromQuery("catalog_product_entity_varchar",
				"value = '" + productName + "'", "entity_id");
		// Update saleLimit in DB
		Browser.sqlDB_updateFieldQuery("cataloginventory_stock_item", "product_id = '" + product_ID + "'",
				"max_sale_qty = '5'");
		Browser.sqlDB_updateFieldQuery("cataloginventory_stock_item", "product_id = '" + product_ID + "'",
				"use_config_max_sale_qty = '0'");
		ecomPage.PDP_itemQtyInfo(productName, 6, "Buy");
		Browser.waitFortheElementXpath("//div[@id='qty-error' and text()='The maximum you may purchase is 5.']");
	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}