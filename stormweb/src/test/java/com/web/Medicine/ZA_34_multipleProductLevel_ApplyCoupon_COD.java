// @Author: Sagar Sen

package com.web.Medicine;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_34_multipleProductLevel_ApplyCoupon_COD extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productNameRX, productName_OTC, paymode, couponName, rxdiscountAmtString, otcdiscountAmtString,
			orderTotalString;
	public int qty = 1, rx_discountPer = 20, otc_discountPer = 5;
	public Double searchListPriceRX, PLPprice_OTC, cartPriceRX, cartPriceOTC, cart_productSubTotal, shippingAmt,
			rxdiscountAmt, otcdiscountAmt, orderTotal_checkout;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		paymode = cod;
		productNameRX = productName_rx;
		productName_OTC = "RETFERRO B12 CAP";
		couponName = "ZMED20";
	}

	@Test(groups = { "MED", "High" })
	public void applyCoupon_productLevel() throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		searchListPriceRX = ecomPage.medicineHomePage_search(productNameRX, "addtocart");
		Browser.waitFortheElementXpath(Elements_Medicines.header_miniCartViewBtn); // viewCartBtn
		PLPprice_OTC = ecomPage.medicineHomePage_search(productName_OTC, "addtocart");
		Browser.clickOnTheElementByXpath(Elements_Medicines.header_miniCartViewBtn); // viewCartBtn
//		ecomPage.medicineTopMenu_click("Ayurveda", "");
//		productName_OTC = Browser.getTextByXpath(Elements_Medicines.PLP_firstproductName_nonRX);
//		PLPprice_OTC = Double
//				.parseDouble(Browser
//						.getTextByXpath("//strong[contains(., '" + productName_OTC
//								+ "')]/following-sibling::div[@data-role='priceBox']/span/span/span")
//						.replaceAll("₹", ""));
//		Browser.clickOnTheElementByXpath("//strong[contains(., '" + productName_OTC
//				+ "')]/following-sibling::div[@class='product-item-inner']/div/div/form/button/span"); // AddtoCart
//		Browser.clickOnTheElementByXpath(Elements_Medicines.header_miniCartViewBtn); // viewCartBtn
		cartPriceRX = ecomPage.checkout_cart_itemQtyInfo(productNameRX, searchListPriceRX, qty);
		cartPriceOTC = ecomPage.checkout_cart_itemQtyInfo(productName_OTC, PLPprice_OTC, qty);
		cart_productSubTotal = cartPriceRX + cartPriceOTC;

		// Cart Page separate code
		if (cart_productSubTotal < 500.00) {
			try {
				Browser.waitFortheElementXpath(Elements_Medicines.checkout_shippingAmtWait);
				shippingAmt = Double.parseDouble(
						Browser.getTextByXpath(Elements_Medicines.checkout_shippingAmt).replaceAll("₹", ""));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			shippingAmt = 0.00;
		}
		Browser.enterTextByXpath(Elements_Medicines.checkout_couponCode, couponName);
		Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_couponApplyBtn);
//		Browser.waitFortheElementXpath("//div[@data-bind='html: message.text' and contains(., 'You used coupon code \""
//				+ couponName + "\"')]");
		rxdiscountAmt = (cartPriceRX * rx_discountPer) / 100;
		otcdiscountAmt = (cartPriceOTC * otc_discountPer) / 100;
		rxdiscountAmtString = Browser.decimalFormat(rxdiscountAmt);
		otcdiscountAmtString = Browser.decimalFormat(otcdiscountAmt);
		Browser.waitFortheElementXpath("//tr[@class='item-info' and contains(., '" + productNameRX
				+ "')]/following-sibling::tr/td/h5/span[text()='₹" + rxdiscountAmtString + "']");
		Browser.waitFortheElementXpath("//tr[@class='item-info' and contains(., '" + productName_OTC
				+ "')]/following-sibling::tr/td/h5/span[text()='₹" + otcdiscountAmtString + "']");
		Double discountAmt = Double.parseDouble(rxdiscountAmtString) + Double.parseDouble(otcdiscountAmtString);
		Browser.waitFortheElementXpath(
				"//th[contains(., 'Discount')]/following-sibling::td[@data-th='Discount' and contains(., '"
						+ discountAmt + "')]");
		orderTotal_checkout = (cart_productSubTotal + shippingAmt) - discountAmt;
		orderTotalString = Browser.decimalFormat(orderTotal_checkout);
		Browser.waitFortheElementXpath(
				"//td[@class='amount']/strong/span[@class='price' and text()='₹" + orderTotalString + "']");
		Thread.sleep(10000);
		Browser.scrollbyxpath(Elements_Medicines.checkout_Button);
		Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_Button);
		ecomPage.checkout_prescriptionPopUp(false);
		ecomPage.shippingPage_orderSummary("", "", orderTotal_checkout, "Healthcare product online order OL.");
		ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);

		// Order confirmation page
		Browser.waitFortheElementXpath(Elements_Medicines.confirmation_heading);
		String orderID = Browser.getTextByXpath(Elements_Medicines.confirmation_getOrderId).replaceAll("[^0-9]", "");
		Browser.waitFortheElementXpath(
				"//div[contains(., 'Items in Order')]/table/tbody/tr/td[contains(., '" + productNameRX + "')]");
		Browser.waitFortheElementXpath(
				"//div[contains(., 'Items in Order')]/table/tbody/tr/td[contains(., '" + productName_OTC + "')]");
		Browser.waitFortheElementXpath("//h5[@class='grand-total-rightl' and text()='" + orderTotalString + "']");
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, orderSuccess_Status);

	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
