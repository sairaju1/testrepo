// @Author: Sagar Sen

package com.web.Medicine;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_16_medicine_PLP_cart_placeOrder_RX_COD extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName, paymode;
	public Double PLPprice, productSubTotal_item, checkout_orderTotal;
	public int qty = 2;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		paymode = cod;
	}

	@Test(groups = { "MED", "High" })
	public void orderRX_prescriptionLater_COD() throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		ecomPage.medicineTopMenu_click("Medicine", "");
		Browser.waitFortheElementXpath("//i[@class='zoylo-icon doorstep-icon']"); // MedicinePageIcon
		productName = Browser.getTextByXpath(Elements_Medicines.PLP_firstProductName_MedicineRX);
		PLPprice = ecomPage.medicine_PLPpage_getProductPrice_addToCartBtn_click(productName);
		Browser.clickOnTheElementByXpath(Elements_Medicines.PLP_shoppingCartLinkText);
		productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, PLPprice, qty);
		checkout_orderTotal = ecomPage.checkout_coupon_summary("", "", productSubTotal_item);
		ecomPage.checkout_prescriptionPopUp(false);
		ecomPage.shippingPage_orderSummary("", "", checkout_orderTotal, "Healthcare product online order COD.");
		ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
		String orderId = ecomPage.order_confirmationPage(productName, checkout_orderTotal, paymode);
		String orderStatus = ecomPage.getOrderStatus_postApi(orderId);
		Assert.assertEquals(orderStatus, orderSuccess_Status);
	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}