// @Author: Sagar Sen

package com.web.Medicine;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_83_OTC_addToCart_uploadPrescription_overrideCart extends LoadProp {
	
	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName, paymode;
	public int qty = 1;
	
	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		productName = productName_nonRX;
	}
	
	@Test(groups = { "MED", "High" })
	public void overrideProductBYprescription() throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		ecomPage.medicineHomePage_search(productName, "addtocart");
		Browser.clickOnTheElementByXpath(Elements_Medicines.header_miniCartViewBtn); // viewCartBtn
		Browser.waitFortheElementXpath(Elements_Medicines.cart_prescriptionHeader);
		Browser.waitFortheElementXpath("//td[@data-th='Item' and contains(., '" + productName + "')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		Browser.clickOnTheElementByXpath("//a[@class='upload-pre']"); // HomePageUploadBtn
		ecomPage.uploadPrescriptionPage(false, "", "");
		ecomPage.uploadPrescription_specifications(false, 0);
		Thread.sleep(3000);
		ecomPage.shippingPage_paymentMethod_placeOrderClick("");
		String orderID = ecomPage.order_confirmationPage("", 0.0, "");
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, orderSuccess_Status);
	}
	
	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
