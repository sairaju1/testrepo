// @Author: Sagar Sen

package com.web.Medicine;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_17_PDP_BuyNow_cancelTrn_failurePage_RX_ONLINE extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName, paymode, orderID;
	public int qty = 1;
	public Double searchListPrice, productSubTotal_item, checkout_orderTotal;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		paymode = online;
		productName = productName_rx;
	}

	@Test(groups = { "MED", "High" })
	public void cancelTransaction_verifyFailurePage_orderStatus_payOnline() throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		searchListPrice = ecomPage.medicineHomePage_search(productName, "");
		ecomPage.PDP_itemQtyInfo(productName, qty, "Buy");
		productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, searchListPrice, qty);
		checkout_orderTotal = ecomPage.checkout_coupon_summary("", "", productSubTotal_item);
		ecomPage.checkout_prescriptionPopUp(false);
		ecomPage.shippingPage_orderSummary("", "", checkout_orderTotal, "Healthcare product online order OL.");
		ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
		RecipientPage.MakePayment_fail();
		Browser.waitFortheElementXpath("//h3[text()='Order UnSuccessful']");
		orderID = Browser.getTextByXpath(Elements_Medicines.confirmation_getOrderId).replaceAll("[^0-9]", "");
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, orderFail_Status);
		Browser.openUrl(EnvironmentURL);
		RecipientPage.header_UserDetails_SelectMenu("My Orders", "");
		Browser.waitFortheElementXpath("//h3[text()='My Orders']");
		Browser.waitFortheElementXpath("//td[text()='##" + orderID + "']/preceding-sibling::td[text()='payment failed']"); // OrderStatus
	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
