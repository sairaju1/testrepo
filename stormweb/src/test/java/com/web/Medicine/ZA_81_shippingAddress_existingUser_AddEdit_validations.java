// @Author: Sagar Sen

package com.web.Medicine;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_81_shippingAddress_existingUser_AddEdit_validations extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName;
	public int qty = 1;
	public Double PLPprice, productSubTotal_item, checkout_orderTotal;

	@BeforeClass(groups = { "MED", "Low" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
	}

	@Test(groups = { "MED", "Low" }, priority = 1)
	public void navigateToShippingPage() throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		ecomPage.medicineTopMenu_click("Healthcare Products", "");
		if (driver.findElements(By.xpath("//div[@class='message info empty']")).size() > 0) {
			System.out.println("No Products available");
			Assert.fail();
		} else {
			productName = Browser.getTextByXpath(Elements_Medicines.PLP_firstproductName_nonRX);
			PLPprice = Double
					.parseDouble(Browser
							.getTextByXpath("//strong[contains(., '" + productName
									+ "')]/following-sibling::div[@data-role='priceBox']/span/span/span")
							.replaceAll("₹", ""));
			Browser.clickOnTheElementByXpath("//strong[contains(., '" + productName
					+ "')]/following-sibling::div[@class='product-item-inner']/div/div/form/button/span"); // AddtoCart
			Browser.clickOnTheElementByXpath(Elements_Medicines.header_miniCartViewBtn); // viewCartBtn
			productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, PLPprice, qty);
			checkout_orderTotal = ecomPage.checkout_coupon_summary("", "", productSubTotal_item);
		}
	}

	@DataProvider(name = "ShipAddress")
	public Object[][] shippingAddresss() throws Exception {
		Object[][] shipData = TestUtils.getTableArray("TestData/ecom.xls", "Address", "ZA_81_EDIT");
		return shipData;
	}

	@Test(groups = { "MED", "Low" }, priority = 2, dataProvider = "ShipAddress")
	public void shippingAddress_Edit_Validation(String Street, String City, String State, String pincode,
			String country, String phoneNum, String AlertVal) throws Exception {
		Browser.waitFortheElementXpath(Elements_Medicines.shippingAddressHeader);
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath(
				"(//div[@class='shipping-address-item'])[1]/button[@class='edit-ship-address-link']"); // firstAddressEditBtn
		Browser.waitFortheElementXpath("//h1[@class='modal-title' and contains(., 'Edit Shipping Address')]"); // editHeader
		Browser.enterTextByXpath("//label[contains(., 'Street')]/following-sibling::div/input", Street);
		Browser.enterTextByXpath("(//label[contains(., 'City')]/following-sibling::div/input)[3]", City);
		Browser.selectXpathByText("//label[contains(., 'Region/State')]/following-sibling::select", State);
		Browser.enterTextByXpath("//label[contains(., 'Post Code')]/following-sibling::div/input", pincode);
		Browser.selectXpathByText("//label[contains(., 'Country')]/following-sibling::select", country);
		Browser.enterTextByXpath("//label[contains(., 'Telephone')]/following-sibling::div/input", phoneNum);
		Browser.clickOnTheElementByXpath("//span[text()='Save Address']"); // SaveAddress
		Thread.sleep(1000);
		String val = driver.switchTo().alert().getText(); // getAlertText
		driver.switchTo().alert().accept(); // closeAllertBox
		Assert.assertEquals(val, AlertVal);
		Browser.clickOnTheElementByXpath("(//button[@data-role='closeBtn'])[2]"); // closeBtn
	}

	@DataProvider(name = "ShipAddress_Add")
	public Object[][] shippingAddresss_Add() throws Exception {
		Object[][] shipData = TestUtils.getTableArray("TestData/ecom.xls", "Address", "ZA_81_ADD");
		return shipData;
	}

	@Test(groups = { "MED", "Low" }, priority = 3, dataProvider = "ShipAddress_Add")
	public void shippingAddress_Add_Validation(String Address, String state, String city, String pinCode,
			String country, String mobileNumber, String AddressVM, String stateVM, String cityVM, String pinCodeVM,
			String countryVM, String mobileNumberVM) throws Exception {
		Thread.sleep(5000);
		Browser.openUrl(EnvironmentURL + "/medicines/checkout/cart");
		Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_Button);
		Thread.sleep(5000);
		Browser.waitFortheElementXpath(Elements_Medicines.shippingAddressHeader);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath("//button/span[contains(., 'New Address')]"); // newAddressBtn
		Thread.sleep(2000);
		Browser.waitFortheElementXpath("//h1[@class='modal-title' and contains(., 'Shipping Address')]"); // shippingAddHeader
		Browser.enterTextByXpath("(//input[@name='street[0]'])[2]", Address);
		Browser.selectXpathByText("(//label[contains(., 'State/Province')])[3]/following-sibling::div/select", state);
		Browser.enterTextByXpath("(//label[contains(., 'City')])[2]/following-sibling::div/input", city);
		Browser.enterTextByXpath("(//label[contains(., 'Postal Code')])[2]/following-sibling::div/input", pinCode);
		Browser.selectXpathByText("(//label[contains(., 'Country')])[2]/following-sibling::div/select", country);
		Browser.enterTextByXpath("(//label[contains(., 'Phone Number')])[2]/following-sibling::div/input",
				mobileNumber);
		Browser.clickOnTheElementByXpath("//span[text()='Save Address']"); // addressSaveBtn
		Browser.scrollbyxpath("(//input[@name='street[0]'])[2]"); // scrollUp
		Thread.sleep(3000);
		Browser.waitFortheElementXpath(
				"(//input[@name='street[0]'])[2]/following-sibling::div/span[text()='" + AddressVM + "']");
		Browser.waitFortheElementXpath(
				"(//label[contains(., 'State/Province')])[4]/following-sibling::div/div/span[text()='" + stateVM
						+ "']");
		Browser.waitFortheElementXpath(
				"(//label[contains(., 'City')])[2]/following-sibling::div/div/span[text()='" + cityVM + "']");
		Browser.scrollbyxpath("(//span[contains(., 'Save in address book')])[2]"); // scrollDown
		Browser.waitFortheElementXpath(
				"(//label[contains(., 'Postal Code')])[2]/following-sibling::div/div/span[text()='" + pinCodeVM + "']");
		Browser.waitFortheElementXpath(
				"(//label[contains(., 'Country')])[2]/following-sibling::div/div/span[text()='" + countryVM + "']");
		Browser.waitFortheElementXpath(
				"(//label[contains(., 'Phone Number')])[2]/following-sibling::div/div[2]/span[text()='" + mobileNumberVM
						+ "']");
		Browser.clickOnTheElementByXpath("//span[text()='Cancel']"); // cancelBtn
		Thread.sleep(2000);
	}

	@AfterClass(groups = { "MED", "Low" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
