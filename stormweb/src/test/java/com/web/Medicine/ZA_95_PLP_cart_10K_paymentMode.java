// @Author: Sagar Sen

package com.web.Medicine;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_95_PLP_cart_10K_paymentMode extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName, PLPprice;
	public Double productPrice;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
	}

	@Test(groups = { "MED", "High" })
	public void PLP_cart_10Kprice() throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		ecomPage.medicineTopMenu_click("Medicine", "");
		Browser.openUrl(EnvironmentURL + "/medicines/home.html?price=10000-60000");
		productName = Browser.getTextByXpath(Elements_Medicines.PLP_firstProductName_MedicineRX);
		PLPprice = Browser
				.getTextByXpath("//strong[contains(., '" + productName
						+ "')]/following-sibling::div[@data-role='priceBox']/span/span/span")
				.replaceAll(",", "").replaceAll("₹", "");
		PLPprice = Browser.decimalFormat(Double.parseDouble(PLPprice)).replaceAll(",", "");
		productPrice = Double.parseDouble(PLPprice);
		Browser.clickOnTheElementByXpath("//strong[contains(., '" + productName
				+ "')]/following-sibling::div[@class='product-item-inner']/div/div/form/button");
		Thread.sleep(2000);
		if (driver.findElements(By.xpath(Elements_Medicines.header_miniCartViewBtn)).size() == 0) {
			while (driver.findElements(By.xpath(Elements_Medicines.header_miniCartViewBtn)).size() == 0) {
				Browser.clickOnTheElementByXpath("//div[@data-block='minicart']"); // minicartIcon
			} 
		}
		Browser.waitFortheElementXpath(Elements_Medicines.header_miniCartViewBtn); // viewCartBtn
		Browser.waitFortheElementXpath("//strong[@class='product-item-name' and contains(., '" + productName + "')]");
		Browser.clickOnTheElementByXpath(Elements_Medicines.header_miniCartViewBtn); // viewCartBtn
		Browser.waitFortheElementXpath(Elements_Medicines.cart_prescriptionHeader);
		Browser.waitFortheElementXpath("//td[@data-th='Item' and contains(., '" + productName + "')]");
		Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_Button);
		ecomPage.checkout_prescriptionPopUp(false);
		Browser.waitFortheElementXpath(Elements_Medicines.shippingAddressHeader);
		Assert.assertEquals(driver.findElements(By.xpath("//input[@value='cashondelivery']")).size(), 0);
	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}