// @Author: Sagar Sen

package com.web.Medicine;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_38_PDP_BuyNow_uploadPrescription_placeOrder_RX_COD extends LoadProp {
	
	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName, paymode, orderID, imgName;
	public int qty = 1;
	public Double searchListPrice, productSubTotal_item, checkout_orderTotal;
	
	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		paymode = cod;
		productName = productName_rx;
	}
	
	@Test(groups = { "MED", "High" }, priority = 1)
	public void PDP_BuyNow_UploadPrescription_RX_COD() throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		ecomPage.medicineTopMenu_click("Medicine", "");
		searchListPrice = ecomPage.medicineHomePage_search(productName, "");
		ecomPage.PDP_itemQtyInfo(productName, qty, "Buy");
		productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, searchListPrice, qty);
		checkout_orderTotal = ecomPage.checkout_coupon_summary("", "", productSubTotal_item);	
//		ecomPage.checkout_prescriptionPopUp(true);
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_prescriptionPopUp_Ready); // prescriptionReady
		Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_prescriptionPopUp_proceed); // popupproceed
		
		Thread.sleep(5000);
		System.out.println("Selecting prescription...");
		
		Browser.clickOnTheElementByXpath(Elements_Medicines.prescription_First_RecentFile); // firstCheckBox
		
		WebElement imgAlt = driver.findElement(By.xpath("(//img[@class='myImg'])[1]"));
		imgName = imgAlt.getAttribute("alt"); // ImageName
		
		Browser.waitUntil_InvisiblityByXpath(Elements_Medicines.prescription_continueDisable); // disabledBtn
		Browser.clickOnTheElementByXpath(Elements_Medicines.prescription_continueBtn); // continueBtn
		
		ecomPage.shippingPage_orderSummary("", "", checkout_orderTotal, "Healthcare product online order OL.");
		ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
		orderID = ecomPage.order_confirmationPage(productName, checkout_orderTotal, paymode);
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, orderSuccess_Status);
	}
	
	@Test(groups = { "MED", "High" }, priority = 2)
	public void ZA_98_Magento_viewDownload_prescription() throws Exception {
		Browser.openUrl(magentoAdminURL);
		ecomPage.Magento_Login(Magento_user, Magento_pwd);
		ecomPage.Magento_sideMenu_click(false, "Sales", "Orders");
		Thread.sleep(5000);
		Browser.waitFortheElementXpath("(//label[@class='data-grid-checkbox-cell-inner'])[1]"); // FirstchkBox
		ecomPage.Magento_Sales_Orders_search(orderID);
		Thread.sleep(3000);
		ecomPage.Magento_Sales_Orders_view(orderID);
		Browser.clickOnTheElementByXpath("//span[text()='Order Prescriptions']"); // prescSideMenu
		Browser.waitFortheElementXpath("(//h1[text()='Prescriptions'])[1]"); // prescHeader
		Browser.waitFortheElementXpath("(//img[@alt='" + imgName + "'])[1]");
		Browser.waitFortheElementXpath("(//td[contains(., '" + imgName + "')])[1]/following-sibling::td/a[text()=' View ']");
	}
	
	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
