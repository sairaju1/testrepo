// @Author: Sagar Sen

package com.web.Medicine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_15_RX_OTC_Filters extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public Double priceRange = 55.95;
	public Double priceRange_otc = 92.90;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
	}

	@Test(groups = { "MED", "High" })
	public void filters_Sorts() throws Exception {

		// MEDICINES FILTER SORTS
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		ecomPage.medicineTopMenu_click("Medicine", "");

		Browser.enterTextByXpath("(//input[@placeholder='Search'])[1]", manufacturer);
		Browser.clickOnTheElementByXpath("//a[contains(., '" + manufacturer + "')]");

		Browser.waitFortheElementXpath(
				"//span[text()='Manufacturer']/following-sibling::span[text()='" + manufacturer + "']");
		int numOfproducts_rx = driver.findElements(By.xpath("//div[@class='product-item-info']")).size();
		String url = driver.getCurrentUrl();
		// Product names in Array
		List<String> names = new ArrayList<String>(numOfproducts_rx);
		for (int i = 1; i <= numOfproducts_rx; i++) {
			String nameText = Browser.getTextByXpath(
					"(//div[@class='product-item-info']/div[@class='product details product-item-details']/strong/a)["
							+ i + "]");
			names.add(nameText);
		}
		String[] productNames = names.toArray(new String[0]);
		Arrays.sort(productNames); // Sort product names

		Browser.selectXpathByValue("(//select[@id='sorter'])[1]", "name"); // sortByName
		Thread.sleep(10000);

		// Product names in Array after sort
		List<String> names_sort = new ArrayList<String>(numOfproducts_rx);
		for (int i = 1; i <= numOfproducts_rx; i++) {
			String nameText_sort = Browser.getTextByXpath(
					"(//div[@class='product-item-info']/div[@class='product details product-item-details']/strong/a)["
							+ i + "]");
			names_sort.add(nameText_sort);
		}
		String[] productNames_sort = names_sort.toArray(new String[0]);
		// Assert product names after sorting in order
		for (int i = 0; i < numOfproducts_rx; i++) {
			Assert.assertEquals(productNames[i], productNames_sort[i]);
		}

		// Product prices in Array
		List<Double> productPricesList = new ArrayList<Double>(numOfproducts_rx);
		for (int i = 1; i <= numOfproducts_rx; i++) {
			Double productPrice = Double.parseDouble(Browser.getTextByXpath(
					"(//div[@class='product-item-info']/div[@class='product details product-item-details']/div[@data-role='priceBox']/span/span/span)["
							+ i + "]")
					.replaceAll("₹", ""));
			productPricesList.add(productPrice);
		}

		Double[] productPrice = productPricesList.toArray(new Double[productPricesList.size()]);

		// Price Filter
		Browser.openUrl(url + "&price=10.66-" + priceRange + "");
		Browser.waitFortheElementXpath("//div[@class='filter-current']/ol/li[contains(., 'Price')]");

		// Product prices in Array after filter
		List<Double> productPrices_filter = new ArrayList<Double>(numOfproducts_rx);
		for (int x = 0; x < numOfproducts_rx; x++) {
			if (productPrice[x] < priceRange) {
				productPrices_filter.add(productPrice[x]);
			}
		}
		int expected_ProductPriceFilter = productPrices_filter.size();
		int actual_ProductPriceFilter = driver.findElements(By.xpath("//div[@class='product-item-info']")).size();
		Assert.assertEquals(actual_ProductPriceFilter, expected_ProductPriceFilter);

		// OTC FILTERS SORT
		ecomPage.medicineTopMenu_click("Ayurveda", "");
		Browser.scrollbyxpath("(//input[@placeholder='Search Brands'])[1]");
		Browser.enterTextByXpath("(//input[@placeholder='Search'])[2]", brands);
		Browser.clickOnTheElementByXpath("//a[contains(., '" + brands + "')]");

		Browser.waitFortheElementXpath("//span[text()='Brands']/following-sibling::span[text()='" + brands + "']");
		int numOfproducts_otc = driver.findElements(By.xpath("//div[@class='product-item-info']")).size();
		String url_otc = driver.getCurrentUrl();
		// Product names in Array
		List<String> names_otc = new ArrayList<String>(numOfproducts_otc);
		for (int i = 1; i <= numOfproducts_otc; i++) {
			String nameText_otc = Browser.getTextByXpath(
					"(//div[@class='product-item-info']/div[@class='product details product-item-details']/strong/a)["
							+ i + "]");
			names_otc.add(nameText_otc);
		}
		String[] productNames_otc = names_otc.toArray(new String[0]);
		Arrays.sort(productNames_otc); // Sort product names
		Browser.selectXpathByValue("(//select[@id='sorter'])[1]", "name"); // sortByName
		Thread.sleep(15000);
		// Product names in Array after sort
		List<String> names_sort_otc = new ArrayList<String>(numOfproducts_otc);
		for (int i = 1; i <= numOfproducts_otc; i++) {
			String nameText_sort_otc = Browser.getTextByXpath(
					"(//div[@class='product-item-info']/div[@class='product details product-item-details']/strong/a)["
							+ i + "]");
			names_sort_otc.add(nameText_sort_otc);
		}
		String[] productNames_sort_otc = names_sort_otc.toArray(new String[0]);
		// Assert product names after sorting in order
		for (int i = 0; i < numOfproducts_otc; i++) {
			Assert.assertEquals(productNames_otc[i], productNames_sort_otc[i]);
		}

		// Product prices in Array
		List<Double> productPricesList_otc = new ArrayList<Double>(numOfproducts_otc);
		for (int i = 1; i <= numOfproducts_otc; i++) {
			Double productPrice_otc = Double.parseDouble(Browser.getTextByXpath(
					"(//div[@class='product-item-info']/div[@class='product details product-item-details']/div[@data-role='priceBox']/span/span/span)["
							+ i + "]")
					.replaceAll("₹", ""));
			productPricesList_otc.add(productPrice_otc);
		}
		Double[] productPrice_otc = productPricesList_otc.toArray(new Double[productPricesList_otc.size()]);
		// Price Filter
		Browser.openUrl(url_otc + "&price=45.9-" + priceRange_otc + "");
		Browser.waitFortheElementXpath("//div[@class='filter-current']/ol/li[contains(., 'Price')]");

		// Product prices in Array after filter
		List<Double> productPrices_filter_otc = new ArrayList<Double>(numOfproducts_otc);
		for (int x = 0; x < numOfproducts_otc; x++) {
			if (productPrice_otc[x] < priceRange_otc) {
				productPrices_filter_otc.add(productPrice_otc[x]);
			}
		}
		int expected_ProductPriceFilter_otc = productPrices_filter_otc.size();
		int actual_ProductPriceFilter_otc = driver.findElements(By.xpath("//div[@class='product-item-info']")).size();
		Assert.assertEquals(actual_ProductPriceFilter_otc, expected_ProductPriceFilter_otc);
	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
