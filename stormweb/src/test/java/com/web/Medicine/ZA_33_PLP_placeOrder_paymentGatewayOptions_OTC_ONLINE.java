// @Author: Sagar Sen

package com.web.Medicine;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_33_PLP_placeOrder_paymentGatewayOptions_OTC_ONLINE extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName, paymode;
	public Double PLPprice, productSubTotal_item, checkout_orderTotal, shipping_orderTotal;
	public int qty = 1;

	@BeforeClass(groups = { "MED", "Low" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		paymode = online;
	}

	@DataProvider(name="PaymentGateways")
	 public String[][] createData1() {
			return new String[][] {
					{"Wallet", "AMAZON PAY" },
					{"Wallet", "MOBIKWIK" },
					{"Wallet", "OLAMONEY" },
					{"Cards", "" },
					{"Paypal", "" },
			};
	}
	
	@Test(groups = { "MED", "Low" }, dataProvider = "PaymentGateways")
	public void otc_placeOrder_Cart_COD(String PaymentGateway, String PaymentGateway_Type) throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		ecomPage.medicineTopMenu_click("Healthcare Products", "Toothpaste");
		if (driver.findElements(By.xpath("//div[@class='message info empty']")).size() > 0) {
			System.out.println("No Products available");
			Assert.fail();
		} else {
			productName = Browser.getTextByXpath(Elements_Medicines.PLP_firstproductName_nonRX);
			PLPprice = Double
					.parseDouble(Browser
							.getTextByXpath("//strong[contains(., '" + productName
									+ "')]/following-sibling::div[@data-role='priceBox']/span/span/span")
							.replaceAll("₹", ""));
			Browser.clickOnTheElementByXpath("//strong[contains(., '" + productName
					+ "')]/following-sibling::div[@class='product-item-inner']/div/div/form/button/span"); // AddtoCart
			Browser.clickOnTheElementByXpath(Elements_Medicines.header_miniCartViewBtn); // viewCartBtn
			productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, PLPprice, qty);
			checkout_orderTotal = ecomPage.checkout_coupon_summary("", "", productSubTotal_item);
			shipping_orderTotal = ecomPage.shippingPage_orderSummary("", "", checkout_orderTotal, "Healthcare product online order COD.");
			ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
			RecipientPage.verify_transactionAmount_PG(shipping_orderTotal);
			RecipientPage.MakePayment_Options(PaymentGateway, PaymentGateway_Type);
			String orderID = ecomPage.order_confirmationPage(productName, checkout_orderTotal, paymode);
			String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
			Assert.assertEquals(orderStatus, orderSuccess_Status);
		}
	}
	
	@AfterMethod(groups = { "MED", "Low" })
	public void LogOut() throws Exception{
		
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
	}

	@AfterClass(groups = { "MED", "Low" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
