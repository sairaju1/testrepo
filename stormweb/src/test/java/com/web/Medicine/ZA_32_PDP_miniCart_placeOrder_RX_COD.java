// @Author: Sagar Sen

package com.web.Medicine;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_32_PDP_miniCart_placeOrder_RX_COD extends LoadProp {
	
	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName, paymode;
	public Double searchListPrice, productSubTotal_item, checkout_orderTotal, miniCart_itemSubTotal, miniCart_subTotal;
	public int qty = 2;
	
	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		paymode = cod;
		productName = productName_rx;
	}
	
	@Test(groups = { "MED", "High" })
	public void PDP_miniCart_RX_COD() throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		searchListPrice = ecomPage.medicineHomePage_search(productName, "");
		ecomPage.PDP_itemQtyInfo(productName, 1, "Add to Cart");
		Thread.sleep(2000);
		miniCart_itemSubTotal = ecomPage.miniCart_qty_itemSubTotal(true, productName, searchListPrice, qty);
		miniCart_subTotal = ecomPage.miniCart_subTotal(false, miniCart_itemSubTotal);
		Browser.clickOnTheElementByXpath(Elements_Medicines.header_miniCartViewBtn); // viewCartBtn
		productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, miniCart_subTotal, 1);
		checkout_orderTotal = ecomPage.checkout_coupon_summary("", "", productSubTotal_item);
		ecomPage.checkout_prescriptionPopUp(false);
		ecomPage.shippingPage_orderSummary("", "", checkout_orderTotal, "Healthcare product online order COD.");
		ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
		String orderID = ecomPage.order_confirmationPage(productName, checkout_orderTotal, paymode);
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, orderSuccess_Status);
	}
	
	
	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}