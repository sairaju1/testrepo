// @Author: Sagar Sen

package com.web.Medicine;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_39_nonPrescriptionItem_placeOrder_RxOtc_COD extends LoadProp {
	
	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productNameRX, productNameOTC, paymode;
	public int qty = 1;
	public Double searchListPriceRX, searchListPriceOTC, cartPriceRX, cartPriceOTC, productSubTotal_item, checkout_orderTotal;
	
	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		paymode = cod;
		productNameRX = productName_rx;
		productNameOTC = productName_nonRX;
	}
	
	@Test(groups = { "MED", "High" })
	public void checkoutWithNonPrescriptionItems_COD () throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		searchListPriceRX = ecomPage.medicineHomePage_search(productNameRX, "addtocart");
		Browser.waitFortheElementXpath(Elements_Medicines.header_miniCartViewBtn); // viewCartBtn
		searchListPriceOTC = ecomPage.medicineHomePage_search(productNameOTC, "addtocart");
		Browser.clickOnTheElementByXpath(Elements_Medicines.header_miniCartViewBtn); // viewCartBtn
		cartPriceRX = ecomPage.checkout_cart_itemQtyInfo(productNameRX, searchListPriceRX, qty);
		cartPriceOTC = ecomPage.checkout_cart_itemQtyInfo(productNameOTC, searchListPriceOTC, qty);
		productSubTotal_item = cartPriceRX + cartPriceOTC;
		checkout_orderTotal = ecomPage.checkout_coupon_summary("", "", productSubTotal_item);
		Browser.clickOnTheElementByXpath("//button[text()='CONTINUE WITH NON-PRESCRIPTION ITEMS']");
		productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productNameOTC, searchListPriceOTC, qty);
		checkout_orderTotal = ecomPage.checkout_coupon_summary("", "", productSubTotal_item);
		ecomPage.shippingPage_orderSummary("", "", checkout_orderTotal, "Healthcare product online order OL.");
		ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
		String orderID = ecomPage.order_confirmationPage(productNameOTC, checkout_orderTotal, paymode);
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, orderSuccess_Status);
	}
	
	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
