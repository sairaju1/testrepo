// @Author: Sagar Sen

package com.web.Medicine;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_14_PDP_BuyNow_placeOrder_MyOrdersView_cancelOrder_RX_COD extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName, paymode;
	public int qty = 2;
	public Double searchListPrice, productSubTotal_item, checkout_orderTotal;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		paymode = cod;
		productName = productName_rx;
	}

	@Test(groups = { "MED", "High" })
	public void myOrders_RX_COD() throws Exception {
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		searchListPrice = ecomPage.medicineHomePage_search(productName, "");
		ecomPage.PDP_itemQtyInfo(productName, qty, "Buy");
		productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, searchListPrice, qty);
		checkout_orderTotal = ecomPage.checkout_coupon_summary("", "", productSubTotal_item);
		ecomPage.checkout_prescriptionPopUp(false);
		ecomPage.shippingPage_orderSummary("", "", checkout_orderTotal, "Healthcare product online order OL.");
		ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
		String orderID = ecomPage.order_confirmationPage(productName, checkout_orderTotal, paymode);
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, orderSuccess_Status);
		Browser.openUrl(EnvironmentURL);

		// My orders
		RecipientPage.header_UserDetails_SelectMenu("My Orders", "");
		Browser.waitFortheElementXpath("//h3[text()='My Orders']");
		Browser.waitFortheElementXpath("//td[text()='##" + orderID + "']"); // OrderNo
		Browser.waitFortheElementXpath("//td[text()='##" + orderID + "']/preceding-sibling::td[text()='Order Placed']"); // OrderStatus
		Browser.waitFortheElementXpath(
				"//td[text()='##" + orderID + "']/preceding-sibling::td[text()='₹ " + Browser.decimalFormat(checkout_orderTotal) + "']"); // orderTotal
		Browser.clickOnTheElementByXpath(
				"//td[text()='##" + orderID + "']/following-sibling::td/a[@class='view-details collapsed']"); // viewBtn
		Browser.waitFortheElementXpath("//table/tr[contains(., '##" + orderID
				+ "')]/following-sibling::tr[contains(., 'Medicine / Product Name')]"); // viewDetailsHeader
		Browser.waitFortheElementXpath("//table/tr[contains(., '##" + orderID
				+ "')]/following-sibling::tr/td/div/div/div/table/tbody/tr/th[text()='" + productName + "']"); // viewProductName
		Browser.waitFortheElementXpath("//table/tr[contains(., '##" + orderID
				+ "')]/following-sibling::tr/td/div/div/div/table/tbody/tr/td[text()='" + qty + "']"); // viewProductQTY
		Browser.waitFortheElementXpath("//table/tr[contains(., '##" + orderID
				+ "')]/following-sibling::tr/td/div/div/div/table/tbody/tr/td[text()='₹" + Browser.decimalFormat(searchListPrice) + "']"); // viewProductPrice
		Browser.waitFortheElementXpath("//table/tr[contains(., '##" + orderID
				+ "')]/following-sibling::tr/td/div/div/div/div[2]/p/span[text()='Payment Mode:']/following-sibling::span[text()=' Cash On Delivery']"); // viewPayMode
		Browser.waitFortheElementXpath("//table/tr[contains(., '##" + orderID
				+ "')]/following-sibling::tr/td/div/div/div/div[2]/p/span[text()='Delivery charges:']/following-sibling::span[text()=' ₹ 49.00']"); // shippingCharge

		// Cancel order
		Browser.clickOnTheElementByXpath(
				"//table/tr[contains(., '##" + orderID + "')]/td/a[text()='Cancel']");
		// Order cancel popup
		
		Browser.waitFortheElementXpath("//h5[text()='Order Cancel Request']"); // popupHeader
		Browser.clickOnTheElementByXpath("//label[text()='MRP Change']"); // selectReason
		Browser.clickOnTheElementByXpath("//button[text()='OK']");
		Browser.waitFortheElementXpath("//h4[text()='Order Cancelled Successfully!']");
		Browser.clickOnTheElementByXpath("//button[text()='Close']");
		Browser.waitFortheElementXpath("//td[text()='##" + orderID + "']/preceding-sibling::td[text()='Cancelled']"); // OrderStatus
		Thread.sleep(3000);
		String orderStatus_cancel = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus_cancel, orderCancel_Status);
		
	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
