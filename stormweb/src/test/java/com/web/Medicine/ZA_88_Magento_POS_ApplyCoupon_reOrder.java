// @Author: Sagar Sen

package com.web.Medicine;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.TestUtils;

public class ZA_88_Magento_POS_ApplyCoupon_reOrder extends LoadProp {
	
	public TestUtils Browser;
	public MedicinePage ecomPage;
	public String customer_number, customer_address, orderId, orderStatus, promoCode;
	
	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		Browser.openUrl(magentoAdminURL);
		ecomPage.Magento_Login(Magento_user, Magento_pwd);
		customer_number = user_id;
		promoCode = Med_Promo;
	}
	
	@Test(groups = { "MED", "High" }, priority = 1)
	public void placeOrder_POS() throws Exception {
		ecomPage.Magento_sideMenu_click(false, "Quick Order", "Quick Order"); // menuMethod
		Browser.waitFortheElementXpath("//h1[text()='Welcome to Admin Quick Order']"); // quickOrderHeading
		customer_address = Browser.sqlDB_selectFromQuery("customer_address_entity",
				"telephone='" + customer_number + "'", "entity_id"); // getAddressEntityId
		ecomPage.Magento_POS_customerdetails(customer_number, customer_address);
		Thread.sleep(2000);
		String alertPinCode = driver.switchTo().alert().getText();
		Assert.assertTrue(alertPinCode.contains("is serviceable"));
		driver.switchTo().alert().accept();
		Thread.sleep(2000);
		ecomPage.Magento_POS_productDetails(productName_rx, "1");
		Browser.enterTextByName("coupon_code", promoCode); // coupon
		Browser.clickOnTheElementByXpath("//input[@value='Place Order']"); // PlaceOrderBtnClick
		Browser.waitFortheElementXpath("//div[@data-ui-id='messages-message-success']"); // orderPlaceMessage
		String getOrder = Browser.getTextByXpath("//div[@data-ui-id='messages-message-success']");
		String[] getOrderSplit = getOrder.split(": ");
		String[] getOrderSplit1 = getOrderSplit[1].split(" ");
		orderId = getOrderSplit1[0];
		String[] orderStatusSplit = ecomPage.getOrderStatus_postApi(orderId).split("\"status\":");
		orderStatus = orderStatusSplit[1].replaceAll("\"", "").replaceAll("}", "");
	}
	
	
	@Test(groups = { "MED", "High" }, dependsOnMethods = "placeOrder_POS")
	public void orderStatus_change() throws Exception {
		ecomPage.Magento_QuickOrder_OrderStatus(orderId, orderStatus);
		ecomPage.Magento_QuickOrder_OrderStatus_table_Action(orderId, "Re-Order");
		ecomPage.Magento_QuickOrder_ReOrder_PaymentMethod("paylater");
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath("(//span[text()='Submit Order'])[2]"); // submitBtn
		Browser.waitFortheElementXpath("//div[@data-ui-id='messages-message-success']"); // orderPlaceMessage
	}
	
	
	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
