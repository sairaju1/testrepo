// @Author: Sagar Sen

package com.web.Medicine;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_5_shopByBrands_placeOrder_COD extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName, paymode;
	public Double PLPprice, productSubTotal_item, checkout_orderTotal;
	public int qty = 1;

	@BeforeClass(groups = { "MED", "Medium" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		paymode = cod;
	}

	@Test(groups = { "MED", "Medium" })
	public void shopBrands_COD() throws Exception {

		Browser.scrollbyxpath("//h3[text()='Top Healthcare Products']"); // scroll to brands
		Thread.sleep(3000);
		int b = 0;
		while (b == 0) {
			Browser.clickOnTheElementByXpath(
					"//div[@class='container position-relative' and contains(., 'Shop by Brand ')]/div[@class='row']/div/div[@id='brands-slider']/div/div[@class='owl-controls']/div/div[@class='owl-next']/em");
			if (driver.findElement(By.xpath("(//img[@alt='" + shopBrands + "'])[2]")).isDisplayed()) {
				b = 1;
			} else {
				continue;
			}
		}
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath("(//img[@alt='" + shopBrands + "'])[2]");
		Browser.waitFortheElementXpath("//div[@class='brand-image']/img[@alt='" + shopBrands + "']");
		productName = Browser.getTextByXpath(Elements_Medicines.PLP_firstproductName_nonRX);
		PLPprice = Double
				.parseDouble(Browser
						.getTextByXpath("//strong[contains(., '" + productName
								+ "')]/following-sibling::div[@data-role='priceBox']/span/span/span")
						.replaceAll("₹", ""));
		Browser.clickOnTheElementByXpath("//strong[contains(., '" + productName
				+ "')]/following-sibling::div[@class='product-item-inner']/div/div/form/button/span"); // AddtoCart
		Browser.clickOnTheElementByXpath(Elements_Medicines.header_miniCartViewBtn); // viewCartBtn
		productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, PLPprice, qty);
		checkout_orderTotal = ecomPage.checkout_coupon_summary("", "", productSubTotal_item);
		ecomPage.shippingPage_orderSummary("", "", checkout_orderTotal, "Healthcare product online order COD.");
		ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
		String orderID = ecomPage.order_confirmationPage(productName, checkout_orderTotal, paymode);
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, orderSuccess_Status);
	}

	@AfterClass(groups = { "MED", "Medium" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
