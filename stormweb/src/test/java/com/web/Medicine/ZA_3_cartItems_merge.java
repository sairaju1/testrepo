// @Author: Sagar Sen

package com.web.Medicine;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_3_cartItems_merge extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName, paymode, ExistingproductName;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		paymode = cod;
		productName = productName_rx;
	}

	@Test(groups = { "MED",
			"High" }, priority = 1, description = "This case adds a product to cart with user logged in and ends session.")
	public void addCartP1_login() throws Exception {
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		Browser.waitFortheElementXpath("//a[contains(., 'Logged in as')]");
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		ecomPage.medicineTopMenu_click("Healthcare Products", "");
		Browser.waitFortheElementXpath("//i[@class='zoylo-icon doorstep-icon']"); // MedicinePageIcon
		ExistingproductName = Browser.getTextByXpath(Elements_Medicines.PLP_firstproductName_nonRX);
		ecomPage.medicine_PLPpage_getProductPrice_addToCartBtn_click(ExistingproductName);
		Browser.clickOnTheElementByXpath(Elements_Medicines.PLP_shoppingCartLinkText);
		Browser.waitFortheElementXpath("//td[@data-th='Item' and contains(., '" + ExistingproductName + "')]");
		Browser.openUrl(EnvironmentURL);
		RecipientPage.Recipient_Logout();
	}

	@Test(groups = { "MED",
			"High" }, priority = 2, description = "This case adds a product to cart as guest user later logs in to check merge.")
	public void addCartP2_guest_merge() throws Exception {
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		ecomPage.medicineHomePage_search(productName, "addtocart");
		Browser.clickOnTheElementByXpath(Elements_Medicines.header_miniCartViewBtn); // viewCartBtn
		Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_Button);
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		Browser.waitFortheElementXpath("(//td[@data-th='Item'])[1]"); // product box
		int p1 = driver.findElements(By.xpath("//td[@data-th='Item' and contains(., '" + ExistingproductName + "')]"))
				.size();
		int p2 = driver.findElements(By.xpath("//td[@data-th='Item' and contains(., '" + productName + "')]")).size();
		if (p1 == 0 && p2 == 0) {
			System.out.println("Merge fail");
			Assert.fail();
		} else {
			System.out.println("Products merged..");
		}
	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}
}
