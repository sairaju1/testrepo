// @Author: Sagar Sen

package com.web.Medicine.removedCases;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_4_MyOrders_cancelVerify_orderConfirmed_COD extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName, paymode;
	public int qty = 1;
	public Double searchListPrice, productSubTotal_item, checkout_orderTotal;

	@BeforeClass(groups = { "MED", "Medium" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		paymode = cod;
		productName = productName_rx;
	}

	@Test(groups = { "MED", "Medium" })
	public void orderConfirmed_cancelVerify() throws Exception {
		Browser.openUrl(EnvironmentURL + "/medicines/");
		ecomPage.medicinePage_loadWait();
		searchListPrice = ecomPage.medicineHomePage_search(productName, "");
		ecomPage.PDP_itemQtyInfo(productName, qty, "Buy");
		productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, searchListPrice, qty);
		checkout_orderTotal = ecomPage.checkout_coupon_summary("", "", productSubTotal_item);
		ecomPage.checkout_prescriptionPopUp(false);
		ecomPage.shippingPage_orderSummary("", "", checkout_orderTotal, "Healthcare product online order OL.");
		ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
		String orderID = ecomPage.order_confirmationPage(productName, checkout_orderTotal, paymode);
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, orderSuccess_Status);
		Browser.openUrl(EnvironmentURL);

		String[] orderTrim = orderID.split("00000");
		System.out.println(orderTrim[1]);

		Browser.sqlDB_updateFieldQuery("sales_order", "entity_id='" + orderTrim[1] + "'", "status='order_confirmed'");
		
		// My orders
		RecipientPage.header_UserDetails_SelectMenu("My Orders", "");
		Browser.waitFortheElementXpath("//h3[text()='My Orders']");
		Browser.waitFortheElementXpath("//td[text()='##" + orderID + "']"); // OrderNo
		Browser.waitFortheElementXpath("//td[text()='##" + orderID + "']/preceding-sibling::td[text()='Order Placed']"); // OrderStatus
		if(driver.findElements(By.xpath("//table/tr[contains(., '##" + orderID + "')]/td/a[text()='Cancel']")).size() == 0) {
			System.out.println("Cannot cancel confirmed order.");
		} else {
			Assert.fail();
		}
	}

	@AfterClass(groups = { "MED", "Medium" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
