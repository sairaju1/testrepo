// @Author: Sagar Sen

package com.web.Medicine.removedCases;

import java.util.ArrayList;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_36_MyOrders_ReOrder_COD extends LoadProp {

	public MedicinePage ecomPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String productName, paymode;
	public int qty;
	public Double productPrice, productSubTotal_item, checkout_orderTotal;

	@BeforeClass(groups = { "MED", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		Browser.sqlDB_deleteQuery("quote", "customer_email='" + user_mail + "'");
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		paymode = cod;
	}

	@Test(groups = { "MED", "High" })
	public void reOrder() throws Exception {
		RecipientPage.header_UserDetails_SelectMenu("My Orders", "");
		Browser.waitFortheElementXpath("//h3[text()='My Orders']");
		Browser.clickOnTheElementByXpath("(//tr/td/following-sibling::td/a[@class='view-details collapsed'])[1]"); // firstViewBtn
		productName = Browser.getTextByXpath("(//tbody/tr/th)[1]"); // firstProductName
//		productName = Browser.stringTo_titleCase(productName);
		productPrice = Double.parseDouble(
				Browser.getTextByXpath("(//tr/th[text()='" + productName + "'])[1]/following-sibling::td[2]")
						.replaceAll("₹", "")); // price
		qty = Integer.parseInt(
				Browser.getTextByXpath("(//tr/th[text()='" + productName + "'])[1]/following-sibling::td[1]")); // quantiry
		Browser.scrollbyxpath("(//table/tr/td/a[text()='Reorder'])[1]");
		Browser.clickOnTheElementByXpath("(//table/tr/td/a[text()='Reorder'])[1]"); // firstReorderBtn
		Thread.sleep(5000);
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		productSubTotal_item = ecomPage.checkout_cart_itemQtyInfo(productName, productPrice, qty);
		checkout_orderTotal = ecomPage.checkout_coupon_summary("", "", productSubTotal_item);
		Thread.sleep(3000);
		ecomPage.checkout_prescriptionPopUp(false);
		ecomPage.shippingPage_orderSummary("", "", checkout_orderTotal, "Healthcare product online order OL.");
		ecomPage.shippingPage_paymentMethod_placeOrderClick(paymode);
		String orderID = ecomPage.order_confirmationPage(productName, checkout_orderTotal, paymode);
		String orderStatus = ecomPage.getOrderStatus_postApi(orderID.replaceAll("[^0-9]", ""));
		Assert.assertEquals(orderStatus, orderSuccess_Status);
	}

	@AfterClass(groups = { "MED", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
