// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZA_111_Affiliates_LocationBasedPricing extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public String firstName, testName, location_1, location_2, location_1_price, location_2_price, number, lastName,
			email, gender, age, address, actualCost_location2, actualCost_location1, aptSlotTime, date, Landmark,
			bookingDetails_listPrice, payMode = "CASH";
	public Person p;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		p = Browser.personData();
		testName = test_Name;
		firstName = p.getFirstName();
		lastName = p.getLastName();
		location_1 = operatingCity_1;
		location_2 = operatingCity_2;
		location_1_price = "1" + Browser.generateRandomNumber(2);
		location_2_price = "2" + Browser.generateRandomNumber(2);
		number = "9" + Browser.generateRandomNumber(9);
		email = p.getEmail();
		gender = "Male";
		age = Integer.toString(p.getAge());
		date = Browser.getModifiedDate(1);
		address = p.getAddress().toString();
		Landmark = "Z" + Browser.generateRandomString(5).toLowerCase();
	}

	@Test(groups = { "ADMINDC", "High" }, priority = 1)
	public void servicePrice_basedLocation() throws Exception {
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		AdminPage.AdminMenu_subMenu("Zoylo Lab Management", "False", "Location Based Service Price");
		AdminPage.locationBased_select_city_servicetype(location_1, "Test");
		AdminPage.locationBased_search(testName);
		AdminPage.locationBased_editFinalPrice(testName, location_1_price);
		driver.navigate().refresh();
		AdminPage.locationBased_select_city_servicetype(location_2, "Test");
		AdminPage.locationBased_search(testName);
		AdminPage.locationBased_editFinalPrice(testName, location_2_price);
	}

	@Test(groups = { "ADMINDC", "High" }, priority = 2)
	public void affiliates_locationPriceCheck() throws Exception {
		Browser.openUrl(uat_affiliates);
		AdminPage.affiliates_login(adminuser_id, adminuser_pw);
		AdminPage.affiliates_customerInfo(number, firstName, lastName, email, age, gender, location_2, date, address,
				Landmark);
		AdminPage.affiliates_custInfo_Save();
		actualCost_location2 = AdminPage.affiliates_addService(testName);
		Assert.assertEquals(actualCost_location2, location_2_price);
		driver.navigate().refresh();
		aptSlotTime = AdminPage.affiliates_customerInfo(number, firstName, lastName, email, age, gender, location_1,
				date, address, Landmark);
		AdminPage.affiliates_custInfo_Save();
		actualCost_location1 = AdminPage.affiliates_addService(testName);
		Assert.assertEquals(actualCost_location1, location_1_price);
		AdminPage.affiliates_bookingPatientList(testName, firstName);
		bookingDetails_listPrice = AdminPage.affiliates_bookingDetails_listPrice(testName);
		Assert.assertEquals(bookingDetails_listPrice, location_1_price);
		AdminPage.affiliates_selectPayMode_ConfirmBooking(payMode);
		Browser.waitFortheID("custMobile");

		// Verify amount and time in amdin
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.lab_addAppointmentList_navigate();
		Thread.sleep(1000);
		AdminPage.lab_addAppointment_searchList("Mobile Number", number);

		Browser.waitFortheElementXpath(
				"//tr/td[text()='+91" + number + "']/following-sibling::td[contains(., '" + aptSlotTime.trim() + "')]");// VerifySlot
		String adminListAmt = Browser.getTextByXpath("//tr/td[text()='+91" + number + "']/following-sibling::td[7]");
		Assert.assertEquals(adminListAmt, location_1_price);
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterEmail", email);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", email);
		Browser.quit();
	}

}
