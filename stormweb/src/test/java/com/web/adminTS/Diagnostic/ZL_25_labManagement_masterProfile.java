// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZL_25_labManagement_masterProfile extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public String profie_testName, profileName, profileCode;
	int profileCost, finalProfileCost;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		Elements_Admin.Admin_PageProperties();
		AdminPage.AdminMenu_subMenu("Zoylo Lab Management", "False", "Zoylo Lab Master Tests");
		profileName = "P" + Browser.generateRandomString(5).toLowerCase();
		profileCode = Browser.generateRandomString(5).toUpperCase();
		profileCost = 500;
		finalProfileCost = 450;
	}

	@Test(groups = { "ADMINDC", "High" }, priority = 1)
	public void masterProfile_addEdit() throws Exception {
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterTest_listingHeader);
		Thread.sleep(2000);
		profie_testName = Browser.getTextByXpath(Elements_Admin.lab_masterTestList_firstTestName);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_subMenu);
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterProfile_listHeader);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_addBtn);
		if (driver.findElements(By.xpath(Elements_Admin.lab_masterProfile_addHeader)).size() == 0) {
			Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_addBtn);
		}
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterProfile_addHeader);
		Browser.enterTextByXpath(Elements_Admin.lab_masterProfile_profileName, profileName);
		Browser.enterTextByXpath(Elements_Admin.lab_masterProfile_profileCode, profileCode);
		Browser.enterTextByXpath(Elements_Admin.lab_masterProfile_lisCode, "LIS_" + profileCode);
		Browser.enterTextByXpath(Elements_Admin.lab_masterProfile_defaultTat, "1");
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_activeChkBox);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_nextBtn);
		Browser.enterTextByXpath(Elements_Admin.lab_masterProfile_listPrice, Integer.toString(profileCost));
		Thread.sleep(1000);
		Browser.enterTextByXpath(Elements_Admin.lab_masterProfile_finalPrice, Integer.toString(finalProfileCost));
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_nextBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_testDrp);
		Browser.enterTextByXpath(Elements_Admin.lab_masterProfile_testDrpInput, profie_testName);
		Browser.clickOnTheElementByXpath(
				"//div[@class='list__tile__title' and contains(., '" + profie_testName + "')]");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_testAddBtn);
		Thread.sleep(1000);
		String listTestname = Browser.getTextByXpath("//tr/td[2]");
		Assert.assertEquals(listTestname, profie_testName);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_nextBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_saveBtn);
		Thread.sleep(2000);
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterProfile_listHeader);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_searchDrp);
		Browser.enterTextByXpath(Elements_Admin.lab_masterProfile_searchInput, profileName);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + profileName + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_searchBtn);
		Thread.sleep(2000);
		Browser.waitFortheElementXpath("(//tr/td[contains(., '" + profileCode + "')])[1]");
		String profilePriceFinal_Listing = Browser
				.getTextByXpath("(//tr/td[contains(., '" + profileCode + "')])[1]/following-sibling::td[1]");

		Assert.assertEquals(Integer.parseInt(profilePriceFinal_Listing), finalProfileCost);
		
		// EDIT
		Browser.clickOnTheElementByXpath("(//tr/td[contains(., '" + profileCode + "')])[1]/following-sibling::td[3]/i");
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_activeChkBox);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_nextBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_nextBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_nextBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_saveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterProfile_listHeader);
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloLabsMasterProfile", "profileCode", profileCode);
		Browser.quit();
	}
}