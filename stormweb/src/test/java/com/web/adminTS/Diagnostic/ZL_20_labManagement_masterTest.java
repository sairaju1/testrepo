// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZL_20_labManagement_masterTest extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public String testName, testCode;
	int testCost, finalTestCost;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		Elements_Admin.Admin_PageProperties();
		AdminPage.AdminMenu_subMenu("Zoylo Lab Management", "False", "Zoylo Lab Master Tests");
		testName = "T" + Browser.generateRandomString(6).toLowerCase();
		testCode = Browser.generateRandomString(6).toUpperCase();
		testCost = 100;
		finalTestCost = 95;
	}

	@Test(groups = { "ADMINDC", "High" }, priority = 1)
	public void masterTest_AddEdit() throws Exception {
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterTest_listingHeader);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterTest_addBtn);

		if (driver.findElements(By.xpath(Elements_Admin.lab_masterTest_addHeader)).size() == 0) {
			Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterTest_addBtn);
		}

		Browser.waitFortheElementXpath(Elements_Admin.lab_masterTest_addHeader);
		// Primary fields **
		Browser.enterTextByXpath(Elements_Admin.lab_masterTest_testName, testName);
		Browser.enterTextByXpath(Elements_Admin.lab_masterTest_testCode, testCode);
		Browser.enterTextByXpath(Elements_Admin.lab_masterTest_lisCode, "LIS_" + testCode);
		Browser.enterTextByXpath(Elements_Admin.lab_masterTest_tat, "1");
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterTest_bookingCheckBox);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterTest_fastingCheckBox);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterTest_nextBtn);
		// ListPrice **
		Browser.enterTextByXpath(Elements_Admin.lab_masterTest_listPrice, Integer.toString(testCost));
		Thread.sleep(1000);
		Browser.enterTextByXpath(Elements_Admin.lab_masterTest_finalPrice, Integer.toString(finalTestCost));
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//a[@href='#listprice']/b");
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterTest_nextBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterTest_saveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterTest_listingHeader);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterTestDrp);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterTestDrp);
		Browser.enterTextByXpath(Elements_Admin.lab_masterTest_searchInput, testName);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + testName + "')]");
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterTest_searchBtn);
		Thread.sleep(1000);
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + testCode + "')]");
		String finalListingPrice_Test = Browser.getTextByXpath(Elements_Admin.lab_masterTest_listingFinalPrice);
		Assert.assertEquals(finalListingPrice_Test, Integer.toString(finalTestCost));

		// EDIT test
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + testCode + "')]/following-sibling::td[3]/i"); // EditBtn
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterTest_addHeader);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterTest_bookingCheckBox);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterTest_nextBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterTest_nextBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterTest_saveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterTest_listingHeader);
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloLabsMasterTest", "testCode", testCode);
		Browser.quit();
	}

}