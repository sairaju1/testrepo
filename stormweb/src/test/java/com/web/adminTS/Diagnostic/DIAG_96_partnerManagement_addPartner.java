// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.company.Company;
import io.codearte.jfairy.producer.person.Address;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class DIAG_96_partnerManagement_addPartner extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public Person p;
	public Company c;
	String partnerName, contactName, contactPhone, contactEmail, partnerCode;
	Address address;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		p = Browser.personData();
		c = Browser.companyData();
		partnerName = c.getName();
		contactName = p.getFullName();
		contactPhone = "9" + Browser.generateRandomNumber(9);
		contactEmail = p.getEmail();
		address = p.getAddress();
		partnerCode = Browser.generateRandomString(5).toUpperCase();
	}

	@Test(groups = { "ADMINDC", "High" })
	public void addPartner() throws Exception {
		AdminPage.AdminMenu_subMenu("Zoylo Partner Management", "False", "Zoylo Partner Details");
		Browser.clickOnTheElementByXpath(Elements_Admin.labPartner_add); // addBtn
		Browser.waitFortheElementXpath(Elements_Admin.labPartner_addHeader); // addHeading
		Browser.enterTextByXpath(Elements_Admin.labPartner_name, partnerName);
		Browser.enterTextByXpath(Elements_Admin.labPartner_code, partnerCode);
		Browser.enterTextByXpath(Elements_Admin.labPartner_contactName, contactName);
		Browser.enterTextByXpath(Elements_Admin.labPartner_contactPhone, contactPhone);
		Browser.enterTextByXpath(Elements_Admin.labPartner_email, contactEmail);
		Browser.clickOnTheElementByXpath(Elements_Admin.labPartner_Branch_City); // selectCity
		Browser.enterTextByXpath(Elements_Admin.labPartner_Branch_CityInput ,operatingCity_2); // searchSuggestion
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='" + operatingCity_2 + "']");
		Browser.enterTextByXpath(Elements_Admin.labPartner_Branch_Address1 , address.toString());
		Browser.clickOnTheElementByXpath(
				"//label[text()='Payment Mode']/following-sibling::div/div[@class='input-group__selections']"); // payMode
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='Debit Card']");
		Browser.enterTextByXpath(Elements_Admin.labPartner_Branch_Pin, Browser.generateRandomNumber(6));
		Browser.clickOnTheElementByXpath(Elements_Admin.operatingCities_saveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.labPartner_add);

		// EDIT PARTNER
		Browser.clickOnTheElementByXpath(
				Elements_Admin.labPartner_Branch_PartnerDrp); // searchDrp
		Browser.enterTextByXpath(
				Elements_Admin.labPartner_Branch_PartnerDrpInput,
				partnerName);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='" + partnerName + "']"); // DrpSelect
		Browser.waitFortheElementXpath("//tr/td[text()='" + partnerName + "']"); // tablePame
		Browser.clickOnTheElementByXpath("//tr/td[text()='" + partnerName + "']/following-sibling::td/i"); // editBtn
		Browser.clickOnTheElementByXpath(Elements_Admin.operatingCities_saveBtn);
		Browser.waitFortheElementXpath("//div[text()=' Add Partner']");
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloPartnerDetails", "partnerCode", partnerCode);
		Browser.quit();
	}

}
