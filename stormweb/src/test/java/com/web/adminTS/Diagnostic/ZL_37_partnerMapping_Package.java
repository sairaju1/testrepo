// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;
import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZL_37_partnerMapping_Package extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public String partnerName, packageName, partnerBookingcode_Package;
	public int partnerPrice_package, partnerDiscount;
	
	@BeforeClass(groups = { "Admin", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		partnerName = partner_name;
		partnerPrice_package = 1000;
		partnerDiscount = 800;
		partnerBookingcode_Package = Browser.generateRandomString(5).toUpperCase();
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		AdminPage.AdminMenu_subMenu("Zoylo Lab Management", "False", "Lab Master Packages");
	}
	
	@Test(groups = { "Admin", "High" })
	public void partnerMapping_packageAddEdit() throws Exception {
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterPackage_listHeader);
		Thread.sleep(2000);
		packageName = Browser.getTextByXpath(Elements_Admin.lab_masterProfileList_firstProfileName);
		AdminPage.AdminMenu_subMenu("Zoylo Partner Mapping", "False", "Zoylo Partner Mapping Details");
		Browser.remove_Document(Environment, "zoyloPartnerService", "zoyloServiceData.zoyloServiceName", packageName);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_partnerMapping_addBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_partnerMapping_packageTab);
		
		//Package Add
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("(" + Elements_Admin.lab_partnerMapping_selectPartner + ")[2]");
		Thread.sleep(1000);
		Browser.enterTextByXpath("(" + Elements_Admin.lab_partnerMapping_selectPartnerInput + ")[2]", partnerName);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + partnerName + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_partnerMapping_selectPackageDrp);
		Thread.sleep(1000);
		Browser.enterTextByXpath(Elements_Admin.lab_partnerMapping_selectPackageInput, packageName);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + packageName + "')]");
		Browser.enterTextByXpath("(" + Elements_Admin.lab_partnerMapping_BookingCode + ")[2]", partnerBookingcode_Package);
		Browser.enterTextByXpath("(" + Elements_Admin.lab_partnerMapping_price + ")[2]",
				Integer.toString(partnerPrice_package));
		Thread.sleep(1000);
		Browser.enterTextByXpath("(" + Elements_Admin.lab_partnerMapping_finalPrice + ")[2]",
				Integer.toString(partnerDiscount));
		Thread.sleep(1000);
		Browser.enterTextByXpath("(" + Elements_Admin.lab_partnerMapping_lisCode + ")[1]", "LIS_" + partnerBookingcode_Package);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_partnerMapping_packageSaveBtn);
		
		System.out.println("**************************** Package Mapping done ****************************");
		
		// Edit Check
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_partnerMapping_subMenu);
		Browser.waitFortheElementXpath(Elements_Admin.lab_partnerMapping_addBtn);
		Browser.enterTextByXpath(Elements_Admin.lab_partnerMapping_search, partnerBookingcode_Package);
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + partnerBookingcode_Package + "')]");
		
		// EDIT Test mapping
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + partnerBookingcode_Package + "')]/following-sibling::td[4]/i");
		if(driver.findElements(By.xpath(Elements_Admin.lab_partnerMapping_editSaveBtn)).size() == 0) {
			Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + partnerBookingcode_Package + "')]/following-sibling::td[4]/i");
		}
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_partnerMapping_editSaveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.lab_partnerMapping_editSaveMsg);
	}
	
	@AfterClass(groups = { "Admin", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}
	
}