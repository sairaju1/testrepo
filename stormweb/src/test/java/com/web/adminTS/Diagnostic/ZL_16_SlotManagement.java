// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZL_16_SlotManagement extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public String cityCode, stateName, CityName, zoneName, capacity, pin;
	public int numberOfslots;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		Elements_Admin.Admin_PageProperties();
		cityCode = Browser.generateRandomString(4).toUpperCase();
		zoneName = Browser.generateRandomString(5).toUpperCase();
		stateName = "Puducherry";
		CityName = "Mudaliarpet";
		capacity = "5";
		numberOfslots = 3;
		pin = Browser.generateRandomNumber(6);
		AdminPage.AdminMenu_subMenu("Zoylo Slot Management", "False", "Zoylo Operating Cities");
		Browser.remove_Document(Environment, "zoyloLabsOperatingCities", "cityData.cityName", CityName);
	}

	@Test(groups = { "ADMINDC", "High" }, priority = 1)
	public void lab_addCity() throws Exception {
		Browser.waitFortheElementXpath(Elements_Admin.operatingCities_Header);
		Browser.clickOnTheElementByXpath(Elements_Admin.operatingCities_AddBtn);
		if (driver.findElements(By.xpath(Elements_Admin.operatingCities_popUpHeader)).size() == 0) {
			Browser.clickOnTheElementByXpath(Elements_Admin.operatingCities_AddBtn);
		}
		// Pop Up
		Browser.waitFortheElementXpath(Elements_Admin.operatingCities_popUpHeader);
		Browser.clickOnTheElementByXpath(Elements_Admin.operatingCities_stateDrp);
		Browser.enterTextByXpath(Elements_Admin.operatingCities_stateInput, stateName);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + stateName + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.operatingCities_cityDrp);
		Browser.enterTextByXpath(Elements_Admin.operatingCities_cityInput, CityName);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + CityName + "')]");
		Browser.enterTextByXpath("//input[@data-vv-name='cityCode']", cityCode);
		Browser.clickOnTheElementByXpath(Elements_Admin.operatingCities_activeBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.operatingCities_saveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.operatingCities_Header);
		Thread.sleep(2000);
		// Search added city in list
		Browser.clickOnTheElementByXpath(Elements_Admin.operatingCities_searchState);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.operatingCities_searchState);
		Thread.sleep(2000);
		Browser.enterTextByXpath(Elements_Admin.operatingCities_searchStateInput, stateName);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + stateName + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.operatingCities_searchSubmitBtn);
		Thread.sleep(2000);
		String tableCityCode = Browser.getTextByXpath(Elements_Admin.operatingCities_searchResult_cityCode)
				.toUpperCase();
		Assert.assertEquals(tableCityCode, cityCode);
	}

	@Test(groups = { "ADMINDC", "High" }, priority = 2)
	public void lab_addZoneSlots() throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.slotCapacity_subMenu);
		Browser.waitFortheElementXpath(Elements_Admin.zone_Header);
		Browser.clickOnTheElementByXpath(Elements_Admin.zone_AddBtn);
		if (driver.findElements(By.xpath(Elements_Admin.zone_popUpHeader)).size() == 0) {
			Browser.clickOnTheElementByXpath(Elements_Admin.zone_AddBtn);
		}
		Browser.waitFortheElementXpath(Elements_Admin.zone_popUpHeader);
		Browser.clickOnTheElementByXpath(Elements_Admin.zone_cityDrp);
		Browser.enterTextByXpath(Elements_Admin.zone_cityInput, CityName);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + CityName + "')]");
		Browser.enterTextByXpath(Elements_Admin.zone_zoneName, zoneName);
		Browser.enterTextByXpath(Elements_Admin.zone_zoneCode, zoneName);
		Browser.clickOnTheElementByXpath(Elements_Admin.zone_SaveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.zone_Header);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.zone_searchCity);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.zone_searchCity);
		Thread.sleep(2000);
		Browser.enterTextByXpath(Elements_Admin.zone_searchInput, CityName);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + CityName + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.zone_searchSubmitBtn);
		Thread.sleep(2000);
		String zoneCode = Browser.getTextByXpath(Elements_Admin.zone_searchResult_zoneCode).toUpperCase();
		Assert.assertEquals(zoneCode, zoneName);
		Browser.clickOnTheElementByXpath("//tbody/tr/td[contains(., '" + zoneName + "')]/following-sibling::td/i");
		Browser.waitFortheElementXpath(Elements_Admin.zone_slotTab);
		Browser.enterTextByXpath(Elements_Admin.zone_capacityInput, capacity);
		Browser.clickOnTheElementByXpath(Elements_Admin.zone_capacitySubmitBtn);
		Thread.sleep(500);
		for (int i = 1; i <= numberOfslots; i++) {
			Browser.clickOnTheElementByXpath(Elements_Admin.zone_selectSlotDrp);
			Browser.clickOnTheElementByXpath("(" + Elements_Admin.zone_slotsList + ")[" + i + "]");
			Browser.clickOnTheElementByXpath("(" + Elements_Admin.zone_plusBtn + ")[1]");
			Thread.sleep(1000);
		}
		Thread.sleep(2000);
		int addedSlot = driver.findElements(By.xpath(Elements_Admin.zone_slotListingCount)).size();
		Assert.assertEquals(addedSlot, numberOfslots);
		Browser.clickOnTheElementByXpath(Elements_Admin.zone_pinCodeTab); // Pincode tab
		Browser.enterTextByID(Elements_Admin.zone_pinInput, pin);
		Browser.clickOnTheElementByXpath("(" + Elements_Admin.zone_plusBtn + ")[2]");
		Thread.sleep(1000);
		String tablePincode = Browser.getTextByXpath(Elements_Admin.zone_searchResult_pinCode);
		Assert.assertEquals(tablePincode, pin);
		Browser.clickOnTheElementByXpath("(" + Elements_Admin.zone_saveBtn + ")[2]");
		Browser.waitFortheElementXpath(Elements_Admin.zone_Header);
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloLabsOperatingCities", "cityCode", cityCode);
		Browser.remove_Document(Environment, "zoyloLabsCityZones", "zoneCode", zoneName);
		Browser.quit();
	}

}