// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZL_39_Appointment_addAppointment extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public String firstName, number, email, pinCode, patient_FirstName, gender, age, city, payMode, testName,
			profileName, packageName;
	public Person p;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		p = Browser.personData();
		firstName = p.getFirstName();
		patient_FirstName = "P" + Browser.generateRandomString(5).toLowerCase();
		number = "9" + Browser.generateRandomNumber(9);
		email = p.getEmail();
		gender = "Male";
		age = "30";
		city = operatingCity_1;
		payMode = "CASH";
		testName = test_Name;
		profileName = profile_Name;
		packageName = package_Name;
	}

	@Test(groups = { "ADMINDC", "High" })
	public void zoyloLab_addAppointment() throws Exception {

//		String zoneToken = Browser.getLoginToken(Environment, "+91" + adminuser_id, adminuser_pw);

		// Add Appointment segment
		AdminPage.lab_addAppointmentList_navigate();
		AdminPage.lab_addAppointmentButton_click();
		pinCode = AdminPage.lab_addAppointment_customerInfo(firstName, number, email, gender, age, city);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_nextBtn);

		// ***************** Service Info *****************
		Browser.waitFortheElementXpath(
				"//span[text()='Customer Name : ']/following-sibling::strong[contains(., '" + firstName + "')]");

		Thread.sleep(2000);
		AdminPage.lab_addAppointment_serviceDateSlot();

		// Select Branch
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_Branch);
		Thread.sleep(1000);
		if (driver.findElements(By.xpath(Elements_Admin.lab_addAppointment_BranchNoData)).size() > 0) {
			Assert.fail("No partners mapped to the selected operating city : " + city);
		} else {
			Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_firstBranch);
		}

		// Add Patient
		AdminPage.lab_addAppointment_addPatient(patient_FirstName, gender);
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + patient_FirstName + "')]"); // PatientName
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + firstName + "')]"); // CustomerName
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_PatientSubmitBtn);

		// Add Test & Profile
		AdminPage.lab_addAppointment_selectTestProfile(testName);
		AdminPage.lab_addAppointment_selectTestProfile(profileName);

		// Add Test & Profile Listing
//		int actualTestCost = Integer
//				.parseInt(Browser.getTextByXpath("//tr/td[contains(., '" + testName + "')]/following-sibling::td[4]"));
//		Assert.assertEquals(actualTestCost, Integer.parseInt(testCost));
//		int actualProfileCost = Integer.parseInt(
//				Browser.getTextByXpath("//tr/td[contains(., '" + profileName + "')]/following-sibling::td[4]"));
//		Assert.assertEquals(actualProfileCost, Integer.parseInt(profileCost));

		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + testName
				+ "')]/following-sibling::td[5]/div/div/div/div/div[@class='input-group__selections']"); // SelectPatient
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("(//div[@class='list__tile__title' and contains(., '" + firstName + "')])[2]"); // PrimaryPatient
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + testName + "')]/following-sibling::td[4]");
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + profileName
				+ "')]/following-sibling::td[5]/div/div/div/div/div[@class='input-group__selections']"); // SelectPatient
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("(//div[@class='list__tile__title' and contains(., '" + firstName + "')])[1]"); // PrimaryPatient
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + testName + "')]/following-sibling::td[4]");

		// Add Package
		AdminPage.lab_addAppointment_selectPackage(packageName);
//		String[] pkgcost = packageCost.split(" ");
//		System.out.println("Pkg cost is : " + pkgcost[0]);
//		// Add Package Listing
//		Browser.waitFortheElementXpath("//tr/td[contains(., '" + packageName + "')]");
//		int actualPackageCost = Integer.parseInt(
//				Browser.getTextByXpath("//tr/td[contains(., '" + packageName + "')]/following-sibling::td[4]"));
//		Assert.assertEquals(actualPackageCost, Integer.parseInt(pkgcost[0]));
		// Package patient selection
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + packageName
				+ "')]/following-sibling::td[5]/div/div/div/div/div[@class='input-group__selections']"); // SelectPatient
		Browser.clickOnTheElementByXpath("(//div[@class='list__tile__title' and contains(., '" + firstName + "')])[1]"); // PrimaryPatient
		Browser.clickOnTheElementByXpath(
				"//div[@class='list__tile__title' and contains(., '" + patient_FirstName + "')]"); // SecondPatient
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + packageName + "')]/following-sibling::td[4]");
//		int quantity = Integer.parseInt(
//				Browser.getTextByXpath("//tr/td[contains(., '" + packageName + "')]/following-sibling::td[3]"));
//		int finalPrice, actualFinalPrice;
//		finalPrice = actualPackageCost * quantity;
//		actualFinalPrice = Integer.parseInt(
//				Browser.getTextByXpath("//tr/td[contains(., '" + packageName + "')]/following-sibling::td[4]"));
//		Assert.assertEquals(actualFinalPrice, finalPrice);

		AdminPage.lab_addAPpointment_selectPayMode_ConfirmBooking(payMode);
		Browser.waitFortheElementXpath(Elements_Admin.lab_addAppointment_addBtn);
		Thread.sleep(500);
		Browser.ScrollUp();
		Thread.sleep(500);
		AdminPage.lab_addAppointment_searchList("Mobile Number", number);
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterEmail", email);
		Browser.quit();
	}

}