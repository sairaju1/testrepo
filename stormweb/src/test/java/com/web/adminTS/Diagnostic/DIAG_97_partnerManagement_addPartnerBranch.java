// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Address;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class DIAG_97_partnerManagement_addPartnerBranch extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public Person p;
	String branchName, branchCode, contactName, contactPhone, contactEmail;
	Address address;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		p = Browser.personData();
		branchName = Browser.generateRandomString(5).toLowerCase();
		contactName = p.getFullName();
		contactPhone = "9" + Browser.generateRandomNumber(9);
		contactEmail = p.getEmail();
		address = p.getAddress();
		branchCode = Browser.generateRandomString(5).toUpperCase();
	}

	@Test(groups = { "ADMINDC", "High" })
	public void addPartnerBranch() throws Exception {
		AdminPage.AdminMenu_subMenu("Zoylo Partner Management", "False", "Zoylo Partner Branch");
		Browser.clickOnTheElementByXpath(Elements_Admin.labPartner_addBranch); // addBtn
		Browser.clickOnTheElementByXpath(Elements_Admin.labPartner_Branch_PartnerDrp); // partnerDrp
		Browser.enterTextByXpath(Elements_Admin.labPartner_Branch_PartnerDrpInput, partner_name); // enterPaernerName
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='" + partner_name + "']"); // SelectPartner
		Browser.enterTextByXpath(Elements_Admin.labPartner_Branch_name, branchName); // branchname
		Browser.enterTextByXpath(Elements_Admin.labPartner_Branch_code, branchCode);
		Browser.enterTextByXpath(Elements_Admin.labPartner_Branch_contactName, contactName);
		Browser.enterTextByXpath(Elements_Admin.labPartner_Branch_number, contactPhone);
		Browser.enterTextByXpath(Elements_Admin.labPartner_Branch_email, contactEmail);
		Browser.clickOnTheElementByXpath(Elements_Admin.labPartner_Branch_City); // selectCity
		Browser.enterTextByXpath(Elements_Admin.labPartner_Branch_CityInput, operatingCity_2); // searchSuggestion
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='" + operatingCity_2 + "']");
		Browser.clickOnTheElementByXpath(Elements_Admin.labPartner_Branch_ZoneName); // cityDrp
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_firstDrpDwnValue); // firstZone
		Browser.enterTextByXpath(Elements_Admin.labPartner_Branch_Address1, address.toString()); // address
		Browser.enterTextByXpath(Elements_Admin.labPartner_Branch_Pin, Browser.generateRandomNumber(6));
		Browser.clickOnTheElementByXpath(Elements_Admin.labPartner_Branch_Save);
		Browser.waitFortheElementXpath(Elements_Admin.labPartner_addBranch); // addBtn

		// Edit Branch
		Browser.clickOnTheElementByXpath("//label[text()='Select Service']/following-sibling::div/div[@class='input-group__selections']");
		Browser.enterTextByXpath(
				"//label[text()='Select Service']/following-sibling::div/div[@class='input-group__selections']/div/input",
				branchName);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='" + branchName + "']"); // DrpSelect
		Browser.waitFortheElementXpath("//tr/td[text()='" + branchName + "']");
		Browser.clickOnTheElementByXpath("//tr/td[text()='" + branchName + "']/following-sibling::td/i");
		Browser.clickOnTheElementByXpath(Elements_Admin.labPartner_Branch_Save);
		Browser.waitFortheElementXpath(Elements_Admin.labPartner_addBranch); // addBtn
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloPartnerBranchDetails", "branchCode", branchCode);
		Browser.quit();
	}

}
