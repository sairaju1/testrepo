// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZA_110_Affiliates_combinationPackages_onlineBooking extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public String firstName_1, lastName_1, city, number_1, email_1, gender, age_1, Locality, firstName_2, lastName_2,
			number_2, email_2, age_2, firstName_3, lastName_3, number_3, email_3, age_3, Landmark, address, date;
	public String payMode = "ONLINE", Pkg_normal, Pkg_1P1, Pkg_2P1;
	public int Pkg_normal_Cost, Pkg_1P1_Cost, Pkg_2P1_Cost, Pkg_normal_Qty, Pkg_1P1_Qty, Pkg_2P1_Qty;
	public Person p, p1, p2;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		p = Browser.personData();
		p1 = Browser.personData();
		p2 = Browser.personData();
		Pkg_normal = rec_packageName;
		Pkg_1P1 = package_Name;
		Pkg_2P1 = package_NameTwo;
		Browser.mongo_update_intAttribute(Environment, "zoyloLabsMasterPackage", "packageData.packageName", Pkg_1P1,
				"maxAllowedPersons", 2);
		Browser.mongo_update_intAttribute(Environment, "zoyloLabsMasterPackage", "packageData.packageName", Pkg_2P1,
				"maxAllowedPersons", 3);
		city = operatingCity_1;
		firstName_1 = p.getFirstName();
		firstName_2 = p1.getFirstName();
		firstName_3 = p2.getFirstName();
		lastName_1 = p.getLastName();
		lastName_2 = p1.getLastName();
		lastName_3 = p2.getLastName();
		email_1 = p.getEmail();
		email_2 = p1.getEmail();
		email_3 = p2.getEmail();
		age_1 = Integer.toString(p.getAge());
		age_2 = Integer.toString(p1.getAge());
		age_3 = Integer.toString(p2.getAge());
		gender = "Male";
		number_1 = "9" + Browser.generateRandomNumber(9);
		number_2 = "8" + Browser.generateRandomNumber(9);
		number_3 = "7" + Browser.generateRandomNumber(9);
		Landmark = "Z" + Browser.generateRandomString(5).toLowerCase();
		address = p.getAddress().toString();
		date = Browser.getModifiedDate(1);
	}

	@Test(groups = { "ADMINDC", "High" })
	public void affiliates_pkgComboBooking() throws Exception {
		Browser.openUrl(uat_affiliates);
		AdminPage.affiliates_login(adminuser_id, adminuser_pw);
		AdminPage.affiliates_customerInfo(number_1, firstName_1, lastName_1, email_1, age_1, gender, city, date,
				address, Landmark);
		AdminPage.affiliates_custInfo_Save();

		AdminPage.affiliates_clickAddPackage();
		Pkg_normal_Cost = Integer.parseInt(AdminPage.affiliates_addPackage(Pkg_normal));
		Pkg_1P1_Cost = Integer.parseInt(AdminPage.affiliates_addPackage(Pkg_1P1));
		Pkg_2P1_Cost = Integer.parseInt(AdminPage.affiliates_addPackage(Pkg_2P1));

		Browser.clickOnTheElementByXpath("//button[text()='Close']"); // closeBtn

		Browser.clickOnTheElementByXpath("//button[text()='Add Patients']"); // addPatientBtn
		AdminPage.affiliates_addPatient(number_2, firstName_2, lastName_2, email_2, age_2, gender);
		AdminPage.affiliates_addPatient(number_3, firstName_3, lastName_3, email_3, age_3, gender);
		AdminPage.affiliates_addPatientList(firstName_1);
		AdminPage.affiliates_addPatientList(firstName_2);
		AdminPage.affiliates_addPatientList(firstName_3);
		Browser.clickOnTheElementByXpath("//button[text()='Submit']"); // addPatientSubmit
		Thread.sleep(2000);
		AdminPage.affiliates_bookingPatientList(Pkg_normal, firstName_1);
		Thread.sleep(2000);
		AdminPage.affiliates_bookingPatientList(Pkg_1P1, firstName_1);
		Thread.sleep(2000);
		AdminPage.affiliates_bookingPatientList(Pkg_1P1, firstName_2);
		Thread.sleep(2000);
		AdminPage.affiliates_bookingPatientList(Pkg_2P1, firstName_1);
		Thread.sleep(2000);
		AdminPage.affiliates_bookingPatientList(Pkg_2P1, firstName_2);
		Thread.sleep(2000);
		AdminPage.affiliates_bookingPatientList(Pkg_2P1, firstName_3);
		Thread.sleep(2000);

		// Get Package list price
		int Pkg_normal_CostAct = Integer.parseInt(AdminPage.affiliates_bookingDetails_listPrice(Pkg_normal));
		int Pkg_1P1_CostAct = Integer.parseInt(AdminPage.affiliates_bookingDetails_listPrice(Pkg_1P1));
		int Pkg_2P1_CostAct = Integer.parseInt(AdminPage.affiliates_bookingDetails_listPrice(Pkg_2P1));

		Assert.assertEquals(Pkg_normal_CostAct, Pkg_normal_Cost);
		Assert.assertEquals(Pkg_1P1_CostAct, Pkg_1P1_Cost);
		Assert.assertEquals(Pkg_2P1_CostAct, Pkg_2P1_Cost);

		int totalBookingCost = Pkg_normal_CostAct + Pkg_1P1_CostAct + Pkg_2P1_CostAct;
		AdminPage.affiliates_selectPayMode_ConfirmBooking(payMode);
		Browser.waitFortheID("custMobile");

		// Verify amount and time in amdin
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		AdminPage.lab_addAppointmentList_navigate();
		Thread.sleep(1000);
		AdminPage.lab_addAppointment_searchList("Mobile Number", number_1);
		String adminListAmt = Browser.getTextByXpath("//tr/td[text()='+91" + number_1 + "']/following-sibling::td[7]");
		Assert.assertEquals(adminListAmt, totalBookingCost);
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterEmail", email_1);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", email_1);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", email_2);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", email_3);
		Browser.quit();
	}

}