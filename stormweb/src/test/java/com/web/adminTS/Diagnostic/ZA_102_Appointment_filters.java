// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZA_102_Appointment_filters extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public String startDate, endDate;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
	}

	@Test(groups = { "ADMINDC", "High" })
	public void appointment_filters() throws Exception {

		startDate = Browser.getDate_format("yyyy-MM-dd", true, -1);
		endDate = startDate;
		System.out.println("Start date is : " + startDate);

		AdminPage.lab_addAppointmentList_navigate();

		// Booking Date filter
		System.out.println("Filtering by booking date.!");
		Browser.clickOnTheElementByXpath(Elements_Admin.labAppointments_magnifierIcon); // magnifierIcon
		Browser.waitFortheElementXpath(Elements_Admin.lab_appointment_Search_BookingDateHeader); // BookingLable
		Browser.enterTextByXpath(Elements_Admin.lab_appointment_Search_bookingDateStart, startDate);
		Browser.enterTextByXpath(Elements_Admin.lab_appointment_Search_bookingDateEnd, endDate);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_appointment_Search_BookingDateHeader); // datePickerHide
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_appointment_searchSubmitBtn);

		Thread.sleep(3000);

		int numberOfpages = driver.findElements(By.xpath(Elements_Admin.lab_appointmentList_paginationSize)).size() - 2;
		int dataAvailable = driver
				.findElements(By.xpath(Elements_Admin.lab_appointmentList_noData))
				.size();
		int page1_records = driver.findElements(By.xpath("//table[@class='datatable table']/tbody/tr")).size();
		if (page1_records > 0 && dataAvailable == 0) {
			for (int i = 1; i <= page1_records; i++) {
				Browser.waitFortheElementXpath("(//td[5][contains(., '" + startDate + "')])[" + i + "]");
			}
		} else {
			System.out.println("Filter has no records available..");
		}
		if (numberOfpages > 1) {
			Browser.clickOnTheElementByXpath("//ul[@class='pagination']/li/a[text()='2']"); // secondPage
			Thread.sleep(3000);
			int page2_records = driver.findElements(By.xpath("//table[@class='datatable table']/tbody/tr")).size();
			for (int x = 1; x <= page2_records; x++) {
				Browser.waitFortheElementXpath("(//td[5][contains(., '" + startDate + "')])[" + x + "]");
			}
		}
		System.out.println(
				"********************************************* Booking date filter verification done ***************************************************");

		Browser.clickOnTheElementByXpath(Elements_Admin.lab_appointment_Search_Reset); // ReSetBtn
		Thread.sleep(3000);

		// Appointment Date filter
		Browser.enterTextByXpath(Elements_Admin.lab_appointment_Search_aptDateStart, startDate);
		Browser.enterTextByXpath(Elements_Admin.lab_appointment_Search_aptDateEnd, endDate);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_appointment_Search_BookingDateHeader); // datePickerHide
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_appointment_searchSubmitBtn);

		Thread.sleep(3000);

		int numberOfpages_AptFilter = driver.findElements(By.xpath(Elements_Admin.lab_appointmentList_paginationSize)).size() - 2;
		int dataAvailable_AptFilter = driver
				.findElements(By.xpath(Elements_Admin.lab_appointmentList_noData))
				.size();
		int page1_records_AptFilter = driver.findElements(By.xpath("//table[@class='datatable table']/tbody/tr"))
				.size();
		if (page1_records_AptFilter > 0 && dataAvailable_AptFilter == 0) {
			for (int i = 1; i <= page1_records_AptFilter; i++) {
				Browser.waitFortheElementXpath("(//td[6][contains(., '" + startDate + "')])[" + i + "]");
			}
		} else {
			System.out.println("Filter has no records available..");
		}
		if (numberOfpages_AptFilter > 1) {
			Browser.clickOnTheElementByXpath("//ul[@class='pagination']/li/a[text()='2']"); // secondPage
			Thread.sleep(3000);
			int page2_records_AptFilter = driver.findElements(By.xpath("//table[@class='datatable table']/tbody/tr"))
					.size();
			for (int x = 1; x <= page2_records_AptFilter; x++) {
				Browser.waitFortheElementXpath("(//td[6][contains(., '" + startDate + "')])[" + x + "]");
			}
		}
		System.out.println(
				"********************************************* Appointment date filter verification done ***************************************************");

		Browser.clickOnTheElementByXpath(Elements_Admin.lab_appointment_Search_Reset); // ReSetBtn
		Thread.sleep(3000);

		// Booking Status filter
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_appointment_Search_BookingStatusDrp); // BookingStatusDrp
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='Cancelled']"); // cancelledStatusDrp
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_appointment_searchSubmitBtn);

		Thread.sleep(3000);

		int numberOfpages_status = driver.findElements(By.xpath(Elements_Admin.lab_appointmentList_paginationSize)).size() - 2;
		int dataAvailable_status = driver
				.findElements(By.xpath(Elements_Admin.lab_appointmentList_noData))
				.size();
		int page1_records_status = driver.findElements(By.xpath("//table[@class='datatable table']/tbody/tr")).size();
		if (page1_records_status > 0 && dataAvailable_status == 0) {
			for (int i = 1; i <= page1_records_status; i++) {
				Browser.waitFortheElementXpath("(//td[9][contains(., 'CANCEL')])[" + i + "]");
			}
		} else {
			System.out.println("Filter has no records available..");
		}
		if (numberOfpages_status > 1) {
			Browser.clickOnTheElementByXpath("//ul[@class='pagination']/li/a[text()='2']"); // secondPage
			Thread.sleep(3000);
			int page2_records_status = driver.findElements(By.xpath("//table[@class='datatable table']/tbody/tr"))
					.size();
			for (int x = 1; x <= page2_records_status; x++) {
				Browser.waitFortheElementXpath("(//td[9][contains(., 'CANCEL')])[" + x + "]");
			}
		}
		System.out.println(
				"********************************************* Appointment status filter verification done ***************************************************");

	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
