// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZL_34_partnerMapping_Profile extends LoadProp {
	
	public TestUtils Browser;
	public AdminPage AdminPage;
	public String profileName, partnerName, partnerBookingcode_Profile;
	int partnerPrice_profile, partnerDiscount;
	
	@BeforeClass(groups = { "Admin", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		partnerName = partner_name;
		partnerBookingcode_Profile = Browser.generateRandomString(5).toUpperCase();
		partnerPrice_profile = 500;
		partnerDiscount = 450;
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		AdminPage.AdminMenu_subMenu("Zoylo Lab Management", "False", "Lab Master Profiles");
	}
	
	@Test(groups = { "Admin", "High" })
	public void partnerMapping_ProfileAddEdit() throws Exception {
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterProfile_listHeader);
		Thread.sleep(2000);
		profileName = Browser.getTextByXpath(Elements_Admin.lab_masterProfileList_firstProfileName);
		AdminPage.AdminMenu_subMenu("Zoylo Partner Mapping", "False", "Zoylo Partner Mapping Details");
		Browser.remove_Document(Environment, "zoyloPartnerService", "zoyloServiceData.zoyloServiceName", profileName);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_partnerMapping_addBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_partnerMapping_profileTab);
		
		//Profile Add
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("(" + Elements_Admin.lab_partnerMapping_selectPartner + ")[3]");
		Thread.sleep(1000);
		Browser.enterTextByXpath("(" + Elements_Admin.lab_partnerMapping_selectPartnerInput + ")[3]", partnerName);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + partnerName + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_partnerMapping_selectProfileDrp);
		Thread.sleep(1000);
		Browser.enterTextByXpath(Elements_Admin.lab_partnerMapping_selectProfileInput, profileName);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + profileName + "')]");
		Browser.enterTextByXpath("(" + Elements_Admin.lab_partnerMapping_BookingCode + ")[3]", partnerBookingcode_Profile);
		Browser.enterTextByXpath("(" + Elements_Admin.lab_partnerMapping_price + ")[3]",
				Integer.toString(partnerPrice_profile));
		Thread.sleep(1000);
		Browser.enterTextByXpath("(" + Elements_Admin.lab_partnerMapping_finalPrice + ")[3]",
				Integer.toString(partnerDiscount));
		Thread.sleep(1000);
		Browser.enterTextByXpath("(" + Elements_Admin.lab_partnerMapping_lisCode + ")[2]", "LIS_" + partnerBookingcode_Profile);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_saveBtn);
		
		System.out.println("**************************** Profile Mapping done ****************************");
		
		// Edit Check
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_partnerMapping_subMenu);
		Browser.waitFortheElementXpath(Elements_Admin.lab_partnerMapping_addBtn);
		Browser.enterTextByXpath(Elements_Admin.lab_partnerMapping_search, partnerBookingcode_Profile);
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + partnerBookingcode_Profile + "')]");
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + partnerBookingcode_Profile + "')]/following-sibling::td[2]");
		
		// EDIT Test mapping
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + partnerBookingcode_Profile + "')]/following-sibling::td[4]/i");
		if(driver.findElements(By.xpath(Elements_Admin.lab_partnerMapping_editSaveBtn)).size() == 0) {
			Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + partnerBookingcode_Profile + "')]/following-sibling::td[4]/i");
		}
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_partnerMapping_editSaveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.lab_partnerMapping_editSaveMsg);
	}
	
	@AfterClass(groups = { "Admin", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}
	
}