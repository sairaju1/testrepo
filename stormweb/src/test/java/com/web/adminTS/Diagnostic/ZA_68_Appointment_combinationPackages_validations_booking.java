// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZA_68_Appointment_combinationPackages_validations_booking extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public Person p, p1, p2;
	public String Pkg_normal, Pkg_1P1, Pkg_2P1, userName_1, number_1, email_1, gender_1, age_1, city, payMode;
	public String userName_2, gender_2;
	public String userName_3, gender_3;
	public int Pkg_normal_Cost, Pkg_1P1_Cost, Pkg_2P1_Cost, Pkg_normal_Qty, Pkg_1P1_Qty, Pkg_2P1_Qty;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		p = Browser.personData();
		p1 = Browser.personData();
		p2 = Browser.personData();
		Pkg_normal = rec_packageName;
		Pkg_1P1 = package_Name;
		Pkg_2P1 = package_NameTwo;
		Browser.mongo_update_intAttribute(Environment, "zoyloLabsMasterPackage", "packageData.packageName", Pkg_1P1,
				"maxAllowedPersons", 2);
		Browser.mongo_update_intAttribute(Environment, "zoyloLabsMasterPackage", "packageData.packageName", Pkg_2P1,
				"maxAllowedPersons", 3);
		userName_1 = p.getFirstName();
		userName_2 = p1.getFirstName();
		userName_3 = p2.getFirstName();
		number_1 = "9" + Browser.generateRandomNumber(9);
		email_1 = p.getEmail();
		gender_1 = "Male";
		gender_2 = "Male";
		gender_3 = "Female";
		age_1 = "30";
		city = operatingCity_1;
		payMode = "CASH";
	}

	@Test(groups = { "ADMINDC", "High" })
	public void validate_book_comboPackage_COD() throws Exception {
		AdminPage.lab_addAppointmentList_navigate();
		AdminPage.lab_addAppointmentButton_click();
		AdminPage.lab_addAppointment_customerInfo(userName_1, number_1, email_1, gender_1, age_1, city);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_nextBtn);

		// ***************** Service Info *****************
		Browser.waitFortheElementXpath(
				"//span[text()='Customer Name : ']/following-sibling::strong[contains(., '" + userName_1 + "')]");

		Thread.sleep(2000);
		AdminPage.lab_addAppointment_serviceDateSlot();

		// Select Branch
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_Branch);
		Thread.sleep(1000);
		if (driver.findElements(By.xpath(Elements_Admin.lab_addAppointment_BranchNoData)).size() > 0) {
			Assert.fail("No partners mapped to the selected operating city : " + city);
		} else {
			Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_firstBranch);
		}

		// Add Patient
		AdminPage.lab_addAppointment_addPatient(userName_2, gender_2);
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + userName_2 + "')]"); // PatientName2
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_PatientSubmitBtn);
		AdminPage.lab_addAppointment_addPatient(userName_3, gender_3);
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + userName_3 + "')]"); // PatientName3
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_PatientSubmitBtn);

		// Add Normal Package
		String pCost = AdminPage.lab_addAppointment_selectPackage(Pkg_normal);
		String[] pkgcost = pCost.split(" ");
		Pkg_normal_Cost = Integer.parseInt(pkgcost[0]);
		// Add Package Listing
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + Pkg_normal + "')]");

		// Add 1P1 Package
		String pCost1 = AdminPage.lab_addAppointment_selectPackage(Pkg_1P1);
		String[] pkgcost1 = pCost1.split(" ");
		Pkg_1P1_Cost = Integer.parseInt(pkgcost1[0]);
		// Add Package Listing
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + Pkg_1P1 + "')]");

		// Add 2P1 Package
		String pCost2 = AdminPage.lab_addAppointment_selectPackage(Pkg_2P1);
		String[] pkgcost2 = pCost2.split(" ");
		Pkg_2P1_Cost = Integer.parseInt(pkgcost2[0]);
		// Add Package Listing
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + Pkg_2P1 + "')]");

		// Add patients to Pkg_normal - u1 and u2
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + Pkg_normal
				+ "')]/following-sibling::td[5]/div/div/div/div/div[@class='input-group__selections']"); // SelectPatient
		Browser.clickOnTheElementByXpath(
				"(//div[@class='list__tile__title' and contains(., '" + userName_1 + "')])[3]"); // PrimaryPatient
		Browser.clickOnTheElementByXpath(
				"(//div[@class='list__tile__title' and contains(., '" + userName_2 + "')])[3]"); // user2
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + Pkg_normal + "')]");
		Pkg_normal_Qty = Integer.parseInt(
				Browser.getTextByXpath("//tr/td[contains(., '" + Pkg_normal + "')]/following-sibling::td[3]"));
		int pkg_normal_finalCost = Pkg_normal_Cost * Pkg_normal_Qty;
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + Pkg_normal + "')]/following-sibling::td[4 and text()='"
				+ pkg_normal_finalCost + "']");

		// Add patients to Pkg_1P1 - u1 and u2
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + Pkg_1P1
				+ "')]/following-sibling::td[5]/div/div/div/div/div[@class='input-group__selections']"); // SelectPatient
		Browser.clickOnTheElementByXpath(
				"(//div[@class='list__tile__title' and contains(., '" + userName_1 + "')])[2]"); // PrimaryPatient
		Browser.clickOnTheElementByXpath(
				"(//div[@class='list__tile__title' and contains(., '" + userName_2 + "')])[2]"); // user2
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + Pkg_1P1 + "')]");
		Pkg_1P1_Qty = Integer
				.parseInt(Browser.getTextByXpath("//tr/td[contains(., '" + Pkg_1P1 + "')]/following-sibling::td[3]"));
		int Pkg_1P1_finalCost = Pkg_1P1_Cost * Pkg_1P1_Qty;
		Browser.waitFortheElementXpath("(//tr/td[contains(., '" + Pkg_1P1 + "')]/following-sibling::td[4 and text()='"
				+ Pkg_1P1_finalCost + "'])[2]");

		// Add patients to Pkg_2P1 - u1 , u2 and u3
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + Pkg_2P1
				+ "')]/following-sibling::td[5]/div/div/div/div/div[@class='input-group__selections']"); // SelectPatient
		Browser.clickOnTheElementByXpath(
				"(//div[@class='list__tile__title' and contains(., '" + userName_1 + "')])[1]"); // PrimaryPatient
		Browser.clickOnTheElementByXpath(
				"(//div[@class='list__tile__title' and contains(., '" + userName_2 + "')])[1]"); // user2
		Browser.clickOnTheElementByXpath(
				"(//div[@class='list__tile__title' and contains(., '" + userName_3 + "')])[1]"); // user3
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + Pkg_2P1 + "')]");
		Pkg_2P1_Qty = Integer
				.parseInt(Browser.getTextByXpath("//tr/td[contains(., '" + Pkg_2P1 + "')]/following-sibling::td[3]"));
		int Pkg_2P1_finalCost = Pkg_2P1_Cost * Pkg_2P1_Qty;
		Browser.waitFortheElementXpath("(//tr/td[contains(., '" + Pkg_2P1 + "')]/following-sibling::td[4 and text()='"
				+ Pkg_2P1_finalCost + "'])[2]");

		int bookingTotal = pkg_normal_finalCost + Pkg_1P1_finalCost + Pkg_2P1_finalCost;

		AdminPage.lab_addAPpointment_selectPayMode_ConfirmBooking(payMode);
		Browser.waitFortheElementXpath(Elements_Admin.lab_addAppointment_addBtn);
		Thread.sleep(500);
		Browser.ScrollUp();
		Thread.sleep(500);
		AdminPage.lab_addAppointment_searchList("Mobile Number", number_1);

		Browser.scrollbyxpath("//tr/td[text()='+91" + number_1 + "']/following-sibling::td[8]"); // ScrolltoActions
		int actualBookingCost = Integer
				.parseInt(Browser.getTextByXpath("//tr/td[text()='+91" + number_1 + "']/following-sibling::td[7]"));

		Assert.assertEquals(actualBookingCost, bookingTotal);
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterEmail", email_1);
		Browser.quit();
	}

}