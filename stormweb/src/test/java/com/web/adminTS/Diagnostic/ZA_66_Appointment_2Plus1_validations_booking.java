// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZA_66_Appointment_2Plus1_validations_booking extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public Person p, p1, p2;
	public String packageName, userName_1, number_1, email_1, gender_1, age_1, city, payMode;
	public String userName_2, gender_2;
	public String userName_3, gender_3;
	public int packageCost;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		p = Browser.personData();
		p1 = Browser.personData();
		p2 = Browser.personData();
		packageName = package_NameTwo;
		Browser.mongo_update_intAttribute(Environment, "zoyloLabsMasterPackage", "packageData.packageName", packageName,
				"maxAllowedPersons", 3);
		userName_1 = p.getFirstName();
		userName_2 = p1.getFirstName();
		userName_3 = p2.getFirstName();
		number_1 = "9" + Browser.generateRandomNumber(9);
		email_1 = p.getEmail();
		gender_1 = "Male";
		gender_2 = "Male";
		gender_3 = "Female";
		age_1 = "30";
		city = operatingCity_1;
		payMode = "CASH";
	}

	@Test(groups = { "ADMINDC", "High" })
	public void validate_book_2P1package_COD() throws Exception {
		AdminPage.lab_addAppointmentList_navigate();
		AdminPage.lab_addAppointmentButton_click();
		AdminPage.lab_addAppointment_customerInfo(userName_1, number_1, email_1, gender_1, age_1, city);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_nextBtn);

		// ***************** Service Info *****************
		Browser.waitFortheElementXpath(
				"//span[text()='Customer Name : ']/following-sibling::strong[contains(., '" + userName_1 + "')]");

		Thread.sleep(2000);
		AdminPage.lab_addAppointment_serviceDateSlot();

		// Select Branch
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_Branch);
		Thread.sleep(1000);
		if (driver.findElements(By.xpath(Elements_Admin.lab_addAppointment_BranchNoData)).size() > 0) {
			Assert.fail("No partners mapped to the selected operating city : " + city);
		} else {
			Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_firstBranch);
		}

		// Add Patient
		AdminPage.lab_addAppointment_addPatient(userName_2, gender_2);
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + userName_2 + "')]"); // PatientName2
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_PatientSubmitBtn);
		AdminPage.lab_addAppointment_addPatient(userName_3, gender_3);
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + userName_3 + "')]"); // PatientName3
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_PatientSubmitBtn);

		// Add Package
		String pCost = AdminPage.lab_addAppointment_selectPackage(packageName);
		String[] pkgcost = pCost.split(" ");
		packageCost = Integer.parseInt(pkgcost[0]);
		// Add Package Listing
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + packageName + "')]");

		// Add patients to package
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + packageName
				+ "')]/following-sibling::td[5]/div/div/div/div/div[@class='input-group__selections']"); // SelectPatient
		Browser.clickOnTheElementByXpath(
				"(//div[@class='list__tile__title' and contains(., '" + userName_1 + "')])[1]"); // PrimaryPatient
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + packageName + "')]");
		int q1 = Integer.parseInt(
				Browser.getTextByXpath("//tr/td[contains(., '" + packageName + "')]/following-sibling::td[3]"));
		int f1 = packageCost * q1;
		Browser.waitFortheElementXpath(
				"(//tr/td[contains(., '" + packageName + "')]/following-sibling::td[4 and text()='" + f1 + "'])[2]");
		// Adding user two
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + packageName
				+ "')]/following-sibling::td[5]/div/div/div/div/div[@class='input-group__selections']/following-sibling::i"); // SelectPatient
		Browser.clickOnTheElementByXpath(
				"(//div[@class='list__tile__title' and contains(., '" + userName_2 + "')])[1]"); // PrimaryPatient
		int q2 = Integer.parseInt(
				Browser.getTextByXpath("//tr/td[contains(., '" + packageName + "')]/following-sibling::td[3]"));
		int f2 = packageCost * q2;
		Browser.waitFortheElementXpath(
				"(//tr/td[contains(., '" + packageName + "')]/following-sibling::td[4 and text()='" + f2 + "'])[2]");
		// Adding user three
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + packageName
				+ "')]/following-sibling::td[5]/div/div/div/div/div[@class='input-group__selections']/following-sibling::i"); // SelectPatient
		Browser.clickOnTheElementByXpath(
				"(//div[@class='list__tile__title' and contains(., '" + userName_3 + "')])[1]"); // PrimaryPatient
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + packageName + "')]");
		int q3 = Integer.parseInt(
				Browser.getTextByXpath("//tr/td[contains(., '" + packageName + "')]/following-sibling::td[3]"));
		int f3 = packageCost * q3;
		Browser.waitFortheElementXpath(
				"(//tr/td[contains(., '" + packageName + "')]/following-sibling::td[4 and text()='" + f3 + "'])[2]");

		AdminPage.lab_addAPpointment_selectPayMode_ConfirmBooking(payMode);
		Browser.waitFortheElementXpath(Elements_Admin.lab_addAppointment_addBtn);
		AdminPage.lab_addAppointment_searchList("Mobile Number", number_1);

		Browser.scrollbyxpath("//tr/td[text()='+91" + number_1 + "']/following-sibling::td[8]"); // ScrolltoActions
		int actualPackageCost = Integer
				.parseInt(Browser.getTextByXpath("//tr/td[text()='+91" + number_1 + "']/following-sibling::td[7]"));
		Assert.assertEquals(actualPackageCost, f3);
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterEmail", email_1);
		Browser.quit();
	}

}
