// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class DIAG_169_Appointment_cancel extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public Person p;
	public String number, payMode, testName, gender, age, city, testCost, firstName;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		p = Browser.personData();
		number = user_id;
		payMode = "CASH";
		testName = test_Name;
		gender = "Male";
		age = "31";
		city = operatingCity_1;
		firstName = "Sagar";
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterPhone", "+91" + number);
	}

	@Test(groups = { "ADMINDC", "High" })
	public void addAppointment_cancel() throws Exception {
		// Add Appointment segment
		AdminPage.lab_addAppointmentList_navigate();
		Thread.sleep(1000);
		AdminPage.lab_addAppointmentButton_click();
		AdminPage.lab_addAppointment_customerInfo("", number, "", gender, age, city);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_nextBtn);
		Browser.waitFortheElementXpath(
				"//span[text()='Customer Name : ']/following-sibling::strong[contains(., '" + firstName + "')]");
		Thread.sleep(2000);
		AdminPage.lab_addAppointment_serviceDateSlot();
		// Select Branch
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_Branch);
		Thread.sleep(1000);
		if (driver.findElements(By.xpath(Elements_Admin.lab_addAppointment_BranchNoData)).size() > 0) {
			Assert.fail("No partners mapped to the selected operating city : " + city);
		} else {
			Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_firstBranch);
		}
		// Add Test & Profile
		AdminPage.lab_addAppointment_selectTestProfile(testName);
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + testName
				+ "')]/following-sibling::td[5]/div/div/div/div/div[@class='input-group__selections']"); // SelectPatient
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("(//div[@class='list__tile__title' and contains(., '" + firstName + "')])[1]"); // PrimaryPatient
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + testName + "')]/following-sibling::td[2]");
		AdminPage.lab_addAPpointment_selectPayMode_ConfirmBooking(payMode);
		Browser.waitFortheElementXpath(Elements_Admin.lab_addAppointment_addBtn);
		AdminPage.lab_addAppointment_searchList("Mobile Number", number);

		// Verify Appointment and Assign phlebo
		Browser.scrollbyxpath("//tr/td[text()='+91" + number + "']/following-sibling::td[8]"); // ScrolltoStatus
		Browser.clickOnTheElementByXpath("//tr/td[text()='+91" + number + "']/following-sibling::td[6]"); // statusDrp
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='CANCEL']"); // Cancel

		Browser.waitFortheElementXpath("//label[text()='Booking Cancellation']"); // cancelLablePopUp
		Browser.clickOnTheElementByXpath("(//div[@class='btn__content' and text()=' Yes'])[1]"); // yesBtn

		driver.navigate().refresh();
		AdminPage.lab_addAppointment_searchList("Mobile Number", number);
		Browser.scrollbyxpath("//tr/td[text()='+91" + number + "']/following-sibling::td[8]"); // ScrolltoStatus
		Browser.waitFortheElementXpath(
				"//tr/td[text()='+91" + number + "']/following-sibling::td[6]/div[contains(., 'CANCEL')]");

	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
