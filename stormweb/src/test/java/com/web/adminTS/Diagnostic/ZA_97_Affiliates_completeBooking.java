// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZA_97_Affiliates_completeBooking extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public Person p;
	public String mobileNumber, firstName, LastName, EmailId, Age, gender, city, date, address, Landmark, pkgCost,
			packageName, bookingDetails_listPrice, payMode = "CASH", barCode;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		p = Browser.personData();
		Browser.openUrl(uat_affiliates);
		AdminPage.affiliates_login(adminuser_id, adminuser_pw);
		mobileNumber = "7" + Browser.generateRandomNumber(9);
		firstName = p.getFirstName();
		LastName = p.getLastName();
		EmailId = p.getEmail();
		Age = Integer.toString(p.getAge());
		gender = "Male";
		city = operatingCity_1;
		date = Browser.getModifiedDate(1);
		address = p.getAddress().toString();
		Landmark = "Z" + Browser.generateRandomString(5).toLowerCase();
		packageName = package_NameTwo;
		barCode = Browser.generateRandomAlphaNumeric(6).toUpperCase();
	}

	@Test(groups = { "ADMINDC", "High" }, priority = 1)
	public void affiliates_completeBooking() throws Exception {
		AdminPage.affiliates_customerInfo(mobileNumber, firstName, LastName, EmailId, Age, gender, city, date, address,
				Landmark);
		AdminPage.affiliates_custInfo_Save();
		AdminPage.affiliates_clickAddPackage();
		pkgCost = AdminPage.affiliates_addPackage(packageName);
		Browser.clickOnTheElementByXpath("//button[text()='Close']"); // closeBtn
		AdminPage.affiliates_bookingPatientList(packageName, firstName);
		bookingDetails_listPrice = AdminPage.affiliates_bookingDetails_listPrice(packageName);
		Assert.assertEquals(bookingDetails_listPrice, pkgCost);
		AdminPage.affiliates_selectPayMode_ConfirmBooking(payMode);
		Browser.waitFortheID("custMobile");
	}

	@Test(groups = { "ADMINDC", "High" }, priority = 2)
	public void admin_completeBooking() throws Exception {
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);

		// Add Appointment segment
		AdminPage.lab_addAppointmentList_navigate();
		Thread.sleep(1000);
		AdminPage.lab_addAppointment_searchList("Mobile Number", mobileNumber);

		// Verify Appointment and Assign phlebo
		Browser.scrollbyxpath("//tr/td[text()='+91" + mobileNumber + "']/following-sibling::td[8]"); // ScrolltoActions
		String adminListAmt = Browser
				.getTextByXpath("//tr/td[text()='+91" + mobileNumber + "']/following-sibling::td[7]");
		Assert.assertEquals(adminListAmt, pkgCost);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//tr/td[text()='+91" + mobileNumber + "']/following-sibling::td[6]"); // statusDrp
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='VERIFIED']"); // VerifyValue
		// Verify popup
		Browser.waitFortheElementXpath(Elements_Admin.lab_appointment_verifyHeader);
		Browser.clickOnTheElementByXpath("(" + Elements_Admin.lab_appointment_yesBtn + ")[2]");
		Thread.sleep(2000);
		driver.navigate().refresh();
		// Assign
		AdminPage.lab_addAppointment_searchList("Mobile Number", mobileNumber);
		Browser.scrollbyxpath("//tr/td[text()='+91" + mobileNumber + "']/following-sibling::td[8]"); // ScrolltoActions
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//tr/td[text()='+91" + mobileNumber + "']/following-sibling::td[6]"); // statusDrp
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='ASSIGNED']"); // AssignValue
		// Assign Popup
		Browser.waitFortheElementXpath(Elements_Admin.lab_appointment_assignPhleboHeader);
		Browser.clickOnTheElementByXpath("(" + Elements_Admin.lab_appointment_assignPhleboDrp + ")[2]");
		Thread.sleep(1000);
		String phleboName = Browser.getTextByXpath(Elements_Admin.lab_appointment_assignFirstPhlebo);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(
				"(//div[@class='list__tile__title' and contains(., '" + phleboName + "')])[2]");
		Browser.clickOnTheElementByXpath("(" + Elements_Admin.lab_appointment_yesBtn + ")[4]"); // YesButton
		Thread.sleep(2000);
		driver.navigate().refresh();
		Thread.sleep(3000);
		// Accepted
		AdminPage.lab_addAppointment_searchList("Mobile Number", mobileNumber);
		Browser.scrollbyxpath("//tr/td[text()='+91" + mobileNumber + "']/following-sibling::td[8]"); // ScrolltoActions
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//tr/td[text()='+91" + mobileNumber + "']/following-sibling::td[6]"); // statusDrp
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='ACCEPTED']"); // AcceptedValue
		Thread.sleep(3000);
		// Reached
		Browser.clickOnTheElementByXpath("//tr/td[text()='+91" + mobileNumber + "']/following-sibling::td[6]"); // statusDrp
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='REACHED']"); // ReachedValue
		Thread.sleep(3000);
		// Engaged
		Browser.clickOnTheElementByXpath("//tr/td[text()='+91" + mobileNumber + "']/following-sibling::td[6]"); // statusDrp
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='ENGAGED']"); // ReachedValue
		Thread.sleep(3000);
		// Add Bar code
		Browser.scrollbyxpath("//tr/td[text()='+91" + mobileNumber + "']"); // scrollToNumber
		Browser.clickOnTheElementByXpath("//tr/td[text()='+91" + mobileNumber + "']"); // view
		Browser.clickOnTheElementByXpath("//div[contains(., '" + packageName + "')]/following-sibling::div/div/span/i"); // barCodeEditBtn
		Browser.waitFortheElementXpath("//label[text()='Update Service']"); // popUpHeader
		Browser.enterTextByXpath("//input[@aria-label='Barcode']", barCode); // enterBarCode
		Browser.clickOnTheElementByXpath("(//div[text()=' Save'])[2]"); // barCodeSaveBtn
		Browser.waitFortheElementXpath("//div[contains(., '" + packageName
				+ "')]/following-sibling::div/div/span[contains(., '" + barCode + "')]"); // viewBarCode
		Browser.clickOnTheElementByXpath("//button[contains(., 'Update Appointment')]"); // updateApt
		Thread.sleep(3000);
		driver.navigate().refresh();
		Thread.sleep(3000);
		// Deposited
		AdminPage.lab_addAppointment_searchList("Mobile Number", mobileNumber);
		Browser.clickOnTheElementByXpath("//tr/td[text()='+91" + mobileNumber + "']/following-sibling::td[6]"); // statusDrp
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='DEPOSITED']"); // DEPOSITEDValue
		Thread.sleep(3000);
		// Report Generated
		Browser.clickOnTheElementByXpath("//tr/td[text()='+91" + mobileNumber + "']/following-sibling::td[6]"); // statusDrp
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='REPORT_GENERATED']"); // ReportDValue
		Thread.sleep(3000);
		// Report Sent
		Browser.clickOnTheElementByXpath("//tr/td[text()='+91" + mobileNumber + "']/following-sibling::td[6]"); // statusDrp
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='REPORT_SENT']"); // ReportSentValue
		Thread.sleep(3000);
		// Completed
		Browser.clickOnTheElementByXpath("//tr/td[text()='+91" + mobileNumber + "']/following-sibling::td[6]"); // statusDrp
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='COMPLETED']"); // COMPLETEDValue
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterEmail", EmailId);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", EmailId);
		Browser.quit();
	}

}
