// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZL_52_labManagement_Phlebotomist extends LoadProp {
	public TestUtils Browser;
	public AdminPage AdminPage;
	public String firstName, mobile, email;
	public Person p;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		p = Browser.personData();
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		AdminPage.AdminMenu_subMenu("Zoylo Lab Management", "False", "Phlebotomist");
		firstName = p.getFirstName();
		mobile = "9" + Browser.generateRandomNumber(9);
		email = p.getEmail();
	}

	@Test(groups = { "ADMINDC", "High" })
	public void addPhlebo() throws Exception {
		Browser.waitFortheElementXpath(Elements_Admin.lab_phlebo_listHeader);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_phlebo_addPhleboBtn);
		// Add phlebo screen
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_phlebo_salutationDrp); // DrpClick
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='Mr']"); // SelectSalutation
		Browser.enterTextByXpath(Elements_Admin.lab_phlebo_firstName, firstName);
		Browser.enterTextByXpath(Elements_Admin.lab_phlebo_lastName, p.getLastName());
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_phlebo_gender);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='Male']"); // SelectGender
		Browser.enterTextByXpath(Elements_Admin.lab_phlebo_phoneNumber, mobile);
		Browser.enterTextByXpath(Elements_Admin.lab_phlebo_emailId, email);
		Browser.enterTextByXpath(Elements_Admin.lab_phlebo_photoId, "Passport");
		Browser.enterTextByXpath(Elements_Admin.lab_phlebo_idNumber, p.getPassportNumber());
		Browser.enterTextByXpath(Elements_Admin.lab_phlebo_regNum, Browser.generateRandomAlphaNumeric(6).toUpperCase());
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_phlebo_eduDrp);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + qualification + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_phlebo_lastName);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_phlebo_zoneDrp);
		Browser.clickOnTheElementByXpath(
				"//html/body/div[1]/div[1]/div[1]/div[3]/div[1]/ul[1]/li[1]/a[1]/div[1]/div[1]"); // SelcectFirstZone
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_phlebo_councilDrp);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='Medical Council of India']"); // SelectCouncil
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_phlebo_medCouncilDrp);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("(//div[@class='list__tile__title' and contains(., 'Medical Council')])[1]"); // SlectFirstMEdCouncil
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_phlebo_activeChk);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_phlebo_saveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.lab_phlebo_listHeader);
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloServiceResource", "emailId", email);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", email);
		Browser.quit();
	}
}