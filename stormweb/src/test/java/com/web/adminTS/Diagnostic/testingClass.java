package com.web.adminTS.Diagnostic;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import objectRepository.Elements_Medicines;
import testBase.LoadProp;
import testBase.MedicinePage;
import testBase.RecipientPage;
import testBase.TestUtils;

public class testingClass extends LoadProp {

	public TestUtils Browser;

	public MedicinePage ecomPage;
	public RecipientPage RecipientPage;

	@BeforeClass()
	public void setUp() throws Exception {
		Browser = new TestUtils(driver);

		LoadBrowserProperties();
		ecomPage = new MedicinePage(driver);
		Browser = new TestUtils(driver);
		RecipientPage = new RecipientPage(driver);
		Elements_Medicines.Medicine_PageProperties();
		Browser.openUrl("file:///Users/sen/Desktop/txt.html");
		
	}

	@Test()
	public void test() throws Exception {
		
		
		WebElement myDiv = driver.findElement(By.xpath("//div[@id='one']"));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		String divText = (String) jse.executeScript("return arguments[0].childNodes[0].nodeValue;", myDiv);
		
		System.out.println("only first text" + divText);
	}
	
	@AfterClass()
	public void close() throws Exception {
		Browser.quit();
	
	}


}
