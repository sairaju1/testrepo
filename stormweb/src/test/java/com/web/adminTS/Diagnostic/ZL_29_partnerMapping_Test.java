// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZL_29_partnerMapping_Test extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public String testCode, partnerName, partnerBookingcode_Test;
	public int partnerPrice_test, partnerDiscount;

	@BeforeClass(groups = { "Admin", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		AdminPage.AdminMenu_subMenu("Zoylo Lab Management", "False", "Zoylo Lab Master Tests");
		partnerName = partner_name;
		partnerBookingcode_Test = Browser.generateRandomString(5).toUpperCase();
		partnerPrice_test = 100;
		partnerDiscount = 90;
	}

	@Test(groups = { "Admin", "High" }, priority = 1)
	public void partnerMapping_TestAddEdit() throws Exception {

		Browser.waitFortheElementXpath(Elements_Admin.lab_masterTest_listingHeader);
		Thread.sleep(2000);
		testCode = Browser.getTextByXpath(Elements_Admin.lab_partnerMapping_listing_firstCode); // firstTestCode

		AdminPage.AdminMenu_subMenu("Zoylo Partner Mapping", "False", "Zoylo Partner Mapping Details");

		Browser.remove_Document(Environment, "zoyloPartnerService", "zoyloServiceCode", testCode);

		Browser.clickOnTheElementByXpath(Elements_Admin.lab_partnerMapping_addBtn);

		// Test Add
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("(" + Elements_Admin.lab_partnerMapping_selectPartner + ")[1]");
		Thread.sleep(1000);
		Browser.enterTextByXpath("(" + Elements_Admin.lab_partnerMapping_selectPartnerInput + ")[1]", partnerName);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + partnerName + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_partnerMapping_selectTestDrp);
		Thread.sleep(1000);
		Browser.enterTextByXpath(Elements_Admin.lab_partnerMapping_selectTestInput, testCode);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + testCode + "')]");
		Browser.enterTextByXpath("(" + Elements_Admin.lab_partnerMapping_BookingCode + ")[1]", partnerBookingcode_Test);
		Browser.enterTextByXpath("(" + Elements_Admin.lab_partnerMapping_price + ")[1]",
				Integer.toString(partnerPrice_test));
		Thread.sleep(1000);
		Browser.enterTextByXpath("(" + Elements_Admin.lab_partnerMapping_finalPrice + ")[1]",
				Integer.toString(partnerDiscount));
		Thread.sleep(1000);
		Browser.enterTextByXpath(Elements_Admin.lab_partnerMapping_TestListCode, "LIS_" + partnerBookingcode_Test);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterTest_saveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.lab_partnerMapping_saveMsg);

		// Edit Check
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_partnerMapping_subMenu);
		Browser.waitFortheElementXpath(Elements_Admin.lab_partnerMapping_addBtn);
		Browser.enterTextByXpath(Elements_Admin.lab_partnerMapping_search, partnerBookingcode_Test);
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + partnerBookingcode_Test + "')]");

		Browser.waitFortheElementXpath("//tr/td[contains(., '" + partnerBookingcode_Test + "')]/following-sibling::td[2]");

		System.out.println("**************************** Test Mapping done ****************************");
		
		// EDIT Test mapping
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + partnerBookingcode_Test + "')]/following-sibling::td[4]/i");
		if(driver.findElements(By.xpath(Elements_Admin.lab_partnerMapping_editSaveBtn)).size() == 0) {
			Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + partnerBookingcode_Test + "')]/following-sibling::td[4]/i");
		}
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_partnerMapping_editSaveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.lab_partnerMapping_editSaveMsg);

	}

	@AfterClass(groups = { "Admin", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}