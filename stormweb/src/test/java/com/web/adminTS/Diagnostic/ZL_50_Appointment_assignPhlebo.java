// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZL_50_Appointment_assignPhlebo extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public String firstName, number, email, pinCode, testName, profileName, packageName, gender, age, city, payMode,
			Token, zonepinAPIcodes, testCost;
	public Person p;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		p = Browser.personData();
		firstName = p.getFirstName();
		number = "9" + Browser.generateRandomNumber(9);
		email = p.getEmail();
		gender = "Male";
		age = "31";
		city = operatingCity_1;
		payMode = "CASH";
		testName = test_Name;
	}

	@Test(groups = { "ADMINDC", "High" })
	public void addAppointment() throws Exception {

		// Add Appointment segment
		AdminPage.lab_addAppointmentList_navigate();
		Thread.sleep(1000);
		AdminPage.lab_addAppointmentButton_click();
		AdminPage.lab_addAppointment_customerInfo(firstName, number, email, gender, age, city);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_nextBtn);
		Browser.waitFortheElementXpath(
				"//span[text()='Customer Name : ']/following-sibling::strong[contains(., '" + firstName + "')]");

		Thread.sleep(2000);
		AdminPage.lab_addAppointment_serviceDateSlot();
		// Select Branch
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_Branch);
		Thread.sleep(1000);
		if (driver.findElements(By.xpath(Elements_Admin.lab_addAppointment_BranchNoData)).size() > 0) {
			Assert.fail("No partners mapped to the selected operating city : " + city);
		} else {
			Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_firstBranch);
		}
		// Add Test & Profile
		AdminPage.lab_addAppointment_selectTestProfile(testName);
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + testName
				+ "')]/following-sibling::td[5]/div/div/div/div/div[@class='input-group__selections']"); // SelectPatient
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("(//div[@class='list__tile__title' and contains(., '" + firstName + "')])[1]"); // PrimaryPatient
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + testName + "')]/following-sibling::td[2]");
		testCost = Browser.getTextByXpath("//tr/td[text()='" + testName + "']/following-sibling::td[4]");
		AdminPage.lab_addAPpointment_selectPayMode_ConfirmBooking(payMode);
		Browser.waitFortheElementXpath(Elements_Admin.lab_addAppointment_addBtn);
		AdminPage.lab_addAppointment_searchList("Mobile Number", number);

		// Verify Appointment and Assign phlebo
		Browser.scrollbyxpath("//tr/td[text()='+91" + number + "']/following-sibling::td[8]"); // ScrolltoActions
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//tr/td[text()='+91" + number + "']/following-sibling::td[6]"); // statusDrp
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='VERIFIED']"); // VerifyValue
		// Verify popup
		Browser.waitFortheElementXpath(Elements_Admin.lab_appointment_verifyHeader);
		Browser.clickOnTheElementByXpath("(" + Elements_Admin.lab_appointment_yesBtn + ")[2]");
		Thread.sleep(2000);
		driver.navigate().refresh();

		// Assign
		AdminPage.lab_addAppointment_searchList("Mobile Number", number);
		Browser.scrollbyxpath("//tr/td[text()='+91" + number + "']/following-sibling::td[8]"); // ScrolltoActions
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//tr/td[text()='+91" + number + "']/following-sibling::td[6]"); // statusDrp
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='ASSIGNED']"); // AssignValue

		// Assign Popup
		Browser.waitFortheElementXpath(Elements_Admin.lab_appointment_assignPhleboHeader);
		Browser.clickOnTheElementByXpath("(" + Elements_Admin.lab_appointment_assignPhleboDrp + ")[2]");
		Thread.sleep(1000);
		String phleboName = Browser.getTextByXpath(Elements_Admin.lab_appointment_assignFirstPhlebo);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(
				"(//div[@class='list__tile__title' and contains(., '" + phleboName + "')])[2]");
		Browser.clickOnTheElementByXpath("(" + Elements_Admin.lab_appointment_yesBtn + ")[4]"); // YesButton
		Thread.sleep(2000);
		driver.navigate().refresh();
		Thread.sleep(3000);
		// View appointment check
		AdminPage.lab_addAppointment_searchList("Mobile Number", number);
		Browser.scrollbyxpath("//tr/td[text()='+91" + number + "']/following-sibling::td[8]"); // ScrolltoActions
		int actualListAmount = Integer
				.parseInt(Browser.getTextByXpath("//tr/td[text()='+91" + number + "']/following-sibling::td[7]"));
		Assert.assertEquals(actualListAmount, Integer.parseInt(testCost));

		// VIEW APPOINTMENT
		Browser.scrollbyxpath("//tr/td[text()='+91" + number + "']");
		Browser.clickOnTheElementByXpath("//tr/td[text()='+91" + number + "']"); // viewApt
		Thread.sleep(2000);
		// View popup
		Browser.waitFortheElementXpath(Elements_Admin.lab_appointment_viewHeader);
		Browser.waitFortheElementXpath(Elements_Admin.lab_appointment_viewStatusAssigned);
		Browser.waitFortheElementXpath(
				"(//div[@class='viewAptField' and contains(., 'Name')]/following-sibling::div[contains(., '" + firstName
						+ "')])[2]");
		Browser.ScrollDown();
		Browser.waitFortheElementXpath(
				"//div[@class='viewAptField' and contains(., 'Name')]/following-sibling::div[contains(., '" + phleboName
						+ "')]");
		int viewAmount = Integer.parseInt(Browser.getTextByXpath(Elements_Admin.lab_appointment_viewTotalAmt));
		Assert.assertEquals(viewAmount, Integer.parseInt(testCost));
		if (payMode.equalsIgnoreCase("CASH")) {
			int balAmt = Integer.parseInt(Browser.getTextByXpath(Elements_Admin.lab_appointment_viewBalAmt));
			Assert.assertEquals(balAmt, viewAmount);
		} else {
			int paidAmt = Integer.parseInt(Browser.getTextByXpath(Elements_Admin.lab_appointment_viewPaidAmt));
			Assert.assertEquals(paidAmt, viewAmount);
		}
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterEmail", email);
		Browser.quit();
	}

}