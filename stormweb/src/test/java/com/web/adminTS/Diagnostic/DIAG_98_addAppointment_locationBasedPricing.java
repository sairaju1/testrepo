// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class DIAG_98_addAppointment_locationBasedPricing extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public String firstName, testName, location_1, location_2, location_1_price, location_2_price, number, email,
			gender, age, actualCost_location2, actualCost_location1;
	public Person p;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		p = Browser.personData();
		testName = test_Name;
		firstName = p.getFirstName();
		location_1 = operatingCity_1;
		location_2 = operatingCity_2;
		location_1_price = "1" + Browser.generateRandomNumber(2);
		location_2_price = "2" + Browser.generateRandomNumber(2);
		number = "9" + Browser.generateRandomNumber(9);
		email = p.getEmail();
		gender = "Male";
		age = "30";
	}

	@Test(groups = { "ADMINDC", "High" }, priority = 1)
	public void servicePrice_basedLocation() throws Exception {
		AdminPage.AdminMenu_subMenu("Zoylo Lab Management", "False", "Location Based Service Price");
		AdminPage.locationBased_select_city_servicetype(location_1, "Test");
		AdminPage.locationBased_search(testName);
		AdminPage.locationBased_editFinalPrice(testName, location_1_price);
		driver.navigate().refresh();
		AdminPage.locationBased_select_city_servicetype(location_2, "Test");
		AdminPage.locationBased_search(testName);
		AdminPage.locationBased_editFinalPrice(testName, location_2_price);
	}

	@Test(groups = { "ADMINDC", "High" }, dependsOnMethods = "servicePrice_basedLocation")
	public void addAppointment_locationPrices() throws Exception {

		// Location 2 Verify
		AdminPage.lab_addAppointmentList_navigate();
		AdminPage.lab_addAppointmentButton_click();
		AdminPage.lab_addAppointment_customerInfo(firstName, number, email, gender, age, location_2);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_nextBtn);
		// ***************** Service Info *****************
		Browser.waitFortheElementXpath(
				"//span[text()='Customer Name : ']/following-sibling::strong[contains(., '" + firstName + "')]");
		Thread.sleep(2000);
		AdminPage.lab_addAppointment_selectTestProfile(testName);
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + testName
				+ "')]/following-sibling::td[5]/div/div/div/div/div[@class='input-group__selections']"); // SelectPatient
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + firstName + "')]"); // PrimaryPatient
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + testName + "')]/following-sibling::td[4]");
		actualCost_location2 = Browser.getTextByXpath("//tr/td[text()='" + testName + "']/following-sibling::td[4]");
		Assert.assertEquals(actualCost_location2, location_2_price);

		driver.navigate().refresh();

		// Location 1 Verify book appointment
		AdminPage.lab_addAppointment_customerInfo(firstName, number, email, gender, age, location_1);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_nextBtn);
		Thread.sleep(2000);
		AdminPage.lab_addAppointment_serviceDateSlot();

		// Select Branch
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_Branch);
		Thread.sleep(1000);
		if (driver.findElements(By.xpath(Elements_Admin.lab_addAppointment_BranchNoData)).size() > 0) {
			Assert.fail("No partners mapped to the selected operating city : " + location_1);
		} else {
			Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_firstBranch);
		}

		AdminPage.lab_addAppointment_selectTestProfile(testName);
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + testName
				+ "')]/following-sibling::td[5]/div/div/div/div/div[@class='input-group__selections']"); // SelectPatient
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + firstName + "')]"); // PrimaryPatient
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + testName + "')]/following-sibling::td[4]");
		actualCost_location1 = Browser.getTextByXpath("//tr/td[text()='" + testName + "']/following-sibling::td[4]");
		Assert.assertEquals(actualCost_location1, location_1_price);
		AdminPage.lab_addAPpointment_selectPayMode_ConfirmBooking("CASH");
		Browser.waitFortheElementXpath(Elements_Admin.lab_addAppointment_addBtn);
		AdminPage.lab_addAppointment_searchList("Mobile Number", number);
		Browser.scrollbyxpath("//tr/td[text()='+91" + number + "']/following-sibling::td[8]"); // ScrolltoActions
		int actualListAmount = Integer
				.parseInt(Browser.getTextByXpath("//tr/td[text()='+91" + number + "']/following-sibling::td[7]"));
		Assert.assertEquals(actualListAmount, Integer.parseInt(location_1_price));
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterEmail", email);
		Browser.quit();
	}

}
