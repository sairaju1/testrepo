// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_105_Appointment_Online_PaymentLink extends LoadProp {
	public TestUtils Browser;
	public AdminPage AdminPage;
	public RecipientPage RecipientPage;
	public String firstName, number, email, pinCode, gender, age, city, payMode, packageName, packageCost, aptTime,
			coupon;
	public Person p;
	public Double value_per = 10.0;
	public int DiscountAmt, finalPrice;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		RecipientPage = new RecipientPage(driver);
		Elements_Admin.Admin_PageProperties();
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		p = Browser.personData();
		firstName = p.getFirstName();
		number = "9" + Browser.generateRandomNumber(9);
		email = p.getEmail();
		gender = "Male";
		age = "30";
		city = operatingCity_1;
		payMode = "ONLINE";
		packageName = package_Name;
		coupon = "ADMINDC";
		Browser.update_Boolean_MongoAttribute(Environment, "zoyloServicePromotion", "promotionCode", coupon,
				"activeFlag", true);
		Browser.update_MongoAttribute(Environment, "zoyloServicePromotion", "promotionCode", coupon,
				"promotionDetailInfo.promotionValueTypeCode", "AMOUNT");
		Browser.update_double_MongoAttribute(Environment, "zoyloServicePromotion", "promotionCode", coupon,
				"promotionDetailInfo.promotionalValue", value_per);
	}

	@Test(groups = { "ADMINDC", "High" }, priority = 1)
	public void LabAppointment() throws Exception {
		// Add Appointment segment
		AdminPage.lab_addAppointmentList_navigate();
		AdminPage.lab_addAppointmentButton_click();
		pinCode = AdminPage.lab_addAppointment_customerInfo(firstName, number, email, gender, age, city);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_nextBtn);

		// ***************** Service Info *****************
		Browser.waitFortheElementXpath(
				"//span[text()='Customer Name : ']/following-sibling::strong[contains(., '" + firstName + "')]");

		Thread.sleep(2000);

		// Select Date SLot
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_serviceDate);
		String currentDate = Browser.getCurrentDate();
		String modDate;
		if (Integer.parseInt(currentDate) >= 30) {
			Thread.sleep(500);
			Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_serviceNextMonthBtn);
			modDate = Browser.getModifiedDate(2);
			Thread.sleep(500);
			Browser.clickOnTheElementByXpath("//td/button/span[text()='" + modDate + "']");
		} else {
			modDate = Browser.getModifiedDate(1);
			Thread.sleep(500);
			Browser.clickOnTheElementByXpath("//td/button/span[text()='" + modDate + "']");
		}

		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_serviceSlotDrp);
		aptTime = Browser.getTextByXpath(Elements_Admin.lab_addAppointment_slotDrp);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_slotDrp);

		// Select Branch
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_Branch);
		Thread.sleep(1000);
		if (driver.findElements(By.xpath(Elements_Admin.lab_addAppointment_BranchNoData)).size() > 0) {
			Assert.fail("No partners mapped to the selected operating city : " + city);
		} else {
			Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_firstBranch);
		}

		// Add Package
		String pCost = AdminPage.lab_addAppointment_selectPackage(packageName);
		String[] pkgcost = pCost.split(" ");
		packageCost = pkgcost[0];
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + packageName
				+ "')]/following-sibling::td[5]/div/div/div/div/div[@class='input-group__selections']"); // SelectPatient
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + firstName + "')]"); // PrimaryPatient
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + packageName + "')]/following-sibling::td[4]");
		Browser.enterTextByXpath("//input[@aria-label='Enter Coupon Code']", coupon); // enterCoupon
		Browser.clickOnTheElementByXpath("//div[text()='APPLY']"); // applyBtn
		DiscountAmt = (int) Math.round(value_per);
		Browser.waitFortheElementXpath("//div[@class='discount mt-3']/lable[text()='- ₹ " + DiscountAmt + " ']"); // DiscountAmt
		finalPrice = Integer.parseInt(packageCost) - DiscountAmt;
		Browser.waitFortheElementXpath("//div[@class='total mt-3']/lable[text()='₹ " + finalPrice + "']"); // finalPrice
		AdminPage.lab_addAPpointment_selectPayMode_ConfirmBooking(payMode);
		Browser.waitFortheElementXpath(Elements_Admin.lab_addAppointment_addBtn);
	}

	@Test(groups = { "ADMINDC", "High" }, priority = 2)
	public void verifyBookingStatus() throws Exception {
		AdminPage.lab_addAppointment_searchList("Mobile Number", number);
		Browser.scrollbyxpath("//tr/td[text()='+91" + number + "']/following-sibling::td[8]"); // ScrolltoActions
		Browser.waitFortheElementXpath(
				"//tr/td[text()='+91" + number + "']/following-sibling::td[text()='" + finalPrice + "']");
		Browser.waitFortheElementXpath(
				"//tr/td[text()='+91" + number + "']/following-sibling::td[contains(., 'PAYMENT_PENDING')]");
		Browser.remove_Document(Environment, "zoyloNotification", "recipientInfo.recipientPhone", "+91" + number);
		System.out.println("Removed notifications of " + number);
		Browser.clickOnTheElementByXpath(
				"//tr/td[text()='+91" + number + "']/following-sibling::td/div/div/i[@title='sendSMS']"); // smsIcon
		Browser.waitFortheElementXpath("//p[text()='Sms sent successfully']"); // smsSuccessMsg
		String fullSMS = Browser.mongoDB_getAttribute(Environment, "zoyloNotification", "recipientInfo.recipientPhone",
				"+91" + number, "messageInfo");
		System.out.println("Message content is : " + fullSMS);
		String[] link = fullSMS.split("\"messageText\" : \"Click on the below link to complete your payment process ");
		String paymentLink = link[1].replaceAll("\"}", "");
		System.out.println("Payment link is : " + paymentLink);

		Browser.openUrl(paymentLink);
		if (driver.findElements(By.xpath("//button[@class='No thanks']")).size() > 0) {
			Browser.clickOnTheElementByXpath("//button[@class='No thanks']");
		}
		RecipientPage.verify_transactionAmount_PG(Double.valueOf(finalPrice));
		RecipientPage.MakePayment("", ""); // NetBanking
		RecipientPage.diagnostic_bookingSuccessPage(packageName, aptTime);

		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.lab_addAppointmentList_navigate();
		AdminPage.lab_addAppointment_searchList("Mobile Number", number);
		Browser.scrollbyxpath("//tr/td[text()='+91" + number + "']/following-sibling::td[8]"); // ScrolltoActions
		Browser.waitFortheElementXpath(
				"//tr/td[text()='+91" + number + "']/following-sibling::td[contains(., 'SCHEDULED')]");
		Browser.waitFortheElementXpath(
				"//tr/td[text()='+91" + number + "']/following-sibling::td[contains(., '" + aptTime + "')]");
		Browser.scrollbyxpath("//tr/td[text()='+91" + number + "']");
		Browser.clickOnTheElementByXpath("//tr/td[text()='+91" + number + "']"); // viewAppointment
		Browser.waitFortheElementXpath(Elements_Admin.lab_appointment_viewHeader);
		Browser.waitFortheElementXpath(
				"(//div[@class='viewAptField' and contains(., 'Name')]/following-sibling::div[contains(., '" + firstName
						+ "')])[2]");
		Browser.waitFortheElementXpath(
				"//strong[text()=' Appointment date & time slot: ']/following-sibling::div[text()='" + aptTime + "']"); // aptTime
		Browser.waitFortheElementXpath(
				"//div[@class='viewAptField' and contains(., 'Paid Amount')]/following-sibling::div/span[text()="
						+ finalPrice + "]");
		Browser.waitFortheElementXpath(
				"//div[@class='viewAptField' and contains(., 'Balance Amount')]/following-sibling::div/span[text()='0']");
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterEmail", email);
		Browser.quit();
	}
}
