// @Author: Sagar Sen

package com.web.adminTS.Diagnostic;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ZL_28_labManagement_masterPackage extends LoadProp {
	public TestUtils Browser;
	public AdminPage AdminPage;
	public String package_testName, package_profileName, packageName, packageCode;
	int packageCost, finalPackageCost;

	@BeforeClass(groups = { "ADMINDC", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		Elements_Admin.Admin_PageProperties();
		AdminPage.AdminMenu_subMenu("Zoylo Lab Management", "False", "Zoylo Lab Master Tests");
		packageName = "P" + Browser.generateRandomString(5).toLowerCase();
		packageCode = Browser.generateRandomString(5).toUpperCase();
		packageCost = 1000;
		finalPackageCost = 950;
	}

	@Test(groups = { "ADMINDC", "High" }, priority = 1)
	public void masterPackage_addEdit() throws Exception {
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterTest_listingHeader);
		Thread.sleep(2000);
		package_testName = Browser.getTextByXpath(Elements_Admin.lab_masterTestList_firstTestName);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterProfile_subMenu);
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterProfile_listHeader);
		Thread.sleep(2000);
		package_profileName = Browser.getTextByXpath(Elements_Admin.lab_masterProfileList_firstProfileName);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_subMenu);
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterPackage_listHeader);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_addBtn);
		if (driver.findElements(By.xpath(Elements_Admin.lab_masterPackage_addHeader)).size() == 0) {
			Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_addBtn);
		}
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterPackage_addHeader);
		Browser.enterTextByXpath(Elements_Admin.lab_masterPackage_packageName, packageName);
		Browser.enterTextByXpath(Elements_Admin.lab_masterPackage_packageCode, packageCode);
		Browser.enterTextByXpath(Elements_Admin.lab_masterPackage_lisCode, "LIS_" + packageCode);
		Browser.enterTextByXpath(Elements_Admin.lab_masterPackage_tat, "1");
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_packageLevel);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_advanceLevel);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_activeChkBox);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_prescriptionChkBox);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_nextBtn);
		Browser.enterTextByXpath(Elements_Admin.lab_masterPackage_listPrice, Integer.toString(packageCost));
		Thread.sleep(2000);
		Browser.enterTextByXpath(Elements_Admin.lab_masterPackage_finalPrice, Integer.toString(finalPackageCost));
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_nextBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_testDrp);
		Browser.enterTextByXpath(Elements_Admin.lab_masterPackage_testDrpInput, package_testName);
		Browser.clickOnTheElementByXpath(
				"//div[@class='list__tile__title' and contains(., '" + package_testName + "')]");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_testAddBtn);
		Thread.sleep(1000);
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + package_testName + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_nextBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_profileDrp);
		Browser.enterTextByXpath(Elements_Admin.lab_masterPackage_profileDrpInput, package_profileName);
		Browser.clickOnTheElementByXpath(
				"//div[@class='list__tile__title' and contains(., '" + package_profileName + "')]");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_profileAddBtn);
		Thread.sleep(1000);
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + package_profileName + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_nextBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_saveBtn);
		Thread.sleep(2000);
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterPackage_listHeader);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_packageSearchBtn);
		Browser.enterTextByXpath(Elements_Admin.lab_masterPackage_packageSearchInput, packageName);
		Browser.clickOnTheElementByXpath(
				"//div[@class='list__tile__title' and contains(., '" + packageName + "')]");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_searchBtn);
		Thread.sleep(2000);
		Browser.waitFortheElementXpath("(//tr/td[contains(., '" + packageCode + "')])[1]");
		
		String listingPackagePrice = Browser.getTextByXpath("(//tr/td[contains(., '" + packageCode + "')])[1]/following-sibling::td[1]");
		Assert.assertEquals(Integer.parseInt(listingPackagePrice), finalPackageCost);
		
		//EDIT
		Browser.clickOnTheElementByXpath("(//tr/td[contains(., '" + packageCode + "')])[1]/following-sibling::td[3]/i");
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_activeChkBox);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_nextBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_nextBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_nextBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_nextBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_masterPackage_saveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.lab_masterPackage_listHeader);
	}

	@AfterClass(groups = { "ADMINDC", "High" })
	public void tearDown() throws Exception {
		Browser.remove_Document(Environment, "zoyloLabsMasterPackage", "packageCode", packageCode);
		Browser.quit();
	}
}