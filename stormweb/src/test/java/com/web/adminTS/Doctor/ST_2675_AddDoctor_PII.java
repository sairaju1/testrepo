//@Author: Sagar Sen

package com.web.adminTS.Doctor;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ST_2675_AddDoctor_PII extends LoadProp{
	public TestUtils Browser;
	public AdminPage AdminPage;
	public RecipientPage RecipientPage;
	public String docEmail, doctorName, LastName, Search_specialization, mobileNumber, YOS;
	
	@BeforeClass(groups={"Admin" , "High"})
	public void launchBrowser() throws Exception{
		LoadBrowserProperties(); // Create driver instance and launch the browser
		Elements_Admin.Admin_PageProperties();// loading UI Page Elements / Locators
		AdminPage = new AdminPage(driver); // Loading Pages
		RecipientPage= new RecipientPage(driver);
		Browser= new TestUtils(driver);
		Browser.openUrl(EnvironmentAdminURL);
	}
	
	@DataProvider(name="DocInfo")
	public Object[][] DocDetails() throws Exception
	{
		Object[][] DocData=TestUtils.getTableArray("TestData/adminOne.xls", "2675", "ST2675");
		return(DocData);
	}
	
	@Test(priority=1, enabled=true, dataProvider="DocInfo", groups={"Admin" , "High"})
	public void addDoctor_PII(String Language, String Gender, String tag, String Specialization,
	String LOP, String imgPath, String councilName, String Qualification, String stateCouncilName, String medNum, String roleName, String clinicName, String MON_Morning_Start,
	String MON_Morning_End, String MON_Afternoon_Start, String MON_Afternoon_End, String MON_Evening_Start, String MON_Evening_End, String MON_Night_Start, String MON_Night_End,
	String zfc, String followUpDays, String generalFee, String followUpFee, String ivfFee, String ivfFollowUpFee) throws Exception{		
		docEmail=Browser.generateRandomAlphaNumeric(7).toLowerCase()+"@zoylo.com";
		mobileNumber="7"+Browser.generateRandomNumber(9);
		doctorName="A"+Browser.generateRandomString(6).toLowerCase();
		LastName="B"+Browser.generateRandomString(6).toLowerCase();
		Search_specialization=Specialization;
		YOS="2004-10-12";
		AdminPage.adminLogin(Admin_Username, Admin_Password);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Doctor Registration");
		
		//Primary Info Tab
		AdminPage.doctor_PrimaryInformation(Language, doctorName, LastName, docEmail, mobileNumber, YOS, Gender, tag, Search_specialization, LOP, imgPath);
		AdminPage.waitUntil_LoaderInvisible("//img[@class='loaderImage']");
		Browser.waitFortheElementXpath("//p[contains(., 'Doctor primary information has been saved successfully.')]");
		AdminPage.waitUntil_LoaderInvisible("//img[@class='loaderImage']");
		
		//Registration Verification Tab
		AdminPage.doctor_RegistrationVerification(councilName, Qualification, stateCouncilName, medNum);
		Browser.waitFortheElementXpath("//p[contains(., 'Doctor Verification Save Successfully.')]");
		
		//Practice Info Tab
		AdminPage.doctor_PracticeInfo(roleName, clinicName);
		String actualClinicName=Browser.getTextByXpath("(//td[@class='text-xs-right'])[1]"); //Get clinic name from table
		Assert.assertTrue(actualClinicName.contains(clinicName));
		Thread.sleep(5000);
		
		// Work Days and Timings
		AdminPage.doctor_workDaysAndTimings(clinicName, MON_Morning_Start, MON_Morning_End, MON_Afternoon_Start, MON_Afternoon_End, MON_Evening_Start, MON_Evening_End, MON_Night_Start, MON_Night_End);
		
		//Fee Structure Tab
		AdminPage.doctor_feeStructure(clinicName, zfc, followUpDays, generalFee, followUpFee, ivfFee, ivfFollowUpFee);
		
		//SEO tab
		AdminPage.doctor_SEO();
		Browser.waitFortheElementXpath("//p[contains(., 'SEO Details Saved Successfully.')]");
		
		//Additional Info Tab
		//AdminPage.waitUntil_LoaderInvisible("//img[@class='loaderImage']");
		Thread.sleep(5000);
		AdminPage.doctor_AdditionalInformation();
		System.out.println("Doctor registration completed.");
	}
	
	@Test(priority=2, enabled=true, groups={"Admin", "High"})
	public void PIIDoctor_ActiveInactiveCheck_LinkingQue() throws Exception{
		
		//Admin activate linking queue
		//AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Application Management");
		Browser.clickOnTheElementByXpath("//div[contains(text(),'Application Management')]");
		Thread.sleep(5000);
		AdminPage.linkingQueue_Search_click_Doctor(doctorName);
		Browser.clickOnTheElementByXpath(Elements_Admin.AppManagement_LinkingQue_ActiveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.AppManagement_LinkingQue_DeActiveBtn);
		
		//Check visibility in front end
		Browser.openUrl(EnvironmentURL);
		RecipientPage.GenericSearch("Doctors", doctor_location2, Search_specialization);
		//RecipientPage.home_location_data_Search(doctor_location2, Search_specialization); //
		AdminPage.checkDoctorVisibility_RecipientEnd(doctorName);
	}
	
	@Test(priority=3, enabled=true, groups={"Admin", "High"})
	public void PIIDoctor_InactiveValidationCheck_DoctorForm() throws Exception{
		Browser.openUrl(EnvironmentAdminURL);
		Thread.sleep(2000);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Application Management");
		for(int i=0;i<=doctorName.length()-1; i++)
	    {
	    	char key = doctorName.charAt(i);
	    	WebDriverWait wait = (new WebDriverWait(driver, 60));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Elements_Admin.AppManagement_Doctor_SearchBar)));
			Thread.sleep(500);
			driver.findElement(By.xpath(Elements_Admin.AppManagement_Doctor_SearchBar)).sendKeys(Character.toString(key));
			System.out.println("Texted = "+doctorName);
			Reporter.log("Text Entered="+doctorName);
	    	Thread.sleep(600);	
	    }
		
		driver.findElement(By.xpath(Elements_Admin.AppManagement_Doctor_SearchBar)).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(By.xpath(Elements_Admin.AppManagement_Doctor_SearchBar)).sendKeys(Keys.ENTER);
		Browser.waitFortheElementXpath("(//td[contains(., '"+doctorName+"')])[1]"); // Wait for table of doctor
		Browser.scrollbyxpath("(//i[@class='fa icon fa-edit'])[1]"); // Edit button xpath
		Browser.clickOnTheElementByXpath("(//i[@class='fa icon fa-edit'])[1]"); //Click Edit Button
		Thread.sleep(7000);
		driver.findElement(By.xpath("//label[contains(., 'Is Active* ?')]")).click();//Inactive checkbox
		Browser.waitFortheElementXpath("//p[contains(., 'Doctor is linked with any clinic, please deactivate the linking to deactivate the doctor.')]");
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Application Management");
		Thread.sleep(5000);
		AdminPage.linkingQueue_Search_click_Doctor(doctorName);
		Browser.clickOnTheElementByXpath(Elements_Admin.AppManagement_LinkingQue_DeActiveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.AppManagement_LinkingQue_ActiveBtn);
		
	}
	
	@AfterClass(groups={"Admin" , "High"})
	public void tearDown() throws Exception{
		
		Browser.remove_Document(Environment, "zoyloServiceResource", "emailId", docEmail);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", docEmail);
		Browser.quit();
	}

}