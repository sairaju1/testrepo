//@Author: Sagar Sen

package com.web.adminTS.Doctor;
import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ST_3352_lookUp_QualificationAdd extends LoadProp{
	public TestUtils Browser;
	public AdminPage AdminPage;
	public String qLookUp="PROVIDER_DOCTOR_QUALIFICATION";
	
	@BeforeClass()
	public void launchBrowser() throws Exception{
		LoadBrowserProperties(); // Create driver instance and launch the browser
		Elements_Admin.Admin_PageProperties();// loading UI Page Elements / Locators
		AdminPage = new AdminPage(driver); // Loading Pages
		Browser= new TestUtils(driver);
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(Admin_Username, Admin_Password);
		//AdminPage.AdminMenu_subMenu("false", "false", "Reference Data");
		Browser.clickOnTheElementByXpath("//div[contains(text(),'Reference Data')]");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., 'Lookup')]");
		Browser.enterTextByID("searchLookTypeCode", qLookUp);
		Browser.clickOnTheElementByID("searchLookup");
		Browser.waitforTextbyxpath("(//td[@class='text-xs-center'])[1]", qLookUp);
		Browser.clickOnTheElementByXpath("//td[contains(., '"+qLookUp+"')]/following-sibling::td[3]");
		Browser.clickOnTheElementByXpath("//a[@href='#tabLookupValue']");
	}
	
	@DataProvider(name="QInfo")
	public Object[][] DocDetails() throws Exception
	{
		Object[][] QData=TestUtils.getTableArray("TestData/adminOne.xls", "3352", "ST_3352");
		return(QData);
	}
	
	@Test(dataProvider="QInfo")
	public void addQualification_LookUp(String code, String name, String sequence) throws Exception{
		
		//Browser.clickOnTheElementByID("addLookupValue");
		Browser.clickOnTheElementByXpath("//button[@id='addLookupValue']//div[@class='btn__content']");
		Thread.sleep(1000);
		Browser.enterTextByID("addValueCode", code);
		Thread.sleep(1000);
		Browser.enterTextByID("addValueName", name);
		Thread.sleep(1000);
		Browser.enterTextByID("addDisplaySequence", sequence);
		Thread.sleep(1000);
		Browser.clickOnTheElementByID("addLookUpValueSave");
		
		Boolean x=Browser.isElementPresent(By.xpath("//p[contains(., 'Lookup value saved successfully')]"));
		
		if(x==false){
			System.out.println("Qualification failed to add is : "+name);
			Browser.clickOnTheElementByID("addLookUpValueCancel");
		}else{
			System.out.println("Qualification added : "+name);
			
		}
		
		Thread.sleep(2000);
	}
	
	@AfterClass()
	public void tearDown() throws Exception{
		Browser.quit();
	}
}
