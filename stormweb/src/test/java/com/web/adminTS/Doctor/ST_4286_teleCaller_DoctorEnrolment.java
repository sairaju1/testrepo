//@Author: Sagar Sen

package com.web.adminTS.Doctor;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ST_4286_teleCaller_DoctorEnrolment extends LoadProp {
	public TestUtils Browser;
	public AdminPage adminPage;
	public List<String> names;
	public String name, emailID;
	public int sizes = 2;
	public int i;

	@BeforeClass(groups = { "Admin", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Elements_Admin.Admin_PageProperties();
		adminPage = new AdminPage(driver);
		Browser = new TestUtils(driver);
		Browser.openUrl(EnvironmentAdminURL);
		adminPage.adminLogin(Admin_Username, Admin_Password);
		adminPage.AdminMenu_subMenu("Kellton Menu Items", "True", "Enroll Doctor");
		names = new ArrayList<String>(sizes);
	}

	@DataProvider(name = "enrollData")
	public Object[][] enrollData() throws Exception {
		Object[][] DocEnrolData = TestUtils.getTableArray("TestData/adminOne.xls", "docEnrol", "ST4286");
		return (DocEnrolData);
	}

	@Test(groups = { "Admin", "High" }, priority = 1, enabled = true, dataProvider = "enrollData")
	public void enrolDoctor(String Salutation, String middleName, String lastName, String yof, String Qualification,
			String otherSpec, String regNum, String email, String Number, String clinicName, String onlineWorking,
			String clinicWorking, String packageOffered) throws Exception {
		Thread.sleep(5000);
		name = "A" + Browser.generateRandomString(4).toLowerCase();
		Browser.clickOnTheElementByXpath(Elements_Admin.docEnrol_salutation);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='" + Salutation + "']");
		Browser.enterTextByID(Elements_Admin.docEnrol_firstname, name);
		Browser.enterTextByID(Elements_Admin.docEnrol_middleName, middleName);
		Browser.enterTextByID(Elements_Admin.docEnrol_lastName, lastName);
		Browser.clickOnTheElementByXpath(Elements_Admin.docEnrol_gender);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='Male']"); // ChoseMale
		Browser.enterTextByName(Elements_Admin.docEnrol_yof, yof);
		Browser.enterTextByXpath(Elements_Admin.docEnrol_qualification, Qualification);
		Browser.clickOnTheElementByXpath(Elements_Admin.docEnrol_spec);
		Browser.clickOnTheElementByXpath(Elements_Admin.docEnrol_firstSpec);
		Browser.enterTextByXpath(Elements_Admin.docEnrol_otherSpec, otherSpec);
		Browser.enterTextByXpath(Elements_Admin.docEnrol_regNum, regNum);
		if (email.equalsIgnoreCase("TRUE")) {
			emailID = Browser.generateRandomAlphaNumeric(6).toLowerCase() + "@gmail.com";
			Browser.enterTextByXpath(Elements_Admin.docEnrol_email, emailID);
		} else {
			Browser.enterTextByXpath(Elements_Admin.docEnrol_email, "");
		}
		if (Number.equalsIgnoreCase("TRUE")) {
			Browser.enterTextByXpath(Elements_Admin.docEnrol_num, "7" + Browser.generateRandomNumber(9));
		} else {
			Browser.enterTextByXpath(Elements_Admin.docEnrol_num, "");
		}
		if (clinicName.equalsIgnoreCase("TRUE")) {
			Browser.enterTextByXpath(Elements_Admin.docEnrol_clinicName,
					"A" + Browser.generateRandomString(4).toLowerCase());
		} else {
			Browser.enterTextByXpath(Elements_Admin.docEnrol_clinicName, "");
		}
		Browser.clickOnTheElementByXpath(Elements_Admin.docEnrol_country);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='India']");
		Browser.clickOnTheElementByXpath(Elements_Admin.docEnrol_state);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='Punjab']");
		Browser.clickOnTheElementByXpath(Elements_Admin.docEnrol_city);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='Jalandhar']");
		Browser.enterTextByXpath(Elements_Admin.docEnrol_address1, Browser.generateRandomAlphaNumeric(10));
		Browser.enterTextByXpath(Elements_Admin.docEnrol_address2, Browser.generateRandomAlphaNumeric(10));
		Browser.enterTextByXpath(Elements_Admin.docEnrol_landmark, "Z" + Browser.generateRandomString(4).toLowerCase());
		Browser.enterTextByID(Elements_Admin.docEnrol_location, "Y" + Browser.generateRandomString(4).toLowerCase());
		Browser.enterTextByXpath(Elements_Admin.docEnrol_pinCode, "500072");
		Browser.enterTextByXpath(Elements_Admin.docEnrol_lat, "19.234");
		Browser.enterTextByXpath(Elements_Admin.docEnrol_long, "88.564");
		Browser.enterTextByXpath(Elements_Admin.docEnrol_clinicNum, "7" + Browser.generateRandomNumber(9));
		if (onlineWorking.equalsIgnoreCase("TRUE")) {
			Browser.clickOnTheElementByXpath("(//label[contains(., 'Wednesday')])[1]"); // onlineWednesday
			Browser.enterTextByXpath(Elements_Admin.docEnrol_onlineTimings, "3pm to 6pm");
			Browser.enterTextByName(Elements_Admin.docEnrol_onlineFee, "250");
		} else {
			Browser.scrollbyxpath("//li[text()='Clinic Working Days']"); // ClinicWorkingInfo
		}
		if (clinicWorking.equalsIgnoreCase("TRUE")) {
			Browser.clickOnTheElementByXpath("(//label[contains(., 'Wednesday')])[2]"); // clinicWednesday
			Browser.enterTextByXpath(Elements_Admin.docEnrol_clinicTimings, "9am to 1pm");
			Browser.enterTextByXpath(Elements_Admin.docEnrol_clinicFee, "300");
		} else {
			Browser.scrollbyxpath(Elements_Admin.docEnrol_clinicFee);
		}
		if (packageOffered.equalsIgnoreCase("TRUE")) {
			Browser.clickOnTheElementByXpath(Elements_Admin.docEnrol_packageBtn);
			Browser.enterTextByXpath(Elements_Admin.docEnrol_package_discount, "20");
			Browser.enterTextByXpath(Elements_Admin.docEnrol_package_extraDetail, "Extra details entered.");
		} else {
			Browser.scrollbyxpath(Elements_Admin.docEnrol_package_extraDetail);
		}
		Browser.scrollbyxpath(Elements_Admin.docEnrol_save);
		Browser.clickOnTheElementByXpath(Elements_Admin.docEnrol_save);
		Thread.sleep(2000);
		if (driver.findElements(By.xpath("//p[contains(., 'Doctor Details have been saved succesfully.')]"))
				.size() != 0) {
			Browser.waitFortheElementXpath("//p[contains(., 'Doctor Details have been saved succesfully.')]");
			System.out.println("Save success");
			names.add(name);
		}
		driver.navigate().refresh();
	}

	@Test(groups = { "Admin", "High" }, priority = 2, enabled = true)
	public void enrolListing() throws Exception {
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Admin.docEnrol_ListingBtn);
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Admin.docEnrol_TeleCallerBtn);
		Browser.clickOnTheElementByID(Elements_Admin.docEnrol_TeleCaller_search);
		Browser.scrollbyxpath(Elements_Admin.docEnrol_TeleCaller_lastPage);
		Browser.clickOnTheElementByXpath(Elements_Admin.docEnrol_TeleCaller_lastPage);
		for (i = 0; i < sizes - 1; i++) {
			if (names.get(i) != "") {
				System.out.println("Valued search");
				Thread.sleep(2000);
				int x = driver.findElements(By.xpath("//tr/td[1]")).size();
				for (int y = 1; y <= x; y++) {
					String actualName = Browser.getTextByXpath("(//tr/td[1])[" + y + "]");
					if (actualName.contains(names.get(i))) {
						System.out.println("Name matched........");
						// Edit record ...
						String actualEmail = Browser.getTextByXpath("(//tr/td[3])[" + y + "]");
						Assert.assertEquals(actualEmail, emailID);
						Browser.scrollbyxpath("(//tr/td[7])[" + y + "]");
						Browser.clickOnTheElementByXpath("(//tr/td[7])[" + y + "]/div/button");
						Browser.waitFortheElementXpath(Elements_Admin.docEnrol_TeleCaller_EditHeader);
						Browser.enterTextByXpath(Elements_Admin.docEnrol_TeleCaller_Edit_text, "Tended.");
						Browser.clickOnTheElementByID(Elements_Admin.docEnrol_TeleCaller_EditSave);
						Browser.waitFortheElementXpath(Elements_Admin.docEnrol_TeleCaller_EditSaveMsg);
					}
				}
			}
		}
	}

	@AfterClass(groups = { "Admin", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}
}
