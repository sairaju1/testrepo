//@Author: Sagar Sen

package com.web.adminTS.Doctor;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ST_2842_LiningQueue_FeeValidation_Doctor extends LoadProp{
	public TestUtils Browser;
	public AdminPage AdminPage;
	public RecipientPage RecipientPage;
	public String docEmail, doctorName, LastName, mobileNumber, Search_specialization, YOS;
	
	@BeforeClass(groups={"Admin" , "High"})
	public void launchBrowser() throws Exception{
		LoadBrowserProperties(); // Create driver instance and launch the browser
		Elements_Admin.Admin_PageProperties();// loading UI Page Elements / Locators
		AdminPage = new AdminPage(driver); // Loading Pages
		RecipientPage= new RecipientPage(driver);
		Browser= new TestUtils(driver);
		Browser.openUrl(EnvironmentAdminURL);
	}
	
	@DataProvider(name="DocInfo")
	public Object[][] DocDetails() throws Exception
	{
		Object[][] DocData=TestUtils.getTableArray("TestData/adminOne.xls", "2675", "ST2842");
		return(DocData);
	}
	
	@Test(dataProvider="DocInfo", priority=1, enabled=true, groups={"Admin", "High"})
	public void feeValidation_addDoc(String Language, String Gender, String tag, String Specialization,
			String LOP, String imgPath, String councilName, String Qualification, String stateCouncilName, String medNum, String roleName, String clinicName, String MON_Morning_Start,
			String MON_Morning_End, String MON_Afternoon_Start, String MON_Afternoon_End, String MON_Evening_Start, String MON_Evening_End, String MON_Night_Start, String MON_Night_End) throws Exception{
		
		docEmail=Browser.generateRandomAlphaNumeric(6).toLowerCase()+"@auto.com";
		doctorName="A"+Browser.generateRandomString(6).toLowerCase();
		LastName="L"+Browser.generateRandomString(4).toLowerCase();
		mobileNumber="7"+Browser.generateRandomNumber(9);
		Search_specialization=Specialization;
		YOS="2016-10-23";
		AdminPage.adminLogin(Admin_Username, Admin_Password);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Doctor Registration");
		
		//Primary Info Tab
		AdminPage.doctor_PrimaryInformation(Language, doctorName, LastName, docEmail, mobileNumber, YOS, Gender, tag, Specialization, LOP, imgPath);
		Browser.waitforTextbyxpath("//p[contains(., 'Doctor primary information has been saved successfully.')]", "Doctor primary information has been saved successfully.");
		Thread.sleep(2000);
		
		//Registration Verification Tab
		AdminPage.doctor_RegistrationVerification(councilName, Qualification, stateCouncilName, medNum);
		Browser.waitforTextbyxpath("//p[contains(., 'Doctor Verification Save Successfully.')]", "Doctor Verification Save Successfully.");
		Thread.sleep(2000);
		
		//Practice Info Tab
		AdminPage.doctor_PracticeInfo(roleName, clinicName);
		String actualClinicName=Browser.getTextByXpath("(//td[@class='text-xs-right'])[1]"); //Get clinic name from table
		Assert.assertTrue(actualClinicName.contains(clinicName));
		
		// Work Days and Timings
		AdminPage.doctor_workDaysAndTimings(clinicName, MON_Morning_Start, MON_Morning_End, MON_Afternoon_Start, MON_Afternoon_End, MON_Evening_Start, MON_Evening_End, MON_Night_Start, MON_Night_End);
		AdminPage.waitUntil_LoaderInvisible("(//img[@class='loaderImage'])[1]");
		Browser.ScrollUp();
	}
	
	@Test(priority=2, enabled=true, groups={"Admin", "High"})
	public void feeValidation_Verify() throws Exception{
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Application Management");
		Thread.sleep(5000);
		AdminPage.linkingQueue_Search_click_Doctor(doctorName);
		Browser.clickOnTheElementByXpath(Elements_Admin.AppManagement_LinkingQue_ActiveBtn);
		Browser.waitFortheElementXpath("//p[contains(., 'First define fee for doctor.')]");
	}
	
	@AfterClass(groups={"Admin" , "High"})
	public void tearDown() throws Exception{
		Browser.remove_Document(Environment, "zoyloServiceResource", "emailId", docEmail);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", docEmail);
		Browser.quit();
	}
}
