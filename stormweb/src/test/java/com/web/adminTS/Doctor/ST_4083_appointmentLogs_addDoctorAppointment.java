//@Author: Sagar Sen

package com.web.adminTS.Doctor;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ST_4083_appointmentLogs_addDoctorAppointment extends LoadProp {

	public TestUtils Browser;
	public AdminPage adminPage;
	public String number, firstName, lastName, email, date, time;

	@BeforeClass(groups = { "Admin", "High" })
	public void launchBrowser() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		adminPage = new AdminPage(driver);
		Elements_Admin.Admin_PageProperties();
		Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", "+919553456665");
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "Prabhusch@gmail.com");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "Prabhusclinic@gmail.com");
		date = Browser.getCurrentDate();
		time = "20:00";
	}

	@Test(groups = { "Admin", "High" }, priority = 1, enabled = true)
	public void aptLogs_addDoctorAppointment_COD() throws Exception {

		number = "6" + Browser.generateRandomNumber(9);
		firstName = "A" + Browser.generateRandomString(5).toLowerCase();
		lastName = "L" + Browser.generateRandomString(3).toLowerCase();
		email = firstName + "@gmail.com";

		Browser.openUrl(EnvironmentAdminURL);
		adminPage.adminLogin(Admin_Username, Admin_Password);
		adminPage.AdminMenu_subMenu("Kellton Menu Items", "True", "Appointment Logs");
		Thread.sleep(5000);
		Browser.waitFortheElementXpath(Elements_Admin.doc_appointmentLog_header);
		adminPage.appointmentLogs_addAppointment_Doctor(number, firstName, lastName, email, "Kuduremukha, Karnataka", "Dr Prabhus Chintamaneni","Prabhusclinic", date, "NIGHT", time, "COD");
		//Browser.waitUntil_InvisiblityByXpath(Elements_Admin.doc_appointmentLog_addAppointmentClinicHeader);
		Thread.sleep(6000);

		// Non kellton Appointments verification
		adminPage.doctorAppointmentSearch_appointmentsMenu("Appointment Date", "Payment Pending");
		Browser.waitFortheElementXpath(Elements_Admin.Admin_Appointments_Doctor_tableList);
		String table_PatientName = Browser.getTextByXpath(Elements_Admin.Admin_Appointments_Doctor_tablePatientName);
		Assert.assertTrue(table_PatientName.contains(firstName));
	}

	@Test(groups = { "Admin", "High" }, priority = 2, enabled = true)
	public void aptLogs_addDoctorAppointment_ONLINE() throws Exception {

		Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", "+919553456665");
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "Prabhusch@gmail.com");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "Prabhusclinic@gmail.com");

		number = "6" + Browser.generateRandomNumber(9);
		firstName = "A" + Browser.generateRandomString(5).toLowerCase();
		lastName = "L" + Browser.generateRandomString(3).toLowerCase();
		email = firstName + "@gmail.com";

		adminPage.AdminMenu_subMenu("Kellton Menu Items", "True", "Appointment Logs");
		Thread.sleep(5000);
		Browser.waitFortheElementXpath(Elements_Admin.doc_appointmentLog_header);
		adminPage.appointmentLogs_addAppointment_Doctor(number, firstName, lastName, email, "Kudremukh", "Dr Prabhus Chintamaneni","Prabhusclinic", date, "NIGHT", time, "ONLINE");
		//Browser.waitUntil_InvisiblityByXpath(Elements_Admin.doc_appointmentLog_addAppointmentClinicHeader);
		Thread.sleep(6000);

		// Non kellton Appointments verification
		adminPage.doctorAppointmentSearch_appointmentsMenu("Appointment Date", "Payment Pending");
		Browser.waitFortheElementXpath(Elements_Admin.Admin_Appointments_Doctor_tableList);
		String table_PatientName = Browser.getTextByXpath(Elements_Admin.Admin_Appointments_Doctor_tablePatientName);
		Assert.assertTrue(table_PatientName.contains(firstName));
	}

	@AfterClass(groups = { "Admin", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
