//@Author: Sagar Sen

package com.web.adminTS.Doctor;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ST_3133_verifyHospitalDataEdit_Changes_Doctor extends LoadProp{
	public TestUtils Browser;
	public AdminPage AdminPage;
	public RecipientPage RecipientPage;
	public String doctorName, hospital_Location, hospital_pincode, hospitalName_Actual, activeInactiveBtn_Actual;
	public String hospitalName="Genetichospital";
	
	@BeforeClass(groups={"Admin" , "High"})
	public void launchBrowser() throws Exception{
		LoadBrowserProperties(); // Create driver instance and launch the browser
		Elements_Admin.Admin_PageProperties();// loading UI Page Elements / Locators
		AdminPage = new AdminPage(driver); // Loading Pages
		RecipientPage= new RecipientPage(driver);
		Browser= new TestUtils(driver);
		Browser.openUrl(EnvironmentAdminURL);
		doctorName="DoctorOCFour";
	}
	
	@Test(priority=1, enabled=true, groups={"Admin" , "High"})
	public void editHospital() throws Exception{
		hospital_Location="D"+Browser.generateRandomString(8).toLowerCase();
		hospital_pincode="5"+Browser.generateRandomNumber(5);
		AdminPage.adminLogin(Admin_Username, Admin_Password);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Application Management");
		Thread.sleep(5000);
		AdminPage.hospitalTab_search(hospitalName);
		Browser.scrollbyxpath("(//i[@class='fa icon fa-edit'])[21]"); // Edit button xpath
		Browser.clickOnTheElementByXpath("(//i[@class='fa icon fa-edit'])[21]"); //Click Edit Button
		Thread.sleep(5000);
		//AdminPage.waitUntil_LoaderInvisible("(//img[@class='loaderImage'])[1]");
		Browser.waitFortheElementXpath(Elements_Admin.Clinic_About_Location);
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_About_Location);
		Thread.sleep(2000);
		
		
		
		driver.findElement(By.xpath(Elements_Admin.Clinic_About_Location)).clear();
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Location, hospital_Location);
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_About_PIN);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_PIN, hospital_pincode);
		Browser.scrollbyxpath(Elements_Admin.Clinic_About_Save);
   	    Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_About_Save);
   	    Thread.sleep(4000);
   	    Browser.clickOnTheElementByXpath("//div[@class='list__tile__title'][contains(text(),'Application Management')]");
   	    Thread.sleep(5000);
   	    
   	   // AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Application Management");
   	   // AdminPage.waitUntil_LoaderInvisible("(//img[@class='loaderImage'])[1]");
		AdminPage.linkingQueue_Search_click_Doctor(doctorName);
		hospitalName_Actual=Browser.getTextByXpath("(//td[2])[3]");
		Assert.assertEquals(hospitalName_Actual, "Genetichospital");
		activeInactiveBtn_Actual=Browser.getTextByXpath("//td[contains(., 'Genetichospital')]/following-sibling::td/button/div");
		if(activeInactiveBtn_Actual.equals("ACTIVATE")){
			System.out.println("Changing status.");
			Browser.clickOnTheElementByXpath("//td[contains(., 'Genetichospital')]/following-sibling::td/button/div");
			//Browser.waitFortheElementXpath("//td[contains(., 'Genetichospital')]/following-sibling::td/button/div[contains(., 'Activate')]");
		}else{
			Browser.openUrl(EnvironmentURL);
			System.out.println("Opened recipient url.");
		}
	}
	
	@Test(priority=2, enabled=true, groups={"Admin" , "High"})
	public void verify_hospitalData_ProfilePage() throws Exception{
		//RecipientPage.home_location_data_Search("Murdeshwar", doctorName);
		RecipientPage.GenericSearch("Doctors", "Shivamogga, Karnataka", "DoctorOCFour");
		Thread.sleep(5000);
		String actual_ProfilePage_DocName=Browser.getTextByXpath("//h1[@class='mb-3 font24']");
		Assert.assertTrue(actual_ProfilePage_DocName.contains(doctorName));
		//Browser.clickOnTheElementByXpath("(//a[contains(text(),'"+hospitalName+"')])[1]");
		//Thread.sleep(5000);
		//String actual_Address_Profile=Browser.getTextByXpath("//html//div[@class='clinic-right-section']/span[1]"); //Profile page address section.
		//Assert.assertTrue(actual_Address_Profile.contains(hospital_Location));
		//Assert.assertTrue(actual_Address_Profile.contains(hospital_pincode));
	}
	
	@AfterClass(groups={"Admin" , "High"})
	public void tearDown() throws Exception{
		Browser.quit();
	}
}
	


