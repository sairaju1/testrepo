//@Author: Sagar Sen

package com.web.adminTS.Doctor;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ST_2850_mappedClinic_deactivateValidation_Doctor extends LoadProp{
	public TestUtils Browser;
	public AdminPage AdminPage;
	
	@BeforeClass(groups={"Admin" , "High"})
	public void launchBrowser() throws Exception{
		LoadBrowserProperties(); // Create driver instance and launch the browser
		Elements_Admin.Admin_PageProperties();// loading UI Page Elements / Locators
		AdminPage = new AdminPage(driver); // Loading Pages
		Browser= new TestUtils(driver);
	}
	
	@Test(groups={"Admin", "High"}, priority=1, enabled=true)
	public void clinicDeactivate_Validation() throws Exception{
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(Admin_Username, Admin_Password);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Application Management");
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Admin.AppManagement_ClinicTab);
		
		for(int i=0;i<=doctor_clinic2.length()-1; i++)
	    {
	    	char key = doctor_clinic2.charAt(i);
	    	WebDriverWait wait = (new WebDriverWait(driver, 60));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(.//*[@id='locationSearch'])[2]"))); // Clinic search bar
			Thread.sleep(500);
			driver.findElement(By.xpath("(.//*[@id='locationSearch'])[2]")).sendKeys(Character.toString(key));
			System.out.println("Texted = "+doctor_clinic2);
			Reporter.log("Text Entered="+doctor_clinic2);
	    	Thread.sleep(600);
	    }
		
		driver.findElement(By.xpath("(.//*[@id='locationSearch'])[2]")).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(By.xpath("(.//*[@id='locationSearch'])[2]")).sendKeys(Keys.ENTER);
		Browser.waitFortheElementXpath("(//td[contains(., '"+doctor_clinic2+"')])[1]"); // Wait for table of clinicName
		Browser.scrollbyxpath("(//i[@class='fa icon fa-edit'])[11]"); // Edit button xpath
		Browser.clickOnTheElementByXpath("(//i[@class='fa icon fa-edit'])[11]"); //Click Edit Button
		AdminPage.waitUntil_LoaderInvisible("(//img[@class='loaderImage'])[1]");
		Browser.waitFortheElementXpath("//input[@data-vv-name='clinic name']"); //wait for clinic name
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath("//label[contains(., 'Is Active')]"); //Active button
		Thread.sleep(4000);
		Browser.waitFortheElementXpath("//p[contains(., 'Clinic is associated with some doctors, please deactivate the linking with the doctors to inactivate the clinic.')]");
	}
	
	@AfterClass(groups={"Admin" , "High"})
	public void tearDown() throws Exception{
		Browser.quit();
	}
}
