//@Author: Sagar Sen

package com.web.adminTS.Doctor;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import objectRepository.Elements_Recipients;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ST_2894_appointmentLog_cancel_Doctor extends LoadProp{
	public TestUtils Browser;
	public AdminPage AdminPage;
	public RecipientPage RecipientPage;
	public String bookingStatus_expected="CANCEL";
	
	@BeforeClass(groups={"Admin" , "High"})
	public void launchBrowser() throws Exception{
		LoadBrowserProperties(); // Create driver instance and launch the browser
		Elements_Admin.Admin_PageProperties();// loading UI Page Elements / Locators
		AdminPage = new AdminPage(driver); // Loading Pages
		RecipientPage= new RecipientPage(driver);
		Browser= new TestUtils(driver);
		Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", "+919553456665");
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "zoylodoctor@gmail.com");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "zoylorecclinic@gmail.com");
	}
	
	@Test(priority=1, enabled=true, groups={"Admin", "High"})
	public void doctorAppointment_Book() throws Exception
	{
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(2000);
		RecipientPage.GenericSearch("Doctors", Doctor_Location, "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor(Doctor_Name);
		Thread.sleep(1000);
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		Thread.sleep(2000);
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		Thread.sleep(1000);
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(2000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, "Booking Successful!");
	}
	
	@Test(priority=2, enabled=true, groups={"Admin", "High"})
	public void DocApptLog_cancel() throws Exception{
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(Admin_Username, Admin_Password);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", " Appointment Logs");
		Browser.waitFortheElementXpath(Elements_Admin.doc_appointmentLog_tabel); //Table xpath
		Browser.enterTextByXpath(Elements_Admin.doc_appointmentLog_recEmailSearch, "lakshmikanth.c@zoylo.com");
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_appointmentLog_recEmailSearch_ApplyBtn);
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_appointmentLog_recEmailSearch_ApplyBtn);
		Thread.sleep(5000);
		String tabelEmail=Browser.getTextByXpath(Elements_Admin.doc_appointmentLog_tabelEmailText); //email coloumn
		Assert.assertEquals(tabelEmail, "lakshmikanth.c@zoylo.com");
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_appointmentLog_tabelEmailText); //email coloumn
		Browser.scrollbyxpath(Elements_Admin.doc_appointmentLog_cancelBtn); //Scroll to cancel Btn
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_appointmentLog_cancelBtn); //Click on cancel btn
		Browser.clickOnTheElementByXpath("(//div[@class='btn__content' and contains(., 'Yes')])[1]"); //Cacnel Yes Btn
		Browser.waitFortheElementXpath("//p[contains(., 'Appointment cancelled successfully.')]"); //Notification
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_appointmentLog_stateTab); //Click state to scroll
		Browser.scrollbyxpath("(//th[text()='Booking Status'])[1]"); //Scroll to Booking status
		String bookingStatus_actual=Browser.getTextByXpath("(//td[9])[1]"); //Status of booking
		Assert.assertEquals(bookingStatus_actual, bookingStatus_expected);
	}
	
	@AfterClass(groups={"Admin" , "High"})
	public void tearDown() throws Exception{
		Browser.quit();
	}

}
