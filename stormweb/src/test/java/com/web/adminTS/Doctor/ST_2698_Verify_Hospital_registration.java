//Author:Gurucharan

package com.web.adminTS.Doctor;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ST_2698_Verify_Hospital_registration extends LoadProp{
	
	public TestUtils Browser;
	public AdminPage adminpage;
	public String Emailuser,Emailclinic;
	
	@BeforeClass(groups={"Admin" , "High"})
    public void Browserlaunch() throws Exception {
   	 LoadBrowserProperties();
   	 Elements_Admin.Admin_PageProperties();
   	 adminpage=new AdminPage(driver);
   	 Browser=new TestUtils(driver);
   	 Browser.openUrl(EnvironmentAdminURL);
    }

	@DataProvider(name="Hospitaldata")
 	public Object[][] Hospitaldata() throws Exception{
 		Object[][] data=TestUtils.getTableArray("TestData/Clinicregistration.xls", "Hospital data", "ST_2698");
 		return data;
 	}
	
	@Test(dataProvider="Hospitaldata",groups={"Admin" , "High"})
	public void Addhospital(String Clinicname,String Mobnum,String Email,String Country,String State,String City,
 			String Address1,String Address2,String Nearest,String Location,String PIN,String Lat,String Long,
 			String FirstName,String MiddleName,String LastName,String MobileNumber,String Email1,String Password,String Confirmpassword) throws Exception{
		
	 Emailuser=Email1;
   	 Emailclinic=Email;	
	 adminpage.adminLogin(Admin_Username,Admin_Password);
   	 adminpage.AdminMenu_subMenu("Kellton Menu Items","False","Hospital Registration");
   	 adminpage.Hospital_registration(Clinicname, Mobnum, Email, Country, State, City, Address1, Address2, Nearest, Location, PIN, Lat, Long, FirstName, MiddleName, LastName, MobileNumber, Email1, Password, Confirmpassword);
   	 Thread.sleep(5000);
   	 Browser.clickOnTheElementByXpath("//div[contains(text(),'Application Management')]");
   	// adminpage.AdminMenu_subMenu("Kellton Menu Items", "False", "Application Management");
   	 Thread.sleep(3000);
   	 Browser.clickOnTheElementByXpath("//a[contains(.,'Hospital Listing')]");
   	 Browser.enterTextByXpath("(//div[@class='autocomplete-input appointment-search']//input)[5]", Clinicname);
   	 Browser.clickOnTheElementByXpath("//span[contains(.,'"+Clinicname+"')]");
   	 String verifyhop= Browser.getTextByXpath("//td[contains(.,'"+Clinicname+"')]");
   	 Assert.assertTrue(verifyhop.contains(Clinicname));
   	 
		}
	
	@AfterClass(groups={"Admin" , "High"})
	public void quit() throws Exception {
		
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", Emailuser);
		Browser.remove_Document(Environment, "zoyloServiceProvider", "genericContactEmail", Emailclinic);
		Browser.quit();
		
	}
}
