//@Author: Sagar Sen

package com.web.adminTS.Doctor;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ST_2837_AddDoctor_NON_PII extends LoadProp{
	public TestUtils Browser;
	public AdminPage AdminPage;
	public RecipientPage RecipientPage;
	public String doctorName, Search_specialization, LastName, Email, mobileNumber, YOS;
	
	@BeforeClass(groups={"Admin" , "High"})
	public void launchBrowser() throws Exception{
		LoadBrowserProperties(); // Create driver instance and launch the browser
		Elements_Admin.Admin_PageProperties();// loading UI Page Elements / Locators
		AdminPage = new AdminPage(driver); // Loading Pages
		RecipientPage= new RecipientPage(driver);
		Browser= new TestUtils(driver);
		Browser.openUrl(EnvironmentAdminURL);
	}
	
	@DataProvider(name="DocInfo")
	public Object[][] DocDetails() throws Exception
	{
		Object[][] DocData=TestUtils.getTableArray("TestData/adminOne.xls", "2675", "ST2837");
		return(DocData);
	}
	
	@Test(priority=1, enabled=true, dataProvider="DocInfo", groups={"Admin" , "High"})
	public void addDoctor_NON_PII(String Language, String Gender, String tag, String Specialization,
	String LOP, String imgPath, String councilName, String Qualification, String stateCouncilName, String medNum, String roleName, String clinicName, String MON_Morning_Start,
	String MON_Morning_End, String MON_Afternoon_Start, String MON_Afternoon_End, String MON_Evening_Start, String MON_Evening_End, String MON_Night_Start, String MON_Night_End,
	String zfc, String followUpDays, String generalFee, String followUpFee, String ivfFee, String ivfFollowUpFee) throws Exception{
		doctorName="N"+Browser.generateRandomString(6).toLowerCase();
		LastName="P"+Browser.generateRandomString(6).toLowerCase();
		Email=Browser.generateRandomString(7).toLowerCase()+"@zoylo.com";
		mobileNumber="7"+Browser.generateRandomNumber(9);
		Search_specialization=Specialization;
		YOS="2004-10-15";
		AdminPage.adminLogin(Admin_Username, Admin_Password);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Doctor Registration");
		
		//Primary Info Tab
		Browser.waitFortheID(Elements_Admin.doc_PrimaryInfo_FirstName);
		driver.findElement(By.xpath(".//*[@id='primaryInformationTab']/div/form/div[3]/div[2]/div/div[1]/div")).click();
		System.out.println("Selected NON-PII doctor");
		AdminPage.doctor_PrimaryInformation(Language, doctorName, LastName, Email, mobileNumber, YOS, Gender, tag, Specialization, LOP, imgPath);
		Browser.waitforTextbyxpath("//p[contains(., 'Doctor primary information has been saved successfully.')]", "Doctor primary information has been saved successfully.");
		Thread.sleep(2000);
		
		//Registration Verification Tab
		AdminPage.doctor_RegistrationVerification(councilName, Qualification, stateCouncilName, medNum);
		Browser.waitforTextbyxpath("//p[contains(., 'Doctor Verification Save Successfully.')]", "Doctor Verification Save Successfully.");
		Thread.sleep(2000);
		
		//Practice Info Tab
		AdminPage.doctor_PracticeInfo(roleName, clinicName);
		String actualClinicName=Browser.getTextByXpath("(//td[@class='text-xs-right'])[1]"); //Get clinic name from table
		Assert.assertTrue(actualClinicName.contains(clinicName));
		Thread.sleep(5000);
		
		// Work Days and Timings
		AdminPage.doctor_workDaysAndTimings(clinicName, MON_Morning_Start, MON_Morning_End, MON_Afternoon_Start, MON_Afternoon_End, MON_Evening_Start, MON_Evening_End, MON_Night_Start, MON_Night_End);
		
		//Fee Structure Tab
		AdminPage.doctor_feeStructure(clinicName, zfc, followUpDays, generalFee, followUpFee, ivfFee, ivfFollowUpFee);
		
		//SEO tab
		AdminPage.doctor_SEO();
		Browser.waitFortheElementXpath("//p[contains(., 'SEO Details Saved Successfully.')]");
		
		//Additional Info Tab
		Thread.sleep(7000);
		//AdminPage.waitUntil_LoaderInvisible("//img[@class='loaderImage']");
		AdminPage.doctor_AdditionalInformation();
		System.out.println("Doctor registration completed.");
	}
	
	@Test(priority=2, enabled=true, groups={"Admin", "High"})
	public void NON_PIIDoctor_ActiveInactiveCheck_LinkingQue() throws Exception{
		
		//Admin activate linking queue
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Application Management");
		Thread.sleep(5000);
		AdminPage.linkingQueue_Search_click_Doctor(doctorName);
		Browser.clickOnTheElementByXpath(Elements_Admin.AppManagement_LinkingQue_ActiveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.AppManagement_LinkingQue_DeActiveBtn);
		
		//Check visibility in front end
		Browser.openUrl(EnvironmentURL);
		RecipientPage.GenericSearch("Doctors", doctor_location2, Search_specialization);
		//RecipientPage.home_location_data_Search(doctor_location2, Search_specialization); //
		AdminPage.checkDoctorVisibility_RecipientEnd(doctorName);
		
		//Admin inactivate linking queue
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Application Management");
		Thread.sleep(5000);
		AdminPage.linkingQueue_Search_click_Doctor(doctorName);
		Browser.clickOnTheElementByXpath(Elements_Admin.AppManagement_LinkingQue_DeActiveBtn);
		Browser.waitFortheElementXpath(Elements_Admin.AppManagement_LinkingQue_ActiveBtn);
		
	}
	
	@AfterClass(groups={"Admin" , "High"})
	public void tearDown() throws Exception{
		
		Browser.remove_Document(Environment, "zoyloServiceResource", "emailId", Email);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", Email);
		Browser.quit();
	}
}