//Author:Gurucharan

package com.web.adminTS.Doctor;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ST_2695_Verify_clinic_registration extends LoadProp{
	
     public AdminPage adminpage;
     public TestUtils Browser;
     public String Emailuser,Emailclinic;
     
     @BeforeClass(groups={"Admin" , "High"})
     public void Browserlaunch() throws Exception {
    	 
    	 LoadBrowserProperties();
    	 Elements_Admin.Admin_PageProperties();
    	 adminpage=new AdminPage(driver);
    	 Browser=new TestUtils(driver);
    	 Browser.openUrl(EnvironmentAdminURL);
     }

     @DataProvider(name="Clinicdata")
 	public Object[][] Clinicdata() throws Exception{
 		

 		Object[][] data=TestUtils.getTableArray("TestData/Clinicregistration.xls", "Clinic data", "ST_2695");
 		return data;
 	}
     
     @Test(dataProvider="Clinicdata",groups={"Admin" , "High"})
     public void Addclinic(String Clinicname,String Mobnum,String Email,String Country,String State,String City,
 			String Address1,String Address2,String Nearest,String Location,String PIN,String Lat,String Long,
 			String FirstName,String MiddleName,String LastName,String MobileNumber,String Email1,String Password,String Confirmpassword) throws Exception {
    	 Emailuser=Email1;
    	 Emailclinic=Email;
    	 adminpage.adminLogin(Admin_Username,Admin_Password);//Admin Login
    	 adminpage.AdminMenu_subMenu("Kellton Menu Items","False","Clinic / Wellness Center Registration");//Clicks on application management and then Clinic registration
    	 adminpage.Clinic_Hospital_registration(Clinicname, Mobnum, Email,Country, State, City, Address1, Address2, Nearest, Location, PIN, Lat, Long,FirstName,MiddleName,LastName,MobileNumber,Email1,Password,Confirmpassword);//Add clinic
    	 Thread.sleep(2000);
    	 Browser.clickOnTheElementByXpath("//div[contains(text(),'Application Management')]");
    	// adminpage.AdminMenu_subMenu("Kellton Menu Items", "False", "Application Management");//Clicks on Application Management
    	 Thread.sleep(5000);
    	 Browser.clickOnTheElementByXpath(Elements_Admin.AppManagement_ClinicTab);//Clicks on clinic tab under application management
    	 Thread.sleep(5000);
    	 Browser.enterTextByXpath(Elements_Admin.Clinic_Search, Clinicname);//Search clinic
    	 Browser.clickOnTheElementByXpath("//span[contains(.,'"+Clinicname+"')]");//Select clinic from list
    	 String verifyclinic=Browser.getTextByXpath("//td[contains(.,'"+Clinicname+"')]");
    	 Assert.assertTrue(verifyclinic.contains(Clinicname));
    	 
     }
     
     	@AfterClass(groups={"Admin" , "High"})
    	 public void remove() throws Exception {
    		 
    		 Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", Emailuser);
    		 Browser.remove_Document(Environment, "zoyloServiceProvider", "genericContactEmail", Emailclinic);
    		 Browser.quit();
    		 
    	 }
    	 
     }


