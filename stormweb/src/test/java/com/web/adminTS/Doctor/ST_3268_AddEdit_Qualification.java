//@Author: Sagar Sen

package com.web.adminTS.Doctor;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.TestUtils;

public class ST_3268_AddEdit_Qualification extends LoadProp{
	public TestUtils Browser;
	public AdminPage AdminPage;
	public String codeVaue, sequence, qualification;
	
	@BeforeClass(groups={"Admin" , "High"})
	public void launchBrowser() throws Exception{
		LoadBrowserProperties(); // Create driver instance and launch the browser
		Elements_Admin.Admin_PageProperties();// loading UI Page Elements / Locators
		AdminPage = new AdminPage(driver); // Loading Pages
		Browser= new TestUtils(driver);
		Browser.openUrl(EnvironmentAdminURL);
		codeVaue=Browser.generateRandomString(4).toUpperCase();
		sequence=Browser.generateRandomNumber(3);
		qualification="A"+Browser.generateRandomString(4).toLowerCase();
	}
	
	@Test(priority=1, enabled=true, groups={"Admin" , "High"})
	public void Qualification_Add() throws Exception{
		AdminPage.adminLogin(Admin_Username, Admin_Password);
		//AdminPage.AdminMenu_subMenu("False", "False", "Reference Data");
		Browser.clickOnTheElementByXpath("//div[contains(text(),'Reference Data')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.navigation_qualificationBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.qualification_lookup);
		Browser.clickOnTheElementByID(Elements_Admin.qualification_lookUpAddBtn);
		Browser.enterTextByID(Elements_Admin.qualification_codeValue, codeVaue);
		Browser.enterTextByID(Elements_Admin.qualification_displaySequence, sequence);
		Browser.enterTextByID(Elements_Admin.qualification_name, qualification);
		Browser.clickOnTheElementByID(Elements_Admin.qualificationValue_SaveBtn);
		Browser.waitFortheElementXpath("//p[contains(., 'Lookup value saved successfully')]");
	}
	
	@Test(priority=2, enabled=true, groups={"Admin" , "High"})
	public void Qualification_Edit() throws Exception{
		Browser.scrollbyxpath("//td[@class='text-xs-center' and contains(., '"+qualification+"')]");
		Browser.clickOnTheElementByXpath("//td[@class='text-xs-center' and contains(., '"+qualification+"')]/following-sibling::td[2]");
		Browser.clickOnTheElementByID(Elements_Admin.qualification_editSave);
		System.out.println("Updated successfully.");
	}
	
	@AfterClass(groups={"Admin" , "High"})
	public void tearDown() throws Exception{
		Browser.quit();
	}
}
