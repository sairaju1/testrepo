//@Author: Sagar Sen

package com.web.adminTS.Doctor;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import objectRepository.Elements_Recipients;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ST_2930_PaymentReport_Doctor extends LoadProp{
	public TestUtils Browser;
	public AdminPage AdminPage;
	public RecipientPage RecipientPage;
	public String amtPaid, advanceAmount, advanceAmount_admin, transactionNumber, transactionNumber_table;
	public int i,c;
	
	@BeforeClass(groups={"Admin" , "High"})
	public void launchBrowser() throws Exception{
		LoadBrowserProperties(); // Create driver instance and launch the browser
		Elements_Admin.Admin_PageProperties();// loading UI Page Elements / Locators
		AdminPage = new AdminPage(driver); // Loading Pages
		RecipientPage= new RecipientPage(driver);
		Browser= new TestUtils(driver);
		Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", "+919553456665");
	 	Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "zoylodoctor@gmail.com");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "zoylorecclinic@gmail.com");
		Browser.remove_Document(Environment, "zoyloPayment", "payerInfo.payerEmail", "lakshmikanth.c@zoylo.com");
	}
	
	@Test(priority=1, enabled=true, groups={"Admin", "High"})
	public void doctorAppointment_Book() throws Exception
	{
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(2000);
		RecipientPage.GenericSearch("Doctors", Doctor_Location, "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor(Doctor_Name);
		Thread.sleep(1000);
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		Thread.sleep(2000);
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		Thread.sleep(1000);
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(2000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, "Booking Successful!");
		Thread.sleep(2000);
		amtPaid=Browser.getTextByXpath("//div[@class='fee-details']/div[2]/h6[2]");
		advanceAmount=amtPaid.replaceAll("[^0-9]", "");
		System.out.println("Advance booking amount is: "+advanceAmount);
	}
	
	@Test(priority=2, enabled=true, groups={"Admin", "High"})
	public void paymentReports_Doctor() throws Exception{
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(Admin_Username, Admin_Password);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", " Payment Reports");
		Thread.sleep(5000);
		//Payment type select method.
		AdminPage.paymentReports_paymentTypeClick("Payment Successful");
		Browser.scrollbyxpath(Elements_Admin.payment_paginationArea);
		Browser.clickOnTheElementByXpath(Elements_Admin.payment_lastPage);
		Browser.scrollbyxpath("//b[contains(., 'Select Transaction Type')]"); //Scroll top
		c=driver.findElements(By.xpath("//tr/td[1]")).size();
		System.out.println("Number of records available are: "+c);
		transactionNumber=Browser.get_Document_Attribute(Environment, "zoyloPayment", "payerInfo.payerEmail", "lakshmikanth.c@zoylo.com", "transactionNumber");
		for(i=1;i<=c;){
			transactionNumber_table=Browser.getTextByXpath("//tr["+i+"]/td[1]");
			if(transactionNumber_table.equalsIgnoreCase(transactionNumber)){
				System.out.println("I match case is:" +i);
				break;
			}else{
				i++;
			}
		}
		advanceAmount_admin=Browser.getTextByXpath("//tr["+i+"]/td[4]");
		Assert.assertEquals(advanceAmount_admin, advanceAmount);
	}
	
	@AfterClass(groups={"Admin" , "High"})
	public void tearDown() throws Exception{
		Browser.quit();
	}
}
