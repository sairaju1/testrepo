//@Author: Sagar Sen

package com.web.adminTS.Doctor;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import objectRepository.Elements_Recipients;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ST_2845_ExistingApt_timeSlotDeactivate_Validation_Doctor extends LoadProp{
	public TestUtils Browser;
	public AdminPage AdminPage;
	public RecipientPage RecipientPage;
	public String error_expected="Appointment booked for comming days. Please cancel appointment first.";
	
	@BeforeClass(groups={"Admin" , "High"})
	public void launchBrowser() throws Exception{
		LoadBrowserProperties(); // Create driver instance and launch the browser
		Elements_Admin.Admin_PageProperties();// loading UI Page Elements / Locators
		AdminPage = new AdminPage(driver); // Loading Pages
		RecipientPage= new RecipientPage(driver);
		Browser= new TestUtils(driver);
		Browser.remove_Document(Environment, "zoyloAppointment", "patientInfo.patientPhone", "+919553456665");
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail", doctor_name_email);
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceProviderInfo.serviceProviderEmail", "zoylorecclinic@gmail.com");
	}
	
	@Test(priority=1, enabled=true, groups={"Admin", "High"})
	public void doctorAppointment_Book() throws Exception
	{
		Browser.openUrl(EnvironmentURL+"/login");
		RecipientPage.Recipient_Sigin(Doctor_Username, Doctor_Password);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		Thread.sleep(2000);
		RecipientPage.GenericSearch("Doctors", Doctor_Location, "");
		Thread.sleep(1000);
		RecipientPage.ListPage_Select_Doctor(Doctor_Name);
		Thread.sleep(1000);
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_ConfirmAppointmentButton);
		Thread.sleep(2000);
		RecipientPage.BookingDetails_ProceedToPay("Self","","","No_Promocode");
		Thread.sleep(1000);
		RecipientPage.MakePayment("Single", "Netbanking");
		Thread.sleep(2000);
		String msg=Browser.getTextByXpath(Elements_Recipients.BookingSucess_Notification);
		Assert.assertEquals(msg, "Booking Successful!");
	}
	
	@Test(priority=2, enabled=true, groups={"Admin", "High"})
	public void doctor_slotsDeactivate_Validation() throws Exception{
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(Admin_Username, Admin_Password);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Application Management");
		Thread.sleep(3000);
		
		for(int i=0;i<=doctor_name.length()-1; i++)
	    {
	    	char key = doctor_name.charAt(i);
	    	WebDriverWait wait = (new WebDriverWait(driver, 60));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Elements_Admin.AppManagement_Doctor_SearchBar)));
			Thread.sleep(500);
			driver.findElement(By.xpath(Elements_Admin.AppManagement_Doctor_SearchBar)).sendKeys(Character.toString(key));
			System.out.println("Texted = "+doctor_name);
			Reporter.log("Text Entered="+doctor_name);
	    	Thread.sleep(600);
	    }
		
		driver.findElement(By.xpath(Elements_Admin.AppManagement_Doctor_SearchBar)).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(By.xpath(Elements_Admin.AppManagement_Doctor_SearchBar)).sendKeys(Keys.ENTER);
		AdminPage.waitUntil_LoaderInvisible("(//img[@class='loaderImage'])[1]");
		Browser.waitFortheElementXpath("(//td[contains(., '"+doctor_name+"')])[1]"); // Wait for table of doctor
		Browser.scrollbyxpath("(//i[@class='fa icon fa-edit'])[1]"); // Edit button xpath
		Browser.clickOnTheElementByXpath("(//i[@class='fa icon fa-edit'])[1]"); //Click Edit Button
		AdminPage.waitUntil_LoaderInvisible("(//img[@class='loaderImage'])[1]");
		Thread.sleep(13000);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_WorkDaysTab); // Clicks on Timings tab
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_WorkDaysTab); // Clicks on Timings tab
		
		//Get Current day
		String day=Browser.getCurrentDay();
		System.out.println("Current booking day is: "+day);
		Browser.clickOnTheElementByXpath("(//div[@class='container fluid wrap']/div[@class='layout'])[1]/div/div/div/label[contains(., '"+day+"')]");
		Browser.scrollbyxpath("(//button[@class='#1b5e20 green darken-2 white--text btn btn--raised'])[1]"); // TO save button
		Browser.clickOnTheElementByXpath("(//button[@class='#1b5e20 green darken-2 white--text btn btn--raised'])[1]"); // Click save button
		
		Browser.waitFortheElementXpath("//div[@class='card__title headline' and contains(., 'Error!')]"); //Error popup
		String error_actual=Browser.getTextByXpath("//div[@class='card__title headline' and contains(., 'Error!')]/following-sibling::div[@class='card__text']"); //Error message path
		Assert.assertEquals(error_actual, error_expected);
	}
	
	@AfterClass(groups={"Admin" , "High"})
	public void tearDown() throws Exception{
		Browser.remove_Document(Environment, "zoyloAppointment", "serviceResourceInfo.serviceResourceEmail", doctor_name_email);
		Browser.quit();
	}
}
