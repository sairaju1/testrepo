//@Author: Sagar Sen

package com.web.adminTS.Doctor;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Admin;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ST_3098_ActiveInactiveCheck_Hospital_Doctor extends LoadProp{
	public TestUtils Browser;
	public AdminPage AdminPage;
	public RecipientPage RecipientPage;
	public String doctorName, hospitalName_Actual, activeInactiveBtn_Actual, currentURL, message_actual;;
	
	@BeforeClass(groups={"Admin" , "High"})
	public void launchBrowser() throws Exception{
		LoadBrowserProperties(); // Create driver instance and launch the browser
		Elements_Admin.Admin_PageProperties();// loading UI Page Elements / Locators
		AdminPage = new AdminPage(driver); // Loading Pages
		RecipientPage= new RecipientPage(driver);
		Browser= new TestUtils(driver);
		Browser.openUrl(EnvironmentAdminURL);
		doctorName=doctor_name;
	}
	
	@Test(priority=1, enabled=true, groups={"Admin" , "High"})
	public void inactiveCheck() throws Exception{
		AdminPage.adminLogin(Admin_Username, Admin_Password);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Application Management");
		Thread.sleep(5000);
		AdminPage.linkingQueue_Search_click_Doctor(doctorName);
		hospitalName_Actual=Browser.getTextByXpath("(//td[2])[2]");
		Assert.assertEquals(hospitalName_Actual, hospital_name);
		activeInactiveBtn_Actual=Browser.getTextByXpath("//td[contains(., '"+hospital_name+"')]/following-sibling::td/button/div");
		if(activeInactiveBtn_Actual.equals("ACTIVATE")){
			Browser.openUrl(EnvironmentURL);
			System.out.println("Opened recipient url.");
		}else{
			System.out.println("Changing status.");
			Browser.clickOnTheElementByXpath("//td[contains(., '"+hospital_name+"')]/following-sibling::td/button/div");
			Browser.waitFortheElementXpath("//td[contains(., '"+hospital_name+"')]/following-sibling::td/button/div[contains(., 'Activate')]");
			Browser.openUrl(EnvironmentURL);
		}
		currentURL=driver.getCurrentUrl();
		if(currentURL.contains(EnvironmentURL)){
			RecipientPage.GenericSearch("Doctors", "Murdeshwar", "");
			//RecipientPage.home_location_data_Search("Murdeshwar", "");
			Thread.sleep(2000);
			//message_actual=Browser.getTextByXpath("//div[@class='data-not-found']");
			//Assert.assertEquals(message_actual, "Record Not found");
			int DC_Count=driver.findElements(By.xpath("//div[@class='doc-details']//h2/a")).size();
			System.out.println("Doctors avaolable Count :"+DC_Count);
			for(int i=1;i<=DC_Count;i++){
			String DC_Name=Browser.getTextByXpath("(//div[@class='doc-details']//h2/a)["+i+"]");
			System.out.println("Doctor Name:"+DC_Name);
			Assert.assertFalse(DC_Name.contains(doctorName));
			}
			
		}
	}
	
	@Test(priority=2, enabled=true, groups={"Admin" , "High"})
	public void activeCheck() throws Exception{
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Application Management");
		Thread.sleep(5000);
		AdminPage.linkingQueue_Search_click_Doctor(doctorName);
		hospitalName_Actual=Browser.getTextByXpath("(//td[2])[2]");
		Assert.assertEquals(hospitalName_Actual, hospital_name);
		activeInactiveBtn_Actual=Browser.getTextByXpath("//td[contains(., '"+hospital_name+"')]/following-sibling::td/button/div");
		if(activeInactiveBtn_Actual.equals("ACTIVATE")){
			System.out.println("Changing status.");
			Browser.clickOnTheElementByXpath("//td[contains(., '"+hospital_name+"')]/following-sibling::td/button/div");
			Browser.waitFortheElementXpath("//td[contains(., '"+hospital_name+"')]/following-sibling::td/button/div[contains(., 'Deactivate')]");
			Browser.openUrl(EnvironmentURL);
		}else{
			Browser.openUrl(EnvironmentURL);
			System.out.println("Opened recipient url.");
		}
		currentURL=driver.getCurrentUrl();
		if(currentURL.contains(EnvironmentURL)){
			RecipientPage.GenericSearch("Doctors", "Murdeshwar", "");
			//RecipientPage.home_location_data_Search("Murdeshwar", "");
			AdminPage.checkDoctorVisibility_RecipientEnd(doctorName);
		}
	}
	
	@AfterClass(groups={"Admin" , "High"})
	public void tearDown() throws Exception{
		Browser.quit();
	}
}
