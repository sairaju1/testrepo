

//@author:Ch.LakshmiKanth

package com.web.adminTS.Doctor;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import objectRepository.Elements_Admin;
import objectRepository.Elements_Recipients;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ST_3640_Admin_ADDWellnessCenter  extends LoadProp{
	
	public AdminPage AdminPage;
	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String Emailname;
	public String WellnessCentername;
	public String Phoneno;
	public String doctorName="DoctorOCFour";
	
	@BeforeClass
	public void LaunchBrowser() throws Exception{
		
		LoadBrowserProperties(); 
	 	Elements_Admin.Admin_PageProperties();
	 	Elements_Recipients.Recipients_PageProperties();
	 	AdminPage=new AdminPage(driver);
	 	RecipientPage=new RecipientPage(driver);
	 	Browser= new TestUtils(driver);
	 	Browser.openUrl(EnvironmentAdminURL);
	 	AdminPage.adminLogin(Admin_Username, Admin_Password);	
	 	
	 	
	}
	
	@DataProvider(name ="Timings" )
	 
	  public static Object[][] credentials() {
	 
	        return new Object[][] { { "10,00,am","09,00,pm"}};
	 
	  }
	
	
	@Test(dataProvider = "Timings", enabled=true,priority=1)
	public void CreateWellnessCenter(String Timings_Start,String Timings_End) throws Exception{
		
		Emailname="kranthikar"+Browser.generateRandomString(2).toLowerCase()+"@wellness.com";
		WellnessCentername="K"+Browser.generateRandomString(9).toLowerCase();
		Phoneno="99"+Browser.generateRandomNumber(8);
		
		
		//Navigating and Clicking on ADD Wellness Center Selecting Provider Type as Wellness Center
		AdminPage.AdminMenu_subMenu("Kellton Menu Items", "False", "Application Management");
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_ApplicationManagment_WellnessTab);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_AddWellness);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_Click_ProviderType);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_Select_ProviderType);
		
		//ADD Values to About Wellness Tab
		AdminPage.AboutWellnesscenter(WellnessCentername, Phoneno, Emailname);
		String Wellness=Browser.getTextByXpath(Elements_Admin.Admin_WellnessTab_AboutWellness_Notification);
		Assert.assertEquals(Wellness, "The Wellnes center Details Added Successfully.");
		Thread.sleep(2000);
		
		//ADD Values to Facilities
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_FacilitiesMenu);
		Thread.sleep(1000);
		AdminPage.FacilitiesInWellnessCenter();
		String Facilities=Browser.getTextByXpath(Elements_Admin.Admin_WellnessTab_Facilities_Notification);
		Assert.assertEquals(Facilities, "The Wellnes center facilities saved successfully.");
		Thread.sleep(2000);
		
		//Add Timings 
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_TimingsMenu);
		Thread.sleep(1000);
		AdminPage.AddTimingsInWellnessCenter(Timings_Start, Timings_End);
		String Timings=Browser.getTextByXpath(Elements_Admin.Admin_WellnessTab_Timings_Notification);
		Assert.assertEquals(Timings, "Save visit timings successfully.");
		Thread.sleep(2000);
		
		//SEO
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_SEOMenu);
		Thread.sleep(1000);
		Browser.enterTextByXpath("//input[@ name='Meta Title']", "KrantikarWellness");
		Browser.enterTextByXpath("//input[@name='Meta Keywords']", "Wellness, krantikar");
		Browser.enterTextByXpath("//input[@name='Meta Description']", "Best Wellness");
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_SEO_Save);
		String SEO=Browser.getTextByXpath(Elements_Admin.Admin_WellnessTab_SEO_Notification);
		Assert.assertEquals(SEO, "SEO Details Saved Successfully.");
		Thread.sleep(2000);
		
		//Admin User tab
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_AddAdminUserMenu);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_AddAdminUser_AddUserButton);
		AdminPage.WellnessCenter_AddAdminUser("Nakula", "Pandava", "four", "9911098762", "nakula@wellness.com", "zoylo@123", "zoylo@123");
		String Adminuser=Browser.getTextByXpath(Elements_Admin.Admin_WellnessTab_AddAdminUser_Notification);
		Assert.assertEquals(Adminuser, "Data Saved Successfully.");
		Thread.sleep(2000);
		
		//Packages Tab
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_PackagesMenu);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_PackagesMenu_AddPackageButton);
		AdminPage.WellnessCenter_Packages("kranthikarpackage", "Hi bestpackage", "6000", "5","10");
		String Packages=Browser.getTextByXpath("(//div[@class='custom-success alert alert--dismissible']/div/p)[8]");
		Assert.assertEquals(Packages, "The Wellnes center package added successfully.");
		Thread.sleep(2000);
		
		//Clinic /Wellness Tab
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_WellnessGalleryMenu);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_WellnessGallery_ApprovalQue);
		String QUE= Browser.getTextByXpath("//div[@class='alert alert-success']");
		Assert.assertTrue(QUE.contains("The Clinic Details Saved Successfully."));
		//Assert.assertEquals(QUE, "The Clinic Details Saved Successfully.");
		Thread.sleep(2000);
		
		//Recipient
		Browser.openUrl(EnvironmentURL);
		//Browser.clickOnTheElementByXpath(Elements_Recipients.ClickOn_WellnessTab);
		//RecipientPage.home_location_data_Search("Dehradun, Uttarakhand", WellnessCentername);
		RecipientPage.GenericSearch("Wellness", "Dehradun, Uttarakhand", WellnessCentername);
		Thread.sleep(7000);
		String WellnessCenter=Browser.getTextByXpath("//div[@class='recipient-search-doctor-information']/h3");
		Assert.assertTrue(WellnessCenter.contains("DoctorOCFour"));
		
			
	}
	
	@Test(priority=2,enabled=true)
	public void AssociateDoctorToWellnessCenter() throws Exception{
	
		String	data="doctoroc4@gmail.com";
	
	
		
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.AdminMenu_subMenu("Kellton Menu Items","False","Application Management");
		Thread.sleep(2000);
		Browser.waitFortheElementXpath("(//input[@id='locationSearch'])[1]");
		for(int i=0;i<=data.length()-1; i++)
	    {
	    	char key = data.charAt(i);
	    	WebDriverWait wait = (new WebDriverWait(driver, 60));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//input[@id='locationSearch'])[1]")));
			Thread.sleep(500);
			driver.findElement(By.xpath("(//input[@id='locationSearch'])[1]")).sendKeys(Character.toString(key));
			System.out.println("Texted = "+data);
			Reporter.log("Text Entered="+data);
	    	Thread.sleep(600);	
	    }
		
		Browser.clickOnTheElementByXpath("//article[@class='media']/p/span");
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath("(//i[@class='fa icon fa-edit'])[1]");
		Thread.sleep(10000);
		Browser.waitFortheID("lastName");
		Thread.sleep(5000);
		//Practice InFo Tab Script In Registered Doctor
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PracticeInfoTab); // Clicks on Practice Info Tab
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PracticeInfoTab);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PracticeInfo_AddOtherClinicBtn); // Clicks on add other clinic button
		//Browser.waitFortheElementXpath(Elements_Admin.doc_PracticeInfo_OtherClinicPopUP); // Wait for pop up load
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PracticeInfo_roleBtn); // role dropdown click
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., 'Doctor Clinic Administrator')]"); // Click selected Gender
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PracticeInfo_ClinicName);
		   // WellnessCentername
		Browser.enterTextByXpath(Elements_Admin.doc_PracticeInfo_ClinicName,  WellnessCentername);
		Thread.sleep(500);
		driver.findElement(By.xpath(Elements_Admin.doc_PracticeInfo_ClinicName)).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(By.xpath(Elements_Admin.doc_PracticeInfo_ClinicName)).sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@class='card__text']/div/input");
		Thread.sleep(2000);
		//Browser.clickOnTheElementByXpath("//div[@class='card__text']//div[contains(., '"+clinicName+"')]//input"); // clinic radio button click
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PracticeInfo_AddClinic_SaveBtn); // Clinic pop up save click
		//Browser.waitforTextbyxpath("//p[contains(., 'Practice Information saved successfully.')]", "Practice Information saved successfully.");
		//Browser.waitFortheElementXpath("(//div[@class='card__text']/div/input)[2]"); // Wait for load invisible
		//Browser.scrollbyxpath("(//button[@class='btn btn--raised theme--dark error'])[1]"); //Scroll to close button
		//Browser.clickOnTheElementByXpath(Elements_Admin.doc_PracticeInfo_PopUP_CancelBtn); // Click on cancel to close popup
		Thread.sleep(10000);
		
		//Fee Structure In Doctor
		Browser.clickOnTheElementByXpath("//b[contains(., 'Fee Structure')]"); 
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath("(//i[@class='material-icons icon input-group__append-icon'][text()='arrow_drop_down'])[11]");
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath("(//div[@class='list__tile__title' and text()='All'])[2]");
		Thread.sleep(3000);
		//Browser.scrollbyxpath("//td[@align='center' and contains(., '"+WellnessCentername+"')]/following-sibling::td/i");
		Browser.clickOnTheElementByXpath("//div[@id='doctorFeeStructure']//td[contains(text(),'"+WellnessCentername+"')]//following-sibling::td/i");
		
		Thread.sleep(3000);
		Browser.waitFortheElementXpath("//div[@class='clinic-top-section']/h4[contains(., '"+WellnessCentername+"')]"); //Wait for clinic name on fee pop up
		Browser.enterTextByXpath(Elements_Admin.doc_FeeStructure_ZFC, "5"); // Enter ZFC
		Browser.enterTextByName(Elements_Admin.doc_FeeStructure_FollupDays, "10"); // Enter follow up days
		Browser.enterTextByXpath(Elements_Admin.doc_FeeStructure_GeneralFee, "1000"); // Enter general fee
		Browser.enterTextByXpath(Elements_Admin.doc_FeeStructure_FollupFee, "800"); // Enter followup fee
		Browser.enterTextByXpath(Elements_Admin.doc_FeeStructure_ivfFee, "800"); // Enter ivf fee
		Browser.enterTextByXpath(Elements_Admin.doc_FeeStructure_ivfFollowUpFee, "800"); // Enter followup ivf fee
		Browser.scrollbyxpath(Elements_Admin.doc_FeeStructure_ivfFollowUpFee);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_FeeStructure_Save); //Click on save fee button
		//Browser.scrollbyxpath("(//td[contains(., '"+WellnessCentername+"')])[2]");
		//Browser.waitFortheElementXpath("//p[contains(., 'physical fee saved success.')]"); // Success message wait
		Thread.sleep(1000);
		//Browser.scrollbyxpath(Elements_Admin.doc_FeeStructure_Close);
		//Browser.clickOnTheElementByXpath(Elements_Admin.doc_FeeStructure_Close); // Click close button to close pop up
		//Browser.scrollbyxpath("//h5[contains(., 'Doctor Registration')]"); //Scroll to menu options
	
		Thread.sleep(3000);
	
		
	
		AdminPage.doctor_AdditionalInformation();	
		
		// Linking Que
		AdminPage.AdminMenu_subMenu("Kellton Menu Items","False","Application Management");
		Thread.sleep(2000);
		AdminPage.linkingQueue_Search_click_Doctor(doctorName);
		Thread.sleep(2000);
		Browser.scrollbyxpath("//td[contains(text(),'"+WellnessCentername+"')]//following-sibling::td/button/div[contains(., 'Activate')]");
		Browser.clickOnTheElementByXpath("//td[contains(text(),'"+WellnessCentername+"')]//following-sibling::td/button/div[contains(., 'Activate')]");
		Thread.sleep(1000);
		//Browser.waitFortheElementXpath(Elements_Admin.AppManagement_LinkingQue_DeActiveBtn);
		String button=Browser.getTextByXpath("//td[contains(text(),'"+WellnessCentername+"')]//following-sibling::td/button/div");
		System.out.println(button);
		Thread.sleep(1000);
		
		//Check visibility in front end
				Browser.openUrl(EnvironmentURL);
				//Browser.clickOnTheElementByXpath(Elements_Recipients.ClickOn_WellnessTab);
				RecipientPage.GenericSearch("Wellness", "Dehradun, Uttarakhand", WellnessCentername); //
				Thread.sleep(3000);
				Browser.clickOnTheElementByXpath("//li[@class='nav-item']//a[contains(., 'Doctors')]");
		String docname=Browser.getTextByXpath("//h2[@class='font18']");
		System.out.println("Recipient name: "+docname);
		//Assert.assertTrue(docname.contains(doctorName));
		System.out.println("Given doc name:"+doctorName);
				
	}
	
	
	@AfterClass
public void CloseBrowser() throws Exception{
		
		//Browser.remove_Document(Environment, "zoyloServiceProvider", "genericContactEmail", Emailname);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", "nakula@wellness.com");
		Browser.quit();
		
	}
}
