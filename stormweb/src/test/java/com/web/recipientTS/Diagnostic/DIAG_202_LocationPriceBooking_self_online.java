// @Author: Sagar Sen

package com.web.recipientTS.Diagnostic;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Recipients;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class DIAG_202_LocationPriceBooking_self_online extends LoadProp {

	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public AdminPage AdminPage;
	public String packageName, location_1, location_2, aptDate, aptDay, aptTime, bookingID, location_1_price,
			finalPrice_location1, finalPrice_location2;

	@BeforeClass(groups = { "DIAG", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Elements_Recipients.Recipients_PageProperties();
		Browser = new TestUtils(driver);
		RecipientPage = new RecipientPage(driver);
		AdminPage = new AdminPage(driver);
		packageName = rec_packageName;
		location_1 = operatingCity_1;
		location_2 = operatingCity_2;
		location_1_price = "1" + Browser.generateRandomNumber(3);
		aptDate = Browser.getModifiedDate(1);
		aptDay = Browser.getCurrentDay_format_modifier("EEEEE", 1).toUpperCase();
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterPhone", "+91" + user_id);
		Browser.remove_Document(Environment, "zoyloAppointment", "requesterInfo.requesterPhone", "+91" + user_id);
	}

	@Test(groups = { "DIAG", "High" }, priority = 1)
	public void AdminSetUp_locationPrice() throws Exception {
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		AdminPage.AdminMenu_subMenu("Zoylo Lab Management", "False", "Location Based Service Price");
		AdminPage.locationBased_select_city_servicetype(location_1, "Package");
		AdminPage.locationBased_search(packageName);
		AdminPage.locationBased_editFinalPrice(packageName, location_1_price);
	}

	@Test(groups = { "DIAG", "High" }, priority = 2)
	public void book_package_locationPrice() throws Exception {
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		RecipientPage.GenericSearch("Diagnostics", location_2, "");
		finalPrice_location2 = RecipientPage.diagnostic_homePage_popularPackageClick(packageName);
		Assert.assertNotEquals(finalPrice_location2, location_1_price);
		Browser.openUrl(EnvironmentURL);
		RecipientPage.GenericSearch("Diagnostics", location_1, "");
		finalPrice_location1 = RecipientPage.diagnostic_homePage_popularPackageClick(packageName);
		Assert.assertEquals(finalPrice_location1, location_1_price);
		// PD page
		RecipientPage.diagnostic_serviceDetailPage(false, packageName, finalPrice_location1);
		aptTime = RecipientPage.diagnostic_checkOut_selectAddress_Schedule(location_1, aptDate, aptDay);
		Browser.waitFortheElementXpath("//div[@class='row']/div[contains(., '" + location_1
				+ "')]/following-sibling::div/div/span[text()='" + aptTime + "']");
		RecipientPage.diagnostic_checkout_bookingFor("Self", "", "", "");
		RecipientPage.diagnostic_checkout_orderSummary("No", packageName, Integer.parseInt(finalPrice_location1), 0);
		RecipientPage.diagnostic_checkout_selectPaymentMode_placeOrder(dcPay_online);
		RecipientPage.verify_transactionAmount_PG(Double.parseDouble(finalPrice_location1));
		RecipientPage.MakePayment("", ""); // NetBanking
		bookingID = RecipientPage.diagnostic_bookingSuccessPage(packageName, aptTime);
		String bookingStatus = Browser.get_Document_Attribute(Environment, "zoyloBooking", "bookingId", bookingID,
				"bookingStatusCode");
		System.out.println("Booking status code is : " + bookingStatus);
		Assert.assertEquals("SCHEDULED", bookingStatus);

		// Verify DC upcoming appointments
		Browser.openUrl(EnvironmentURL);
		RecipientPage.header_UserDetails_SelectMenu("My Appointments", "diagnostic");
		Browser.waitFortheElementXpath("(//h5[text()='" + packageName + "'])[1]");

		// Verify admin appointment
		Browser.openUrl(EnvironmentAdminURL);
		String zoneCode = Browser.get_Document_Attribute(Environment, "zoyloLabsCityZones", "cityData.cityName",
				location_1, "zoneCode");
		AdminPage.lab_addAppointmentList_navigate();
		AdminPage.lab_addAppointment_searchList("Mobile Number", user_id);
		Browser.waitFortheElementXpath(
				"//td[text()='+91" + user_id + "']/following-sibling::td[text()='" + zoneCode + "']");
	}

	@AfterClass(groups = { "DIAG", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
