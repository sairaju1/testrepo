// @Author: Sagar Sen

package com.web.recipientTS.Diagnostic;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Person;
import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class DIAG_206_Book_package_signUpRedirection_addAddress_cod extends LoadProp {

	public TestUtils Browser;
	public Person p;
	public RecipientPage RecipientPage;
	public String packageName, location, finalPrice, aptDate, aptDay, aptTime, bookingID, firstname, lastname, mobile,
			email, Password, state;

	@BeforeClass(groups = { "DIAG", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		RecipientPage = new RecipientPage(driver);
		p = Browser.personData();
		packageName = rec_packageName;
		location = operatingCity_2;
		aptDate = Browser.getModifiedDate(1);
		aptDay = Browser.getCurrentDay_format_modifier("EEEEE", 1).toUpperCase();
		Browser.openUrl(EnvironmentURL);
		firstname = p.getFirstName();
		lastname = p.getLastName();
		email = p.getEmail();
		mobile = "7" + Browser.generateRandomNumber(9);
		Password = "Zoylo@123";
		state = "Telangana";
	}

	@Test(groups = { "DIAG", "High" })
	public void packageApt_signUpRedirection_addAddress() throws Exception {
		RecipientPage.GenericSearch("Diagnostics", location, packageName);
		// PD page
		finalPrice = RecipientPage.diagnostic_serviceDetailPage(false, packageName, "");
		// SignUP
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_link);
		RecipientPage.SignUp_Details(firstname, lastname, mobile, email, Password);
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_signupButton);
		Thread.sleep(2000);
		String OTP = Browser.getOtp(email, Environment);
		Browser.waitFortheElementXpath(Elements_Recipients.signUp_otp);
		Browser.enterTextByXpath(Elements_Recipients.signUp_otp, OTP);
		Browser.clickOnTheElementByXpath(Elements_Recipients.signUp_OTP_submitButton);
		aptTime = RecipientPage.diagnostic_checkOut_addAddress_Schedule("Default automation", "Address line two", Elements_Recipients.dcAddressLocality, state, location,  "500081");
		Browser.waitFortheElementXpath("//div[@class='row']/div[contains(., '" + location
				+ "')]/following-sibling::div/div/span[text()='" + aptTime + "']");
		RecipientPage.diagnostic_checkout_bookingFor("Self", "", "", "");
		RecipientPage.diagnostic_checkout_orderSummary("No", packageName, Integer.parseInt(finalPrice), 0);
		RecipientPage.diagnostic_checkout_selectPaymentMode_placeOrder(dcPay_cod);
		bookingID = RecipientPage.diagnostic_bookingSuccessPage(packageName, aptTime);
		String bookingStatus = Browser.get_Document_Attribute(Environment, "zoyloBooking", "bookingId", bookingID,
				"bookingStatusCode");
		System.out.println("Booking status code is : " + bookingStatus);
		Assert.assertEquals("SCHEDULED", bookingStatus);
	}

	@AfterClass(groups = { "DIAG", "High" })
	public void tearDown() throws Exception {
		String id = Browser.get_Document_ID(Environment, "zoyloUser", "emailInfo.emailAddress", email);
		Browser.remove_Document(Environment, "zoyloUser", "emailInfo.emailAddress", email);
		Browser.remove_Document(Environment, "zoyloUserProfile", "zoyloId", id);
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterPhone", "+91" + mobile);
		Browser.remove_Document(Environment, "zoyloAppointment", "requesterInfo.requesterPhone", "+91" + mobile);
		Browser.quit();
	}

}
