// @Author: Sagar Sen

package com.web.recipientTS.Diagnostic;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import objectRepository.Elements_Recipients;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_91_Book_package_priceAlertPopUp_COD extends LoadProp {

	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String packageName, location_1, location_2, aptTime, bookingID, location_1_Pprice, location_2_Pprice;
	
	@BeforeClass(groups = { "DIAG", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Elements_Recipients.Recipients_PageProperties();
		Browser = new TestUtils(driver);
		RecipientPage = new RecipientPage(driver);
		packageName = rec_packageName;
		location_1 = operatingCity_1;
		location_2 = operatingCity_2;
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterPhone", "+91" + user_id);
		Browser.remove_Document(Environment, "zoyloAppointment", "requesterInfo.requesterPhone", "+91" + user_id);
	}
	
	@Test(groups = { "DIAG", "High" })
	public void priceAlertPopUp() throws Exception {
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		RecipientPage.GenericSearch("Diagnostics", location_1, "");
		location_1_Pprice = RecipientPage.diagnostic_homePage_popularPackageClick(packageName);
		Browser.openUrl(EnvironmentURL);
		RecipientPage.GenericSearch("Diagnostics", location_2, "");
		location_2_Pprice = RecipientPage.diagnostic_homePage_popularPackageClick(packageName);
		RecipientPage.diagnostic_serviceDetailPage(false, packageName, location_2_Pprice);
		Browser.waitFortheElementXpath("//h3[text()='Address']"); // addressLable
		Browser.clickOnTheElementByXpath("//p[contains(., '" + location_1 + "')]/preceding-sibling::div/label"); // locationRadioBtn
		Browser.waitFortheElementXpath("//h5[text()='CONFIRMATION']"); // alertPopUp
		Browser.clickOnTheElementByXpath("//button[text()='PROCEED']"); // alertProceedBtn
		Browser.waitFortheElementXpath(Elements_Recipients.dcScheduleLabel); // schedulePopUpLable
		Browser.clickOnTheElementByXpath(Elements_Recipients.dcFirstActiveSlot); // firstTimeSlot
		aptTime = Browser.getTextByXpath(Elements_Recipients.dcFirstActiveSlot);
		Browser.clickOnTheElementByXpath(Elements_Recipients.dcScheduleOkBtn); // OkBtn
		RecipientPage.diagnostic_checkout_bookingFor("Self", "", "", "");
		RecipientPage.diagnostic_checkout_orderSummary("No", packageName, Integer.parseInt(location_1_Pprice), 0);
		RecipientPage.diagnostic_checkout_selectPaymentMode_placeOrder(dcPay_cod);
		bookingID = RecipientPage.diagnostic_bookingSuccessPage(packageName, aptTime);
	}
	
	@AfterClass(groups = { "DIAG", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}