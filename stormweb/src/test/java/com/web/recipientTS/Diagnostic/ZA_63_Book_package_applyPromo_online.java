// @Author: Sagar Sen

package com.web.recipientTS.Diagnostic;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import testBase.AdminPage;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class ZA_63_Book_package_applyPromo_online extends LoadProp {

	public TestUtils Browser;
	public AdminPage AdminPage;
	public RecipientPage RecipientPage;
	public String packageName, location, aptDate, aptDay, aptTime, bookingID, coupon;
	public Double value_per = 50.0;
	public int pkgPrice, discPer = (int) Math.round(value_per), grandPrice;

	@BeforeClass(groups = { "DIAG", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		AdminPage = new AdminPage(driver);
		RecipientPage = new RecipientPage(driver);
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterPhone", "+91" + user_id);
		packageName = rec_packageName;
		location = operatingCity_2;
		aptDate = Browser.getModifiedDate(1);
		aptDay = Browser.getCurrentDay_format_modifier("EEEEE", 1).toUpperCase();
		coupon = dc_promo;
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		Browser.update_double_MongoAttribute(Environment, "zoyloServicePromotion", "promotionCode", coupon,
				"promotionDetailInfo.promotionalValue", value_per);
	}

	@Test(groups = { "DIAG", "High" })
	public void book_Package_applyPromo() throws Exception {
		Thread.sleep(5000);
		RecipientPage.GenericSearch("Diagnostics", location, packageName);
		// PD page
		pkgPrice = Integer.parseInt(RecipientPage.diagnostic_serviceDetailPage(false, packageName, ""));
		aptTime = RecipientPage.diagnostic_checkOut_selectAddress_Schedule(location, aptDate, aptDay);
		Browser.waitFortheElementXpath("//div[@class='row']/div[contains(., '" + location
				+ "')]/following-sibling::div/div/span[text()='" + aptTime + "']");
		RecipientPage.diagnostic_checkout_bookingFor("Self", "", "", "");
		RecipientPage.selectAvailable_Coupon(coupon);
		grandPrice = RecipientPage.diagnostic_checkout_orderSummary("Apply", packageName, pkgPrice, discPer);
		RecipientPage.diagnostic_checkout_selectPaymentMode_placeOrder(dcPay_online);
		RecipientPage.verify_transactionAmount_PG(Double.valueOf(Integer.toString(grandPrice)));
		RecipientPage.MakePayment("", ""); // NetBanking
		bookingID = RecipientPage.diagnostic_bookingSuccessPage(packageName, aptTime);
		String bookingStatus = Browser.get_Document_Attribute(Environment, "zoyloBooking", "bookingId", bookingID,
				"bookingStatusCode");
		System.out.println("Booking status code is : " + bookingStatus);
		Assert.assertEquals("SCHEDULED", bookingStatus);

		// Verify admin appointment
		Browser.openUrl(EnvironmentAdminURL);
		AdminPage.adminLogin(adminuser_id, adminuser_pw);
		AdminPage.lab_addAppointmentList_navigate();
		AdminPage.lab_addAppointment_searchList("Mobile Number", user_id);
		int adminGrandTotal = Integer.parseInt(Browser.getTextByXpath("//td[text()='+91" + user_id + "']/following-sibling::td[7]"));
		Assert.assertEquals(adminGrandTotal, grandPrice);
	}

	@AfterClass(groups = { "DIAG", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}
}
