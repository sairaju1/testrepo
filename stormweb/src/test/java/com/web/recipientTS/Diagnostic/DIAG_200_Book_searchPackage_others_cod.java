// @Author: Sagar Sen

package com.web.recipientTS.Diagnostic;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.codearte.jfairy.producer.person.Person;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class DIAG_200_Book_searchPackage_others_cod extends LoadProp {

	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String packageName, location, finalPrice, aptDate, aptDay, aptTime, bookingID;
	public Person p;

	@BeforeClass(groups = { "DIAG", "High" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		p = Browser.personData();
		RecipientPage = new RecipientPage(driver);
		packageName = rec_packageName;
		location = operatingCity_2;
		aptDate = Browser.getModifiedDate(1);
		aptDay = Browser.getCurrentDay_format_modifier("EEEEE", 1).toUpperCase();
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterPhone", "+91" + user_id);
	}

	@Test(groups = { "DIAG", "High" })
	public void searchPackage_Book_Others() throws Exception {
		Thread.sleep(5000);
		RecipientPage.GenericSearch("Diagnostics", location, packageName);
		// PD page
		finalPrice = RecipientPage.diagnostic_serviceDetailPage(false, packageName, "");
		aptTime = RecipientPage.diagnostic_checkOut_selectAddress_Schedule(location, aptDate, aptDay);
		Browser.waitFortheElementXpath("//div[@class='row']/div[contains(., '" + location
				+ "')]/following-sibling::div/div/span[text()='" + aptTime + "']");
		RecipientPage.diagnostic_checkout_bookingFor("Others", p.getFirstName(), "8" + Browser.generateRandomNumber(9),
				"Male");
		RecipientPage.diagnostic_checkout_orderSummary("No", packageName, Integer.parseInt(finalPrice), 0);
		RecipientPage.diagnostic_checkout_selectPaymentMode_placeOrder(dcPay_cod);
		bookingID = RecipientPage.diagnostic_bookingSuccessPage(packageName, aptTime);
		String bookingStatus = Browser.get_Document_Attribute(Environment, "zoyloBooking", "bookingId", bookingID,
				"bookingStatusCode");
		System.out.println("Booking status code is : " + bookingStatus);
		Assert.assertEquals("SCHEDULED", bookingStatus);
	}

	@AfterClass(groups = { "DIAG", "High" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}