// @Author: Sagar Sen

package com.web.recipientTS.Diagnostic;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.LoadProp;
import testBase.RecipientPage;
import testBase.TestUtils;

public class DIAG_209_Book_Package_paymentGatewayOptions extends LoadProp {

	public TestUtils Browser;
	public RecipientPage RecipientPage;
	public String packageName, location, id;

	@BeforeClass(groups = { "DIAG", "Low" })
	public void setUp() throws Exception {
		LoadBrowserProperties();
		Browser = new TestUtils(driver);
		RecipientPage = new RecipientPage(driver);
		packageName = rec_packageName;
		location = operatingCity_2;
		Browser.openUrl(EnvironmentURL + "/login");
		RecipientPage.Recipient_Sigin(user_id, user_pw);
		id = Browser.get_Document_Attribute(Environment, "zoyloLabsMasterPackage", "packageData.packageName",
				packageName, "_id");
	}

	@DataProvider(name = "PaymentGateways")
	public String[][] createData1() {
		return new String[][] { { "Wallet", "AMAZON PAY" }, { "Wallet", "MOBIKWIK" }, { "Wallet", "OLAMONEY" },
				{ "Cards", "" }, { "Paypal", "" }, };
	}

	@Test(groups = { "DIAG", "Low" }, dataProvider = "PaymentGateways") //
	public void labApt_paymentOptions(String PaymentGateway, String PaymentGateway_Type) throws Exception { //
		Browser.openUrl(EnvironmentURL + "/diagnostics/package-detail/" + id);
		Browser.waitFortheElementXpath("//h1[text()='" + packageName + " ']");
		Browser.clickOnTheElementByXpath("//div[@class='sticky-top' and contains(., '" + packageName + "')]/a"); // bookBtn
		RecipientPage.diagnostic_checkOut_selectAddress_Schedule(location, "", "");
		RecipientPage.diagnostic_checkout_bookingFor("Self", "", "", "");
		RecipientPage.diagnostic_checkout_selectPaymentMode_placeOrder(dcPay_online);
		RecipientPage.MakePayment_Options(PaymentGateway, PaymentGateway_Type);
		Browser.waitFortheElementXpath("//span[contains(., 'BOOKING CONFIRMED')]"); // confirmHeader
		Browser.waitFortheElementXpath("//p[text()='Package Name']/following-sibling::p[text()='" + packageName + "']"); // pkgName
	}
	
	@AfterMethod(groups = { "DIAG", "Low" })
	public void LogOut() throws Exception{
		
		Browser.remove_Document(Environment, "zoyloBooking", "requesterInfo.requesterPhone", "+91" + user_id);
	}

	@AfterClass(groups = { "DIAG", "Low" })
	public void tearDown() throws Exception {
		Browser.quit();
	}

}
