package objectRepository;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;


public class Elements_Recipients  {
	
	public static Properties prop = new Properties();
	public static FileInputStream inStream;
    public static WebDriver driver;

    public static By enrollment1_h5 = By.xpath("//h5");// Another way of initialization to avoid xpath/id by defining in test case

    //SignIn elements
  	public static String SignIn_Mobile,SignIn_Password,SignIn_Login, SignIn_signUp;
  	
  //SignUp page elements
  	public static String signUp_link, signUp_firstName, signUp_lastName, signUp_MobileNumber, signUp_Email, signUp_password, signUp_signupButton, signUp_TermsConditions, signUp_signInLink, signUp_otp, 
  	signUp_OTP_submitButton, signUp_firstName_Error, signUp_lastName_Error, signUp_MobileNo_Error, signUp_Email_Error, signUp_Password_Error;
  	
  	//Notifications and Other Button
  	public static String BookingSucess_Notification, Click_ConfirmAppointmentButton,Click_PaymentToProceedButton;
  		
  //Home Page
  	public static String home_locationSearch, home_locationSearch_Suggestion, home_headerUserIcon,home_logout,home_MyProfile,home_getLoginUserName,home_BookHomeHealthcare,
  	home_PracticeLogin_customerLogin, home_customerLogin_PagePlaceHolder, home_AppStoreLink,home_GetOnlineConsultation,home_homeheathcare_ScrollToElement,home_homehaelthcare_Booknow,
  	
  	home_AppStore_Page,home_androidLink,home_androidLink_Page ,home_zoylologo,home_signIn, home_ProfileElipse,  home_zoylologo2,home_footer_ClickDiagnosticinHyderabad, home_ClickOnDiagnosticTab,
  	home_specializationSuggestion, home_listPracticeBtn,home_OnlineConsultation_Proceed,home_header_MyConsultation,home_ASkZoy,home_header_MyConsultation_ChatConversation,home_header_MyConsultation_PreAssessment,
  	home_header_MyConsultation_PreAssessment_Symptoms,MyConsultation_PreConsultation_Age,MyConsultation_PreConsultation_Gender,MyConsultation_PreConsultation_Name,MyConsultation_PreConsultation_DiseaseHeadingOne,
  	MyConsultation_PreConsultation_DiseaseHeadingTwo,MyConsultation_PreConsultation_DiseaseHeadingThree,Myconsultation_ChatConversion_Click_activeUser,MyConsultation_ChatConversionTab,MyConsultation_ChatConversionTab_GetimageName;
  	
	//forgot Password
  	public static String forgotpasswd_link,forgotpasswd_entermobnum,forgotpasswd_submit,forgotpasswd_getotp,forgotpasswd_submitotp,
  	forgotpasswd_resetpwd,forgotpasswd_confirmreset,forgotpasswd_submitreset;
  	
  //Profile Page
  	public static String  ProfilePage_Click_DefaultClinic, ProfilePage_doctorName, ProfilePage_bookButton, ProfilePage_ConfirmButton, ProfilePage_OtherClinicTab, ProfilePage_aptDate, ProfilePage_clincName, ClinicPage_doctorsTab;
  
  	
  //MyProfile
  	public static String myProfile_editButton, myProfile_EditfirstName, myProfile_EditlastName, myProfile_Editgender, myProfile_EditbloodGroup, myProfile_EditDOB, myProfile_EditCountry, myProfile_EditState, myProfile_EditCity, myProfile_EditpinCode, myProfile_SaveButton
  	, myProfile_BloodGroup, myProfile_Address, myProfile_FullName, myProfile_ProfilePicture_EditButton, myProfile_ProfileSrc, myProfile_ProfilePicture_Message, myProfile_Save_successMessage, myProfile_Change_OldPassword, myProfile_Change_NewPassword, myProfile_OldPassword_SuccessFail, myProfile_Change_OldPassword_Error, myProfile_Change_NewPassword_Error, 
  	myProfile_editAddress_Btn, myProfile_changePasswordBtn, myProfile_Change_ConfirmPassword, myProfile_Change_ConfirmPassword_Error,myProfile_Editphone,myProfile_Phone,myprofile_Address_AddressType,
  	myprofile_Address_AddressLineone,myprofile_Address_AddressLineTwo,myprofile_Address_Locality,myprofile_Address_Country,myprofile_Address_State,myprofile_Address_city,myprofile_Address_pincode,
  	myprofile_Address_Add,myprofile_Click_AddAddress;	
  	
  	
  //Header
  	public static String  header_myAppointments,header_myAppointments_Reschedule,header_myAppointments_ConfirmAppointment,header_myAppointments_RescheduleMsg,header_myAppointments_RescheduleMsg_Close,
  	header_myAppointments_AppointmentDate,header_myAppointments_CancelAppointmentLink,header_myAppointments_CancelAppointment_Confirm,header_myAppointments_CancelTab,header_myAppointments_CancelTab_AppointmentDate,
  	header_myAppointments_PackageTab,header_myAppointments_Package_Reschedule,header_myAppointments_Package_Reschedule_Confirm,header_myAppointments_Package_Reschedule_Confirm_Msg,
  	header_myAppointments_Package_Reschedule_Confirm_PopupClose,header_myAppointments_Package_Cancel_Link,header_myAppointments_Package_Cancel_Confirm,header_myAppointments_Package_CancelTab,
  	header_myAppointments_Package_CancelTab_Date;
  	

 
  //Booking Sucessfull
  	public static String bookingSucesssfull_AppointmentBookedFor,bookingSucessfull_AmountToBepaid;
  
 
  //Footer
  	public static String  footer_BookDoctorAppointments_Link;
  	
  //Generic Search
  	public static String Select_WellnessMenu,GenericSearch_Click_Module,GenericSearch_Enter_ModuleData,GenericSearch_ModuleData_Suggession,GenericSearch_Button;
  	
  	
  //Booking Details
  	public static String bookingDetails_otherRadioButton,bookingDetails_Others_Name, bookingDetails_Others_Contact,bookingDetails_Changeslot,bookingDetails_ChangeSlot_NightTab,bookingDetails_ChangeSlot_OkButton,
  	 bookingDetails_Others_NameError,bookingDetails_OC_ProceedToPay,bookingDetails_OC_EnterPromocode,bookingDetails_OC_Promocode_Apply,bookingDetails_IAgree_ValidationMsg, bookingDetails_termsAgree,bookingDetails_WaitForAppointmentSchedule,
  	bookingDetails_Promocode, bookingDetails_Promocode_Apply,bookingDetails_GetTime, bookingDetails_Others_Age, bookingDetails_selfAge;
  	 
  	
  //Wellness
  	
  	public static String Wellness_Package_Pricerange_Book,Wellness_Package_Calendar_Ok,Wellness_Package_Confirm,Wellness_Package_PackageStartingPrice_Book,Wellness_Package_ZFCFixedPrice_Book,
  	Wellness_Package_ZFCPercentage_Book,Wellness_doctor_Book,Wellness_doctor_Confirm,Wellness_ListPage_BookPackage;
  	
  	//Clinic
  	
  	public static String Clinic_SelectDoctorTab;
  	
  	//HomeHealthCare
  	public static String HomeHealthCare_Name,HomeHealthCare_Mobile,HomeHealthCare_State,HomeHealthCare_City,HomeHealthCare_Email,HomeHealthCare_Package,HomeHealthCare_Submit,HomeHealthCare_BookNow,
  	 HomeHealthCare_EnterOTP, HomeHealthCare_VerifyOTP,HomeHealthCare_Msg,HomeHealthCare_ClosePopUp;
  	
  	//PMS 
  	public static String PMS_SigIn_Mobile,PMS_SigIn_Password,PMS_Login,PMS_Select_Online,PMS_Select_Clinic,PMS_Select_Clinic_SelectButton;
  	
  	//PMS_prescription
  	public static String PMS_Prescription_Complaints_SaveNotification,PMS_Prescription_disease_SaveNotification,PMS_Prescription_investigation_SaveNotification, PMS_Prescription_Medication_Formulation,
  	 PMS_Prescription_Medication_BrandName, PMS_Prescription_Medication_Dosage,PMS_Prescription_Medication_Frequency,PMS_Prescription_Medication_Duration,PMS_Prescription_Medication_DurationType,
  	 PMS_Prescription_AddNewMedication,PMS_Prescription_Medication_SaveNotification,PMS_Prescription_Downlaod_URL,PMS_Prescription_Complaints_Description,PMS_Prescription_Complaints_SinceValue,PMS_Prescription_Complaints_ProblemSince,
  	PMS_Prescription_Complaints_Severity,PMS_Prescription_Complaints_Save,PMS_Prescription_Diseases_Description,PMS_Prescription_Diseases_NatureOFDiagnosis,PMS_Prescription_Diseases_Save,
  	PMS_Prescription_Investigation,PMS_Prescription_Investigation_Save,PMS_Prescription_Medication_Save,PMS_Prescription_EndPrescription,PMS_Prescription_ViewPrescription,
  	PMS_Prescription_ViewPrescription_Close,PMS_Prescription_Download,PMS_Click_ONlineConsultation,PMS_CloseNotification,PMS_GeneratePrescription,PMS_SignedINName,PMS_GetChat_Text,PMS_Chat_Attachfile, PMS_Chat_User ;
  	
  	//PMS ADD APpointment
  	public static String PMS_ADDAppointment, PMS_ADDAppointment_Next,PMS_ADDAppointment_Mobile,PMS_ADDAppointment_EnterAddress,PMS_ADDAppointment_Confirm,PMS_ADDAppointment_Sucessfull_Msg,
  	PMS_ADDAppointment_OK,PMS_DashBoard_Scheduled,PMS_DashBoard_Details,PMS_ADDAppointment_StartConsultation,PMS_ADDAppointment_EndPrescription;
  	
  	//PMS_New_Prescription
  	public static String PMS_Prescription_Vitals_Height,PMS_Prescription_Vitals_Weight, PMS_Prescription_Vitals_bp,PMS_Prescription_Vitals_Temperature;
  	public static String PMS_Prescription_Complaints_Click, PMS_Prescription_Complaints_EnterComplaints, PMS_Prescription_Complaints_Select_Suggession;
  	public static String PMS_Prescription_Diseases_Click, PMS_Prescription_Diseases_EnterDiseases, PMS_Prescription_Diseases_Select_Suggession,PMS_Prescription_Notes;
  	public static String PMS_Prescription_Medication, PMS_Prescription_Medication_duration, PMS_Prescription_Medication_TimeofTheDay,PMS_Prescription_Medication_ToBeTaken,PMS_Prescription_Medication_ADDDrug,
  	PMS_Prescription_GeneralADvice,PMS_Prescription_ReviewAfter_Number,PMS_Prescription_ReviewAfter_Period,PMS_Prescription_Submit,PMS_Prescription_GeneratePrescription,PMS_PrescriptionTab,PMS_PreAssessmentTab_UserName,
  	PMS_Prescription_GeneratePrescription_confirm,PMS_Prescription_Download_Attribute,PMS_PresAssessmentTab,PMS_PreAssessment_GetSymptom;
  	
  
  	
  	
  	
  	
  	
  	
  	//Online Consultation
  	public static String OC_StartConsultation,OC_Notification, OC_Chat_TextArea, OC_Chat_textArea_Send,Menu_AskZoylo,OC_Consultation,OC_LetGetStarted,OC_StartNewAssessment,OC_MySelf,OC_Click_SearchSymptoms,
  	OC_Enter_Symptoms,OC_Symptoms_Suggession,OC_Select_Yes,OC_Select_No,OC_Select_OK,OC_OkayUnderstood,OC_ShowMyResult,OC_Button_StartConsultation,OC_Start_PresConsultataion_Assessment,
  	OC_EndSession,OC_EndChating,OC_NoThanks,OC_SkipToDcoctorConsultation,OC_Checkout_ProceedToPayment,OC_BookingSucessfull_StartConsultation,OC_PreAssessment_Male,OC_PreAssessment_Age,OC_PreAssessment_Age_Done,
  	OC_PreAssessment_NeverSmoking, OC_PreAssessment_NO,OC_PreAssessment_MySelf,OC_PreAssessment_OKUnderstood,OC_PreAssessment_ClickHereToChat, OC_PreAssessment_Others,OC_Quro_StartConsultation,ASKZOY_ConsultNow,
  	OC_Download,OC_Checkout_EnterCouponCode, OC_Checkout_EnterCouponCode_Apply;
  	
  	public static String pdf_container, pdf_temperature, pdf_pulse, pdf_respiration, pdf_bp1, pdf_bp2, pdf_spo2, pdf_height, pdf_weight, pdf_complaintText,
	pdf_complaintSinceNum, pdf_complaintSinceUnit, pdf_complaintSeverity, pdf_diseaseText, pdf_diseaseNature, pdf_medecineName, pdf_medecineBrand,
	pdf_medecineDosage, pdf_medecineFrequency, pdf_medecineDuration, pdf_medecineDurationType,pdf_medicineTimeoftheDay,pdf_medicinetoBeTaken;
  	
  	//Payment Selection Page
  	public static String PaymentSelection_WaitforAmount_Display,PaymentSelection_ToPay;
  	
  	//	-------------------------------------------
  	
  	
  

  	
	
	
	
	//myAppointments
	public static String myAppointments_fromDate, myAppointments_toDate, myAppointment_dateSubmitButton, myAppointment_userCards, myAppointment_noRecords, myAppointment_rescheduleButton, myAppointment_CancelButton, myAppointment_BookingDay, myAppointment_rescheduleBookButton, myAppointment_RescheduleConfirm, myAppointment_OK, myAppointment_CancelPopUp,
	myAppointment_CancelYesButton, myAppointment_cancelSuccess, myAppointment_reBook;
	
	
	
	
	//Listing Page
	public static String listing_ProviderName, listingPage_Doctor_BookAppointment_Button,listingPage_Diagnostic_SearchTestname,listingPage_Diagnostic_SelectTestCheckBox,listingPage_Diagnostic_BookButton,listingPage_Diagnostic_Packagelink,listingPage_Diagnostic_SearchPackageName,listingPage_Diagnostic_SelectPackageCheckbox, listingPage_Name_Specialisation_Dropdown, 
	listingPage_Name_Specialisation_SearchBox, listingPage_Location_Dropdown, listingPage_Location_SearchBox, listingPage_Location_SuggestionOne, listingPage_ChangeClinicLink,listingPage_Current_Location,listingPage_Userimage_Icon,listingPage_DiagnosticProfile_SelectTestCheckBox,listingPage_Diagnostic_Testlink, listingPage_Doc_clinicName, listingPage_filterBtn,
	listingPage_filter_distanceTxt, listingPage_filter_distanceSlider, listingPage_filter_applyFilterBtn, listingPage_filter_onlineCheckBox, listingPage_filter_clearAllBtn, listingPage_filter_selectDate,listingPage_Diagnostic_PackageTab,listingPage_Diagnostic_Package_Expand,
	listingPage_Diagnostic_Totalamount;
	
	
	
	
	
	
	
	//Family Details
	public static String family_AddNewMember, family_Relationship, family_firstName, family_lastName, family_gender, family_Save, family_Relationship_Error, family_firstName_Error, family_lastName_Error, family_gender_Error, family_EditMember, family_NameOfFirstMember, family_DOB, family_DOB_error, family_edit_update;
	
	//Promocode
	public static String bookingDetails_ClickOnApplyCoupon,bookingDetails_EnterpromoCode,bookingDetails_Applypromocode, bookingDetails_AmtAfterApplyingPromo;
	
	//Package
	public static String Myappointments_Package_ReschedulePackage,Myappointment_Package_Calendar_Next,Myappointment_Package_Cancel,
	Myappointment_Package_Cancel_Confirm, Myappointment_Package_Cancel_FinalConfirm, Myappointment_Package_CancelledTab,Myappointment_Package_Calendar_Ok, Myappointment_Package_Reschedule_Confirm,
	Myappointment_Package_Reschedule_FinalConfirm, Myappointment_Package_GetDate;
	
	//Doctor tab Myappointments
	public static String Myappointment_Doctor_Reschedule,Myappointment_Doctor_Calendar_Next,Myappointment_Doctor_Reschedule_SelectSession,Myappointment_Doctor_Reschedule_Confirm,Myappointment_Doctor_GetDate,
	Myappointment_Doctor_CancelAppointment,Myappointment_Doctor_Cancel_Confirm,Myappointment_Doctor_Cancel_FinalConfirm,Myappointment_Doctor_CanceledTab,Myappointment_Diagnostic_ProfileIcon;
	
	//Zoylo Wallet
	public static String wallet_search, wallet_searchBtn, wallet_appointmentID, wallet_balanceAmt, wallet_options, wallet_balance;
	
	// slots
	public static String slot_sessions, slot_session_first, slot_session_last;
	
	
	//Clinic Page
	public static String ClinicListingPage_Package_BookPackge,ClinicListingPage_Package_Calender_Ok,ClinicListingPage_Package_Confirm,
	ClinicListingPage_Doctor_BookAppointment,ClinicListingPage_ClickDoctorTab, ClinicListingPage_Doctor_Confirm;
	
	
	//Doctor Enrollment
		public static String DoctorEnroll_Name,DoctorEnroll_City,DoctorEnroll_Email,DoctorEnroll_Phone,DoctorEnroll_OTP,DoctorEnroll_Registration,DoctorEnroll_SendOTP,DoctorEnroll_Verify,
		DoctorEnroll_ocTerms, DoctorEnroll_terms, DoctorEnroll_termsCheck, DoctorEnroll_ocTermsCheck;
	
	
	 
	 //My Prescription
	 public static String myPrescription_viewBtn;
	 
	 // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ DIAGNOSTIC ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 public static String homePage_DiagnosticIcon, dcHomePage_pathologyHeading, dcHomePage_popularPackageHeading, dcAddressLabel, dcScheduleLabel, dcFirstActiveSlot, 
	 dcScheduleOkBtn, dcOthersGender, dcAddAddressBtn, dcAddressPopUpHeader, dcAddressType, dcAddressLine1, dcAddressLine2, dcAddressLocality, dcAddressPin;
	
	public static WebDriver Recipients_PageProperties()throws Exception{
	  	
		//SignIn elements
		SignIn_Mobile="//input[@id='phoneLbl']";
		SignIn_Password="//input[@id='passLbl']";
		SignIn_Login="//button[@type='submit']";
		//SignIn_signUp="//span[contains(., 'Sign Up')]";
		
		
		//SignUp page
		
		signUp_link="//a[contains(text(),'Sign Up')]"; //XPATH
		signUp_firstName="//input[@id='firstName']"; //XPATH
		signUp_lastName="//input[@id='lastName']"; //XPATH
		signUp_MobileNumber="//input[@id='phoneNumber']"; //XPATH
		signUp_Email="//input[@id='emailId']"; //Xpath
		signUp_password="//input[@id='password']"; //Xpath
		signUp_signupButton="//button[@type='submit']"; //Xpath
		signUp_otp="//input[@placeholder='Enter OTP']"; //Xpath
		signUp_OTP_submitButton="//button[contains(text(),'Submit')]"; //Xpath		
		signUp_TermsConditions=""; //XPATH
		
		//Notifications
		BookingSucess_Notification="//h2[contains(@class,'green-text d-flex align-items-center flex-column flex-md-row')]";
		Click_ConfirmAppointmentButton="//button[contains(text(),'Confirm Appointment')]";
		Click_PaymentToProceedButton="//button[contains(text(),'Proceed to Pay')]";
		
		
		//HOME PAGE
		
		home_locationSearch="(//input[@id='location-detect'])[1]"; //Xpath
		home_locationSearch_Suggestion="(//span[@class='pac-matched'])[1]"; //Xpath
		home_getLoginUserName="//span[contains(@class,'user white-text font-bold')]";
		home_headerUserIcon="//a[@class='d-none d-md-block']"; //Xpath
		home_logout="//a[contains(text(),'Logout')]"; //XPATH
		home_MyProfile="//a[contains(text(),'My Profile')]";
		home_BookHomeHealthcare="//a[contains(., ' Book Home Healthcare')]";
		home_GetOnlineConsultation="//a[contains(., 'Get Online Consultation')]";
		home_homeheathcare_ScrollToElement="(//button[contains(.,'I wish to Partner with zoylo')])[1]";
		home_homehaelthcare_Booknow="//a[@class='btn btn-primary mt-3' and text()='Book Now']";
		home_OnlineConsultation_Proceed="(//button[@type='button'][contains(.,'Proceed')])[2]";
		home_header_MyConsultation="//a[contains(text(),'My Consultation')]";
		home_ASkZoy="//span[@class='icon ask-zoy-icon']";
		home_header_MyConsultation_ChatConversation="//a[contains(., 'Chat Conversation')]";
		home_header_MyConsultation_PreAssessment="//a[contains(., 'Pre Consultation')]";
		home_header_MyConsultation_PreAssessment_Symptoms="//div[@class='container-fluid pt-5 scrollbar']//div[2]//div[1]//div[1]//h5[1]";
		MyConsultation_PreConsultation_Age="(//div[@class='col-md-6'])[2]/p[1]/span[2]";
		MyConsultation_PreConsultation_Gender="(//div[@class='col-md-6'])[2]/p[2]/span[2]";
		MyConsultation_PreConsultation_Name="(//div[@class='col-md-6'])[2]/p[3]/span[2]";
		MyConsultation_PreConsultation_DiseaseHeadingOne="(//h2[@class='mb-0'])[1]";
	  	MyConsultation_PreConsultation_DiseaseHeadingTwo="(//h2[@class='mb-0'])[2]";
	  	MyConsultation_PreConsultation_DiseaseHeadingThree="(//h2[@class='mb-0'])[3]";
	  	Myconsultation_ChatConversion_Click_activeUser="//div[@class='card mx-auto p-2 rounded-0 active']//p[@class='font12 font-medium mb-0']";
	  	MyConsultation_ChatConversionTab="//a[contains(., 'Chat Conversation')]";
	  	MyConsultation_ChatConversionTab_GetimageName="//img[@class='doc-image']";
		
		//Forgot password
		forgotpasswd_link="//a[contains(@class,'grey-text font14')]";
		forgotpasswd_entermobnum="";
		forgotpasswd_submit="//button[contains(@type,'submit')]";
		forgotpasswd_getotp="//input[contains(@placeholder,'Enter OTP')]";
		forgotpasswd_submitotp="//button[@type='button'][contains(text(),'Submit')]";
		forgotpasswd_resetpwd="//input[@id='newPassword']";
		forgotpasswd_confirmreset="//input[@id='confirmPassword']";
		forgotpasswd_submitreset="//button[contains(@type,'submit')]";
		
		//Profile Page
		
		ProfilePage_Click_DefaultClinic="//div[@class='card mb-1 accordion-card']//header[@class='card-header']";
		ProfilePage_OtherClinicTab="(//div[@class='card mb-1 accordion-card'])[2]"; //Xpath
		ProfilePage_ConfirmButton="//div[@id='accordion1']//button[@type='button'][contains(text(),'Confirm Appointment')]"; //Xpath
		
		//MyProfile
		myProfile_editButton="//a[@class='btn btn-primary']"; //Xpath
		myProfile_EditfirstName="//input[@name='Name']"; //Xpath
		myProfile_EditlastName="//input[@data-vv-name='last name']"; //Xpath
		myProfile_EditDOB="//div[@class='vdp-datepicker form-control p-0']//div//input"; //Xpath [YYYY-MM-DD]
		myProfile_Editphone="//input[@id='phone']";  //Xpath
		myProfile_Editgender="gender"; //ID
		myProfile_EditbloodGroup="blood-group"; //ID
		myProfile_SaveButton="//button[@class='btn btn-primary mr-4']"; //Xpath
		myProfile_FullName="(//p[@class='mb-1'])[1]"; //Xpath
		myProfile_Phone="(//p[@class='mb-1'])[4]";    //Xpath
		myprofile_Address_AddressType="//select[@data-vv-name='Address Type']";
	  	myprofile_Address_AddressLineone="//input[@id='addressLine1']";
	  	myprofile_Address_AddressLineTwo="//input[@id='addressLine2']";
	  	myprofile_Address_Locality="//input[@id='locality']";
	  	myprofile_Address_Country="//select[@data-vv-name='Country']";
	  	myprofile_Address_State="//select[@data-vv-name='State']";
	  	myprofile_Address_city="//select[@data-vv-name='City']";
	  	myprofile_Address_pincode="//input[@id='pin']";
	  	myprofile_Address_Add="//button[contains(text(),'Add')]";
	  	myprofile_Click_AddAddress="//a[contains(text(),'Add New')]";
		
		
		//Header	
		header_myAppointments="//a[contains(text(),'My Appointments')]"; //Xpath
		header_myAppointments_Reschedule="//button[contains(text(),'RESCHEDULE APPOINTMENT')]"; //Xpath
		header_myAppointments_ConfirmAppointment="//button[contains(text(),'CONFIRM APPOINTMENT')]";
		header_myAppointments_RescheduleMsg="//h4[contains(text(),'Appointment rescheduled Successfully!')]";
		header_myAppointments_RescheduleMsg_Close="//button[contains(text(),'Close')]";
		header_myAppointments_AppointmentDate="(//p[@class='font14'])[1]";
		header_myAppointments_CancelAppointmentLink="//a[contains(text(),'CANCEL APPOINTMENT')]";
		header_myAppointments_CancelAppointment_Confirm="//button[contains(text(),'Yes')]";
		header_myAppointments_CancelTab="//a[contains(text(),'Cancelled')]";
		header_myAppointments_CancelTab_AppointmentDate="(//p[@class='font1 grey-text d-flex align-items-center'])[1]";
		header_myAppointments_PackageTab="//a[contains(@href,'/my-appointments/package')]";
		header_myAppointments_Package_Reschedule="//button[contains(., 'RESCHEDULE APPOINTMENT')]";
		header_myAppointments_Package_Reschedule_Confirm="//button[contains(text(),'CONFIRM APPOINTMENT')]";
		header_myAppointments_Package_Reschedule_Confirm_Msg="(//h4[@class='brand-color mt-4'])[1]";
		header_myAppointments_Package_Reschedule_Confirm_PopupClose="//button[contains(text(),'Close')]";
		header_myAppointments_Package_Cancel_Link="//a[contains(., 'CANCEL APPOINTMENT')]";
		header_myAppointments_Package_Cancel_Confirm="//button[contains(text(),'YES')]";
		header_myAppointments_Package_CancelTab="//a[contains(., 'Cancelled')]";
		header_myAppointments_Package_CancelTab_Date="(//p[@class='font18 mt-5 mb-0'])";
		
		
		//Booking Sucessfull
		
		bookingSucesssfull_AppointmentBookedFor="//div[@class='grey-box']/div[1]/p";
		bookingSucessfull_AmountToBepaid="(//div[@class='d-flex align-items-center justify-content-between my-4']//h6)[2]";
		
		//Footer
		footer_BookDoctorAppointments_Link="//a[contains(text(),'Book Doctor Appointments')]";
		
		//GenericSearch
		GenericSearch_Click_Module="//button[@class='btn btn-secondary dropdown-toggle']";
		GenericSearch_Enter_ModuleData="//input[@class='header-search form-control form-control-lg  autocomplete-input']";
		GenericSearch_ModuleData_Suggession="(//b[@class='autocomplete-anchor-text'])[1]";
		GenericSearch_Button="//span[@class='icon search-icon m-0']";
		Select_WellnessMenu="//a[@class='dropdown-item'][contains(text(),'Wellness')]";
		
		
		//Booking Details
		bookingDetails_otherRadioButton="//label[contains(text(),'Others')]"; //Xpath
		bookingDetails_Others_Name="//input[@id='name']"; // Xpath
		bookingDetails_Others_Contact="//input[@id='number']"; // Input Xpath
		bookingDetails_Changeslot="//a[@class='font12 secondary-color font-medium']"; //Xpath
		bookingDetails_ChangeSlot_NightTab="//span[@class='icon night-icon']"; //Xpath
		bookingDetails_ChangeSlot_OkButton="//button[contains(text(),'OK')]"; //Xpath
		bookingDetails_Others_NameError="//span[contains(text(),'The patient name field is required.')]"; // Xpath
		bookingDetails_Promocode="//input[@id='coupon']";  //Xpath
		bookingDetails_Promocode_Apply="//a[contains(text(),'Apply')]"; //Xpath
		bookingDetails_GetTime="//div[contains(@class,'timings')]/p[2]";
		bookingDetails_OC_ProceedToPay="//button[contains(text(),'Proceed to Pay')]";
		bookingDetails_OC_EnterPromocode="//input[@id='coupon']";
		bookingDetails_OC_Promocode_Apply="//a[contains(text(),'Apply')]";
		bookingDetails_IAgree_ValidationMsg="//span[@class='font12 help-block text-danger booking-error']";
		bookingDetails_termsAgree="tandc";  //ID
		bookingDetails_WaitForAppointmentSchedule="//h5[contains(text(),'Appointment schedule')]";
		bookingDetails_Others_Age = "//input[@id='ageOther']";
		bookingDetails_selfAge = "//input[@id='age']";
		
		//Payment Selection Page
		
		PaymentSelection_WaitforAmount_Display="//div[@class='grey-bg border-dashed-grey']";
		PaymentSelection_ToPay="//div[@class='grey-bg border-dashed-grey']//h6";
		
		
		
		
		//Wellness
		
		Wellness_Package_Pricerange_Book="//div[@class='media-body w-100'][contains(., 'PriceRange')]/following-sibling::div/button";
		Wellness_Package_Calendar_Ok="//button[contains(text(),'OK')]";	
		Wellness_Package_Confirm="//button[contains(text(),'Confirm')]";
		Wellness_Package_PackageStartingPrice_Book="//div[@class='media-body w-100'][contains(., 'PackageStartingPrice')]/following-sibling::div/button";
		Wellness_Package_ZFCFixedPrice_Book="//div[@class='media-body w-100'][contains(., 'LiangtseZFCFixedPrice')]/following-sibling::div/button";
		Wellness_Package_ZFCPercentage_Book="//div[@class='media-body w-100'][contains(., 'ZFCFixedPricePercentage')]/following-sibling::div/button";
		Wellness_doctor_Book="//div[@class='d-flex align-items-start w-100' and contains(., 'Dr AkashWellness Doctor')]//following-sibling::div/button";
		Wellness_doctor_Confirm="//button[contains(text(),'Confirm')]";
		Wellness_ListPage_BookPackage="//div[@class='d-flex align-items-center justify-content-center w-100'][contains(., 'Liangtsewellness')]/following-sibling::div";
		
		//Clinic
		Clinic_SelectDoctorTab="//a[@class='nav-link']";
		
		//HomeHealthCare
		 HomeHealthCare_Name="//input[@id='name']";
		 HomeHealthCare_Mobile="//input[@id='phoneNumber']";
		 HomeHealthCare_State="//select[@data-vv-name='state']";
		 HomeHealthCare_City="//select[@data-vv-name='city']";
		 HomeHealthCare_Email=" //input[@id='emailId']";
		 HomeHealthCare_Package="(//select[contains(@searchable,'Search here..')])[3]";
		 HomeHealthCare_Submit="//button[contains(text(),'SUBMIT')]";
		 HomeHealthCare_BookNow="//div[@class='caption-blk white-text']//button[@class='btn btn-primary'][contains(text(),'book now')]";
		 HomeHealthCare_EnterOTP="otpCode";
		 HomeHealthCare_VerifyOTP="//button[contains(text(),'Verify Otp')]";
		 HomeHealthCare_Msg="//span[@class='statusMsg']";
		 HomeHealthCare_ClosePopUp="//button[contains(text(),'×')]";
		 
		 
		 //PMS 
		 PMS_SigIn_Mobile="//input[@name='mobile']";
		 PMS_SigIn_Password="//input[@name='password']";
		 PMS_Login="//button[contains(text(),'Login')]";
		 PMS_Select_Online="//label[contains(text(),'Online')]";
		 PMS_Select_Clinic="//label[contains(text(),'Zoylo Rec Clinic')]";
		 PMS_Select_Clinic_SelectButton="//a[contains(text(),'Select')]";
		 PMS_SignedINName="//div[@class='user']/span[2]";
		 PMS_GetChat_Text="(//div[@class='container']/div/p)[1]";
		 PMS_Chat_Attachfile ="//input[@id='file']";
		 PMS_Chat_User="//div[@class='doctor-detail-section active']//h2";
		 
		 
		 //PMS -Prescription
		 
		 PMS_Prescription_Complaints_SaveNotification="//div[@class='alert alert-success' and contains(., 'EPrescription Complaints Saved Sucessfully')]";
		 PMS_Prescription_disease_SaveNotification="//div[@class='alert alert-success' and contains(., 'Health Record Category saved successfully')]";
		 PMS_Prescription_investigation_SaveNotification ="//div[@class='alert alert-success' and contains(., 'Investigation Information saved')]";
		 PMS_Prescription_Medication_Formulation="//input[@id='formulationName']";
		 PMS_Prescription_Medication_BrandName="//input[@id='brandName']";
		 PMS_Prescription_Medication_Dosage="//input[@id='dosageText']";
		 PMS_Prescription_Medication_Frequency="";
		 PMS_Prescription_Medication_Duration="//input[@id='dosageDuration']";
		 PMS_Prescription_Medication_DurationType="";
		 PMS_Prescription_AddNewMedication="//a[contains(text(),'Add a New Medication')]";
		 PMS_Prescription_Medication_SaveNotification="//div[@class='alert alert-success' and contains(., 'Doctor Consultation medication information saved successfully')]";
		 PMS_Prescription_Downlaod_URL="//span[@class='downloadUrl']";
		 PMS_Prescription_Complaints_Description="description";
		 PMS_Prescription_Complaints_SinceValue="sinceValue";
		 PMS_Prescription_Complaints_ProblemSince="sinceCode";
		 PMS_Prescription_Complaints_Severity="severity";
		 PMS_Prescription_Complaints_Save="//div[@id='complaintsBlock']//span[contains(text(),'SAVE')]";
		 PMS_Prescription_Diseases_Description="//div[@class='diseases-block']//input[@id='description']";
		 PMS_Prescription_Diseases_NatureOFDiagnosis="natureOfdiagnosis"; 
		 PMS_Prescription_Diseases_Save="//button[contains(text(),'SAVE')]";
		 PMS_Prescription_Investigation="investigationInfoText";
		 PMS_Prescription_Investigation_Save="//div[@id='investigationBlock']//span[contains(text(),'SAVE')]";
		 PMS_Prescription_Medication_Save="//div[@id='medicationBlock']//span[contains(text(),'SAVE')]";
		 PMS_Prescription_EndPrescription="//button[@type='button'][contains(text(),'End Prescription')]";
		 PMS_Prescription_ViewPrescription="//a[contains(text(),'View Prescription')]";
		 PMS_Prescription_ViewPrescription_Close="//i[@class='fa fa-close']";
		 PMS_Prescription_Download="//a[contains(text(),'Download')]";
		 PMS_Click_ONlineConsultation="//i[@class='sprite sprite-dr-nav-icon-2 onlineconsultation-icon']";
		 PMS_CloseNotification="//a[@class='close-noti']";
		 PMS_GeneratePrescription="//a[@class='generatePrescBtn']";
		 PMS_PresAssessmentTab="//li[contains(.,'Pre-Assessment')]";
		 
		 //PMS ADD Appointment
		 PMS_ADDAppointment="//button[contains(text(),'Add Appointment')]";
		 PMS_ADDAppointment_Next="//a[contains(text(),'NEXT')]";
		 PMS_ADDAppointment_Mobile="//input[@name='patientMobile']";
		 PMS_ADDAppointment_EnterAddress="//input[@placeholder='Enter Address']";
		 PMS_ADDAppointment_Confirm="//div[@class='appointment-confirm-btn']/button";
		 PMS_ADDAppointment_Sucessfull_Msg="(//div[@class='appointment-heading']//h3)[2]";
		 PMS_ADDAppointment_OK="//a[contains(text(),'OK')]";
		 PMS_DashBoard_Scheduled="//div[@class ='fc-title scheduled-status']";
		 PMS_DashBoard_Details="//a[contains(., 'Details')]";
		 PMS_ADDAppointment_StartConsultation="//span[contains(text(),'Start Consultation')]";
		 PMS_ADDAppointment_EndPrescription="//button[contains(text(),'End PRESCRIPTION')]";
		 
		 //PMS_New_Prescription
		 PMS_Prescription_Vitals_Height="//input[@id='height']";
		 PMS_Prescription_Vitals_Weight="//input[@id='weight']";
		 PMS_Prescription_Vitals_bp="//input[@id='bp']";
		 PMS_Prescription_Vitals_Temperature="//input[@id='temperature']";
		 PMS_Prescription_Complaints_Click="(//div[@class='multiselect__select'])[1]";
		// PMS_Prescription_Complaints_EnterComplaints="//input[@id='ajax'][@placeholder='Enter complaint']";
		 PMS_Prescription_Complaints_EnterComplaints="//input[@placeholder='Enter atleast 3 characters of complaint']";
		// PMS_Prescription_Complaints_Select_Suggession="(//span[@class='multiselect__option'])[1]";
		 PMS_Prescription_Complaints_Select_Suggession="//span[contains(@class,'option--highlight')]";
		 
		 PMS_Prescription_Diseases_Click="(//div[@class='multiselect__select'])[2]";
		// PMS_Prescription_Diseases_EnterDiseases="//input[@id='ajax'][@placeholder='Enter Diseases']";
		 PMS_Prescription_Diseases_EnterDiseases="//input[@placeholder='Enter atleast 3 characters of diesease']";
		 PMS_Prescription_Diseases_Select_Suggession="//span[@data-select='Press enter to select'][contains(.,'Dengue fever')]";
		 PMS_Prescription_Notes="//div[@class='form-group mt-0 select-list']//textarea[@id='notes']";
		 PMS_Prescription_Medication="//input[@id='medication']";
		 PMS_Prescription_Medication_duration="//input[@id='duration']";
		 PMS_Prescription_Medication_TimeofTheDay="//span[contains(text(),'Afternoon')]";
		 PMS_Prescription_Medication_ToBeTaken="//label[contains(text(),'To be taken')]//preceding-sibling::select";
		 PMS_Prescription_Medication_ADDDrug="//button[contains(., 'ADD DRUG')]";
		 PMS_Prescription_GeneralADvice="//div[@class='form-group']//textarea[@id='notes']";
		 PMS_Prescription_ReviewAfter_Number="//input[@id='reviewAfterValue']";
		 PMS_Prescription_ReviewAfter_Period="(//select[@class='form-control custom-select'])[2]";
		 PMS_Prescription_Submit="//button[contains(., 'SUBMIT')]";
		 PMS_Prescription_GeneratePrescription="//button[@type='button'][contains(.,'GENERATE PRESCRIPTION')] ";
		 PMS_Prescription_GeneratePrescription_confirm="(//button[@type='button'][contains(.,'ok')])[2]";
		 PMS_Prescription_Download_Attribute="//a[@class='secondary-color text-uppercase font14']";
		 PMS_PrescriptionTab="//a[contains(., 'Prescription')]";
		 PMS_PreAssessment_GetSymptom="(//div[@class='diseaseContent']/ul/li[1])[2]";
		 PMS_PreAssessmentTab_UserName="//h2[contains(text(),'Kanth Cherukuri')]";
		 
		 
		//input[@placeholder='Enter atleast 3 characters of complaint']
		//input[@placeholder='Enter atleast 3 characters of diesease']
		 
		 //OC
		 OC_StartConsultation="//button[contains(text(),'START CONSULTATION')]";
		 OC_Notification="//p[@class='font12 m-0']";
		 OC_Chat_TextArea="//textarea[@placeholder='Enter your message here...']";
		 OC_Chat_textArea_Send="//button[@id='btn-chat']";
		 Menu_AskZoylo="//div/a[contains(., 'Ask')]";
		 OC_LetGetStarted="//button[contains(., 'GET STARTED')]";
		 OC_StartNewAssessment="//span[text()='Start new assessment']";
		 OC_MySelf="//span[contains(., 'Myself')]";
		 OC_Click_SearchSymptoms="(//input[@placeholder='Search for your symptom...'])[2]";
		 OC_Enter_Symptoms="//div[@class='SearchSymptomDropDown__DropDownWrapper-jEUVdE dAzYFt']//input[@placeholder='Search for your symptom...']";
		 OC_Symptoms_Suggession="//div[@class='SearchSymptomDropDown__OptionWrapper-hprbge hOQlcX']//div[1]";
		 OC_Select_Yes="//span[text()='Yes']";
		 OC_Select_No="//span[@class='ButtonSet__Button-iTRulc bSUJaL'][contains(text(), 'No')]";
		 OC_Select_OK="//span[text()='OK']";
		 OC_OkayUnderstood="//span[text()='Okay. Understood']";
		 OC_ShowMyResult="//span[text()='Show my result']";
		 OC_Button_StartConsultation="//button[contains(.,'START CONSULTATION')]";
		 OC_Start_PresConsultataion_Assessment="//button[text()='START PRE-CONSULTATION ASSESSMENT']";
		 OC_EndSession="//button[contains(., 'End Session')]";
		 OC_EndChating="//button[contains(text(),'Yes, End Chating')]";
		 OC_NoThanks="//a[contains(., 'No Thanks')]";
		 OC_SkipToDcoctorConsultation="//a[contains(., 'SKIP TO DOCTOR CONSULTATION')]";
		 OC_Checkout_ProceedToPayment="//button[text()='PROCEED TO PAYMENT']";
		 OC_BookingSucessfull_StartConsultation="//button[contains(., 'START CONSULTATION')]";
		 OC_PreAssessment_Male="//span[contains(text(),'Male')]";
		 OC_PreAssessment_Age="//input[@placeholder='Type your age...']";
		 OC_PreAssessment_Age_Done="//span[contains(text(),'Done')]";
		 OC_PreAssessment_NeverSmoking="//span[contains(., 'Never Smoked')]";
		 OC_PreAssessment_NO="//span[@class='ButtonSet__Button-iTRulc gCSlJJ'][contains(text(),'No')]";
		 OC_PreAssessment_MySelf="//span[contains(., 'Myself')]";
		 OC_PreAssessment_OKUnderstood="//span[contains(., 'OK, I understand')]";
		 OC_PreAssessment_ClickHereToChat="//span[contains(., 'Click here to start chat')]";
		 OC_PreAssessment_Others="//span[text()='Someone Else']";
		 OC_Quro_StartConsultation="//span[contains(., 'Start the consultation')]";
		 ASKZOY_ConsultNow="(//a[contains(., 'CONSULT NOW')])[1]";
		 OC_Download="//a[@class='download-presc secondary-color font10 font-medium text-uppercase']";
		 OC_Checkout_EnterCouponCode="//input[@id='coupon']";
		 OC_Checkout_EnterCouponCode_Apply="//a[contains(text(),'Apply')]";
		
		 
		//PDF Container
			pdf_container="//div[@class='pdfContainer']"; //Xpath
			pdf_temperature="//span[text()='body temperature']/following-sibling::label"; //Xpath
			pdf_pulse="//span[text()='pulse']/following-sibling::label"; //Xpath
			pdf_respiration="//span[text()='respiration rate']/following-sibling::label"; //Xpath
			pdf_bp1="//span[text()='systolic']/following-sibling::label"; //Xpath
			pdf_bp2="//span[text()='diastolic']/following-sibling::label"; //Xpath
			pdf_spo2="//span[text()='spo2']/following-sibling::label"; //Xpath
			pdf_height="//span[text()='body height']/following-sibling::label"; //Xpath
			pdf_weight="//span[text()='body weight']/following-sibling::label"; //Xpath
			
			
			pdf_complaintText="(//tr[2])[4]"; //Xpath
			pdf_complaintSinceNum="(//tr[3])[2]//td[2]"; //Xpath
			pdf_complaintSinceUnit="(//tr[3])[2]//td[3]"; //Xpath
			pdf_complaintSeverity="(//tr[3])[2]//td[4]"; //Xpath
			
			
			
			
			
//			pdf_complaintText="(//tr[2])[2]/following-sibling::tr/td[1]"; //Xpath
//			pdf_complaintSinceNum="(//tr[2])[2]/following-sibling::tr/td[2]"; //Xpath
//			pdf_complaintSinceUnit="(//tr[2])[2]/following-sibling::tr/td[3]"; //Xpath
//			pdf_complaintSeverity="(//tr[2])[2]/following-sibling::tr/td[4]"; //Xpath
			
			pdf_diseaseText="(//tr[2])[5]"; //Xpath
			pdf_diseaseNature="(//tr[3]/td[2]/label)[2]"; //Xpath
			pdf_medecineName="//tr[3]/td[1]/label"; //Xpath
			pdf_medecineBrand="(//tr[3]/td[2]/label)[3]"; //Xpath
			pdf_medecineDosage="//tr[3]//td[2]//label"; //Xpath
			pdf_medecineFrequency="(//tr[3]/td[4]/label)[2]"; //Xpath
			pdf_medecineDuration="//tr[3]/td[5]/label"; //Xpath
			pdf_medecineDurationType="//tr[3]/td[6]/label"; //Xpath
			pdf_medicineTimeoftheDay="//tr[3]//td[3]";
			pdf_medicinetoBeTaken="//tr[3]//td[4]/label";
	//______________________________________________________________	
		
		
		
		
		
		
		
		
		
		// slots
		slot_sessions="//div[@class='days']";
		slot_session_first="(//div[@class='VueCarousel-slide']/ul/li/a)[1]";
		slot_session_last="(//div[@class='VueCarousel-slide']/ul/li/a)[last()]";
		
		//HOME PAGE
		
		
		
		
		
		home_customerLogin_PagePlaceHolder="//input[@placeholder='Email']"; //XPATH
		home_AppStoreLink="//a[@href='http://m.onelink.me/34dfbfec']"; //XPATH
		home_AppStore_Page="//img[@alt='iTunes']"; //XPATH
		home_androidLink="//a[@href='http://m.onelink.me/7a85373']"; //XPATH
		home_androidLink_Page="//span[@class='gb_Za gb_Xa']"; //XPATH
		home_signIn="//a[contains(., 'Sign in')]";
		home_zoylologo="(//*[@class='logo'])[2]";
		home_zoylologo2="//*[@class='logo']";
		home_ProfileElipse="//div[@class='dots']"; //XPATH
		home_footer_ClickDiagnosticinHyderabad="//a[contains(., 'Diagnostics in Hyderabad')]";
		home_ClickOnDiagnosticTab="//span[contains(text(),'diagnostics')]";
		home_specializationSuggestion="(//li[@class='specialities']//div)[1]//ul[@class='options-list']/li";
		home_listPracticeBtn="//button[contains(., 'List Your Practice')]";
		
		//My Prescription
		myPrescription_viewBtn="//a[text()='view']"; //Xpath
		
		//Zoylo wallet
		wallet_search="Appointment Id"; //Name
		wallet_searchBtn="//a[text()='Search']";
		wallet_appointmentID="//span[contains(., 'Appointment ID')]/following-sibling::span";
		wallet_balanceAmt="//div[@class='zoylo-wallet cashback']/ul/li[5]/div/span[2]";
		wallet_options="//a[@href='/profile/myWallet']";
		wallet_balance="//span[@class='balance']";
		
		
		
	
		
		//myAppointments
		myAppointments_fromDate="//label[contains(., 'From')]//following-sibling::div/div/input[@placeholder='dd - mm - yyyy']"; //Xpath
		myAppointments_toDate="//label[contains(., 'To')]//following-sibling::div/div/input[@placeholder='dd - mm - yyyy']"; //Xpath
		myAppointment_dateSubmitButton="(//button[@type='submit'])[2]"; //Xpath
		myAppointment_userCards="//div[@class='appointment-section']"; //Xpath
		myAppointment_noRecords="//div[@class='no-data']"; //Xpath
		myAppointment_rescheduleButton="//input[@placeholder='Reschedule Appointment']"; //Xpath
		myAppointment_CancelButton="//a[@class='cancel-btn']"; //Xpath
		myAppointment_BookingDay="//p[@class='date_time']"; //Xpath
		myAppointment_rescheduleBookButton="//a[contains(., 'Reschedule Appointment')]"; //Xpath
		myAppointment_RescheduleConfirm="//a[contains(., 'Confirm')]"; //Xpath
		myAppointment_OK="//a[contains(., 'Ok')]"; //Xpath
		myAppointment_CancelPopUp="//span[contains(., 'Do you want to proceed with cancellation?')]"; //Xpath
		myAppointment_CancelYesButton="//a[contains(., 'Yes')]"; //Xpath
		myAppointment_cancelSuccess="//span[contains(., 'Appointment Cancelled Successfully.')]"; //Xpath
		myAppointment_reBook="//a[contains(., 'Rebook Appointment')]";
		Myappointment_Diagnostic_ProfileIcon="//div[@class='user-image']/a/img";
		
		
		//MyProfile
		
		
	
		
		
		
		
		myProfile_EditCountry="//div[@class='change-password-box']/div[5]/select"; //Xpath
		myProfile_EditState="//div[@class='change-password-box']/div[6]/select"; //Xpath
		myProfile_EditCity="//div[@class='change-password-box']/div[7]/select"; //Xpath
		myProfile_EditpinCode="pin code"; //Name
		
		myProfile_BloodGroup="//div[@class='my-profile-details-left']/div[5]/label[2]"; //Xpath
		myProfile_Address="//span[contains(., 'Primary Address')]/following-sibling::strong"; //Xpath
		
		
		myProfile_ProfilePicture_EditButton="//a[contains(., 'CHANGE PHOTO')]"; //Xpath
		myProfile_ProfileSrc="//div[@class='user-image']/img"; //Xpath
		myProfile_ProfilePicture_Message="//div[@class='profile-image']/following-sibling::p[contains(., 'Please press save button to update image.')]"; //Xpath
		myProfile_Save_successMessage="//p[contains(., 'User has been updated successfully.')]"; //Xpath
		myProfile_Change_OldPassword="password"; //Name
		myProfile_Change_NewPassword="new password"; //Name
		myProfile_Change_ConfirmPassword="confirm password"; //Name
		myProfile_OldPassword_SuccessFail="//p[@class='success-failed']"; //Xpath
		myProfile_Change_OldPassword_Error="//input[@name='password']/following-sibling::span"; //Xpath
		myProfile_Change_NewPassword_Error="//input[@name='new password']/following-sibling::span"; //Xpath
		myProfile_Change_ConfirmPassword_Error="//input[@name='confirm password']/following-sibling::span";
		myProfile_editAddress_Btn="//a[contains(., 'EDIT ADDRESS')]";
		myProfile_changePasswordBtn="//a[@class='change']";
		
		
		
		
		signUp_firstName_Error="//input[@name='First Name']//following-sibling::span[@class='error_span']"; //XPATH
		signUp_lastName_Error="//input[@name='Last Name']//following-sibling::span[@class='error_span']"; //XPATH
		signUp_MobileNo_Error="//input[@name='Mobile No']//following-sibling::span[@class='error_span']"; //XPATH
		signUp_Email_Error="(//input[@name='Email'])[1]//following-sibling::span[@class='error_span']"; //XPATH
		signUp_Password_Error="//input[@name='Password']//following-sibling::span[@class='error_span']"; //XPATH
		
		//Profile Page
		ProfilePage_doctorName="//div[@class='recipient-search-doctor-information']//h1"; //Xpath
		ProfilePage_bookButton="//a[contains(., 'Book Appointment')]"; //Xpath
		
		
		ProfilePage_aptDate="//input[@class='available-btn']";
		ProfilePage_clincName="//div[@class='recipient-search-doctor-information']/h3";
		ClinicPage_doctorsTab="//a[@href='#doctor']";
		
		//Family Details
		family_AddNewMember="//label[@class='add-member']"; //Xpath
		family_Relationship="//select[@name='Relationship']"; //Xpath
		family_firstName="First name"; //Name
		family_lastName="last name"; //Name
		family_gender="//select[@name='Gender']"; //Xpath
		family_Save="//button[contains(., 'ADD')]"; //Xpath
		family_Relationship_Error="//select[@name='Relationship']/following-sibling::span"; //Xpath
		family_firstName_Error="//input[@name='First name']/following-sibling::span"; //Xpath
		family_lastName_Error="//input[@name='last name']/following-sibling::span"; //Xpath
		family_gender_Error="//select[@name='Gender']/following-sibling::span"; //Xpath
		family_EditMember="(//a[contains(., 'EDIT')])[1]"; //Xpath
		family_NameOfFirstMember="(//div[@class='family-details-box']//div//label)[2]";
		family_DOB="//input[@placeholder='dd - mm - yyyy']";
		family_DOB_error="//div[@class='vdp-datepicker']/following-sibling::span";
		family_edit_update="//button[contains(., 'UPDATE')]";
		
		//Listing Page
		listing_ProviderName="//div[@class='profile-detail']//h1//a";
		listingPage_Doctor_BookAppointment_Button="//span[@class='re-green-btn book-appointment-full book-appointment-sticky button-doc']"; //Xpath
		listingPage_Diagnostic_SearchTestname="//div[@class='test-name-wrapper']//input";
		listingPage_Diagnostic_SelectTestCheckBox="//ul[@class='test-list']/li[1]/input";
		listingPage_Diagnostic_BookButton="//span[contains(., 'Book')]";
		listingPage_Diagnostic_Packagelink="(//a[contains(., 'Packages')])[2]";
		listingPage_Diagnostic_SearchPackageName="//div[@class='test-name-wrapper']//input";
		listingPage_Diagnostic_SelectPackageCheckbox="//ul[@class='test-list package']/li/input[@type='checkbox']";
		listingPage_Name_Specialisation_Dropdown="(//ul[@class='m-menu']//li[2]/a)[1]";
		listingPage_Name_Specialisation_SearchBox="(//input[@placeholder='Type & Select'])[1]";
		listingPage_Location_Dropdown="(//span[@class='recipient-address']/a)[1]"; //Xpath
		listingPage_Location_SearchBox="(//input[@placeholder='Enter your Location'])[1]"; //Xpath
		listingPage_Location_SuggestionOne="(//div[@class='pac-item'])[1]";
		listingPage_ChangeClinicLink="//a[@class='change-clinic-link active']"; //Xpath
		listingPage_Current_Location="(//h4[contains(., 'Get My Current Location')])[1]";
		listingPage_Userimage_Icon="(//img[@class='user-image-icon'])[2]";
		listingPage_DiagnosticProfile_SelectTestCheckBox="//li[@class='checked']/input";
		listingPage_Diagnostic_Testlink="//a[contains(., 'Tests')]";
		listingPage_Doc_clinicName="//div[@class='change-clinic']/label";
		listingPage_filterBtn="//div[@class='map-detail map-diagnostic show-list-page']//a[contains(., 'Filters')]";
		listingPage_filter_distanceTxt="(//h3[contains(., 'Distance (in Km)')])[1]";
		listingPage_filter_distanceSlider="//div[@class='vue-slider']/div[2]";
		listingPage_filter_applyFilterBtn="(//a[contains(., 'Apply Filters')])[1]";
		listingPage_filter_onlineCheckBox="//label[@for='online-consultation']";
		listingPage_filter_clearAllBtn="(//a[@class='clear-all'])[1]";
		listingPage_filter_selectDate="(//span[@value='selectDate'])[1]";
		listingPage_Diagnostic_PackageTab="//div[@class='diagnostic-search-tab']//ul//li//a[@href='javascript:void(0)'][contains(text(),'Packages')]";
		listingPage_Diagnostic_Package_Expand="//span[@id='0']";
		listingPage_Diagnostic_Totalamount="//div[@class='test-wrapper']/ul[1]/li[1]/span";
	
		
		
		
		
		
		
	
		
		
		
		
		
		
		
		
		
		//PromoCode
		
		bookingDetails_ClickOnApplyCoupon="//input[@value='Apply Coupon']";
		bookingDetails_EnterpromoCode="(//div[@class='enter-coupon-code']/input[1])[2]";
		bookingDetails_Applypromocode="(//input[@value='Apply'])[1]";
		bookingDetails_AmtAfterApplyingPromo="//span[contains(., 'After Applying Coupon')]/following-sibling::span"; //Xpath
		  
		
		//Packages
		
		Myappointments_Package_ReschedulePackage="//a[contains(., 'Reschedule Appointment')]";
		Myappointment_Package_Calendar_Next="(//span[@class='next'])[7]";
		Myappointment_Package_Cancel="//a[contains(., 'Cancel Appointment')]";
		Myappointment_Package_Cancel_Confirm="//a[text()='Yes']";
		Myappointment_Package_Cancel_FinalConfirm="//a[@class='re-green-btn pull-right']";
		Myappointment_Package_CancelledTab="//a[contains(., 'Cancelled Appointments')]";
		Myappointment_Package_Calendar_Ok="//a[contains(text(),'Ok')]";
		Myappointment_Package_Reschedule_Confirm="//a[text()='Confirm']";
		Myappointment_Package_Reschedule_FinalConfirm="//a[contains(., 'Ok')]";
		Myappointment_Package_GetDate="(//span[@class='date'])[1]";
		
		//Doctors:- Myappointment
		Myappointment_Doctor_Reschedule="//input[@placeholder='Reschedule Appointment']";
		Myappointment_Doctor_Calendar_Next="(//span[@class='next'])[7]";
		Myappointment_Doctor_Reschedule_SelectSession="//a[contains(., 'Confirm')]";
		Myappointment_Doctor_Reschedule_Confirm="//a[contains(., 'Ok ')]";
		Myappointment_Doctor_GetDate="//div[@class='doctor-schedule']/p[1]";
		Myappointment_Doctor_CancelAppointment="//a[contains(., 'Cancel Appointment')]";
		Myappointment_Doctor_Cancel_Confirm="//a[text()='Yes']";
		Myappointment_Doctor_Cancel_FinalConfirm="//a[text()='Ok']";
		Myappointment_Doctor_CanceledTab="//a[contains(., 'Cancelled Appointments')]";
		
		
		//Clinic Listing Page
		ClinicListingPage_Package_BookPackge="(//a[contains(text(),'Book Package')])";
		ClinicListingPage_Package_Calender_Ok="//a[contains(., 'Ok')]";
		ClinicListingPage_Package_Confirm="//a[contains(., 'Confirm')]";
		ClinicListingPage_ClickDoctorTab="(//a[contains(., 'Doctors')])[1]";
		ClinicListingPage_Doctor_BookAppointment="(//a[contains(., 'Book Appointment')])[1]";
		ClinicListingPage_Doctor_Confirm="//a[contains(., 'Confirm ')]";
		
		
		//Doctor Enrollment
		DoctorEnroll_Name="name";
		DoctorEnroll_City="city";
		DoctorEnroll_Registration="registrationNumber";
		DoctorEnroll_Email="email";
		DoctorEnroll_Phone="mobileNo";
		DoctorEnroll_OTP="otpCode";
		DoctorEnroll_SendOTP="//button[@id='sendOtp']";
		DoctorEnroll_Verify="verify";
		DoctorEnroll_ocTerms="//label[@for='ocThroughZoylo']/a";
		DoctorEnroll_terms="(//a[@href='/terms'])[1]";
		DoctorEnroll_termsCheck="//label[@for='agree']";
		DoctorEnroll_ocTermsCheck="//label[@for='ocThroughZoylo']";
		
		
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ DIAGNOSTIC ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		 homePage_DiagnosticIcon = "//a[@href='/diagnostics/pathology']/span";
		 dcHomePage_pathologyHeading = "//a[text()=' Pathology ']";
		 dcHomePage_popularPackageHeading = "//h1[text()='POPULAR PATHOLOGY PACKAGES']";
		 dcAddressLabel = "//h3[text()='Address']";
		 dcScheduleLabel = "//h5[text()='Schedule']";
		 dcFirstActiveSlot = "//li/a[@class='active']";
		 dcScheduleOkBtn = "//footer/button[text()='OK']";
		 dcOthersGender = "genderOther";
		 dcAddAddressBtn = "//div[@class='add-new']/a";
		 dcAddressPopUpHeader = "//h5[text()='ADD NEW ADDRESS']";
		 dcAddressType = "//select[@data-vv-name='Address Type']";
		 dcAddressLine1 = "addressLine1";
		 dcAddressLine2 = "addressLine2";
		 dcAddressLocality = "locality";
		 dcAddressPin = "pin";
		
		
		
		return driver;	
        
       /*
        * INFORMATION
        * Listing Page Book button xpath = //div[@class='left-center-location' and contains(.,'Zoylodoctor')]/following-sibling::div/div[@class='doctor-booking-detail']/a
        * Session Xpath for slots = //div[@class='days']//ul//li//a[contains(., 'Afternoon')]
        */
}

}
