package objectRepository;
import java.io.FileInputStream;
import java.util.Properties;
import org.openqa.selenium.WebDriver;

public class Elements_Admin {
	public static Properties prop = new Properties();
	public static FileInputStream inStream;
	public static WebDriver driver;
	
	//SignIn elements
  	public static String SignIn_Mobile,SignIn_Password,SignIn_Login, SignIn_OTP, SignIn_OTP_SubmitBtn;
  	
  	//Sign out elements
  	public static String elipse, logout;
  	
  	//Menu bar
  	public static String KelltonMenu;
  	
  	//Doctor Registration
  	public static String doc_PrimaryInfo_salutation, doc_PrimaryInfo_salutationName, doc_PrimaryInfo_FirstName, doc_PrimaryInfo_lastName, doc_PrimaryInfo_Email, doc_PrimaryInfo_MobileNumber, doc_PrimaryInfo_YearsOfExperience,
  	doc_PrimaryInfo_GenderBtn, doc_PrimaryInfo_Save, doc_RegistrationTab, doc_Registration_MedRegNum, doc_Registration_Save, doc_PracticeInfoTab, doc_PracticeInfo_AddOtherClinicBtn
  	, doc_PracticeInfo_OtherClinicPopUP, doc_PracticeInfo_roleBtn, doc_PracticeInfo_ClinicName, doc_PracticeInfo_AddClinic_SaveBtn, doc_PracticeInfo_PopUP_CancelBtn, doc_WorkDaysTab
  	, doc_WorkDays_Weekly, doc_FeeStructureTab, doc_FeeStructure_ZFC, doc_FeeStructure_FollupDays, doc_FeeStructure_GeneralFee, doc_FeeStructure_FollupFee, doc_FeeStructure_ivfFee,
  	doc_FeeStructure_ivfFollowUpFee, doc_FeeStructure_Save, doc_FeeStructure_Close, doc_SEOtab, doc_SEO_Save, doc_AdditionalInfoTab, doc_AdditionalInfo_AwardsTab, doc_AdditionalInfo_AwardsTab_ValidateBtn;
  	
  	//Non kelton appointments doctor
  	public static String Admin_Appointments_Doctor, Admin_Appointments_Doctor_FilterBy, Admin_Appointments_Doctor_Date, Admin_Appointments_Doctor_Status, Admin_Appointments_Doctor_Search, Admin_Appointments_Doctor_tableList, Admin_Appointments_Doctor_tablePatientName;
  	
  	
  	//Application Management
  	public static String AppManagement_LinkingQueTab, AppManagement_LinkingQue_SearchBar, AppManagement_LinkingQue_ActiveBtn, AppManagement_LinkingQue_DeActiveBtn, AppManagement_Doctor_SearchBar, AppManagement_ClinicTab, AppManagement_HospitalTab, AppManagement_ClinicTab_Search;
  	
  	//User Management
  	public static String userManagement_addUser, userManagement_firstName, userManagement_lastName, userManagement_Email, userManagement_mobileNumber, userManagement_password, userManagement_Add, userManagement_lastPagination, userManagement_SearchBar;
  	
  	//Users Tab
  	public static String users_SearchBar, users_editBtn, users_zoyloEmpBtn, users_updateBtn;

  	//Clinic registration
  	public static String Clinic_About_Name,Clinic_About_MobileNumber,Clinic_About_Email,Clinic_About_Country,Clinic_About_State,Clinic_About_City,Clinic_About_Address1,
  	Clinic_About_Address2,Clinic_About_Nearest,Clinic_About_Location,Clinic_About_PIN,Clinic_About_Lat,Clinic_About_Long,Clinic_About_Save,Clinic_Facilities,Clinic_Facility_Save,
  	Clinic_SEO,Clinic_SEO_Save,Clinic_AddAdminuser,Clinic_Adminuser_firstname,Clinic_Adminuser_middlename,Clinic_Adminuser_lastname,Clinic_Adminuser_mobnum,Clinic_Adminuser_Email1,
  	Clinic_Adminuser_pwd,Clinic_Adminuser_confirmpwd,Clinic_Adminuser_Save,Clinic_Adminuser_roleField,Clinic_Adminuser_docClinicRole,Clinic_Search;
  	
  	//Hospital registration
  	public static String hop_About_Country,hop_About_State,hop_About_City, hop_Adminuser_Save;
  	
  	//Doctor-Appointment Log
  	public static String doc_appointmentLog_recEmailSearch, doc_appointmentLog_recMobSearch, doc_appointmentLog_recEmailSearch_ApplyBtn, doc_appointmentLog_tabel, doc_appointmentLog_tabelEmailText, doc_appointmentLog_tabelMobText, doc_appointmentLog_cancelBtn,
  	doc_appointmentLog_rescheduleBtn, doc_appointmentLog_rescheduleCalendar, doc_appointmentLog_rescheduleSessions, doc_appointmentLog_recSave, doc_appointmentLog_recClose, doc_appointmentLog_stateTab, doc_appointmentLog_header, 
  	doc_appointmentLog_addAppointmentBtn, doc_appointmentLog_addAppointmentHeader, doc_appointmentLog_addAppointment_phoneNumber, doc_appointmentLog_addAppointment_firstName, doc_appointmentLog_addAppointment_lastName, doc_appointmentLog_addAppointment_email,
  	doc_appointmentLog_addAppontment_location, doc_appointmentLog_addAppointment_doctor, doc_appointmentLog_addAppointment_search, doc_appointmentLog_addAppointmentClinicHeader, doc_appointmentLog_addAppointmentDate, doc_qppointmentLog_addAppointmentDatePicker,
  	doc_appointmentLog_addAppointmentSession, doc_appointmentLog_addAppointmentTimeSlot, doc_appointmentLog_addAppointmentSave, doc_appointmentLog_addAppointmentGender;
  	
  	//Payment Report
  	public static String payment_Type, payment_paginationArea, payment_lastPage;
  	  	
  	//Package appointments- wellness center
  	public static String PackageAppointments_Wellnessbutton, PackageAppointments_BookingToDate, PackageAppointments_BookingFromDate , PackageAppointments_Mobile,
  	PackageAppointments_Select,PackageAppointments_Select_Reschedule,PackageAppointments_Select_Reschedule_Save,PackageAppointments_Reschedule_Notification,
  	PackageAppointments_Cancel,PackageAppointments_Cancel_Confirm,PackageAppointments_Cancel_Notification, PackageAppointments_Apply;
  	
  	//Package appointments - Clinic
  	
  	public static String PackageAppointments_Clinic_BookingToDate,PackageAppointments_Clinic_Mobile,PackageAppointments_Clinic_BookingFromDate,
  	PackageAppointments_Clinic_Select,PackageAppointments_Clinic_Select_Reschedule,PackageAppointments_Clinic_Select_Reschedule_Save,PackageAppointments_Clinic_Reschedule_Notification,
  	PackageAppointments_Clinic_Cancel,PackageAppointments_Clinic_Cancel_Confirm,PackageAppointments_Clinic_Cancel_Notification,PackageAppointments_Clinic_AppointmentFromeDate,
  	PackageAppointments_Clinic_AppointmentDate_SelectTodayDate,PackageAppointments_Clinic_AppointmentToDate,PackageAppointments_Clinic_AppointmentDate_Apply,PackageAppointments_Clinic_BookingAppointmentFromDate,
  	PackageAppointments_Clinic_BookingDate_SelectTodayDate,PackageAppointments_Clinic_BookingDate_Apply;
  	
  	//QUalification
  	public static String navigation_qualificationBtn, qualification_lookup, qualification_lookUpAddBtn, qualification_codeValue, qualification_displaySequence, qualification_name, qualificationValue_SaveBtn, qualification_editSave;
  	
  	// Doctor Enroll Elements
  	public static String docEnrol_salutation, docEnrol_firstname, docEnrol_middleName, docEnrol_lastName, docEnrol_gender, docEnrol_yof, docEnrol_qualification, docEnrol_spec, docEnrol_firstSpec, docEnrol_otherSpec, docEnrol_regNum, docEnrol_email,
  	docEnrol_num, docEnrol_clinicName, docEnrol_country, docEnrol_state, docEnrol_city, docEnrol_address1, docEnrol_address2, docEnrol_landmark, docEnrol_location, docEnrol_pinCode, docEnrol_lat, docEnrol_long, docEnrol_clinicNum
  	, docEnrol_onlineTimings, docEnrol_onlineFee, docEnrol_clinicTimings, docEnrol_clinicFee, docEnrol_packageBtn, docEnrol_package_discount, docEnrol_package_extraDetail, docEnrol_otherClinicBtn, docEnrol_otherClinicName, docEnrol_save,
  	docEnrol_ListingBtn, docEnrol_TeleCallerBtn, docEnrol_TeleCaller_search, docEnrol_TeleCaller_lastPage, docEnrol_TeleCaller_EditHeader, docEnrol_TeleCaller_Edit_text, docEnrol_TeleCaller_EditSave, docEnrol_TeleCaller_EditSaveMsg;
  	
  	//Wallets
  	public static String Admin_Wallet_CreditWalletMenu,Admin_Wallet_CreditWallet_Search,Admin_Wallet_CreditWallet_Search_Suggession,
  	Admin_Wallet_CreditWallet_CreditType, Admin_Wallet_CreditWallet_AppointmentID,Admin_Wallet_CreditWallet_Amount,Admin_Wallet_CreditWallet_Comments,
  	Admin_Wallet_CreditWallet_Save, Admin_Wallet_CreditWallet_Save_Confirm,Admin_Wallet_CreditWallet_Notification, Admin_Wallet_walletAmount, 
  	Admin_Wallet_WalletUsersMenu, Admin_Wallet_WalletUsers_Search, Admin_Wallet_DebitMenu, Admin_Wallet_DebitSearch, Admin_Wallet_DebitTrnsID,
  	Admin_Wallet_DebitComment, Admin_Wallet_Debit_CreditAmt, Admin_Wallet_DebitSuccessMsg, Admin_Wallet_WalletUsersDebitColmn, 
  	Admin_Wallet_WalletUsersDebitColmnComment, Admin_Wallet_WalletUsersDebitAmt;
  	
  	//Wellness
  	public static String Admin_ApplicationManagment_WellnessTab, Admin_WellnessTab_AddWellness, Admin_WellnessTab_Click_ProviderType,Admin_WellnessTab_Select_ProviderType,Admin_WellnessTab_FacilitiesMenu,
  	Admin_WellnessTab_TimingsMenu,Admin_WellnessTab_SEOMenu, Admin_WellnessTab_AddAdminUserMenu, Admin_WellnessTab_PackagesMenu,Admin_WellnessTab_WellnessGalleryMenu,Admin_WellnessTab_Facilities_Notification,
  	Admin_WellnessTab_Timings_Notification,Admin_WellnessTab_SEO_Notification, Admin_WellnessTab_AddAdminUser_Notification,Admin_WellnessTab_Packages_Notifiaction,Admin_WellnessTab_WellnessGallery_Notification,
  	Admin_WellnessTab_AboutWellness_Notification,Admin_WellnessTab_SEO_Save,Admin_WellnessTab_AddAdminUser_AddUserButton,Admin_WellnessTab_PackagesMenu_AddPackageButton,Admin_WellnessTab_WellnessGallery_ApprovalQue,
  	Admin_WellnessTab_PackagesMenu_Packagename,Admin_WellnessTab_PackagesMenu_ClickPackageCategory,Admin_WellnessTab_PackagesMenu_SelectPackageCategory,Admin_WellnessTab_PackagesMenu_Description,
  	Admin_WellnessTab_PackagesMenu_SelectFixedPrice, Admin_WellnessTab_PackagesMenu_EnterFixedPrice,Admin_WellnessTab_PackagesMenu_ZFC,Admin_WellnessTab_PackagesMenu_Save,
  	Admin_WellnessTab_AddAdminUserMenu_firstname,Admin_WellnessTab_AddAdminUserMenu_middlename,Admin_WellnessTab_AddAdminUserMenu_lastname,Admin_WellnessTab_AddAdminUserMenu_mobile,
  	Admin_WellnessTab_AddAdminUserMenu_email,Admin_WellnessTab_AddAdminUserMenu_Password,Admin_WellnessTab_AddAdminUserMenu_ConfirmPassword,Admin_WellnessTab_AddAdminUserMenu_ClickRolename,
  	Admin_WellnessTab_AddAdminUserMenu_SelectRoleName,Admin_WellnessTab_AddAdminUserMenu_Save,Admin_WellnessTab_FacilitiesMenu_CarParking,Admin_WellnessTab_FacilitiesMenu_BikeParking,Admin_WellnessTab_FacilitiesMenu_Ambulance,
  	Admin_WellnessTab_FacilitiesMenu_CreditCard,Admin_WellnessTab_FacilitiesMenu_DeditCard,Admin_WellnessTab_FacilitiesMenu_Save,Admin_WellnessTab_AboutWellnessMenu_Wellnessname,
  	Admin_WellnessTab_AboutWellnessMenu_Mobileno,Admin_WellnessTab_AboutWellnessMenu_email,  
  	Admin_WellnessTab_AboutWellnessMenu_ClickCountry, Admin_WellnessTab_AboutWellnessMenu_SelectCountry, Admin_WellnessTab_AboutWellnessMenu_ClickState,
  	Admin_WellnessTab_AboutWellnessMenu_SelectState,  Admin_WellnessTab_AboutWellnessMenu_ClickCity, Admin_WellnessTab_AboutWellnessMenu_SelectCity,  Admin_WellnessTab_AboutWellnessMenu_AddressLineOne,
  	Admin_WellnessTab_AboutWellnessMenu_AddresssLineTwo, Admin_WellnessTab_AboutWellnessMenu_LandMark,  Admin_WellnessTab_AboutWellnessMenu_SubArea,  Admin_WellnessTab_AboutWellnessMenu_Pincode,
  	Admin_WellnessTab_AboutWellnessMenu_Latitude, Admin_WellnessTab_AboutWellnessMenu_Longitude,Admin_WellnessTab_AboutWellnessMenu_Save;
  	
  	
  	//Online Consultancy
  	public static String Admin_OCReportsTab,Admin_OCReports_Search,Admin_OCReportSearch_Submit,Admin_OCReport_ViewHeader,Admin_OCReport_StatusDropDwn,Admin_OCSupport_StatusChangeComment,Admin_OCSupport_StatusChangeSubmitBtn,
  	Admin_OCReport_StatusChangeSuccessMsg,Admin_OCSupport_StatusCloseBtn;
  	
  	
  	//Non Kelton Menu
  	public static String Admin_AppointmentMenu,Admin_AppointmentMenu_Diagnostic,Admin_AppointmentMenu_Diagnostic_Click_FiterBy,Admin_AppointmentMenu_Diagnostic_SelectStatus,
  	Admin_AppointmentMenu_Diagnostic_SelectStatus_Option,Admin_AppointmentMenu_Diagnostic_Search,Admin_AppointmentMenu_Diagnostic_SearchData,Admin_AppointmentMenu_Diagnostic_GetAppointmentDate,
  	Admin_AppointmentMenu_Select_PaymentPending;
  	  	
  	//Add Wellness
  	public static String Wellness_Add_Appointmenu;
  	
  	//Wellness and add Diagnostic Link URl
  	public static String URL_Signin;
  	
  	//Email Reports
  	public static String Admin_EmailReports_Pagination;
  	
  	//Enrollment Listing
  	public static String enrolmentListing_Menu, enrolmentListing_doctorTab, enrolmentListing_searchBtn;
  	
  	// **************************************	Zoylo Labs	************************************** //
  	
  	public static String operatingCities_Header, operatingCities_AddBtn, operatingCities_popUpHeader, operatingCities_stateDrp, operatingCities_stateInput, operatingCities_cityDrp,
  	operatingCities_cityInput, operatingCities_activeBtn, operatingCities_saveBtn, operatingCities_searchState, operatingCities_searchStateInput, operatingCities_searchSubmitBtn, 
  	operatingCities_searchResult_cityCode, slotCapacity_subMenu, zone_cityDrp, zone_cityInput, zone_SaveBtn, zone_Header, zone_AddBtn, zone_popUpHeader, zone_zoneName, zone_zoneCode
  	, zone_searchCity, zone_searchInput, zone_searchSubmitBtn, zone_searchResult_zoneCode, zone_selectSlotDrp, zone_slotTab, zone_capacityInput, zone_capacitySubmitBtn, zone_slotsList
  	, zone_saveBtn, zone_plusBtn, zone_slotListingCount, zone_pinCodeTab, zone_pinInput, zone_searchResult_pinCode;
  	
  	// Lab Management
  	// Master Test
  	public static String lab_masterTestDrp, lab_masterTest_searchInput, lab_masterTest_searchBtn, lab_masterTest_listingHeader, lab_masterTest_addBtn, lab_masterTest_addHeader, 
  	lab_masterTest_testName, lab_masterTest_testCode, lab_masterTest_lisCode, lab_masterTest_tat, lab_masterTest_bookingCheckBox, lab_masterTest_fastingCheckBox, lab_masterTest_nextBtn
  	, lab_masterTest_listPrice, lab_masterTest_finalPrice, lab_masterTest_discountPer, lab_masterTest_saveBtn, lab_masterTest_listingFinalPrice;
  	
  	// Master Profile
  	public static String lab_masterProfile_nextBtn, lab_masterProfile_listPrice, lab_masterProfile_discountPer, lab_masterProfile_testAddBtn, lab_masterTestList_firstTestName, 
  	lab_masterProfile_subMenu, lab_masterProfile_listHeader, lab_masterProfile_addBtn, lab_masterProfile_addHeader, lab_masterProfile_profileName, lab_masterProfile_profileCode, 
  	lab_masterProfile_lisCode, lab_masterProfile_defaultTat, lab_masterProfile_activeChkBox, lab_masterProfile_testDrp, lab_masterProfile_testDrpInput, lab_masterProfile_saveBtn,
  	lab_masterProfile_searchBtn, lab_masterProfile_searchDrp, lab_masterProfile_searchInput, lab_masterProfile_finalPrice;
  	
  	// Master Package
  	public static String lab_masterProfileList_firstProfileName, lab_masterPackage_nextBtn, lab_masterPackage_listPrice, lab_masterPackage_discountPer, lab_masterPackage_testDrp,
  	lab_masterPackage_testDrpInput, lab_masterPackage_testAddBtn, lab_masterPackage_profileAddBtn, lab_masterPackage_searchBtn, lab_masterPackage_subMenu, lab_masterPackage_listHeader,
  	lab_masterPackage_addBtn, lab_masterPackage_addHeader, lab_masterPackage_packageName, lab_masterPackage_packageCode, lab_masterPackage_lisCode, lab_masterPackage_tat,
  	lab_masterPackage_packageLevel, lab_masterPackage_advanceLevel, lab_masterPackage_activeChkBox, lab_masterPackage_prescriptionChkBox, lab_masterPackage_profileDrp,
  	lab_masterPackage_profileDrpInput, lab_masterPackage_saveBtn, lab_masterPackage_packageSearchBtn, lab_masterPackage_packageSearchInput, lab_masterPackage_finalPrice;
  	
	// Partner Branch
	public static String labPartner_addBranch, labPartner_Branch_PartnerDrp, labPartner_Branch_PartnerDrpInput, labPartner_Branch_name, labPartner_Branch_code, 
	labPartner_Branch_contactName, labPartner_Branch_number, labPartner_Branch_email, labPartner_Branch_City, labPartner_Branch_CityInput, labPartner_Branch_ZoneName, 
	labPartner_Branch_Address1, labPartner_Branch_Pin, labPartner_Branch_Save;
	
	// Partner
	public static String labPartner_add, labPartner_addHeader, labPartner_name, labPartner_code, labPartner_contactName, labPartner_contactPhone, labPartner_email,
	labLocation_cityDrp, labLocation_cityDrpInput, labLocation_serviceDrp, labLocation_searchBtn, labLocation_SearchDrp, labLocation_SearchInput;
  	
  	
  	// Partner Mapping
  	public static String lab_partnerMapping_selectPartner, lab_partnerMapping_selectPartnerInput, lab_partnerMapping_BookingCode, lab_partnerMapping_price, lab_partnerMapping_discountPer,
  	lab_partnerMapping_TestListCode, lab_partnerMapping_listing_firstCode, lab_partnerMapping_selectTestDrp, lab_partnerMapping_selectTestInput, lab_partnerMapping_selectPackageDrp, 
  	lab_partnerMapping_selectPackageInput, lab_partnerMapping_listing_firstName, lab_partnerMapping_servicePrice, lab_partnerMapping_lisCode, lab_partnerMapping_packageSaveBtn, 
  	lab_partnerMapping_subMenu, lab_partnerMapping_addBtn, lab_partnerMapping_search, lab_partnerMapping_editSaveBtn, lab_partnerMapping_saveMsg, lab_partnerMapping_editSaveMsg, 
  	lab_partnerMapping_profileTabm, lab_partnerMapping_selectProfileDrp, lab_partnerMapping_selectProfileInput, lab_partnerMapping_profileTab, lab_partnerMapping_packageTab, 
  	lab_partnerMapping_finalPrice;
  	
  	// Lab Appointment
  	public static String lab_addAppointment_menu, lab_addAppointment_firstTestName, lab_addAppointment_fistTestCost, lab_addAppointment_firstProfileName, lab_addAppointment_firstProfileCost
  	, lab_addAppointment_firstPackageName, lab_addAppointment_firstPackageCost, lab_addAppointment_listHeader, lab_addAppointment_addBtn, lab_addAppointment_customerTab, 
  	lab_addAppointment_customerFisrtName, lab_addAppointment_customerLastName, lab_addAppointment_customerNumber, lab_addAppointment_customerEmail, lab_addAppointment_genderDrp, lab_addAppointment_Source,
  	lab_addAppointment_customerAge, lab_addAppointment_customerAddress, lab_addAppointment_customerPinDrp, lab_addAppointment_stateDrp, lab_addAppointment_stateInput, lab_addAppointment_cityDrp
  	, lab_addAppointment_cityInput, lab_addAppointment_nextBtn, lab_addAppointment_serviceDate, lab_addAppointment_serviceNextMonthBtn, lab_addAppointment_serviceSlotDrp, lab_addAppointment_addPatientBtn
  	, lab_addAppointment_PatientHeader, lab_addAppointment_PatientFirstName, lab_addAppointment_PAtientLastName, lab_addAppointment_PatientGenderDrp, lab_addAppointment_PatientEmail, 
  	lab_addAppointment_PatientNum, lab_addAppointment_PatientAge, lab_addAppointment_PatientSubmitBtn, lab_addAppointment_addTestProfileBtn, lab_addAppointment_testProfileHeader, lab_addAppointment_search,
  	lab_addAppointment_closeBtn, lab_addAppointment_paginationDrp, lab_addAppointment_addPackageBtn, lab_addAppointment_packageHeader, lab_addAppointment_serviceSearch, lab_addAppointment_confirmBooking, 
  	lab_appointment_verifyHeader, lab_appointment_yesBtn, lab_appointment_assignPhleboHeader, lab_appointment_assignPhleboDrp, lab_appointment_assignFirstPhlebo, lab_appointment_viewHeader , 
  	lab_appointment_viewStatusAssigned, lab_appointment_viewTotalAmt, lab_appointment_viewBalAmt, lab_appointment_viewPaidAmt, lab_appointment_submitBtn, lab_appointment_searchSubmitBtn, 
  	lab_addAppointment_Branch, lab_addAppointment_BranchNoData, lab_addAppointment_firstBranch, lab_firstDrpDwnValue, lab_addAppointment_slotDrp, lab_addAppointment_testProfileSearch
  	, labAppointments_magnifierIcon, labAppointments_searchParameterDrp, lab_appointment_Search_BookingDateHeader, lab_appointment_Search_bookingDateStart, lab_appointment_Search_bookingDateEnd, 
  	lab_appointmentList_noData, lab_appointment_Search_Reset, lab_appointment_Search_aptDateStart, lab_appointment_Search_aptDateEnd, lab_appointmentList_paginationSize, lab_appointment_Search_BookingStatusDrp;
  	
  	// Phlebo Screen
  	public static String lab_phlebo_searchBtn, lab_phlebo_paginationDrp, lab_phlebo_firstName, lab_phlebo_lastName, lab_phlebo_gender, lab_phlebo_listHeader, lab_phlebo_addPhleboBtn, lab_phlebo_salutationDrp
  	, lab_phlebo_phoneNumber, lab_phlebo_emailId, lab_phlebo_photoId, lab_phlebo_idNumber, lab_phlebo_regNum, lab_phlebo_eduDrp, lab_phlebo_zoneDrp, lab_phlebo_councilDrp, lab_phlebo_medCouncilDrp, 
  	lab_phlebo_activeChk, lab_phlebo_saveBtn;
  	
  	// Affiliates
  	public static String affiliates_SignInMobile, affiliates_SignInPassword, affiliates_SignInHeader, affiliates_LoginBtn, affiliates_appointmentHeadr;
  	
	public static WebDriver Admin_PageProperties()throws Exception{
		
		//SignIn elements
		SignIn_Mobile="mobile";
		SignIn_Password="password";
		SignIn_Login="(//button[@type='submit'])[2]";
		SignIn_OTP="OTP"; //Name
		SignIn_OTP_SubmitBtn="(//button[@class='yellow-btn' and @type='submit'])[1]";
		
		//Sign out elements
		elipse="//div[@class='menu__activator']/div[@class='toolbar__title']";
		logout="//div[@class='list__tile__title' and contains(., 'Logout')]";
		
		//ENrollment Listing
		enrolmentListing_Menu="//div[@class='list__tile__title' and contains(., 'Enrollment Listing')]";
		enrolmentListing_doctorTab="//div[@class='list__tile__title' and contains(., 'Doctor Enrollment')]";
		enrolmentListing_searchBtn="search"; //ID
		
		//Qualification
		navigation_qualificationBtn="//div[@class='list__tile__title' and contains(., 'Qualification')]";
		qualification_lookup="//a[@href='#tabLookupValue']";
		qualification_lookUpAddBtn="addLookupValue"; //ID
		qualification_codeValue="addValueCode"; //ID
		qualification_displaySequence="addDisplaySequence"; //ID
		qualification_name="addValueName"; //ID
		qualificationValue_SaveBtn="addLookUpValueSave"; //ID
		qualification_editSave="editLookUpValueSave"; //ID
		
		//Payment Report
		payment_Type="//div[@class='input-group__selections']";
		payment_paginationArea="//div[@class='text-xs-center']";
		payment_lastPage="(//a[@class='pagination__item'])[last()]";
					
		//Doctor-Appointment Log
		doc_appointmentLog_recEmailSearch="(//input[@data-vv-name='email address'])[1]";
		doc_appointmentLog_recMobSearch="(//input[@data-vv-name='enter Mobile No'])[1]";
		doc_appointmentLog_recEmailSearch_ApplyBtn="(//div[contains(., 'apply')])[19]";
		doc_appointmentLog_tabel="(//table[@class='datatable table']/tbody)[1]";
		doc_appointmentLog_tabelEmailText="(//td[@data-v-53234ed3=''])[5]";
		doc_appointmentLog_tabelMobText="(//td[@data-v-53234ed3=''])[4]";
		doc_appointmentLog_rescheduleBtn="(//i[@title='Reschedule'])[1]";
		doc_appointmentLog_rescheduleCalendar="(//div[@class='vdp-datepicker__calendar'])[1]";
		doc_appointmentLog_rescheduleSessions="//div[@class='layout' and @data-v-72ae89aa='']";
		doc_appointmentLog_recSave="//div[@class='btn__content' and contains(., 'Save')]";
		doc_appointmentLog_recClose="//div[@class='btn__content' and contains(., 'Close')]";
		doc_appointmentLog_cancelBtn="(//i[@title='Cancel'])[1]";
		doc_appointmentLog_stateTab="(//td[21])[1]";
		doc_appointmentLog_header="//a[@href='#doctorappointments']";
		doc_appointmentLog_addAppointmentBtn=".//*[@id='doctorappointments']//button[contains (., 'Add Appointment')]";
		doc_appointmentLog_addAppointmentHeader="//div[@class='pageHeading' and text()='Doctor Appointment - Add']";
		doc_appointmentLog_addAppointment_phoneNumber="mobileNumber"; //ID
		doc_appointmentLog_addAppointment_firstName="patientName"; //ID
		doc_appointmentLog_addAppointment_lastName="patientLastName"; //ID
		doc_appointmentLog_addAppointment_email="emailId"; // ID
		doc_appointmentLog_addAppontment_location="map"; //ID
		doc_appointmentLog_addAppointment_doctor="//div[@data-vv-name='Doctor']/div[@class='input-group__input']";
		doc_appointmentLog_addAppointment_search="save"; //ID
		doc_appointmentLog_addAppointmentClinicHeader="//strong[contains(., 'CLINICS LIST')]"; //Xpath
		doc_appointmentLog_addAppointmentDate="//input[@data-vv-name='Date']";
		doc_qppointmentLog_addAppointmentDatePicker="(//div[@class='vdp-datepicker__calendar'])[1]";
		doc_appointmentLog_addAppointmentSession="//div[@data-vv-name='Session']";
		doc_appointmentLog_addAppointmentTimeSlot="//div[@data-vv-name='TimeSlots']";
		doc_appointmentLog_addAppointmentSave="(.//*[@id='save'])[2]";
		doc_appointmentLog_addAppointmentGender="//div[@data-vv-name='Gender']";
		
		// Doctor Enroll Elements
		docEnrol_salutation="//div[@name='Salutation Title']/div[@class='input-group__input']/i";
		docEnrol_firstname="firstname"; //ID
		docEnrol_middleName="middleName"; //ID
		docEnrol_lastName="lastName"; //ID
		docEnrol_gender="//div[@data-vv-name='gender']/div[@class='input-group__input']/i";
		docEnrol_yof="Years of experience"; //name
		docEnrol_qualification="//input[@data-vv-name='Qualification']";
		docEnrol_spec="//div[@data-vv-name='specialization']/div[@class='input-group__input']/i";
		docEnrol_firstSpec="((//div[@class='menu__content'])[4]/div/ul/li/a/div/div[@class='list__tile__title'])[1]";
		docEnrol_otherSpec="//input[@data-vv-name='Other Specialization']";
		docEnrol_regNum="//input[@data-vv-name='Medical Registration Number']";
		docEnrol_email="//input[@data-vv-name='Email']";
		docEnrol_num="//input[@data-vv-name='mobile']";
		docEnrol_clinicName="//input[@data-vv-name='Clinic Name']";
		docEnrol_country="//div[@data-vv-name='country']/div[@class='input-group__input']/i";
		docEnrol_state="//div[@data-vv-name='state']/div[@class='input-group__input']/i";
		docEnrol_city="//div[@data-vv-name='city']/div[@class='input-group__input']/i";
		docEnrol_address1="//input[@data-vv-name='address line 1']";
		docEnrol_address2="//input[@data-vv-name='address line 2']";
		docEnrol_landmark="//input[@data-vv-name='landmark']";
		docEnrol_location="location"; // ID
		docEnrol_pinCode="//input[@data-vv-name='pincode']";
		docEnrol_lat="//input[@data-vv-name='latitude']";
		docEnrol_long="//input[@data-vv-name='longitude']";
		docEnrol_clinicNum="//input[@data-vv-name='Clinic Phone Number']";
		docEnrol_onlineTimings="//input[@aria-label='Online Timings']";
		docEnrol_onlineFee="Online Consultation Fees"; //Name
		docEnrol_clinicTimings="//input[@aria-label='Clinic Timings']";
		docEnrol_clinicFee="//input[@aria-label='Clinic Visit Consultation Fee']";
		docEnrol_packageBtn="(//div[@class='input-group--selection-controls__ripple'])[1]";
		docEnrol_package_discount="//input[@name='Discount Offered']";
		docEnrol_package_extraDetail="//input[@data-vv-name='Extra Details']";
		docEnrol_otherClinicBtn="//div[@class='flex xs3' and contains(., 'Any Other Clinic (2nd clinic)')]/following-sibling::div/div/div[1]/div/div[1]";
		docEnrol_otherClinicName="//input[@data-vv-name='Alternate Clinic Name']";
		docEnrol_save="//div[@class='btn__content' and contains(., 'Save')]";
		docEnrol_ListingBtn="//div[@class='list__tile__title' and contains(., 'Enrollment Listing')]";
		docEnrol_TeleCallerBtn="//div[@class='list__tile__title' and contains(., 'Telecaller Doctor Enrollment')]";
		docEnrol_TeleCaller_search="search"; //ID
		docEnrol_TeleCaller_lastPage="(//ul[@class='pagination']/li/a[@class='pagination__item'])[last()]";
		docEnrol_TeleCaller_EditHeader="//div[text()='TELLECALLER DOCTOR ENROLLMENT- EDIT']";
		docEnrol_TeleCaller_Edit_text="//textarea[@data-vv-scope='editComment']";
		docEnrol_TeleCaller_EditSave="emrollmentSave"; //ID
		docEnrol_TeleCaller_EditSaveMsg="//p[text()='Service resource lead information updated successfully']";

		//Menu bar
		KelltonMenu="//div[@class='list__tile__title' and contains(., 'Menu')]";
		
		//Doctor Registration
		doc_PrimaryInfo_salutation="//div[@name='Salutation Title']"; //Xpath
		doc_PrimaryInfo_salutationName="//li[contains(., 'Dr')]"; //Xpath
		doc_PrimaryInfo_FirstName="firstname"; //ID
		doc_PrimaryInfo_lastName="lastName"; //ID
		doc_PrimaryInfo_Email="email"; //ID
		doc_PrimaryInfo_MobileNumber="mobileNo"; //ID
		doc_PrimaryInfo_YearsOfExperience="//input[@data-vv-name='year of start of practice']";
		doc_PrimaryInfo_GenderBtn="//label[contains(., 'Gender')]/following-sibling::div//i";
		doc_PrimaryInfo_Save="(//button[@class='btn btn--raised theme--dark success'])[2]";
		doc_RegistrationTab="//a[@href='#RegistrationVerification']";
		doc_Registration_MedRegNum="medicalRegistrationNumber"; //ID
		doc_Registration_Save="(//button[@class='btn btn--raised theme--dark success'])[3]";
		doc_PracticeInfoTab="//a[@href='#practiceInfoTab']";
		doc_PracticeInfo_AddOtherClinicBtn="//div[@class='btn__content' and contains(., 'Add Other Clinic')]";
		doc_PracticeInfo_OtherClinicPopUP="(//div[@class='card__text']/div/input)[1]";
		doc_PracticeInfo_roleBtn="//label[contains(., 'select role')]/following-sibling::div//i";
		doc_PracticeInfo_ClinicName=".//*[@id='locationSearch']";
		doc_PracticeInfo_AddClinic_SaveBtn="(//button[@class='btn btn--raised theme--dark success'])[1]";
		doc_PracticeInfo_PopUP_CancelBtn="(//button[@class='btn btn--raised theme--dark error'])[1]";
		doc_WorkDaysTab="//a[@href='#schedulerInfoTab']";
		doc_WorkDays_Weekly="//label[contains(., 'Weekly')]";
		doc_FeeStructureTab="//a[@href='#doctorFeeStructure']";
		doc_FeeStructure_ZFC="//input[@data-vv-name='zfc']";
		doc_FeeStructure_FollupDays="follow up days"; //Name
		doc_FeeStructure_GeneralFee="(//input[@class='input-text big'])[1]";
		doc_FeeStructure_FollupFee="(//input[@class='input-text big'])[2]";
		doc_FeeStructure_ivfFee="(//input[@class='input-text big'])[3]";
		doc_FeeStructure_ivfFollowUpFee="(//input[@class='input-text big'])[4]";
		doc_FeeStructure_Save="(//button//div[contains(., 'Save')])[1]";
		//doc_FeeStructure_Close="(//button//div[contains(., 'Close')])[1]";
		doc_FeeStructure_Close="//div[@class='doctor-fee-btn-wrapper']//div[@class='btn__content'][contains(text(),'Close')]";
		doc_SEOtab="//a[@href='#seoInfo']";
		doc_SEO_Save="(//button[@class='btn btn--raised theme--dark success'])[5]";
		doc_AdditionalInfoTab="//a[@href='#additionalInfo']";
		doc_AdditionalInfo_AwardsTab="//div[@id='additionalInfo']/form/div/div[1]/div[1]/ul/li[contains(., 'Awards & Recognitions')]";
		doc_AdditionalInfo_AwardsTab_ValidateBtn="//button//div[contains(., 'Validate And Move to Approval Que')]";

		//Application Management
		AppManagement_LinkingQueTab="//a[@href='#linkingqueue']";
		AppManagement_LinkingQue_SearchBar="(.//*[@id='locationSearch'])[3]";
		AppManagement_LinkingQue_ActiveBtn="//div[@class='btn__content' and contains(., 'Activate')]";
		AppManagement_LinkingQue_DeActiveBtn="//div[@class='btn__content' and contains(., 'Deactivate')]";
		AppManagement_Doctor_SearchBar="(.//*[@id='locationSearch'])[1]";
		AppManagement_ClinicTab="//a[@href='#Clinic']";
		AppManagement_HospitalTab="//a[@href='#hospitallisting']";
		AppManagement_ClinicTab_Search="(//input[@id='locationSearch'])[2]";
		
		
		//User Management
		userManagement_addUser="//button//div[contains(., 'Add User')]";
		userManagement_firstName="//div[@class='input-group__input']/input[@data-vv-name='first name']";
		userManagement_lastName="//div[@class='input-group__input']/input[@data-vv-name='last name']";
		userManagement_Email="//div[@class='input-group__input']/input[@data-vv-name='email']";
		userManagement_mobileNumber="//div[@class='input-group__input']/input[@data-vv-name='mobile']";
		userManagement_password="//div[@class='input-group__input']/input[@data-vv-name='password']";
		userManagement_Add="(//button[@type='button'])[6]";
		userManagement_lastPagination="(//a[@class='pagination__item' and @href='#!'])[last()]";
		userManagement_SearchBar="//input[@type='text']";
		
		//Non Kelton APpointments doctor
		Admin_Appointments_Doctor="//div[@class='list__tile__title'][contains(text(), 'Doctors')]";
		Admin_Appointments_Doctor_FilterBy="//div/label[contains(., 'Filter By')]/following-sibling::div[1]";
		Admin_Appointments_Doctor_Date="//div/label[contains(., 'Period')]/following-sibling::div[1]";
		Admin_Appointments_Doctor_Status="//div/label[contains(., 'Status')]/following-sibling::div[1]";
		Admin_Appointments_Doctor_Search="//button[@class='synm-btn btn btn--raised primary']";
		Admin_Appointments_Doctor_tableList="//div[@class='layout tableListDetails row']";
		Admin_Appointments_Doctor_tablePatientName="//div[@class='layout tableListDetails row']/div[4]/div[1]";
		
		//Users Tab
	  	users_SearchBar="(.//*[@id='locationSearch'])[1]";
	  	users_editBtn="//td[17]/i";
	  	users_zoyloEmpBtn="//label[contains(., 'Zoylo Employee')]";
	  	users_updateBtn="//div[@class='btn__content' and contains(.,'Update')]";
		
		//Clinic Registration
		Clinic_About_Name="//input[@data-vv-name='clinic name']";
		Clinic_About_MobileNumber="//input[@data-vv-name='mobile no.']";
		Clinic_About_Email="//input[@data-vv-name='email address']";
		Clinic_About_Country="(//i[@class='material-icons icon input-group__append-icon'])[2]";
		Clinic_About_State="(//i[@class='material-icons icon input-group__append-icon'])[3]";
		Clinic_About_City="(//i[@class='material-icons icon input-group__append-icon'])[4]";
		Clinic_About_Address1="//input[@data-vv-name='address line 1']";
		Clinic_About_Address2="//input[@name='Address Line 2']";
		Clinic_About_Nearest="//input[@data-vv-name='landmark']";
		Clinic_About_Location="//input[@id='location']";
		Clinic_About_PIN="//input[@data-vv-name='pincode']";
		Clinic_About_Lat="//input[@data-vv-name='latitude']";
		Clinic_About_Long="//input[@id='longitude']";
		Clinic_About_Save="(//div[@class='btn__content' and contains(., 'Save')])[1]";
		Clinic_Facilities="//a[contains(.,'Facilities')]";
		Clinic_Facility_Save="(//button[@class='btn btn--raised theme--dark success'])[3]";
		Clinic_SEO="//a[contains(.,'SEO')]";
		Clinic_SEO_Save="(//button[@class='btn btn--raised theme--dark success'])[5]";
		Clinic_AddAdminuser="//a[contains(.,'Add Admin User')]";
		Clinic_Adminuser_firstname="//input[@id='first_name']";
		Clinic_Adminuser_middlename="//input[@data-vv-name='middle name']";
		Clinic_Adminuser_lastname="//input[@id='last_name']";
		Clinic_Adminuser_mobnum="(//input[@data-vv-name='mobile no.'])[2]";
		Clinic_Adminuser_Email1="(//div[@class='input-group__input']//input[@data-vv-name='email address'])[2]";
		Clinic_Adminuser_pwd="//input[@id='password']";
		Clinic_Adminuser_confirmpwd="//input[@name='password']";
		Clinic_Adminuser_Save="(//div[@class='btn__content' and contains(., 'Save')])[5]";
		Clinic_Adminuser_roleField="(//div[@class='input-group__selections'])[5]";
		Clinic_Adminuser_docClinicRole="(//div[contains(.,'Doctor Clinic Administrator')])[6]";
		Clinic_Search="(//input[@id='locationSearch'])[2]";
		
		//Hospital Registration
		hop_About_Country="(//div[@class='input-group__selections'])[1]";
		hop_About_State="(//div[@class='input-group__selections'])[2]";
		hop_About_City="(//div[@class='input-group__selections'])[3]";
		hop_Adminuser_Save="(//div[@class='btn__content' and contains(., 'Save')])[4]";
		
	  	
	  	//Package appointments - Wellness Center
	  	PackageAppointments_Wellnessbutton="//a[@class='tabs__item']";
	  	PackageAppointments_BookingToDate="//input[@aria-label='Booking To Date']";
		PackageAppointments_BookingFromDate="//input[@aria-label='Booking From Date']";
	  	PackageAppointments_Mobile="(//td[@data-v-0a6eadcf=''])[3]";
	  	PackageAppointments_Select="//td[@data-v-0a6eadcf='']/select";
	  	PackageAppointments_Select_Reschedule="//option[@value='SCHEDULED']";
	  	PackageAppointments_Select_Reschedule_Save="//button[@class='btn btn--raised success']/div";
	  	PackageAppointments_Reschedule_Notification="//div[@class='custom-success alert alert--dismissible']/div/p";
	  	PackageAppointments_Cancel="//option[@value='CANCEL']";
	  	PackageAppointments_Cancel_Confirm="//div[text()='Yes']";
	  	PackageAppointments_Cancel_Notification="//div[@class='custom-success alert alert--dismissible']/div/p";
	  	PackageAppointments_Apply="(//button[@class='btn btn--raised theme--dark primary'])[2]";
	  	
	  	//Package appointments -Clinic
	  	PackageAppointments_Clinic_BookingToDate="//input[@aria-label='Booking To Date']";
	  	PackageAppointments_Clinic_BookingFromDate="//input[@aria-label='Booking From Date']";
	  	PackageAppointments_Clinic_Mobile="(//td[@data-v-0a6eadcf=''])[3]";
	  	PackageAppointments_Clinic_Select="//td[@data-v-0a6eadcf='']/select";
	  	PackageAppointments_Clinic_Select_Reschedule="//option[@value='SCHEDULED']";
	  	PackageAppointments_Clinic_Select_Reschedule_Save="//button[@class='btn btn--raised success']/div";
	  	PackageAppointments_Clinic_Reschedule_Notification="//div[@class='custom-success alert alert--dismissible']/div/p";
	  	PackageAppointments_Clinic_Cancel="//option[@value='CANCEL']";
	  	PackageAppointments_Clinic_Cancel_Confirm="//div[text()='Yes']";
	  	PackageAppointments_Clinic_Cancel_Notification="//div[@class='custom-success alert alert--dismissible']/div/p";
	  	PackageAppointments_Clinic_AppointmentFromeDate="//input[@aria-label='Appointment From Date']";
	  	PackageAppointments_Clinic_BookingAppointmentFromDate="//input[@aria-label='Booking From Date']";
	  	PackageAppointments_Clinic_AppointmentDate_SelectTodayDate="//button[@class='btn btn--floating btn--small btn--flat btn--active btn--current']//span";
	  	PackageAppointments_Clinic_BookingDate_SelectTodayDate="//button[@class='btn btn--floating btn--small btn--flat btn--active btn--current']//span";
	  	PackageAppointments_Clinic_AppointmentToDate="//input[@aria-label='Appointment To Date']";
	  	PackageAppointments_Clinic_AppointmentDate_Apply="(//div[@class='btn__content' and contains(., 'apply')])[1]";
	  	PackageAppointments_Clinic_BookingDate_Apply="(//div[@class='btn__content' and contains(., 'apply')])[2]";
	    	
	  	//Wallet
	  	Admin_Wallet_CreditWalletMenu="//b[contains(., 'Credit Wallet')]";
	  	Admin_Wallet_CreditWallet_Search="(//input[@id='locationSearch'])[2]";
	  	Admin_Wallet_CreditWallet_Search_Suggession="//article[@class='media']/p";
	  	Admin_Wallet_CreditWallet_CreditType="(//div[@class='input-group__selections'])[2]";
	  	Admin_Wallet_CreditWallet_AppointmentID="//input[@data-vv-name='order ID']";
	  	Admin_Wallet_CreditWallet_Amount="//input[@data-vv-name='amount']";
	  	Admin_Wallet_CreditWallet_Comments="//textarea[@data-vv-name='comments']";
	  	Admin_Wallet_CreditWallet_Save="//button[@class='synm-btn btn btn--raised theme--dark primary']";
	  	Admin_Wallet_CreditWallet_Save_Confirm="(//button[@class='green--text darken-1 btn btn--flat'])[2]";
	  	Admin_Wallet_CreditWallet_Notification="//div[@class='custom-success alert alert--dismissible']/div/p";
	  	Admin_Wallet_walletAmount="//div[@class='flex xs2']/h6";
	  	Admin_Wallet_WalletUsersMenu="//b[contains(., 'Wallet Users')]";
	  	Admin_Wallet_WalletUsers_Search="(.//*[@id='locationSearch'])[1]";
	  	Admin_Wallet_DebitMenu="//b[contains(., 'Debit Wallet')]";
	  	Admin_Wallet_DebitSearch="(.//*[@id='locationSearch'])[3]";
	  	Admin_Wallet_DebitTrnsID="//input[@data-vv-name='transactionID']";
	  	Admin_Wallet_DebitComment=".//*[@id='debitWallet']//div//textarea[@data-vv-name='comments']";
	  	Admin_Wallet_Debit_CreditAmt="(//div[@id='debitWallet']//div//h6)[3]";
	  	Admin_Wallet_DebitSuccessMsg="//p[contains(., 'Wallet amount debited successfully')]";
	  	Admin_Wallet_WalletUsersDebitColmn="//div[@id='usersWallet']//tr/td[contains(., 'Roll Back')]";
	  	Admin_Wallet_WalletUsersDebitColmnComment="//div[@id='usersWallet']//tr/td[contains(., 'Roll Back')]/following-sibling::td[1]";
	  	Admin_Wallet_WalletUsersDebitAmt="//div[@id='usersWallet']//tr/td[contains(., 'Roll Back')]/following-sibling::td[3]";
	  	
	  	//Wellness
	  	Admin_ApplicationManagment_WellnessTab="//ul[@class='tabs__container']/li[3]/a";
	  	Admin_WellnessTab_AddWellness="//button/div[@class='btn__content' and contains(., 'ADD wellness center')]";
	  	Admin_WellnessTab_Click_ProviderType="(//div[@class='input-group__selections'])[1]";
	  	Admin_WellnessTab_Select_ProviderType="(//div[@class='list__tile__title' and contains(., 'Wellness Center')])[1]";
	  	Admin_WellnessTab_FacilitiesMenu="//b[contains(., 'Facilities')]";
	  	Admin_WellnessTab_TimingsMenu="//b[contains(., 'Timings')]";
	  	Admin_WellnessTab_SEOMenu="//b[contains(., 'SEO')]";
	  	Admin_WellnessTab_AddAdminUserMenu="//b[contains(., 'Add Admin User')]";
	  	Admin_WellnessTab_PackagesMenu="//b[contains(., 'Packages')]";
	  	Admin_WellnessTab_WellnessGalleryMenu="//b[contains(., 'Clinic / Wellness Gallery')]";
	  	Admin_WellnessTab_AboutWellness_Notification="(//div[@class='custom-success alert alert--dismissible']/div/p)[1]";
	  	Admin_WellnessTab_Facilities_Notification="(//div[@class='custom-success alert alert--dismissible']/div/p)[2]";
	  	Admin_WellnessTab_Timings_Notification="(//div[@class='custom-success alert alert--dismissible']/div/p)[3]";
	  	Admin_WellnessTab_SEO_Notification="(//div[@class='custom-success alert alert--dismissible']/div/p)[7]";
	  	Admin_WellnessTab_AddAdminUser_Notification="(//div[@class='custom-success alert alert--dismissible']/div/p)[5]";
	  	Admin_WellnessTab_Packages_Notifiaction="";
	  	Admin_WellnessTab_WellnessGallery_Notification="";
	  	Admin_WellnessTab_SEO_Save="(//div[text()='Save'])[5]";
	  	Admin_WellnessTab_AddAdminUser_AddUserButton="//button/div[@class='btn__content' and contains(., 'Add User')]";
	  	Admin_WellnessTab_PackagesMenu_AddPackageButton="//button/div[@class='btn__content' and text()='Add Package']";
	  	Admin_WellnessTab_WellnessGallery_ApprovalQue="//button/div[@class='btn__content' and contains(., 'Validate And Move to Approval Que')]";
	  	Admin_WellnessTab_PackagesMenu_Packagename="//input[@id='packageName']";
	  	Admin_WellnessTab_PackagesMenu_ClickPackageCategory="(//div[@class='input-group__selections'])[5]";
	  	Admin_WellnessTab_PackagesMenu_SelectPackageCategory="(//div[@class='card']/ul/li[1]/a/div/div)[1]";
	  	Admin_WellnessTab_PackagesMenu_Description="//input[@id='packageDescription']";
	  	Admin_WellnessTab_PackagesMenu_SelectFixedPrice="//div/label[contains(., 'Fixed Price')]/following-sibling::div/div[@class='input-group--selection-controls__ripple']";
	  	Admin_WellnessTab_PackagesMenu_EnterFixedPrice="(//input[@id='priceRangeTo'])[1]";
	  	Admin_WellnessTab_PackagesMenu_ZFC="//input[@data-vv-name='ZFC Percentage']";
	  	Admin_WellnessTab_PackagesMenu_Save="//button/div[@class='btn__content' and contains(., 'Add Package')]";
	  	Admin_WellnessTab_AddAdminUserMenu_firstname="//input[@id='first_name']";
	  	Admin_WellnessTab_AddAdminUserMenu_middlename="//input[@data-vv-name='middle name']";
	  	Admin_WellnessTab_AddAdminUserMenu_lastname="//input[@id='last_name']";
	  	Admin_WellnessTab_AddAdminUserMenu_mobile="(//input[@data-vv-name='mobile no.'])[2]";
	  	Admin_WellnessTab_AddAdminUserMenu_email="(//input[@data-vv-name='email address'])[2]";
	  	Admin_WellnessTab_AddAdminUserMenu_Password="//input[@id='password']";
	  	Admin_WellnessTab_AddAdminUserMenu_ConfirmPassword="//input[@name='password']";
	  	Admin_WellnessTab_AddAdminUserMenu_ClickRolename="(//div[@class='input-group__selections'])[5]";
	  	Admin_WellnessTab_AddAdminUserMenu_SelectRoleName="//div[@class='list__tile__title' and contains(., 'Doctor Clinic Administrator')]";
	  	Admin_WellnessTab_AddAdminUserMenu_Save="(//div[text()='Save'])[5]";
	  	Admin_WellnessTab_FacilitiesMenu_CarParking="//label[contains(., 'Car Parking')]";
	  	Admin_WellnessTab_FacilitiesMenu_BikeParking="//label[contains(., 'Bike Parking')]";
	  	Admin_WellnessTab_FacilitiesMenu_Ambulance="//label[contains(., 'Ambulance')]";
	  	Admin_WellnessTab_FacilitiesMenu_CreditCard="//label[contains(., 'Credit Card')]";
	  	Admin_WellnessTab_FacilitiesMenu_DeditCard="//label[contains(., 'Debit Card')]";
	  	Admin_WellnessTab_FacilitiesMenu_Save="(//button[@class='btn btn--raised theme--dark success'])[3]";
	  	Admin_WellnessTab_AboutWellnessMenu_Wellnessname="//input[@data-vv-name='clinic name']";
	  	Admin_WellnessTab_AboutWellnessMenu_Mobileno="//input[@aria-label='Mobile Number']";
	  	Admin_WellnessTab_AboutWellnessMenu_email="//input[@data-vv-name='email address']";
	  	Admin_WellnessTab_AboutWellnessMenu_ClickCountry="(//div[@class='input-group__selections'])[2]";
	  	Admin_WellnessTab_AboutWellnessMenu_SelectCountry="//div[@class='list__tile__title' and contains(., 'India')]";
	  	Admin_WellnessTab_AboutWellnessMenu_ClickState="(//div[@class='input-group__selections'])[3]";
	  	Admin_WellnessTab_AboutWellnessMenu_SelectState="//div[@class='list__tile__title' and contains(., 'Odisha')]";
	  	Admin_WellnessTab_AboutWellnessMenu_ClickCity="(//div[@class='input-group__selections'])[4]";
	  	Admin_WellnessTab_AboutWellnessMenu_SelectCity="//div[@class='list__tile__title' and text()='Berhampur']";
	  	Admin_WellnessTab_AboutWellnessMenu_AddressLineOne="//input[@ data-vv-name='address line 1']";
	  	Admin_WellnessTab_AboutWellnessMenu_AddresssLineTwo="//input[@ name='Address Line 2']";
	  	Admin_WellnessTab_AboutWellnessMenu_LandMark="//input[@data-vv-name='landmark']";
	  	Admin_WellnessTab_AboutWellnessMenu_SubArea="//input[@id='location']";  
	  	Admin_WellnessTab_AboutWellnessMenu_Pincode="//input[@data-vv-name='pincode']";
	  	Admin_WellnessTab_AboutWellnessMenu_Latitude="//input[@data-vv-name='latitude']";
	  	Admin_WellnessTab_AboutWellnessMenu_Longitude="//input[@id='longitude']";
	  	Admin_WellnessTab_AboutWellnessMenu_Save="(//button[@class='btn btn--raised theme--dark success'])[1]";
	  	
	  	
	  	//Online Consultancy
	  	
	  	Admin_OCReportsTab="//a[@href='#ocreports']";
	  	Admin_OCReports_Search="(.//*[@id='locationSearch'])[1]";
	  	Admin_OCReportSearch_Submit="(//button[contains(., 'Submit')])[1]";
	  	Admin_OCReport_ViewHeader="//div[@class='card__title heading' and contains(., 'Change Status')]";
	  	Admin_OCReport_StatusDropDwn="//div[@data-vv-name='OC Status']/div[1]";
	  	Admin_OCSupport_StatusChangeComment="//div[@class='input-group__input']/input[@data-vv-name='comment']";
	  	Admin_OCSupport_StatusChangeSubmitBtn="(//button/div[contains(., 'Submit')])[1]";
	  	Admin_OCReport_StatusChangeSuccessMsg="//p[contains(., 'Online consultation status updated successfully.')]";
	  	Admin_OCSupport_StatusCloseBtn="//button/div[contains(., 'close')]";
		
		//Non Kelton Menu
		Admin_AppointmentMenu="(//div[@class='list__tile__title'][contains(text(),'Appointments')])[4]";
		Admin_AppointmentMenu_Diagnostic="(//div[@class='list__tile__title'][contains(text(), 'Diagnostics')])[2]";
		Admin_AppointmentMenu_Diagnostic_Click_FiterBy="(//div[@class='input-group__selections__comma'])[1]";
		//Admin_AppointmentMenu_Diagnostic_Select_FilterByOption="//div[@class='list__tile__title'][contains(text(),'Booking Date')]";
		Admin_AppointmentMenu_Diagnostic_SelectStatus="(//div[@class='input-group__selections__comma'])[3]";
		Admin_AppointmentMenu_Diagnostic_SelectStatus_Option="(//div[@class='list__tile__title'][contains(text(), 'Payment Pending')])[1]";
		Admin_AppointmentMenu_Diagnostic_Search="//button/div[contains(text(),'Search')]";
		Admin_AppointmentMenu_Diagnostic_SearchData="(//div[@class='flex xs2'])[2]";
		Admin_AppointmentMenu_Diagnostic_GetAppointmentDate="(//div[@class='flex xs2'])[2]/div[2]";
		Admin_AppointmentMenu_Select_PaymentPending="(//div[@class='list__tile__title'][contains(text(), 'Payment Pending')])[1]";
		
		//Email Reports
		Admin_EmailReports_Pagination="(//ul[@class='pagination']/li)[last()-1]/a";
		
		//Add Wellness
		Wellness_Add_Appointmenu="//div[@class='btn__content'][contains(., 'Add Appointment')]";
		URL_Signin="//a[contains(text(),'Sign in')]";
		
		// **************************************	Zoylo Labs	************************************** //
		
		operatingCities_Header = "//label[contains(text(),'Operating Cities - List')]";
		operatingCities_AddBtn = "//div[@class='btn__content' and contains(., 'Add City')]";
		operatingCities_popUpHeader = "//label[contains(text(),'City - Add')]";
		operatingCities_stateDrp = "(//div[@class='input-group__selections'])[1]";
		operatingCities_stateInput = "//div[@id='example-2']/div/div/div/div[2]/div/div/div/div/div/div/input";
		operatingCities_cityDrp = "(//div[@class='input-group__selections'])[2]";
		operatingCities_cityInput = "//div[@id='city']/div/div/div/input";
		operatingCities_activeBtn = "//label[contains(., 'Active')]";
		operatingCities_saveBtn = "//div[@class='btn__content' and contains(., 'Save')]";
		operatingCities_searchState =  operatingCities_stateDrp;
		operatingCities_searchStateInput = "//div[@id='example-2']/main/div/div[2]/div[2]/div/div/div/div/div/input";
		operatingCities_searchSubmitBtn = "//div[@class='btn__content' and contains(., 'Search')]";
		operatingCities_searchResult_cityCode = "//tbody/tr/td[2]";
		slotCapacity_subMenu = "//div[@class='list__tile__title' and contains(., 'Zoylo Slot Capacity Management')]";
		zone_cityDrp = operatingCities_stateDrp;
		zone_cityInput = "//div[@id='example-2']/div/div/div/div/div[2]/div/div/div/div/div/div/input";
		zone_SaveBtn = operatingCities_saveBtn;
		zone_Header = "//label[contains(text(),'Zones - List')]";
		zone_AddBtn = "//div[@class='btn__content' and contains(., 'Add Zone')]";
		zone_popUpHeader = "//label[contains(text(),'Zone - Add')]";
		zone_zoneName = "//input[@data-vv-as='zone name']";
		zone_zoneCode = "//input[@data-vv-as='zone code']";
		zone_searchCity = operatingCities_searchState;
		zone_searchInput = "//div[@id='city']/div/div/div/input";
		zone_searchSubmitBtn = operatingCities_searchSubmitBtn;
		zone_searchResult_zoneCode = operatingCities_searchResult_cityCode;
		zone_selectSlotDrp = operatingCities_stateDrp;
		zone_slotTab = "//a[@href='#slots']";
		zone_capacityInput = "//input[@data-vv-name='slot capacity Number']";
		zone_capacitySubmitBtn = "//div[@class='btn__content' and contains(., 'Submit')]";
		zone_slotsList = "//div[@class='menu__content']/div/ul/li/a/div/div";
		zone_saveBtn = operatingCities_saveBtn;
		zone_plusBtn = "//div[@class='btn__content' and contains(., 'add')]";
		zone_slotListingCount = "(//tbody)[1]/tr/td[1]";
		zone_pinCodeTab = "//a[@href='#pincode']";
		zone_pinInput = "Pincode";
		zone_searchResult_pinCode = "(//tbody)[2]/tr/td[1]";
		
		// LAB Management
		// Master Test
		lab_masterTestDrp = operatingCities_stateDrp;
		lab_masterTest_searchInput = "//div[@id='example-2']/main/div/div[2]/div/div/div/div/div/div/input";
		lab_masterTest_searchBtn = operatingCities_searchSubmitBtn;
		lab_masterTest_listingHeader = "//label[contains(., 'Lab Master Test - Listing')]";
		lab_masterTest_addBtn = "//div[@class='btn__content' and contains(., 'Add Lab Master Test')]";
		lab_masterTest_addHeader = "//label[contains(., 'Lab Master Test - Add')]";
		lab_masterTest_testName = "//input[@data-vv-name='testName']";
		lab_masterTest_testCode = "//input[@data-vv-name='testCode']";
		lab_masterTest_lisCode = "//input[@data-vv-name='lisCode']";
		lab_masterTest_tat = "//input[@aria-label='Default Tat']";
		lab_masterTest_bookingCheckBox = "//label[contains(., 'Booking Eligibility')]";
		lab_masterTest_fastingCheckBox = "//label[contains(., 'Fasting')]";
		lab_masterTest_nextBtn = "//div[@class='btn__content' and contains(., 'Next')]";
		lab_masterTest_listPrice = "//label[contains(., 'List Price')]/following-sibling::div/input";
		lab_masterTest_finalPrice = "//label[contains(., 'Final Price')]/following-sibling::div/input";
		lab_masterTest_discountPer = "//input[@data-vv-name='discount percentage']";
		lab_masterTest_saveBtn = "//div[@class='btn__content' and contains(., 'Save Test')]";
		lab_masterTest_listingFinalPrice = "//tr/td[3]";
		// Master Profile
		lab_masterProfile_nextBtn = lab_masterTest_nextBtn;
		lab_masterProfile_listPrice = lab_masterTest_listPrice;
		lab_masterProfile_discountPer = lab_masterTest_discountPer;
		lab_masterProfile_finalPrice = lab_masterTest_finalPrice;
		lab_masterProfile_testAddBtn = zone_plusBtn;
		lab_masterTestList_firstTestName = "(//tr/td[1])[1]";
		lab_masterProfile_subMenu = "//div[@class='list__tile__title' and contains(., 'Lab Master Profiles')]";
		lab_masterProfile_listHeader = "//label[contains(text(),'LAB MASTER Profile - LISTING')]";
		lab_masterProfile_addBtn = "//div[@class='btn__content' and contains(., 'Add Lab Master Profile')]";
		lab_masterProfile_addHeader = "//label[contains(text(),'Zoylo Lab master Profile - Add')]";
		lab_masterProfile_profileName = "//label[contains(., 'Profile Name')]/following-sibling::div/input";
		lab_masterProfile_profileCode = "//label[contains(., 'Profile Code')]/following-sibling::div/input";
		lab_masterProfile_lisCode = "//label[contains(., 'LIS Code')]/following-sibling::div/input";
		lab_masterProfile_defaultTat = "//label[contains(., 'Default Tat')]/following-sibling::div/input";
		lab_masterProfile_activeChkBox = "//label[contains(text(),'Active?')]";
		lab_masterProfile_testDrp = "//div[contains(., 'Select Test *')]/following-sibling::div/div/div[@class='input-group__selections']";
		lab_masterProfile_testDrpInput = "//div[contains(., 'Select Test *')]/following-sibling::div/div/div[@class='input-group__selections']/div/input";
		lab_masterProfile_saveBtn = "//div[@class='btn__content' and contains(., 'Save Profile')]";
		lab_masterProfile_searchBtn = zone_searchSubmitBtn;
		lab_masterProfile_searchDrp = "//label[contains(., 'Select Profile')]/following-sibling::div/div[@class='input-group__selections']";
		lab_masterProfile_searchInput = "//label[contains(., 'Select Profile')]/following-sibling::div/div[@class='input-group__selections']/div/input";
		// Master Package
		lab_masterProfileList_firstProfileName = lab_masterTestList_firstTestName;
		lab_masterPackage_nextBtn = lab_masterTest_nextBtn;
		lab_masterPackage_listPrice = lab_masterTest_listPrice;
		lab_masterPackage_finalPrice = lab_masterTest_finalPrice;
		lab_masterPackage_discountPer = lab_masterTest_discountPer;
		lab_masterPackage_testDrp = lab_masterProfile_testDrp;
		lab_masterPackage_testDrpInput = lab_masterProfile_testDrpInput;
		lab_masterPackage_testAddBtn = "(" + zone_plusBtn + ")[1]";
		lab_masterPackage_profileAddBtn = "(" + zone_plusBtn + ")[2]";
		lab_masterPackage_searchBtn = operatingCities_searchSubmitBtn;
		lab_masterPackage_subMenu = "//div[@class='list__tile__title' and contains(., 'Lab Master Packages')]";
		lab_masterPackage_listHeader = "//label[contains(text(),'Lab Master Packages - Listing')]";
		lab_masterPackage_addBtn = "//div[@class='btn__content' and contains(., 'Add Lab Master Package')]";
		lab_masterPackage_addHeader = "//label[contains(text(),'Zoylo Lab Master Package - Add')]";
		lab_masterPackage_packageName = "//label[contains(., 'Package Name')]/following-sibling::div/input";
		lab_masterPackage_packageCode = "//label[contains(., 'Package Code')]/following-sibling::div/input";
		lab_masterPackage_lisCode = "//label[contains(., 'LIS Code')]/following-sibling::div/input";
		lab_masterPackage_tat = "//label[contains(., 'Default Tat')]/following-sibling::div/input";
		lab_masterPackage_packageLevel = "//div[contains(., 'Package Level')]/div/div[@class='input-group__selections']";
		lab_masterPackage_advanceLevel = "//div[@class='list__tile__title' and contains(., 'Advanced')]";
		lab_masterPackage_activeChkBox = "(//label[contains(text(),'Active')])[1]";
		lab_masterPackage_prescriptionChkBox = "//label[contains(text(),'Prescription')]";
		lab_masterPackage_profileDrp = "//div[contains(., 'Select profile *')]/following-sibling::div/div/div[@class='input-group__selections']";
		lab_masterPackage_profileDrpInput = "//div[contains(., 'Select profile *')]/following-sibling::div/div/div[@class='input-group__selections']/div/input";
		lab_masterPackage_saveBtn = "//div[@class='btn__content' and contains(., 'save Package')]";
		lab_masterPackage_packageSearchBtn = "//label[contains(., 'Select package')]/following-sibling::div/div[@class='input-group__selections']";
		lab_masterPackage_packageSearchInput = "//label[contains(., 'Select package')]/following-sibling::div/div[@class='input-group__selections']/div/input";
		// Partner Mapping
		lab_partnerMapping_selectPartner = "//label[contains(., 'Select Partner')]/following-sibling::div/div[@class='input-group__selections']";
		lab_partnerMapping_selectPartnerInput = "//label[contains(., 'Select Partner')]/following-sibling::div/div[@class='input-group__selections']/div/input";
		lab_partnerMapping_BookingCode = "//label[contains(.,'Partner Booking Code')]/following-sibling::div/input";
		lab_partnerMapping_price = "//label[contains(.,'Partner Service Price')]/following-sibling::div/input";
		lab_partnerMapping_discountPer = "//label[contains(.,'Discount Percentage')]/following-sibling::div/input";
		lab_partnerMapping_finalPrice = "//label[contains(.,'Final Price')]/following-sibling::div/input";
		lab_partnerMapping_TestListCode = "//label[contains(., 'Partner LisCode')]/following-sibling::div/input";
		lab_partnerMapping_listing_firstCode = "(//tr/td[2])[1]";
		lab_partnerMapping_listing_firstName = "(//tr/td[1])[1]";
		lab_partnerMapping_selectTestDrp = "//label[contains(., 'Select Test')]/following-sibling::div/div[@class='input-group__selections']";
		lab_partnerMapping_selectTestInput = "//label[contains(., 'Select Test')]/following-sibling::div/div[@class='input-group__selections']/div/input";
		lab_partnerMapping_selectPackageDrp = "//label[contains(., 'Select Package')]/following-sibling::div/div[@class='input-group__selections']";
		lab_partnerMapping_selectPackageInput = "//label[contains(., 'Select Package')]/following-sibling::div/div[@class='input-group__selections']/div/input";
		lab_partnerMapping_servicePrice = "//label[contains(.,'Partner Service Price')]/following-sibling::div/input";
		lab_partnerMapping_lisCode = "//label[contains(., 'Partner LIS Code')]/following-sibling::div/input";
		lab_partnerMapping_packageSaveBtn = "//div[@class='btn__content' and contains(., 'Save Package')]";
		lab_partnerMapping_subMenu = "//div[@class='list__tile__title' and contains(., 'Zoylo Partner Mapping Details')]";
		lab_partnerMapping_addBtn = "//div[@class='btn__content' and contains(., 'Add Service')]";
		lab_partnerMapping_search = "//input[@type='text']";
		lab_partnerMapping_editSaveBtn = "//div[@class='btn__content' and contains(., 'Save Partner Service')]";
		lab_partnerMapping_saveMsg = "//p[contains(., 'Partner service saved successfully')]";
		lab_partnerMapping_editSaveMsg = "//p[contains(., 'Partner service updated successfully')]";
		lab_partnerMapping_profileTab = "//a[@href='#profiles']/b";
		lab_partnerMapping_selectProfileDrp = "//label[contains(., 'Select Profile')]/following-sibling::div/div[@class='input-group__selections']";
		lab_partnerMapping_selectProfileInput = "//label[contains(., 'Select Profile')]/following-sibling::div/div[@class='input-group__selections']/div/input";
		lab_partnerMapping_packageTab = "//a[@href='#packages']/b";
		
		// Lab APpointment
		lab_addAppointment_menu = "//div[@class='list__tile__title' and contains(., 'Zoylo Lab Appointments')]";
		lab_addAppointment_firstTestName = "(//tr/td[contains(., 'TEST')])[1]/preceding-sibling::td[1]";
		lab_addAppointment_fistTestCost = "(//tr/td[contains(., 'TEST')])[1]/following-sibling::td[3]";
		lab_addAppointment_firstProfileName = "(//tr/td[contains(., 'PROFILE')])[1]/preceding-sibling::td[1]";
		lab_addAppointment_firstProfileCost = "(//tr/td[contains(., 'PROFILE')])[1]/following-sibling::td[3]";
		lab_addAppointment_firstPackageName = "(//tr/td[contains(., 'PACKAGE')])[1]/preceding-sibling::td[1]";
		lab_addAppointment_firstPackageCost = "(//tr/td[contains(., 'PACKAGE')])[1]/following-sibling::td[3]";
		lab_addAppointment_listHeader = "//label[contains(., 'Zoylo Lab Appointments - List')]";
		lab_addAppointment_addBtn = "//div[text()='Create Booking']";
		lab_addAppointment_customerTab = "//div[text()='Customer Info']";
		lab_addAppointment_customerFisrtName = "//input[@data-vv-name='customerFirstName']";
		lab_addAppointment_customerLastName = "//input[@data-vv-name='customerLastName']";
		lab_addAppointment_customerNumber = "//input[@data-vv-name='customerMobileNumber']";
		lab_addAppointment_customerEmail = "//input[@data-vv-name='customerEmailId']";
		lab_addAppointment_genderDrp = "//label[contains(., 'Gender')]/following-sibling::div/div[@class='input-group__selections']";
		lab_addAppointment_Source = "//label[contains(., 'Source')]/following-sibling::div/div[@class='input-group__selections']";
		lab_addAppointment_customerAge = "//input[@data-vv-name='customerAge']";
		lab_addAppointment_customerAddress = "//input[@data-vv-name='addressLineOne']";
		lab_addAppointment_customerPinDrp = "//label[contains(., 'Pincode')]/following-sibling::div/div[@class='input-group__selections']";
		lab_addAppointment_stateDrp = "//label[contains(., 'State')]/following-sibling::div/div[@class='input-group__selections']";
		lab_addAppointment_stateInput = "//label[contains(., 'State')]/following-sibling::div/div[@class='input-group__selections']/div/input";
		lab_addAppointment_cityDrp = "//label[contains(., 'City')]/following-sibling::div/div[@class='input-group__selections']";
		lab_addAppointment_cityInput = "//label[contains(., 'City')]/following-sibling::div/div[@class='input-group__selections']/div/input";
		lab_addAppointment_nextBtn = lab_masterTest_nextBtn;
		lab_addAppointment_serviceDate = "//div[@class='input-group__input']/input[@data-vv-name='selectDate']";
		lab_addAppointment_serviceNextMonthBtn = "(//div[@class='btn__content'])[2]";
		lab_addAppointment_serviceSlotDrp = "//label[contains(., 'Select Slot')]/following-sibling::div/div[@class='input-group__selections']";
		lab_addAppointment_addPatientBtn = "//div[@class='btn__content' and contains(., 'Add Patient')]";
		lab_addAppointment_PatientHeader = "//label[contains(., 'Add Patient')]";
		lab_addAppointment_Branch = "//label[contains(., 'Branch Details')]/following-sibling::div/div[@class='input-group__selections']";
		lab_addAppointment_BranchNoData = "(//div[@class='list__tile__title' and text()='No data available'])[2]";
		lab_addAppointment_firstBranch = "/html[1]/body[1]/div[1]/div[1]/div[1]/div[4]/div[1]/ul[1]/li[1]/a[1]/div[1]/div[1]";
		lab_addAppointment_PatientFirstName = "//input[@data-vv-name='firstName']";
		lab_addAppointment_PAtientLastName = "//input[@data-vv-name='lastName']";
		lab_addAppointment_PatientGenderDrp = "(//label[contains(., 'Gender')]/following-sibling::div/div[@class='input-group__selections'])[1]";
		lab_addAppointment_PatientEmail = "//input[@data-vv-name='emailId']";
		lab_addAppointment_PatientNum = "//input[@data-vv-name='mobileNumber']";
		lab_addAppointment_PatientAge = "//input[@data-vv-name='age']";
		lab_addAppointment_PatientSubmitBtn = zone_capacitySubmitBtn;
		lab_addAppointment_addTestProfileBtn = "//div[@class='btn__content' and contains(., ' Add test profile ')]";
		lab_addAppointment_testProfileHeader = "//label[contains(., 'ADD Test/Profile')]";
		lab_addAppointment_closeBtn = doc_appointmentLog_recClose;
		lab_addAppointment_paginationDrp = "(//div[@class='input-group__input']/i)[2]";
		lab_addAppointment_addPackageBtn = "//div[@class='btn__content' and contains(., 'Add package')]";
		lab_addAppointment_packageHeader = "//label[contains(., 'Add Package')]";
		lab_addAppointment_serviceSearch = "//div[@class='input-group input-group--append-icon input-group--text-field input-group--single-line']//input[@type='text']";
		lab_addAppointment_testProfileSearch = "//i[text()='search']/preceding-sibling::input";
		lab_addAppointment_confirmBooking = "//div[@class='btn__content' and contains(., 'Confirm Booking')]";
		lab_addAppointment_search = "//input[@placeholder='Search hear ...']";
		lab_addAppointment_slotDrp = "(//div[@class='list__tile__title' and contains(., 'PM')])[1]";
		lab_appointment_verifyHeader = "//label[text()='Booking Verification']";
		lab_appointment_yesBtn = "//div[@class='btn__content' and contains(., ' Yes')]";
		lab_appointment_assignPhleboHeader = "//label[text()='Asign Phlebotomist']";
		lab_appointment_assignPhleboDrp = "//label[contains(., 'Select Phlebotomist')]/following-sibling::div[@class='input-group__input']";
		lab_appointment_assignFirstPhlebo = "(//div[@class='dialog__content' and contains(., 'Select Phlebotomist')]/following-sibling::div[@class='menu__content']/div/ul/li[1]/a/div/div)[2]";
		lab_appointment_viewHeader = "//label[text()='Zoylo Lab Appointment - View']";
		lab_appointment_viewStatusAssigned = "//div[@class='viewAptField' and contains(., 'Status')]/following-sibling::div[contains(., 'ASSIGNED')]";
		lab_appointment_viewTotalAmt = "//div[@class='viewAptField' and contains(., 'Total Amount')]/following-sibling::div";
		lab_appointment_viewBalAmt = "//div[@class='viewAptField' and contains(., 'Balance Amount')]/following-sibling::div";
		lab_appointment_viewPaidAmt = "//div[@class='viewAptField' and contains(., 'Paid Amount')]/following-sibling::div";
		lab_appointment_submitBtn = zone_capacitySubmitBtn;
		lab_appointment_searchSubmitBtn = operatingCities_searchSubmitBtn;
		lab_appointment_Search_BookingDateHeader = "//label[text()='Booking Date']";
		lab_appointment_Search_bookingDateStart = "//div[contains(., 'Booking Date')]/div/input[@placeholder='Start date']";
		lab_appointment_Search_bookingDateEnd = "//div[contains(., 'Booking Date')]/div/input[@placeholder='End date']";
		lab_appointmentList_noData = "//table[@class='datatable table']/tbody/tr/td[text()='No data available']";
		lab_appointment_Search_Reset = "//div[text()='Reset']";
		lab_appointment_Search_aptDateStart = "//div[contains(., 'Appointment Date')]/div/input[@placeholder='Start date']";
		lab_appointment_Search_aptDateEnd = "//div[contains(., 'Appointment Date')]/div/input[@placeholder='End date']";
		lab_appointmentList_paginationSize = "//ul[@class='pagination']/li";
		lab_appointment_Search_BookingStatusDrp = "//div[contains(., 'Booking status')]/div[@class='input-group__input']";
		
		// Phlebo Screen
		lab_phlebo_searchBtn = operatingCities_searchSubmitBtn;
		lab_phlebo_paginationDrp = lab_addAppointment_paginationDrp;
		lab_phlebo_firstName = lab_addAppointment_PatientFirstName;
		lab_phlebo_lastName = lab_addAppointment_PAtientLastName;
		lab_phlebo_gender = lab_addAppointment_genderDrp;
		lab_phlebo_listHeader = "//label[text()='Phlebotomist List']";
		lab_phlebo_addPhleboBtn = "//div[@class='btn__content' and contains(., 'Add Phlebotomist')]";
		lab_phlebo_salutationDrp = "//label[contains(., 'Salutations')]/following-sibling::div/div[@class='input-group__selections']";
		lab_phlebo_phoneNumber = "//input[@data-vv-name='Phone Number']";
		lab_phlebo_emailId = "//input[@data-vv-name='Email Id']";
		lab_phlebo_photoId = "//input[@data-vv-name='Photo Id Name']";
		lab_phlebo_idNumber = "//input[@data-vv-name='Photo Id Number']";
		lab_phlebo_regNum = "//input[@data-vv-name='Medical Registration Number']";
		lab_phlebo_eduDrp = "//div//label[text()='Educational Qualification']/following-sibling::div[1]/div[1]";
		lab_phlebo_zoneDrp = "//div//label[text()='Associated Zone']/following-sibling::div[1]/div[1]";
		lab_phlebo_councilDrp = "//label[contains(., 'Select Council Type')]/following-sibling::div/div[@class='input-group__selections']";
		lab_phlebo_medCouncilDrp = "//label[contains(., 'Medical State Council Type')]/following-sibling::div/div[@class='input-group__selections']";
		lab_phlebo_activeChk = "//label[text()='Active?']";
		lab_phlebo_saveBtn = "//div[@class='btn__content' and contains(., 'Save')]";
		lab_firstDrpDwnValue = "(//div[@class='list__tile__title'])[1]";
		labAppointments_magnifierIcon = "//button[contains(., 'Create Booking')]/preceding-sibling::button[2]";
		labAppointments_searchParameterDrp = "(//div[@class='layout']/div/div/div/div[@class='input-group__selections'])[3]";
		
		// Partner Branch
		labPartner_addBranch = "//div[text()=' Add Branch']";
		labPartner_Branch_PartnerDrp = "//label[text()='Select Partner']/following-sibling::div/div[@class='input-group__selections']";
		labPartner_Branch_PartnerDrpInput = "//label[text()='Select Partner']/following-sibling::div/div[@class='input-group__selections']/div/input";
		labPartner_Branch_name = "//input[@data-vv-name='Branch Name']";
		labPartner_Branch_code = "//input[@data-vv-name='Branch Code']";
		labPartner_Branch_contactName = "//input[@data-vv-name='Contact Name']";
		labPartner_Branch_number = lab_phlebo_phoneNumber;
		labPartner_Branch_email = docEnrol_email;
		labPartner_Branch_City = "//label[text()='Select City']/following-sibling::div/div[@class='input-group__selections']";
		labPartner_Branch_CityInput = "//label[text()='Select City']/following-sibling::div/div[@class='input-group__selections']/div/input";
		labPartner_Branch_ZoneName = "//label[text()='Zone Name']/following-sibling::div/div[@class='input-group__selections']";
		labPartner_Branch_Address1 = Clinic_About_Address1;
		labPartner_Branch_Pin = Clinic_About_PIN;
		labPartner_Branch_Save = "//div[@class='btn__content' and contains(., 'Save Branch')]";
		
		// Partner
		labPartner_add = "//div[text()=' Add Partner']";
		labPartner_addHeader = "//strong[text()='Zoylo Partner Details - ADD']";
		labPartner_name = "//input[@data-vv-name='partner name']";
		labPartner_code = "//input[@data-vv-name='partner Code']";
		labPartner_contactName = labPartner_Branch_contactName;
		labPartner_contactPhone = "//input[@data-vv-name='contact Phone']";
		labPartner_email = docEnrol_email;
		labLocation_cityDrp = "//label[text()='City']/following-sibling::div/div[@class='input-group__selections']";
		labLocation_cityDrpInput = "//label[text()='City']/following-sibling::div/div[@class='input-group__selections']/div/input";
		labLocation_serviceDrp = "//label[text()='Service Type']/following-sibling::div/div[@class='input-group__selections']";
		labLocation_searchBtn = "//button[@class='btn btn--raised success']/div";
		labLocation_SearchDrp = "//label[text()='Service Name']/following-sibling::div/div[@class='input-group__selections']";
		labLocation_SearchInput = "//label[text()='Service Name']/following-sibling::div/div[@class='input-group__selections']/div/input";
		
		// Affiliates
		affiliates_SignInMobile = "mobileNumber"; // ID
		affiliates_SignInPassword = "Password"; // Name
		affiliates_SignInHeader = "//h4[text()='Provide Your Mobile Number To Login']"; // xpath
		affiliates_LoginBtn = "//button[text()='Login']"; // xpath
		affiliates_appointmentHeadr = "//h6[text()='Zoylo Lab Appointment']"; // xpath
		
		return driver;
	}
}
