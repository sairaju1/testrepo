package objectRepository;

import org.openqa.selenium.WebDriver;

public class Elements_DocPMS {
	public static WebDriver driver;
	
	//Choose clinic popup
	public static String chooseClinic_PopUp, chooseClinic_PopUp_SelectBtn;
	
	//PMS side menu
	public static String menu_dashboard;
	
	//PMS dashboard
	public static String dashboard_calendar_monthBtn, dashboard_calendar_DayHeader, dashboard_calendar_appointmentCard,
	dashboard_calendar_aptPopUp, dashboard_calendar_aptPopUp_detailsBtn, dashboard_userIcon, dashboard_userIcon_logoutBtn,
	dashboard_addAppointment, pms_logoutBtn;
	
	//pms appointment details pop up
	public static String pms_aptDetailsPopUp_header, pms_aptDetailsPopUp_startConsultationBtn, pms_aptDetailsPopUp_viewPrescription;
	
	//e-Prescription screen
	public static String prescription_heading, prescription_saveBtn, prescription_vital_temperature, prescription_vital_pulse, prescription_vitals_saveNotification,
	prescription_vital_respiration, prescription_vital_bloodpressure, prescription_vital_sp, prescription_vital_height, prescription_vital_weight, 
	prescription_complaints_complaint_diseases, prescription_complaints_sinceValue, prescription_complaints_sinceCode, prescription_complaints_severity,
	prescription_diseases_nature, prescription_complaints_saveNotification, prescription_diseases_saveNotification, prescription_investigation_text, 
	prescription_investigation_saveBtn, prescription_investigation_saveNotification, prescription_medication_formulationName, prescription_medication_brandName,
	prescription_medication_dosage, prescription_medication_frequency, prescription_medication_dosageDuration, prescription_medication_durationType,
	prescription_medication_saveNotification, prescription_notesText, prescription_notes_saveNotification, prescription_endBtn, prescription_addNewMedication;
	
	//PMS Add appointment
	public static String addAppointment_ChooseDoctor, addAppointment_timeSlot, addAppointment_consultationType, addAppointment_NextBtn, addAppointment_patientMobile,
	addAppointment_patientAddress, addAppointment_confirm, addAppointment_successMessage, addAppointment_successMessageOKbtn;
	
	//PDF Container
	public static String pdf_container, pdf_temperature, pdf_pulse, pdf_respiration, pdf_bp1, pdf_bp2, pdf_spo2, pdf_height, pdf_weight, pdf_complaintText,
	pdf_complaintSinceNum, pdf_complaintSinceUnit, pdf_complaintSeverity, pdf_diseaseText, pdf_diseaseNature, pdf_medecineName, pdf_medecineBrand,
	pdf_medecineDosage, pdf_medecineFrequency, pdf_medecineDuration, pdf_medecineDurationType;
	
	public static WebDriver DocPMS_PageProperties(){
		
		//Choose clinic popup
		chooseClinic_PopUp="//h3[contains(., 'Choose Clinics')]"; //Xpath
		chooseClinic_PopUp_SelectBtn="//a[@class='doctor-green-btn' and contains(., 'Select')]";
		
		//PMS side menu
		menu_dashboard="//span[contains(., 'Dashboard')]";
		
		//PMS dashboard
		dashboard_calendar_monthBtn="//button[contains(., 'month')]";
		dashboard_calendar_DayHeader="//div[@class='fc-row fc-widget-header']";
		dashboard_calendar_appointmentCard="//div[@class='fc-content']";
		dashboard_calendar_aptPopUp="//div[@class='calendar-popup']/div[@class='name']";
		dashboard_calendar_aptPopUp_detailsBtn="(//div[@class='calendar-popup']/div[@class='details'])[2]/a";
		dashboard_userIcon="//span[@class='user-icon']";
		dashboard_userIcon_logoutBtn="//span[contains(., 'Logout')]";
		dashboard_addAppointment="//button[text()='Add Appointment']";
		pms_logoutBtn="//div[@class='user-detail-container']/ul/li[2]"; //Xpath
		
		//PDF Container
		pdf_container="//div[@class='pdfContainer']"; //Xpath
		pdf_temperature="//span[text()='body temperature']/following-sibling::label"; //Xpath
		pdf_pulse="//span[text()='pulse']/following-sibling::label"; //Xpath
		pdf_respiration="//span[text()='respiration rate']/following-sibling::label"; //Xpath
		pdf_bp1="//span[text()='systolic']/following-sibling::label"; //Xpath
		pdf_bp2="//span[text()='diastolic']/following-sibling::label"; //Xpath
		pdf_spo2="//span[text()='spo2']/following-sibling::label"; //Xpath
		pdf_height="//span[text()='body height']/following-sibling::label"; //Xpath
		pdf_weight="//span[text()='body weight']/following-sibling::label"; //Xpath
		pdf_complaintText="(//tr[2])[2]/following-sibling::tr/td[1]"; //Xpath
		pdf_complaintSinceNum="(//tr[2])[2]/following-sibling::tr/td[2]"; //Xpath
		pdf_complaintSinceUnit="(//tr[2])[2]/following-sibling::tr/td[3]"; //Xpath
		pdf_complaintSeverity="(//tr[2])[2]/following-sibling::tr/td[4]"; //Xpath
		pdf_diseaseText="(//tr[3]/td[1]/label)[2]"; //Xpath
		pdf_diseaseNature="(//tr[3]/td[2]/label)[2]"; //Xpath
		pdf_medecineName="(//tr[3]/td[1]/label)[3]"; //Xpath
		pdf_medecineBrand="(//tr[3]/td[2]/label)[3]"; //Xpath
		pdf_medecineDosage="(//tr[3]/td[3]/label)[2]"; //Xpath
		pdf_medecineFrequency="(//tr[3]/td[4]/label)[2]"; //Xpath
		pdf_medecineDuration="//tr[3]/td[5]/label"; //Xpath
		pdf_medecineDurationType="//tr[3]/td[6]/label"; //Xpath
		
		//PMS Add appointment
		addAppointment_ChooseDoctor="choosedoctor"; //Name
		addAppointment_timeSlot="timeslot"; //Name
		addAppointment_consultationType="consultationType"; //Name
		addAppointment_NextBtn="//a[text()='NEXT']"; //Xpath
		addAppointment_patientMobile="patientMobile"; //Name
		addAppointment_patientAddress="//input[@name='patientAddress']"; //Xpath
		addAppointment_confirm="//button[text()='CONFIRM']"; //Xpath
		addAppointment_successMessage="//h3[contains(., 'Your appointment has been successfully booked')]"; //Xpath
		addAppointment_successMessageOKbtn="//a[text()='OK']"; //Xpath
		
		//pms appointment details pop up
		pms_aptDetailsPopUp_header="(//h2[contains(., 'APPOINTMENT DETAILS')])[2]";
		pms_aptDetailsPopUp_startConsultationBtn="//span[contains(., 'Start Consultation')]";
		pms_aptDetailsPopUp_viewPrescription="//a[text()='View Prescription']";
		
		//e-Prescriotion screen
		prescription_heading="ePrescriptionHeading"; //ID
		prescription_saveBtn="//button[contains(., 'SAVE')]";
		prescription_vital_temperature="temparature"; //ID
		prescription_vital_pulse="pulse"; //ID
		prescription_vital_respiration="respiration"; //ID
		prescription_vital_bloodpressure="bloodpressure"; //ID
		prescription_vital_sp="sp"; //ID
		prescription_vital_height="height"; //ID
		prescription_vital_weight="Weight"; //ID
		prescription_vitals_saveNotification="//div[@class='alert alert-success' and contains(., 'Doctor Consultation Vital information saved successfully')]"; //Xpath
		prescription_complaints_complaint_diseases="description"; //ID
		prescription_complaints_sinceValue="sinceValue"; //ID
		prescription_complaints_sinceCode="sinceCode"; //ID
		prescription_complaints_severity="severity"; //ID
		prescription_complaints_saveNotification="//div[@class='alert alert-success' and contains(., 'EPrescription Complaints Saved Sucessfully')]"; //xpath
		prescription_diseases_nature="natureOfdiagnosis"; //ID
		prescription_diseases_saveNotification="//div[@class='alert alert-success' and contains(., 'Health Record Category saved successfully')]"; //Xpath
		prescription_investigation_text="investigationInfoText";
		prescription_investigation_saveBtn="//p//span[contains(., 'SAVE')]";
		prescription_investigation_saveNotification="//div[@class='alert alert-success' and contains(., 'Investigation Information saved')]"; //Xpath
		prescription_medication_formulationName=".//*[@id='formulationName']";
		prescription_medication_brandName=".//*[@id='brandName']";
		prescription_medication_dosage=".//*[@id='dosageText']";
		prescription_medication_frequency="(//select[@id='frequencyCode'])[1]"; //Xpath
		prescription_medication_dosageDuration=".//*[@id='dosageDuration']";
		prescription_medication_durationType="(//select[@id='frequencyCode'])[2]"; //Xpath
		prescription_medication_saveNotification="//div[@class='alert alert-success' and contains(., 'Doctor Consultation medication information saved successfully')]"; //Xpath
		prescription_notesText="complaintsNotes"; //ID
		prescription_notes_saveNotification="//div[@class='alert alert-success' and contains(., 'EPrescription Notes Saved Sucessfully')]"; //Xpath
		prescription_endBtn="//button[@class='endPrescriptionButton']"; //Xpath
		prescription_addNewMedication="//a[contains(., 'Add a New Medication')]"; //Xpath
		
		return driver;
	}

}
