package objectRepository;

import java.io.FileInputStream;
import java.util.Properties;
import org.openqa.selenium.WebDriver;

public class Elements_Medicines {

	public static Properties prop = new Properties();
	public static FileInputStream inStream;
	public static WebDriver driver;

	// Med Home Page
	public static String miniCartCount, homePage_uploadPrescriptionBtn, pushNotification_popUp_NoBtn,
			genericSearch_resultWait, searchResult_FirstAddToCart, genericSearch_firstProduct_Price,
			header_miniCartViewBtn, header_miniCartIcon;

	// Cart Page
	public static String cart_prescriptionHeader;

	// PLP page
	public static String PLP_firstProductName_MedicineRX, PLP_shoppingCartLinkText, PLP_firstproductName_nonRX;

	// PDP page
	public static String PDP_discountBlock, PDP_discountName, PDP_discountValue;

	// Checkout Page
	public static String checkout_shippingAmt, checkout_couponCode, checkout_couponApplyBtn, checkout_couponValidMsg,
			checkout_getDiscountPer, checkout_Button, checkout_paymentGroup, checkout_comments, checkout_placeOrderBtn,
			checkout_shippingAmtWait, checkout_prescriptionPopUp_Ready, checkout_prescriptionPopUp_proceed,
			checkout_prescriptionPopUp_Later;

	// Prescription page
	public static String prescription_chooseFile, prescription_First_RecentFile, prescription_continueDisable,
			prescription_continueBtn, prescription_selectItemHeader, prescription_specifyBtn, prescription_addMed,
			prescription_addMedProceed;

	// Shipping page
	public static String shippingAddressHeader, shipping_enterCouponName, shipping_invalidCouponMessage,
			shipping_cartSubTotal, payment_COD2, payment_COD_radioBtn, shippingAddress_type, shippingAddress_Street,
			shippingAddress_state, shippingAddress_city, shippingAddress_pin, shippingAddress_phone, shipping_couponApplyBtn;

	// Order confirmation
	public static String confirmation_heading, confirmation_getOrderId, confirmation_prescriptionAdded,
			confirmation_paymentCOD, confirmation_paymentPayLater;

	public static WebDriver Medicine_PageProperties() throws Exception {

		// Med Home page
		miniCartCount = "//span[@class='counter-number']/span[text()='0']";
		homePage_uploadPrescriptionBtn = "//i[@class='zoylo-icon upload-your-prescription']";
		pushNotification_popUp_NoBtn = "//button[@class='No thanks']";
		genericSearch_resultWait = "//span[@data-bind='text: result.textAll']";
		searchResult_FirstAddToCart = "(//button[@class='action tocart primary']/span[text()='Add to Cart'])[1]";
		genericSearch_firstProduct_Price = "(//div[@class='price'])[1]";
		header_miniCartViewBtn = "//span[text()='VIEW CART']";
		header_miniCartIcon = "//div[@data-block='minicart']";

		// PLP Page
		PLP_firstProductName_MedicineRX = "(//div[@class='product-item-info']/div[@class='product photo product-item-photo'  and contains(., 'Prescription Required')])[1]/following-sibling::div/strong/a";
		PLP_shoppingCartLinkText = "//a[text()='shopping cart']";
		PLP_firstproductName_nonRX = "(//strong[@class='product name product-item-name']/a)[1]";

		// PDP page
		PDP_discountBlock = "//div[@class='get-additional-block']";
		PDP_discountName = "//div[@class='get-additional-code']/span";
		PDP_discountValue = "//div[@class='get-additional-pnl']/h4";

		// Cart Page
		cart_prescriptionHeader = "//td[contains(., 'Prescription')]";

		// Checkout Page
		checkout_shippingAmt = "//span[@data-th='Shipping']";
		checkout_shippingAmtWait = "//span[@data-th='Shipping' and text()='₹49.00']";
		checkout_couponCode = "//input[@name='coupon_code']";
		checkout_couponApplyBtn = "//div[text()='Apply']";
		checkout_couponValidMsg = "//div[@data-bind='html: message.text' and contains(., 'used')]";
		checkout_getDiscountPer = "//th[@class='mark']/span[contains(., 'Discount')]";
		checkout_Button = "//span[text()='Checkout']";
		checkout_paymentGroup = "(//div[@class='payment-group'])[2]";
		checkout_comments = "//textarea[@name='comment-code']";
		checkout_placeOrderBtn = "//button[@data-role='opc-continue']/span[text()='Place Order']";
		checkout_prescriptionPopUp_Ready = "//div[@class='col-sm-6 prescription-ready']/p/input";
		checkout_prescriptionPopUp_proceed = "//p[@class='popup-proceed']/input";
		checkout_prescriptionPopUp_Later = "//div[@class='col-sm-6 submit-delivery']/p/input";

		// Prescription Page
		prescription_chooseFile = "//label[text()='Choose Prescription File']";
		prescription_First_RecentFile = "(//span[@class='checkmark'])[1]";
		prescription_continueDisable = "//button[@id='continue' and @disabled='disabled']";
		prescription_continueBtn = "//button[text()='Continue']";
		prescription_selectItemHeader = "//p[@class='selected-items']";
		prescription_specifyBtn = "//button[contains(., 'Specify')]";
		prescription_addMed = "//button[@name='add']";
		prescription_addMedProceed = "//p[@class='upload-proceed']/input";

		// Shipping page
		shippingAddressHeader = "//span[text()='Shipping Address']";
		shipping_enterCouponName = "(//input[@id='discount-code'])[2]";
		shipping_invalidCouponMessage = "(//div[@data-ui-id='checkout-cart-validationmessages-message-error' and text()='Coupon code is not valid'])[2]";
		shipping_cartSubTotal = "//tr[@class='totals sub']/td/span";
		payment_COD2 = "(//span[text()='Cash On Delivery'])[2]";
		payment_COD_radioBtn = "(//input[@value='cashondelivery'])[2]";
		shippingAddress_type = "(//select[@name='custom_attributes[customer_address_type]'])[1]";
		shippingAddress_Street = "(//input[@name='street[0]'])[1]";
		shippingAddress_state = "(//select[@name='region_id'])[1]";
		shippingAddress_city = "city";
		shippingAddress_pin = "postcode";
		shippingAddress_phone = "telephone";
		shipping_couponApplyBtn = "//button[@value='Apply']";

		// Order confirmation
		confirmation_heading = "//h3[text()='Order Placed Successfully']";
		confirmation_getOrderId = "//div[@class='order-placed-block']/p[contains(., 'Your order')]";
		confirmation_prescriptionAdded = "//div[@class='precription-added']";
		confirmation_paymentCOD = "//div/p[text()='Cash On Delivery']";
		confirmation_paymentPayLater = "//div/p[contains(., 'Pay Later')]";

		return driver;

	}

}
