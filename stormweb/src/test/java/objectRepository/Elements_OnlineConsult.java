package objectRepository;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;


public class Elements_OnlineConsult  {
	
	public static Properties prop = new Properties();
	public static FileInputStream inStream;
    public static WebDriver driver;
    
    public static By enrollment1_h5 = By.xpath("//h5");// Another way of initialization to avoid xpath/id by defining in test case

    //SignIn elements
  	public static String SignIn_Email,SignIn_Password,SignIn_Login;
  	
  	//Home page
  	public static String homePage_consultOnline;
  	
  	//OnlineConsult listing page
  	public static String onlineListPage_banner;
  	
  	//OnlineConsult booking success popup
  	public static String onlineBooking_SuccessPopUp, onlineBooking_Success_StartBtn;
  	
  	//Online Chat screen recipient
  	public static String chatHeader, chatInput, chatSendBtn;
  	
  	//OC page
  	public static String oc_startConsultation, oc_addQuery, oc_issuePopUpH3, oc_issueCommentBox, oc_issueSubmitBtn,
  	oc_issueSubmitMsg, oc_issuesucessOkBtn;
  	
  	//PMS
  	public static String pms_oc_supportElipse, pms_oc_generatePrescBtn, pms_oc_prescibePopUp_heading,
  	pms_oc_prescibePopUp_diseaseDiscription, pms_oc_complaints_saveBtn, pms_oc_diseasesSaveBtn,
  	pms_oc_InvestigationSaveBtn, pms_oc_medicineSaveBtn, pms_oc_endPrescription, pms_oc_viewPrescriptionCloseBtn,
  	pms_oc_downloadPrescriptionBtn, pms_oc_downloadPrescriptionURL;
  	
  	//Admin
  	public static String admin_ocReportsTab, admin_ocReports_ocID, admin_ocReports_search, admin_ocSupportTab,
  	admin_ocSupportSearch, admin_ocSupportSearch_Submit, admin_ocReportSearch_Submit, admin_ocSupport_issueComment,
  	admin_ocSupport_viewIssue_closeBtn, admin_ocSupport_viewIssue_header, admin_ocSupport_1stchangeStatusBtn,
  	admin_ocSupport_statusHeader, admin_ocSupport_statusDropDown, admin_ocSupport_StatusCloseDrp, 
  	admin_ocSupport_statusChangeComment, admin_ocSupport_statusChangeSubmitBtn, admin_ocSupport_statusChangeSuccessMsg
  	,admin_ocSupport_statusCloseBtn, admin_ocSupport_status1stcolumn, admin_0cSupport_ViewUserName, admin_ocSupport_ViewUserNum
  	,admin_ocSupport_ViewDocName, admin_ocSupport_ViewDocNum, admin_ocReport_statusChangeSuccessMsg, admin_ocReport_viewHeader
  	,admin_ocReport_statusDropDwn;

	public static WebDriver Users_PageProperties()throws Exception{
	  	
		
		//SignIn elements
				SignIn_Email="email";
				SignIn_Password="pass";
				SignIn_Login="send2";
				
		//Home page
				homePage_consultOnline="//span[contains(., 'consult online')]"; //Xpath
		
		//OnlineConsult listing page
				onlineListPage_banner="//img[@src='/static/img/how-it-work.7b17ef3.png']";
				
		//OC page
				oc_startConsultation="//a[@href='/onlineConsult/doctors']";
				oc_addQuery="//a[contains(., 'Query')]"; //Xpath
				oc_issuePopUpH3="//h3[contains(., 'Select an issue')]";
				oc_issueCommentBox="textbox"; //Name
				oc_issueSubmitBtn="//a[contains(., 'Submit')]";
				oc_issueSubmitMsg="//p[contains(., 'Thank you. Your query has been successfully submitted.')]";
				oc_issuesucessOkBtn="//a[contains(., 'Ok')]";
				
		//OnlineConsult booking success popup		
				onlineBooking_SuccessPopUp="//div[@class='booking-container booking-main-content']";
				onlineBooking_Success_StartBtn="//div[@class='booking-container booking-main-content']/div/button";
				
		//Online Chat screen recipient
				chatHeader="//div[@class='right-top-content']"; //Path
				chatInput="//input[@class='input-section']"; //PAth
				chatSendBtn="//button[contains(., 'Send')]"; //Path
		
		//PMS
				pms_oc_supportElipse="//li[@class='conversation']/a";
				pms_oc_generatePrescBtn="//a[@class='generatePrescBtn']";
				pms_oc_prescibePopUp_heading="//span[contains(text(),'Doctor Online Prescription')]";
				pms_oc_prescibePopUp_diseaseDiscription="//div[@class='diseases-block']//input[@id='description']";
				pms_oc_complaints_saveBtn="//div[@id='complaintsBlock']//span[contains(text(),'SAVE')]";
				pms_oc_diseasesSaveBtn="//button[contains(text(),'SAVE')]";
				pms_oc_InvestigationSaveBtn="//div[@id='investigationBlock']//span[contains(text(),'SAVE')]";
				pms_oc_medicineSaveBtn="//div[@id='medicationBlock']//span[contains(text(),'SAVE')]";
				pms_oc_endPrescription="//button[text()='End Prescription']";
				pms_oc_viewPrescriptionCloseBtn="//i[@class='fa fa-close']";
				pms_oc_downloadPrescriptionBtn="//a[contains(text(),'Download')]";
				pms_oc_downloadPrescriptionURL="//span[@class='downloadUrl']";
				
		//Admin
				admin_ocReportsTab="//a[@href='#ocreports']";
				admin_ocReports_ocID="//div[@id='ocreports']/div/div[@class='container']/div/div/table/tbody/tr/td[1]";
				admin_ocReports_search="(.//*[@id='locationSearch'])[1]";
				admin_ocSupportTab="//a[@href='#supportissues']";
				admin_ocSupportSearch="//input[@slot='activator']";
				admin_ocSupportSearch_Submit="(//button[contains(., 'Submit')])[3]";
				admin_ocReportSearch_Submit="(//button[contains(., 'Submit')])[1]";
				admin_ocSupport_issueComment="//div[@class='container fluid']/div[5]/div[2]/li";
				admin_0cSupport_ViewUserName="//div[@class='container fluid']/div[6]/div[2]/li";
				admin_ocSupport_viewIssue_closeBtn="//button[contains(., 'Close')]";
				admin_ocSupport_viewIssue_header="//span[contains(., 'Issue Details')]";
				admin_ocSupport_1stchangeStatusBtn="(//a[contains(., 'Change Status')])[1]";
				admin_ocSupport_statusHeader="//div[@class='card__title heading' and contains(., 'Change Status')]";
				admin_ocSupport_statusDropDown="//div[@class='input-group__selections__comma' and contains(., 'Open')]";
				admin_ocSupport_StatusCloseDrp="(//div[@class='list__tile__title' and contains(., 'Close')])[1]";
				admin_ocSupport_statusChangeComment="//div[@class='input-group__input']/input[@data-vv-name='comment']";
				admin_ocSupport_statusChangeSubmitBtn="(//button/div[contains(., 'Submit')])[1]";
				admin_ocSupport_statusChangeSuccessMsg="//p[contains(., 'Customer query updated successfully.')]";
				admin_ocReport_statusChangeSuccessMsg="//p[contains(., 'Online consultation status updated successfully.')]";
				admin_ocSupport_statusCloseBtn="//button/div[contains(., 'close')]";
				admin_ocSupport_status1stcolumn="(//div[@id='supportissues']/div/div[@class='container']/div/div/table/tbody/tr/td[8])[1]";
				admin_ocSupport_ViewUserNum="//div[@class='container fluid']/div[7]/div[2]/li";
				admin_ocSupport_ViewDocName="//div[@class='container fluid']/div[8]/div[2]/li";
				admin_ocSupport_ViewDocNum="//div[@class='container fluid']/div[9]/div[2]/li";
				admin_ocReport_viewHeader="//div[@class='card__title heading' and contains(., 'Change Status')]";
				admin_ocReport_statusDropDwn="//div[@data-vv-name='OC Status']/div[1]";
		
		return driver;	
        
   
}

}
