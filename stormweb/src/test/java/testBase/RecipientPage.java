package testBase;
import objectRepository.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RecipientPage extends LoadProp {
	//FirefoxDriver Mobile = new FirefoxDriver();
	public   WebDriver driver;
	public TestUtils Browser;
	public boolean Slots;

	public RecipientPage(WebDriver driver) throws Exception {
		this.driver=driver;
		Elements_Recipients.Recipients_PageProperties();
		Browser= new TestUtils(driver);
	}
	
	/*  
	 *  @Author      : Ch.LakshmiKanth
	 *  @Description : This method is used to Recipient Login 
	 *  @Parameters  : Mobilenumber,Password
	 *  @Return      : 
	 */
	public void Recipient_Sigin(String Mobilenumber,String Password) throws Exception{
		
		if(driver.findElements(By.xpath("//button[@class='No thanks']")).size() > 0) {
			Browser.clickOnTheElementByXpath("//button[@class='No thanks']");
		}
		
		Browser.enterTextByXpath(Elements_Recipients.SignIn_Mobile,Mobilenumber);
		Browser.enterTextByXpath(Elements_Recipients.SignIn_Password, Password);
		Browser.clickOnTheElementByXpath(Elements_Recipients.SignIn_Login);
		
	}
	
	/*  
	 *  @Author      : Ch.LakshmiKanth
	 *  @Description : This method is used to Recipient Registration
	 *  @Parameters  : firstname,lastname,mobile,email,Password
	 *  @Return      : 
	 */
	public void SignUp_Details(String firstname,String lastname,String mobile,String email,String Password) throws Exception{
		
		Browser.enterTextByXpath(Elements_Recipients.signUp_firstName, firstname);
		Browser.enterTextByXpath(Elements_Recipients.signUp_lastName, lastname);
		Browser.enterTextByXpath(Elements_Recipients.signUp_MobileNumber,mobile);
		Browser.enterTextByXpath(Elements_Recipients.signUp_Email, email);
		Browser.enterTextByXpath(Elements_Recipients.signUp_password, Password);
	}
	
	
	/*  
	 *  @Author      : Ch.LakshmiKanth
	 *  @Description : This method is used to Select Module(Doctors,Wellness,Diagnostics) Enter Location , Select Location Suggession and
	 *                 Enter Clinic/Doctor and select Suggession
	 *  @Parameters  : locationName, data
	 *  @Return      : 
	 */
	public void GenericSearch(String Module,String locationName, String data) throws InterruptedException
	{
		Thread.sleep(5000);
		if(driver.findElement(By.xpath("//span[@class='icon hyderabad m-0']")).isDisplayed()){
			Browser.clickOnTheElementByXpath("//h6[contains(text(),'Hyderabad')]");
			
		}
		//homePagePopUpHandler(LoadProp.popupClose);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.GenericSearch_Click_Module);
		Browser.clickOnTheElementByXpath("//a[@class='dropdown-item'][contains(text(),'"+Module+"')]");
		Thread.sleep(1000);
		Browser.enterTextByXpath(Elements_Recipients.home_locationSearch, locationName);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_locationSearch_Suggestion);
		Thread.sleep(1000);
		if(data=="" || data==null)
		{
			System.out.println("No data provided");
		}
		else{
			for(int i=0;i<=data.length()-1; i++)
		    {
		    	char key = data.charAt(i);
		    	WebDriverWait wait = (new WebDriverWait(driver, 60));
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Elements_Recipients.GenericSearch_Enter_ModuleData)));
				Thread.sleep(500);
				driver.findElement(By.xpath(Elements_Recipients.GenericSearch_Enter_ModuleData)).sendKeys(Character.toString(key));
				System.out.println("Texted = "+data);
				Reporter.log("Text Entered="+data);
		    	Thread.sleep(600);
		    	
		    }
		Browser.clickOnTheElementByXpath(Elements_Recipients.GenericSearch_ModuleData_Suggession);
		}
		Browser.clickOnTheElementByXpath(Elements_Recipients.GenericSearch_Button);
	}
	
	/*  
	 *  @Author      : Ch.LakshmiKanth
	 *  @Description : This method is used to Search the particular doctor in List page and Click On Book button
	 *  @Parameters  : DoctorName
	 *  @Return      : 
	 */
	public void ListPage_Select_Doctor(String DoctorName) throws Exception{
		
		Actions action = new Actions(driver);
		//Browser.waitFortheElementXpath("//li[@class='active']//h5[@class='m-0'][contains(text(),'₹')]");
		while (driver.findElements(By.xpath("//ul[@class='search-list']/li/div/div//h2//a[contains(text(),'"+DoctorName+"')]")).size()==0){
		//driver.findElement(By.xpath("//li[@class='active']//h5[@class='m-0'][contains(.,'₹')]")).click();
			driver.findElement(By.xpath("(//h5[contains(text(),'₹')])[1]")).click();
		Thread.sleep(1000);
		action.sendKeys(Keys.PAGE_DOWN).build().perform();
			}
			System.out.println("Came Out of loop");
			WebElement we = driver.findElement(By.xpath("(//ul[@class='search-list']//li//div[contains(.,'"+DoctorName+"')]//following-sibling::button)[1]"));
			action.moveToElement(we).click().build().perform();
			Thread.sleep(1000);
			System.out.println("Clicked on Book button");
	}
	
	/*  
	 *  @Author      : Ch.LakshmiKanth
	 *  @Description : This method is used to Change the Date for Doctor Appointment
	 *  @Parameters  : DoctorName
	 *  @Return      : 
	 */
	public void Doctor_SelectAppointment_Date(int value){
		
		Browser.clickOnTheElementByXpath("//div[@class='vdp-datepicker']//div//input[@type='text']");
		String moddate=Browser.getModifiedDate(value);
		Browser.clickOnTheElementByXpath("//span[@class='cell day'][contains(text(),'"+moddate+"')]");
	}
	
	/*  
	 *  @Author      : Ch.LakshmiKanth
	 *  @Description : This method is used For ONLINE PAYMENT 
	 *  @Parameters  : DoctorName
	 *  @Return      : 
	 */
	public void OnlinePayment() throws Exception{
		
		/*Browser.waitFortheElementXpath("//label[@for='cardpayment']");
		Browser.clickOnTheElementByXpath("//label[@for='cardpayment']");
		Thread.sleep(5000);
		JavascriptExecutor js= (JavascriptExecutor)driver;
		js.executeScript("document.getElementById('tandc').checked=true;");
		System.out.println("checked");*/
		
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("(//button[@type='button'])");
		Thread.sleep(2000);
		driver.switchTo().frame(0);
		Browser.clickOnTheElementByXpath("//div[@tab='netbanking']");
		Browser.clickOnTheElementByXpath("//div[@class='mchild l-3 item-inner'][contains(.,'ICICI')]");
		Browser.clickOnTheElementByXpath("//button[@type='submit']");
		Thread.sleep(1000);
		String winHandleBefore = driver.getWindowHandle();
		for(String handle : driver.getWindowHandles())
        {
            driver.switchTo().window(handle);
        }
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//button[@class='success']");
		Thread.sleep(4000);
		driver.switchTo().window(winHandleBefore);
		Thread.sleep(1000);
		
		
	}
	
	
	/*  
	 *  @Author      : Ch.LakshmiKanth
	 *  @Description : This method is used to Change The Time Slot (Session) for Doctor Appointment
	 *  @Parameters  : 
	 *  @Return      : 
	 */
	
	public void Doctor_ChangeTimeslot() throws Exception{
		
		Browser.clickOnTheElementByXpath(Elements_Recipients.bookingDetails_Changeslot);
		Browser.clickOnTheElementByXpath(Elements_Recipients.bookingDetails_ChangeSlot_NightTab);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("(//div[@class='carousel-caption']//ul//li[2])[1]");
		Browser.clickOnTheElementByXpath(Elements_Recipients.bookingDetails_ChangeSlot_OkButton);
			
	}
	
	/*  
	 *  @Author      : Ch.LakshmiKanth
	 *  @Description : This method is used to Recipient Logout
	 *  @Parameters  : 
	 *  @Return      : 
	 */
	
	public void Recipient_Logout() throws Exception{
		
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_headerUserIcon);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_logout);
	}
	
	/*  
	 *  @Author      : Ch.LakshmiKanth
	 *  @Description : This method is used to Calculate Coupon Code amount
	 *  @Parameters  : ZFCPerc
	 *  @Return      : 
	 */
	 public int Calculate_PromoCodeamount(int ZFCPerc){
		 
		 String Advance=Browser.getTextByXpath("(//h6[@class='brand-color'])[2]").replaceAll("[^0-9]", "");
		 
			System.out.println("Getting from Calculation:"+Advance);
			System.out.println("ZFC Is :"+ZFCPerc);
			double AdvancePayment=Double.valueOf(Advance);
			//int AdvancePayment=Integer.parseInt(Advance);
			double Promocodeamount=(AdvancePayment*ZFCPerc)/100;
			int CouponCode=(int)((AdvancePayment)-(Promocodeamount));
			int FinalAdvance=Math.round(CouponCode);
			System.out.println("Coupon Amount finally To Be Paid:"+FinalAdvance);
			return FinalAdvance;
		 
	 }
	 
	 /*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Enter Promo Code/Coupon Code
		 *  @Parameters  : 
		 *  @Return      : 
		 */
	public void EnterPromoCode(String Promocode) throws Exception{
		
		Browser.enterTextByXpath(Elements_Recipients.bookingDetails_Promocode, Promocode);
		Browser.clickOnTheElementByXpath(Elements_Recipients.bookingDetails_Promocode_Apply);
	}
	
	
	 /*  
	 *  @Author      : Ch.LakshmiKanth
	 *  @Description : This method is used to OnlinePayment for Others
	 *  @Parameters  : 
	 *  @Return      : 
	 */
	
	public void OnlinePayment_Others() throws Exception{
		
		/*Browser.waitFortheElementXpath("//label[@for='cardpayment']");
		Browser.clickOnTheElementByXpath("//label[@for='cardpayment']");
		Thread.sleep(5000);
		JavascriptExecutor js= (JavascriptExecutor)driver;
		js.executeScript("document.getElementById('tandc').checked=true;");
		System.out.println("checked");*/
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("(//button[@type='button'])");
		//Browser.clickOnTheElementByXpath("(//button[@type='button'])");
		Thread.sleep(2000);
		driver.switchTo().frame(0);
		//Thread.sleep(1000);
		//Browser.clickOnTheElementByXpath("//label[contains(., 'Email')]");
		driver.findElement(By.xpath("//input[@name='email']")).click();
		Browser.enterTextByXpath("//input[@name='email']","yaswanth@gmail.com");
		Browser.clickOnTheElementByXpath("//div[@tab='netbanking']");
		Browser.clickOnTheElementByXpath("//div[@class='mchild l-3 item-inner'][contains(.,'ICICI')]");
		Browser.clickOnTheElementByXpath("//button[@type='submit']");
		Thread.sleep(1000);
		String winHandleBefore = driver.getWindowHandle();
		for(String handle : driver.getWindowHandles())
        {
            driver.switchTo().window(handle);
        }
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//button[@class='success']");
		Thread.sleep(4000);
		driver.switchTo().window(winHandleBefore);
		Thread.sleep(1000);
		
	}
	
	 /*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Enter  Other Details
		 *  @Parameters  : Name phone
		 *  @Return      : 
		 */
	
	public void BookAppointment_Others(String Name,String phone) throws Exception{
		
		Browser.clickOnTheElementByXpath(Elements_Recipients.bookingDetails_otherRadioButton);
		Browser.enterTextByXpath(Elements_Recipients.bookingDetails_Others_Name, Name);
		Browser.enterTextByXpath(Elements_Recipients.bookingDetails_Others_Contact, phone);
	}
	
	 /*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Select Wellness Menu
		 *  @Parameters  : 
		 *  @Return      : 
		 */
	
	public void SelectWellness_Menu(){
		
		Browser.clickOnTheElementByXpath(Elements_Recipients.GenericSearch_Click_Module);
		Browser.clickOnTheElementByXpath(Elements_Recipients.Select_WellnessMenu);
	}
	
	
	 /*  
	 *  @Author      : Ch.LakshmiKanth
	 *  @Description : This method is used to Select To Book appointmentFor(Self/Others), Enter Promocode if no Promocode then Enter No_Promocode
	 *  @Parameters  : AppointmentFor,Name,phone,PromocodeValue,PromocodeName
	 *  @Return      : 
	 */
	
	public void BookingDetails_ProceedToPay(String AppointmentFor,String Name,String phone,String PromocodeName) throws Exception{
		
		Browser.waitFortheElementXpath(Elements_Recipients.bookingDetails_WaitForAppointmentSchedule); 
		if(AppointmentFor.equalsIgnoreCase("Others")){
			
			System.out.println("Booking Appointment Selected :"+AppointmentFor);
			driver.findElement(By.xpath(Elements_Recipients.bookingDetails_otherRadioButton)).click();
			Browser.enterTextByXpath(Elements_Recipients.bookingDetails_Others_Name, Name);
			Browser.enterTextByXpath(Elements_Recipients.bookingDetails_Others_Contact, phone);		
		}

		if( PromocodeName.contains("No_Promocode")){
			
			System.out.println("No Promocode");

		}else{
			
			Thread.sleep(1000);
			Browser.enterTextByXpath(Elements_Recipients.bookingDetails_Promocode, PromocodeName);
			Thread.sleep(2000);
			Browser.clickOnTheElementByXpath(Elements_Recipients.bookingDetails_Promocode_Apply);
			Thread.sleep(1000);

		}  
		
		Browser.ScrollDown();
		Browser.clickOnTheElementByXpath(Elements_Recipients.Click_PaymentToProceedButton);
		
		}
		
	
	
	/*  
	 *  @Author      : Ch.LakshmiKanth
	 *  @Description : This method is used to JusPay Payment Flow
	 *  @Parameters  : 
	 *  @Return      : 
	 */
	
	public void juspay() throws Exception{
		
		Thread.sleep(3000);
		driver.switchTo().frame(1);
		System.out.println("Switch to Iframe");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//a[contains(text(),'Net Banking')]");
		System.out.println("Click on NetBanking");
		Browser.clickOnTheElementByXpath("//div[@class='nb-logo nb_icici']");							// juspay
		Browser.clickOnTheElementByXpath("//button[@class='btn btn-primary'][contains(@id,'btn')]");	// juspay
		Thread.sleep(1000);
//		String winHandleBefore = driver.getWindowHandle();
//		for(String handle : driver.getWindowHandles())
//        {
//            driver.switchTo().window(handle);
//        }
		Browser.clickOnTheElementByXpath("//button[@class='success']");
		//driver.switchTo().window(winHandleBefore);
		
	}
	
		public void razorpay() throws Exception
		{
		
		Thread.sleep(3000);
		driver.switchTo().frame(0);
		System.out.println("Switch to Iframe");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@tab='netbanking']");
		System.out.println("Click on NetBanking");
		Browser.clickOnTheElementByXpath("//div[@class='mchild l-3 item-inner'][contains(.,'ICICI')]"); // Old razorpay
		Browser.clickOnTheElementByXpath("//button[@type='submit']");									// Old razorpay
		Thread.sleep(1000);
		String winHandleBefore = driver.getWindowHandle();
		for(String handle : driver.getWindowHandles())
        {
            driver.switchTo().window(handle);
        }
		Browser.clickOnTheElementByXpath("//button[@class='success']");
		driver.switchTo().window(winHandleBefore);
		
		}
	
	/*  
	 *  @Author      : Ch.LakshmiKanth
	 *  @Description : This method is used to Select The PaymentType and Book Appointment
	 *  @Parameters  : PaymentType, PaymentMode
	 *  @Return      : 
	 */
	
	
		public void MakePayment(String PaymentType, String PaymentMode) throws Exception{
			
			NewJuspay();
		
		/*Browser.waitFortheElementXpath(Elements_Recipients.PaymentSelection_WaitforAmount_Display);
		Thread.sleep(2000);
		int amountToBePaid= Integer.parseInt(Browser.getTextByXpath("//div[@class='grey-bg border-dashed-grey']//h6").replaceAll("[^0-9]", ""));
		System.out.println("Amount To Be Paid :"+amountToBePaid);
		int WallertBalance=Integer.parseInt(Browser.getTextByXpath("//div[@class='font12 font-medium']").replaceAll("[^0-9]", ""));	
		System.out.println("Available wallet Balance :"+WallertBalance);
		
		if(PaymentType.equalsIgnoreCase("Single")&& (PaymentMode.equalsIgnoreCase("netbanking"))){
			
			//Single Payment with netbanking		
		if((WallertBalance>0 || WallertBalance>amountToBePaid) && driver.findElement(By.xpath("//input[@id='zoycash']")).isSelected()){
				driver.findElement(By.xpath("//label[@for='zoycash']")).click();
				Thread.sleep(1000);
				//Browser.clickOnTheElementByXpath("//label[@for='cardpayment']");
				//Browser.clickOnTheElementByXpath("//label[@for='juspay']");
				System.out.println("Zoycash check box Unchecked");
				Browser.clickOnTheElementByXpath("//button[@type='button'][contains(., 'Continue')]");
				
				
			}else{
				// Default Card Payment with netbanking
				Browser.clickOnTheElementByXpath("(//button[@type='button'])");
			}
		//Payment Popup
		juspay();
		//razorpay();
		}
		// Only Zoylo Cash Payment
		else if (PaymentType.equalsIgnoreCase("Single")&& PaymentMode.equalsIgnoreCase("Wallet") && amountToBePaid<(WallertBalance)||amountToBePaid<=WallertBalance ) {
			System.out.println("Total Payment From Zoylo wallet");
			if(driver.findElement(By.xpath("//input[@id='zoycash']")).isSelected()){
			Browser.clickOnTheElementByXpath("(//button[@type='button'])");
			}
		}
		
		//Partial Payment with Zoylo Cash and Netbanking
		else if (PaymentType.equalsIgnoreCase("Multiple")&& amountToBePaid>WallertBalance) {
			
			if(PaymentMode.equalsIgnoreCase("netbanking")){
			System.out.println("Wallet Partial Payment with Net Banking");
			Boolean zoylocash=driver.findElement(By.xpath("//input[@id='zoycash']")).isSelected();
			System.out.println("Zoylocash status:"+zoylocash);
			//Boolean netbaking=driver.findElement(By.xpath("//input[@id='cardpayment']")).isSelected(); // Old razorpay
			Boolean netbaking=driver.findElement(By.xpath("//input[@id='juspay']")).isSelected();
			System.out.println("netbanking status:"+netbaking);
			if(zoylocash==true && netbaking==true){
				Browser.clickOnTheElementByXpath("(//button[@type='button'])");
				juspay();
				//razorpay();
	
			}else{
				driver.findElement(By.xpath("//label[@for='zoycash']")).click();
				Browser.clickOnTheElementByXpath("(//button[@type='button'])");
				juspay();
				//razorpay();
			}
			
			
			}		
		}// To -satrt another else if
*/		
		
		
		}//main
		
		
		/*  
		 *  @Author      : Sagar Sen
		 *  @Description : This method is used to Select net banking and fail the transaction
		 *  @Parameters  : 
		 *  @Return      : 
		 */
		public void MakePayment_fail() throws Exception {
			Thread.sleep(2000);
			Browser.waitFortheElementXpath("//a[contains(., 'NET BANKING')]");
			Browser.clickOnTheElementByXpath("//a[contains(., 'NET BANKING')]");
			//driver.findElement(By.xpath("//input[@value='NB_ICICI']")).click();
			Browser.clickOnTheElementByXpath("//img[@alt='icici-bank-logo']");
			Browser.clickOnTheElementByXpath("//button[@type='button']");
			Browser.clickOnTheElementByXpath("//button[text()='Failure']");
		}
		
		
		//New Juspaymethod
		
		public void NewJuspay() throws Exception{
			Thread.sleep(2000);
			Browser.waitFortheElementXpath("//a[contains(., 'NET BANKING')]");
			Browser.clickOnTheElementByXpath("//a[contains(., 'NET BANKING')]");
			//driver.findElement(By.xpath("//input[@value='NB_ICICI']")).click();
			Browser.clickOnTheElementByXpath("//img[@alt='icici-bank-logo']");
			Thread.sleep(2000);
			Browser.clickOnTheElementByXpath("//button[@type='button']");
			Thread.sleep(2000);
			Browser.clickOnTheElementByXpath("//button[@class='success']");
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Select Wallet or Card and Book Transaction 
		 *  @Parameters  : PaymentType PaymentType_Options For(Wallet-> Options are AMAZON PAY,MOBIKWIK, OLAMONEY
		 *  @Return      : 
		 */
		public void MakePayment_Options(String PaymentType, String PaymentType_Option) throws Exception{
			
			if(PaymentType.equalsIgnoreCase("Wallet")){
				
				Browser.clickOnTheElementByXpath("//li[contains(.,'"+PaymentType_Option+"')]/a");
				Thread.sleep(7000);
				if(driver.findElements(By.xpath("//button[@class='success']")).size() == 0) {
					Browser.clickOnTheElementByXpath("//li[contains(.,'"+PaymentType_Option+"')]/a");
					System.out.println("2nd time clicked");
				}
				Browser.waitFortheElementXpath("//button[@class='success']");
				Browser.clickOnTheElementByXpath("//button[@class='success']");
				
			}else if (PaymentType.equalsIgnoreCase("Cards")) {
				
				Browser.clickOnTheElementByXpath("//a[@class='nav-link'][contains(text(),'CARDS')]");
				Thread.sleep(1000);
				driver.switchTo().frame(0);
				Browser.enterTextByXpath("//input[@type='text'][contains(@id,'card')]","cherukuri");
				driver.switchTo().parentFrame();
				driver.switchTo().frame(1);
				Thread.sleep(1000);
				Browser.enterTextByXpath("//input[@name='card_number']", "4111111111111111");
				driver.switchTo().parentFrame();
				driver.switchTo().frame(2);
				Browser.enterTextByXpath("//input[@name='card_exp_month']", "03");
				driver.switchTo().parentFrame();
				driver.switchTo().frame(3);
				Browser.enterTextByXpath("//input[@name='card_exp_year']", "22");
				driver.switchTo().parentFrame();
				driver.switchTo().frame(4);
				Browser.enterTextByXpath("//input[@name='security_code']", "123");
				driver.switchTo().parentFrame();
				Browser.clickOnTheElementByXpath("(//button[@id='common_pay_btn'])[2]");
				Browser.waitFortheElementXpath("//button[@class='success']");
				Browser.clickOnTheElementByXpath("//button[@class='success']");
				
			}else if (PaymentType.equalsIgnoreCase("Paypal")) {
				
				Browser.clickOnTheElementByXpath("//a[@role='tab'][contains(@id,'')][contains(.,'PAYPAL')]");
				Thread.sleep(1000);
				Browser.clickOnTheElementByXpath("//button[@type='button'][contains(.,'Login')]");
				Thread.sleep(12000);
				Browser.waitFortheElementXpath("//a[@class='btn full ng-binding']");
				Thread.sleep(5000);
				System.out.println("Clicked on login");
				Browser.clickOnTheElementByXpath("//a[@class='btn full ng-binding']");
				Thread.sleep(3000);
				Browser.waitFortheElementXpath("//input[@name='login_email']");
				Browser.enterTextByXpath("//input[@name='login_email']", "lpalanisamy-buyer@paypal.com");
				Browser.enterTextByXpath("//input[@name='login_password']", "Admin@123");
				Browser.clickOnTheElementByXpath("//button[@id='btnLogin']");
				Thread.sleep(7000);
				Browser.waitFortheElementXpath("//button[@track-submit='choose_FI_interstitial']");
				Thread.sleep(3000);
				Browser.clickOnTheElementByXpath("//button[@track-submit='choose_FI_interstitial']");
				Browser.waitFortheElementXpath("//input[@name='cvv']");
				Browser.enterTextByXpath("//input[@name='cvv']", "123");
				Browser.clickOnTheElementByXpath("//input[@value='Pay Now']");
				Thread.sleep(7000);
				driver.switchTo().frame("threeDSFrame");
				System.out.println("switched To iframe One");
				driver.switchTo().frame("authWindow");
				System.out.println("switch to frame two");
				Browser.enterTextByXpath("//input[@id='password']", "kanth");
				Browser.clickOnTheElementByXpath("//input[@value='Submit']");
				Thread.sleep(3000);	
			
			}else if (PaymentType.equalsIgnoreCase("Netbanking")) {
				
				NewJuspay();
				
			}
			
			
			
			
		}
		
		
	    
	 /*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to search by appointment ID and verify the wallet deduction 
		 *  @Parameters  : Amounttype,appointmentID
		 *  @Return      : 
		 */
	
	public int wallet_transactionVerification(String Amounttype,String appointmentID) throws Exception{
		
		String aptNum,amount,deditaptNum,amounttwo,deducted_Credit_1, deducted_Credit_2, deducted_Dedit_1,deducted_Dedit_2;
		
		if(Amounttype.equalsIgnoreCase("CreditAmount")){
			
			Browser.clickOnTheElementByXpath("//a[contains(., 'Credit Amount')]");
			Browser.clickOnTheElementByXpath("//a[@id='filter-by-date-trigger']");
			Browser.enterTextByXpath("//input[@name='Appointment Id']", appointmentID);
			Browser.clickOnTheElementByXpath("//a[contains(., 'Search')]");
			Browser.clickOnTheElementByXpath("//a[@id='filter-by-date-trigger']");
			Thread.sleep(2000);
			aptNum=Browser.getTextByXpath("//div[@id='wallet-credit']//tbody//tr[1]//td[1]").replaceAll("[^a-zA-Z0-9\\s+]", "");
			Assert.assertEquals(aptNum, appointmentID);
			Thread.sleep(1000);
			deducted_Credit_1=Browser.getTextByXpath("//div[@id='wallet-credit']//tbody//tr[1]//td[5]").replaceAll("[^\\d.]", "");
			deducted_Credit_2=deducted_Credit_1.replaceAll("(?<=^\\d+)\\.0*$", "");
			int creditamount=Integer.parseInt(deducted_Credit_2);
			System.out.println("Credited amount :"+creditamount);
			return creditamount;
		}else{
			
			Browser.clickOnTheElementByXpath("//a[contains(., 'Debit Amount')]");
			Thread.sleep(1000);
			Browser.clickOnTheElementByXpath("//a[@id='filter-by-date-trigger']");
			Browser.enterTextByXpath("//input[@name='Appointment Id']", appointmentID);
			Browser.clickOnTheElementByXpath("//a[contains(., 'Search')]");
			Browser.clickOnTheElementByXpath("//a[@id='filter-by-date-trigger']");
			Thread.sleep(1000);
			deditaptNum=Browser.getTextByXpath("//div[@id='wallet-debit']//tbody//tr[1]//td[1]").replaceAll("[^a-zA-Z0-9\\s+]", "");
			Assert.assertEquals(deditaptNum, appointmentID);
			deducted_Dedit_1 =Browser.getTextByXpath("//div[@id='wallet-debit']//tbody//tr[1]//td[5]").replaceAll("[^\\d.]", "");
			deducted_Dedit_2=deducted_Dedit_1.replaceAll("(?<=^\\d+)\\.0*$", "");
			int deditamount=Integer.parseInt(deducted_Dedit_2);
			System.out.println("Dedited Amount :"+deditamount);
			return deditamount;
		}		
		
	}
	
	/*  
	 *  @Author      : Ch.LakshmiKanth
	 *  @Description : This method is used for doctor timeslot to Select the session and click on the 2nd time slot
	 *  @Parameters  : session(NIGHT,EVENING,AFTERNOON,MORNING)
	 *  @Return      : 
	 */
	
		public void Doctor_ChangeSession(String session){
		
		Browser.clickOnTheElementByXpath("//span[@class='"+session+"']");
		Browser.clickOnTheElementByXpath("(//ul[@class='d-flex align-items-center filter-list mt-0 flex-wrap']/li[2])[1]");
	}
	
	
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to select  doctor Module, search location from generic search 
		 *  				and search doctor from home page by clicking on Book doctor appointment menu
		 *  @Parameters  : Module,Location,data
		 *  @Return      : 
		 */
		
		public void Home_Search_Doctor(String Module,String Location,String data) throws Exception{
			
			Thread.sleep(2000);
			Browser.clickOnTheElementByXpath(Elements_Recipients.GenericSearch_Click_Module);
			Browser.clickOnTheElementByXpath("//a[@class='dropdown-item'][contains(text(),'"+Module+"')]");
			Browser.enterTextByXpath(Elements_Recipients.home_locationSearch, Location);
			Thread.sleep(1000);
			Browser.clickOnTheElementByXpath(Elements_Recipients.home_locationSearch_Suggestion);
			Browser.clickOnTheElementByXpath("(//a[contains(text(),'Book Doctor Appointment')])[1]");
				
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Search The Particular Online Consulation Doctor
		  					and Click On respective Consultation Type
		 *  
		 *  @Parameters  : Doctor,consultationType
		 *  @Return      : 
		 */
		
		public void OC_ListPage_Search_Doctor(String Doctor) throws Exception{
			
			Actions action = new Actions(driver);
			while (driver.findElements(By.xpath("//div[@class='d-flex align-items-start' and contains(., '"+Doctor+"')]")).size()==0){
			driver.findElement(By.xpath("(//div[@class='doc-details flex-grow-1']/p[2])[1]")).click();
			action.sendKeys(Keys.PAGE_DOWN).build().perform();
			//Browser.scrollbyxpath("//div[@class='d-flex align-items-start' and contains(., '"+Doctor+"')]");
			}
			System.out.println("Came Out of loop");
			WebElement we =driver.findElement(By.xpath("//div[@class='col-md-8 col-lg-7 col-xl-8'][contains(., '"+Doctor+"')]//following-sibling::div//div//div[2]/button"));
			action.moveToElement(we).click().build().perform();
			Thread.sleep(1000);
			
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Click on Online Consultation Button and Click on
		 *                 Selected Speciallity and Click on Proceed Button
		 *  @Parameters  : Speciality
		 *  @Return      : 
		 */
		
		public void HomePage_Select_OnlineConsultation(String Speciality) throws Exception{
			
			Browser.clickOnTheElementByXpath(Elements_Recipients.home_GetOnlineConsultation);
			Browser.clickOnTheElementByXpath("//div[@id='online-filter']//a[contains(text(),'"+Speciality+"')]");
			Thread.sleep(1000);
			Browser.clickOnTheElementByXpath(Elements_Recipients.home_OnlineConsultation_Proceed);
			
			
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Enter the HomeHealthCare Details
		 *  
		 *  @Parameters  :Name, Mobile,State,City,Email,PackageName
		 *  @Return      : 
		 */
		
		public void HomeHealthCare_Details(String Name, String Mobile,String State,String City, String Email, String PackageName) throws Exception{
			
			Browser.enterTextByXpath(Elements_Recipients.HomeHealthCare_Name, Name);
			Browser.enterTextByXpath(Elements_Recipients.HomeHealthCare_Mobile, Mobile);
			Browser.selectXpathByText(Elements_Recipients.HomeHealthCare_State, State);
			Browser.selectXpathByText(Elements_Recipients.HomeHealthCare_City, City);
			Browser.enterTextByXpath(Elements_Recipients.HomeHealthCare_Email, Email);
			Browser.selectXpathByText(Elements_Recipients.HomeHealthCare_Package,PackageName);
			Browser.clickOnTheElementByXpath(Elements_Recipients.HomeHealthCare_Submit);
			
		}
		
			
		
		//PMS 
		
		public void PMSLogin(String MobNum, String password) throws InterruptedException{

			Browser.enterTextByXpath(Elements_Recipients.PMS_SigIn_Mobile, MobNum);	
			System.out.println("Entered mobile number");
			Browser.enterTextByXpath(Elements_Recipients.PMS_SigIn_Password, password);
			System.out.println("Entered password");
			Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Login);
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Thread.sleep(3000);
			
		}
		
		public void PMS_Logout() throws Exception{
			
			Browser.clickOnTheElementByXpath("//div[@class='user']//span[2]");
			Thread.sleep(1000);
			Browser.clickOnTheElementByXpath("//span[contains(text(),'Logout')]");
			
		}
		
		//Clinic
		
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is For Clinic used to Select the Package and Click on Book appointment
		 *                 For Current Day 
		 *  @Parameters  : Package
		 *  @Return      : 
		 */
		public void Clinic_SelectPackage_Book_Confirm(String Package) throws Exception{
			
			Actions action = new Actions(driver);
			WebElement we =driver.findElement(By.xpath("//div[@class='media-body w-100'][contains(., '"+Package+"')]/following-sibling::div/button"));
			action.moveToElement(we).click().build().perform();
			Browser.clickOnTheElementByXpath("//button[contains(text(),'OK')]");
			Thread.sleep(2000);
			Browser.clickOnTheElementByXpath("//button[contains(text(),'Confirm')]");
			
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Select menu (Profile ,My Appointment etc)
		 *  @Parameters  : Menu (My Profile,,My Consultation,My Orders,Zoylo Wallet,Logout)
		  					For My AppointmentsTab:(diagnostic,package,doctor)
		  					for My Profile Tab: (view-profile,family-profile)
		 *  @Return      : 
		 */
		
		public void header_UserDetails_SelectMenu(String Menu,String Tab) throws Exception{
			
			Browser.clickOnTheElementByXpath(Elements_Recipients.home_headerUserIcon);
			Thread.sleep(1000);
			Browser.clickOnTheElementByXpath("//a[contains(text(),'"+Menu+"')]");
			Thread.sleep(2000);
			if(Menu.equals("My Profile")){
			
				Browser.clickOnTheElementByXpath("//a[@href='/my-profile/"+Tab+"']");	
			
			}else if (Menu.equals("My Appointments")) {
				
				Browser.clickOnTheElementByXpath("//a[contains(@href,'/my-appointments/"+Tab+"')]");
				
			}
			
			
			
		}
		
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Recshedule the Booked appointment for Clinic
		 *  @Parameters  : 
		 *  @Return      : 
		 */
		
		public String Clinic_Reschedule_Appointent() throws Exception{
			
			Actions action = new Actions(driver);
			WebElement Reschedule = driver.findElement(By.xpath(Elements_Recipients.header_myAppointments_Package_Reschedule));
			action.moveToElement(Reschedule).click().build().perform();
			Thread.sleep(3000);
			String TodayDate=Browser.getCurrentDate();
			String MonthLastDate=Browser.GetCurrentMonthLastDate();
			String Rescheduledate=Browser.getModifiedDate(1);
			if(TodayDate.equals(MonthLastDate)){
				
				Browser.clickOnTheElementByXpath("(//span[@class='next'])[7]");
				Browser.clickOnTheElementByXpath("(//span[text()='"+Rescheduledate+"'])[3]");
				
			}else{
				
				Browser.clickOnTheElementByXpath("(//span[text()='"+Rescheduledate+"'])[3]");
				
			}
			
			Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_Package_Reschedule_Confirm);
			return Rescheduledate;
			
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Cancel the Booked appointment for Clinic
		 *  @Parameters  : 
		 *  @Return      : 
		 */
		
		
		public void Clinic_Cancel_Appointment(){
			Actions action = new Actions(driver);
			WebElement Canceled = driver.findElement(By.xpath(Elements_Recipients.header_myAppointments_Package_Cancel_Link));
			action.moveToElement(Canceled).click().build().perform();
			Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_Package_Cancel_Confirm);
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Reschedule the Booked appointment for Doctor
		 *  @Parameters  : 
		 *  @Return      : 
		 */
		
		public String Doctor_Reschedule() throws Exception{
			Actions action = new Actions(driver);
			WebElement wek = driver.findElement(By.xpath(Elements_Recipients.header_myAppointments_Reschedule));
			action.moveToElement(wek).click().build().perform();
			Thread.sleep(2000);
			String modifieddate=Browser.getModifiedDate(1);
			Browser.clickOnTheElementByXpath("(//div//span[(text()='"+modifieddate+"')])[3]");
			Browser.clickOnTheElementByXpath("//span[@class='icon night-icon']");
			Browser.clickOnTheElementByXpath("(//ul[@class='d-flex align-items-center filter-list mt-0 flex-wrap']/li[2])[1]");
			Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_ConfirmAppointment);
			String alert=Browser.getTextByXpath(Elements_Recipients.header_myAppointments_RescheduleMsg);
			Assert.assertEquals(alert, "Appointment rescheduled Successfully!");
			Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_RescheduleMsg_Close);
			return modifieddate;
			
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Cancel the Booked appointment for Doctor
		 *  @Parameters  : 
		 *  @Return      : 
		 */
		
		public void Doctor_Cancel_Appointment() throws Exception{
			Actions action = new Actions(driver);
			WebElement cancel = driver.findElement(By.xpath(Elements_Recipients.header_myAppointments_CancelAppointmentLink));
			action.moveToElement(cancel).click().build().perform();
			Thread.sleep(1000);
			Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_CancelAppointment_Confirm);
			Thread.sleep(1000);
		}
		
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Select the Package and Confirm the appointment for Wellness
		 *  @Parameters  : PackageName
		 *  @Return      : 
		 */
		
		public void Wellness_SelectPackage_Book(String PackageName) throws Exception{
			
			Actions action = new Actions(driver);
			WebElement we = driver.findElement(By.xpath("//div[@class='media-body w-100'][contains(., '"+PackageName+"')]/following-sibling::div/button"));
			action.moveToElement(we).click().build().perform();
			Browser.clickOnTheElementByXpath(Elements_Recipients.Wellness_Package_Calendar_Ok);
			Thread.sleep(1000);
			WebElement confirm= driver.findElement(By.xpath(Elements_Recipients.Wellness_Package_Confirm));
			action.moveToElement(confirm).click().build().perform();
			
			
		}
		
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Reschedule the Booked appointment for Wellness
		 *  @Parameters  : 
		 *  @Return      : Rescheduledate
		 */
		
		public String Wellness_Reschedule() throws Exception{
		
			Actions action = new Actions(driver);
			WebElement Reschedule = driver.findElement(By.xpath(Elements_Recipients.header_myAppointments_Package_Reschedule));
			action.moveToElement(Reschedule).click().build().perform();
			Thread.sleep(2000);
			String TodayDate=Browser.getCurrentDate();
			String MonthLastDate=Browser.GetCurrentMonthLastDate();
			String Rescheduledate=Browser.getModifiedDate(1);
			if(TodayDate.equals(MonthLastDate)){
				
				Browser.clickOnTheElementByXpath("(//span[@class='next'])[7]");
				Browser.clickOnTheElementByXpath("(//span[text()='"+Rescheduledate+"'])[3]");
				
			}else{
				
				Browser.clickOnTheElementByXpath("(//span[text()='"+Rescheduledate+"'])[3]");
				
			}
			
			Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_Package_Reschedule_Confirm);
			return Rescheduledate;
			
			
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Cancel the Booked appointment for Wellness
		 *  @Parameters  : 
		 *  @Return      : 
		 */
		
		public void Wellness_Cancel(){
			
			Actions action = new Actions(driver);
			WebElement Canceled = driver.findElement(By.xpath(Elements_Recipients.header_myAppointments_Package_Cancel_Link));
			action.moveToElement(Canceled).click().build().perform();
			Browser.clickOnTheElementByXpath(Elements_Recipients.header_myAppointments_Package_Cancel_Confirm);
			
		}
		
		
		//New Onlonie Consultation
		
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Select the symptoms answer in loop
		 *  @Parameters  : as required
		 *  @Return      : 
		 */
		public void OC_Symptoms_Quro() throws Exception{
			
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Click_SearchSymptoms);
			Browser.enterTextByXpath(Elements_Recipients.OC_Enter_Symptoms, "Stomachpain");
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Symptoms_Suggession);
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Select_No);
			Thread.sleep(3000);
			while(Integer.parseInt(Browser.getTextByXpath("(//span[contains(., 'Investigating symptoms')])[1]").replaceAll("[^0-9]", "")) != 100){
			System.out.println("Inside while..");
			//int i= driver.findElements(By.xpath("//span[text()='No']")).size();
			//Browser.clickOnTheElementByXpath("(//span[text()='No'])["+i+"]");
			//Browser.clickOnElementByXpath_js("(//span[text()='No'])["+i+"]");
			Browser.clickOnTheElementByXpath("//span[text()='Done']");
			Thread.sleep(3000);
			}
			
			Thread.sleep(2000);	
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Select_No);
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Select_OK);
			
			
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Enter the Vitals Detaisl in PMS Prescription
		 *  @Parameters  : height,weight,bp,temp
		 *  @Return      : 
		 */
		
		public void PMS_Prescription_Vitals(String height,String Weight,String bp,String temp) throws Exception{
			
			Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Vitals_Height, height);
			Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Vitals_Weight, Weight);
			Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Vitals_bp, bp);
			Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Vitals_Temperature, temp);
			Thread.sleep(1000);
			
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Enter the COMPLAINTS in PMS Prescription
		 *  @Parameters  : Complaints
		 *  @Return      : 
		 */
		public void PMS_Prescription_Complaints(String Complaint) throws Exception{
			
			Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Complaints_Click);
			Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Complaints_EnterComplaints, Complaint);
			Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Complaints_Select_Suggession);
			System.out.println("clicked on compaliant suggession");
			Thread.sleep(3000);
			
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Enter the Diseases in PMS Prescription
		 *  @Parameters  : disease
		 *  @Return      : 
		 */
		public void PMS_Prescription_Diseases(String disease) throws Exception{
			
			Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Diseases_Click);
			Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Diseases_EnterDiseases, disease);
			Browser.clickOnTheElementByXpath("//span[@data-select='Press enter to select'][contains(.,'"+disease+"')]");
			System.out.println("clicked on Diseases suggession");
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Enter the notess in PMS Prescription
		 *  @Parameters  : notes
		 *  @Return      : 
		 */
		
		public void PMS_Prescreption_Notes(String notes) throws Exception{
			Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Notes, notes);
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Enter the  Medication in PMS Prescription
		 *  @Parameters  : medicine, dosage
		 *  @Return      : 
		 */
		
		public void PMS_Prescription_Medication(String medicine,String dosage) throws Exception{
			Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Medication, medicine);
			Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_Medication_duration, dosage);
			Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Medication_TimeofTheDay);
			Browser.selectXpathByText(Elements_Recipients.PMS_Prescription_Medication_ToBeTaken, "before meal");
			Browser.clickOnTheElementByXpath(Elements_Recipients.PMS_Prescription_Medication_ADDDrug);
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Enter the  General Advice in PMS Prescription
		 *  @Parameters  :	Advice
		 *  @Return      : 
		 */
		
		public void PMS_Prescription_GeneralAdvice(String Advice) throws Exception{
			Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_GeneralADvice, Advice);
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Enter the  Review period in PMS Prescription
		 *  @Parameters  : number , period
		 *  @Return      : 
		 */
		
		public void PMS_Prescription_Review(String number,String period) throws Exception{
			Browser.enterTextByXpath(Elements_Recipients.PMS_Prescription_ReviewAfter_Number, number);
			Browser.selectXpathByValue(Elements_Recipients.PMS_Prescription_ReviewAfter_Period, period);
		}
		
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is used to Enter the  Promocode in OC Consultation
		 *  @Parameters  : Promocode
		 *  @Return      : 
		 */
		
		public void OC_Promocode(String Promocode) throws Exception{
			if(Promocode.contains("NO_Promocode")){
				
				Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Checkout_ProceedToPayment);
				
			}else{
				Browser.enterTextByXpath(Elements_Recipients.OC_Checkout_EnterCouponCode, Promocode);
				Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Checkout_EnterCouponCode_Apply);
				Browser.clickOnTheElementByXpath(Elements_Recipients.OC_Checkout_ProceedToPayment);
			}
			
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is OC Guest user Login Who had not registered with zoylo
		 *  @Parameters  : Name , mobileNumber
		 *  @Return      : 
		 */
		
		public void OC_GuestUser_Login(String Name, String mobileNumber) throws Exception{
			
			Browser.enterTextByXpath("//input[@placeholder='Enter Name']", Name);
			Browser.enterTextByXpath("//input[@id='phoneLbl']", mobileNumber);
			Browser.clickOnTheElementByXpath("(//button[contains(., 'Submit')])[1]");
			Thread.sleep(2000);
			String OTP =Browser.getOtp_Mobile("+91"+mobileNumber, Environment);
			Thread.sleep(4000);
			Browser.enterTextByXpath("//input[@placeholder='Enter OTP']", OTP);
			Browser.clickOnTheElementByXpath("//button[@class='btn btn-primary'][contains(., 'Submit')]");
			
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is Used to Get the Details of Guest User who is not registered
		 *  @Parameters  : lastName , email
		 *  @Return      : 
		 */
		
		public void OC_GuestUser_GetDetails(String lastName,String email) throws Exception{
			
			Browser.enterTextByXpath("//input[@id='name']", lastName);//fullname
			Browser.enterTextByXpath("//input[@id='email']", email);//email
			Browser.clickOnTheElementByXpath("//button[contains(., 'Proceed')]");//proceed button
			
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is Used to select  Pre-Assessement answers
		 *  @Parameters  : UserType
		 *  @Return      : 
		 */
		
		public void OC_GuestUser_PreAssessment_Questions(String UserType) throws Exception{
			
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_LetGetStarted);
			Browser.clickOnTheElementByXpath("//span[contains(., 'Click here to start chat')]");
			Thread.sleep(2000);
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_PreAssessment_OKUnderstood);
			Thread.sleep(2000);
			if(UserType.contains("MySelf")){
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_PreAssessment_MySelf);
			Thread.sleep(2000);
			}else{
				Browser.clickOnTheElementByXpath(Elements_Recipients.OC_PreAssessment_Others);
				Thread.sleep(2000);
			}
			
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_PreAssessment_Male);
			Thread.sleep(2000);
			Browser.enterTextByXpath(Elements_Recipients.OC_PreAssessment_Age, "35");
			Thread.sleep(2000);
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_PreAssessment_Age_Done);
			Thread.sleep(2000);
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_PreAssessment_NeverSmoking);
			Thread.sleep(2000);
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_PreAssessment_NO);
			Thread.sleep(2000);
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_PreAssessment_NO);
		}
		
		
		public void OC_LoggedUser_PresAssessment_Questions(String UserType) throws Exception{
			
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_LetGetStarted);
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_StartNewAssessment);
			Thread.sleep(2000);
			if(UserType.contains("MySelf")){
			Browser.clickOnTheElementByXpath(Elements_Recipients.OC_MySelf);
			Thread.sleep(2000);
			}else{
				
				Browser.clickOnTheElementByXpath(Elements_Recipients.OC_PreAssessment_Others);
				Thread.sleep(2000);
				Browser.clickOnTheElementByXpath(Elements_Recipients.OC_PreAssessment_Male);
				Thread.sleep(2000);
				Browser.enterTextByXpath(Elements_Recipients.OC_PreAssessment_Age, "35");
				Thread.sleep(2000);
				Browser.clickOnTheElementByXpath(Elements_Recipients.OC_PreAssessment_Age_Done);
				Thread.sleep(2000);
				Browser.clickOnTheElementByXpath(Elements_Recipients.OC_PreAssessment_NeverSmoking);
				Thread.sleep(2000);
				Browser.clickOnTheElementByXpath(Elements_Recipients.OC_PreAssessment_NO);
				Thread.sleep(2000);
				Browser.clickOnTheElementByXpath(Elements_Recipients.OC_PreAssessment_NO);
				
			}
			
		}
		
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is Used Get The Pdf URL
		 *  @Parameters  : AppointmentID
		 *  @Return      : 
		 */
		
		
		public String OC_GetPDF_URL(String AppointmentID) throws Exception{
			
			String AuthorizationToken=Browser.getLoginToken("uat", "+91" + adminuser_id, adminuser_pw);
			String Url="https://uat-gateway.zoylo.com/zoylogateway-0.0.1-SNAPSHOT/zoyloehealth/zoyloehealth-0.0.1-SNAPSHOT/api/zoylodoctorconsultation/refresh/url/"+AppointmentID;
			RequestSpecification res = RestAssured.given();
			res.header("Content-Type","application/json");
			res.header("Authorization",AuthorizationToken);
			Response resp = res.put(Url);
			JsonPath jsonPathEvaluator = resp.jsonPath();
			String GetPDFPath=jsonPathEvaluator.getString("data");
			System.out.println("PDF Path :"+GetPDFPath);
			
			return GetPDFPath;
			
		}
		
		/*  
		 *  @Author      : Ch.LakshmiKanth
		 *  @Description : This method is Used to clickon Clever Tap Notification
		 *  @Parameters  : 
		 *  @Return      : 
		 */
		
		public void Click_CleverTap_Notification(){
			
			if(driver.findElements(By.xpath("//button[@class='No thanks']")).size() > 0) {
				Browser.clickOnTheElementByXpath("//button[@class='No thanks']");
			}
		}
		
		
		
		//Generic Rest Assured API Method
				/*  
				 *  @Author      : Ch.LakshmiKanth
				 *  @Description : This method is used to post Request API
				 *  @Parameters  : as required
				 *  @Return      : String
				 */
				public Response API_ResponseCode(String contenttype,String AuthorizationToken,String APIBODY,String PostURL){
					
					RequestSpecification request = RestAssured.given();
					
					request.header("Content-Type", contenttype);
					request.header("Authorization",AuthorizationToken);
					request.body(APIBODY);
					Response response = request.post(PostURL);
					//String responeone= response.body().asString();
					
					return response;
				}
				
				
			
				
				
					
				
				
		//Responsive View Methods for Doctors
				/*  
				 *  @Author      : Ch.LakshmiKanth
				 *  @Description : This method is used For Generic Search in Responsive view
				 *  @Parameters  :	Location, Module, data
				 *  @Return      : 
				 */		
		public void Responsive_GenericSearch(String Location,String Module, String data) throws Exception{
			
			Browser.clickOnTheElementByXpath("(//input[@id='location-detect'])[2]");
			Browser.enterTextByXpath("(//input[@id='location-detect'])[1]",Location);
			Browser.clickOnTheElementByXpath("(//span[@class='pac-matched'])[1]");
			Browser.clickOnTheElementByXpath("//a[@class='secondary-color text-uppercase font14 font-medium']//span[@class='icon go-arrow-back'] ");
			Thread.sleep(2000);
			Browser.clickOnTheElementByXpath(Elements_Recipients.GenericSearch_Click_Module);
			Browser.clickOnTheElementByXpath("//a[@class='dropdown-item'][contains(text(),'"+Module+"')]");
			
			if(data=="" || data==null)
			{
				System.out.println("No data provided");
			}
			else{
				for(int i=0;i<=data.length()-1; i++)
			    {
			    	char key = data.charAt(i);
			    	WebDriverWait wait = (new WebDriverWait(driver, 60));
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class,'autocomplete-wrapper')]/input")));
					Thread.sleep(500);
					driver.findElement(By.xpath("//div[contains(@class,'autocomplete-wrapper')]/input")).sendKeys(Character.toString(key));
					System.out.println("Texted = "+data);
					Reporter.log("Text Entered="+data);
			    	Thread.sleep(600);
			    	
			    }
			Browser.clickOnTheElementByXpath(Elements_Recipients.GenericSearch_ModuleData_Suggession);
			}
			Browser.clickOnTheElementByXpath(Elements_Recipients.GenericSearch_Button);
				
		}
		
		
		/*  
		 *  @Author      : Sagar Sen
		 *  @Description : This method clicks on diagnostic icon on home page
		 *  @Parameters  : 
		 *  @Return      : 
		 */	
		public void homePage_DiagnosticIcon_Click() {
			Browser.clickOnTheElementByXpath(Elements_Recipients.homePage_DiagnosticIcon);
			Browser.waitFortheElementXpath(Elements_Recipients.dcHomePage_pathologyHeading);
		}
		
		/*  
		 *  @Author      : Sagar Sen
		 *  @Description : This method clicks on specified popular package and return its price
		 *  @Parameters  : packageName
		 *  @Return      : 
		 */	
	public String diagnostic_homePage_popularPackageClick(String packageName) throws Exception {
		String package_finalPrice = null;
		// Scroll to specific popular package
		Thread.sleep(2000);
		Browser.scrollbyxpath("//div[@class='card diag-card' and contains(., '" + packageName + "')]");

		package_finalPrice = Browser.getTextByXpath_ParentElementOnly(
				"//div[@class='card diag-card' and contains(., '" + packageName + "')]/a/div[2]/div/p[2]").replaceAll("₹", "").replaceAll("\\s","");

		Browser.clickOnTheElementByXpath(
				"//div[@class='card-body d-flex flex-column align-items-start justify-content-between' and contains(., '"
						+ packageName + "')]/following-sibling::div/a[contains(., 'BOOK NOW')]");

		return package_finalPrice;
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : This method verifies service detail page and clicks on book appointment
	 *  @Parameters  : verifyData (verifies tests, gender aplicability etc), packageName, finalPrice
	 *  @Return      : 
	 */	
	public String diagnostic_serviceDetailPage(boolean verifyData, String packageName, String finalPrice) {
		String rfinalPrice = null;
		Browser.waitFortheElementXpath("//h1[text()='" + packageName + " ']");
		if(finalPrice.isEmpty() == true) {
			rfinalPrice = Browser.getTextByXpath("//div[contains(., '" + packageName + "')]/following-sibling::div/h4")
					.replaceAll("₹", "").replaceAll("\\s", "");
		} else {
			Browser.waitFortheElementXpath("//h4[text()='₹" + finalPrice + "']");
		}
		
		if (verifyData == true) {

		}
		Browser.clickOnTheElementByXpath("//div[@class='sticky-top' and contains(., '" + packageName + "')]/a");
		return rfinalPrice;
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : Selects address based on provided location i.e city name
	 *  @Parameters  : String location_city, String aptDate, String aptDay
	 *  @Return      : timeSlot
	 */	
	public String diagnostic_checkOut_selectAddress_Schedule(String location_city, String aptDate, String aptDay) {
		String aptTime;
		Browser.waitFortheElementXpath(Elements_Recipients.dcAddressLabel); // addressLable
		Browser.clickOnTheElementByXpath("//p[contains(., '" + location_city + "')]/preceding-sibling::div/label"); // locationRadioBtn
//		Browser.clickOnTheElementByXpath("//div[@class='row']/div[contains(., '" + location_city
//				+ "')]/following-sibling::div/button[text()='Schedule']"); // scheduleBtn
		Browser.waitFortheElementXpath(Elements_Recipients.dcScheduleLabel); // schedulePopUpLable
//		Browser.clickOnTheElementByXpath("//a[contains(., '" + aptDate + "') and contains(., '" + aptDay + "')]"); // dayDate
		Browser.clickOnTheElementByXpath(Elements_Recipients.dcFirstActiveSlot); // firstTimeSlot
		aptTime = Browser.getTextByXpath(Elements_Recipients.dcFirstActiveSlot);
		Browser.clickOnTheElementByXpath(Elements_Recipients.dcScheduleOkBtn); // OkBtn
		return aptTime;
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : Add address based on provided location i.e city name
	 *  @Parameters  : address1, address2, locality, state, city, pincode
	 *  @Return      : timeSlot
	 */	
	public String diagnostic_checkOut_addAddress_Schedule(String address1, String address2, String locality,
			String state, String city, String pincode) throws Exception {
		String aptTime = null;
		Browser.waitFortheElementXpath(Elements_Recipients.dcAddressLabel); // addressLable
		Browser.clickOnTheElementByXpath(Elements_Recipients.dcAddAddressBtn); // addAddressBtn
		Browser.waitFortheElementXpath(Elements_Recipients.dcAddressPopUpHeader); // addAddressPopUp
		Browser.selectXpathByText(Elements_Recipients.dcAddressType, "Primary Address"); // AddressType
		Browser.enterTextByID(Elements_Recipients.dcAddressLine1, address1); // addressLine1
		Browser.enterTextByID(Elements_Recipients.dcAddressLine2, address2); // addressLine2
		Browser.enterTextByID(Elements_Recipients.dcAddressLocality, locality); // locality
		Browser.selectXpathByText(Elements_Recipients.myprofile_Address_Country, "India"); // country
		Browser.selectXpathByText(Elements_Recipients.myprofile_Address_State, state); // state
		Thread.sleep(1000);
		Browser.selectXpathByText(Elements_Recipients.myprofile_Address_city, city); // city
		Browser.enterTextByID(Elements_Recipients.dcAddressPin, pincode);
		Browser.clickOnTheElementByXpath("//button[text()='Add']"); // addBtn
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath("//div[@class='row']/div[contains(., '" + city
				+ "')]/following-sibling::div/button[text()='Schedule']"); // scheduleBtn
		Browser.waitFortheElementXpath("//h5[text()='Schedule']"); // schedulePopUpLable
//		Browser.clickOnTheElementByXpath("//a[contains(., '" + aptDate + "') and contains(., '" + aptDay + "')]"); // dayDate
		Browser.clickOnTheElementByXpath(Elements_Recipients.dcFirstActiveSlot); // firstTimeSlot
		aptTime = Browser.getTextByXpath(Elements_Recipients.dcFirstActiveSlot);
		Browser.clickOnTheElementByXpath(Elements_Recipients.dcScheduleOkBtn); // OkBtn
		return aptTime;
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : Book for self or others
	 *  @Parameters  : bookingFor (Self or Others)
	 *  @Return      : 
	 */	
	public void diagnostic_checkout_bookingFor(String bookingFor, String otherName, String otherNum, String otherGender) throws Exception {
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath("//label[text()='" + bookingFor + "']");
		if (bookingFor.equalsIgnoreCase("Others")) {
			Browser.enterTextByXpath(Elements_Recipients.bookingDetails_Others_Name, otherName); // otherName
			Browser.enterTextByXpath(Elements_Recipients.bookingDetails_Others_Contact, otherNum); // otherNum
			Browser.selectIDbyText(Elements_Recipients.dcOthersGender, otherGender); //
			Browser.enterTextByXpath(Elements_Recipients.bookingDetails_Others_Age, "30");
		} else {
			Browser.enterTextByXpath(Elements_Recipients.bookingDetails_selfAge, "30");
			Browser.selectIDbyText(Elements_Recipients.myProfile_Editgender, "Male");
		}
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : Apply available coupons
	 *  @Parameters  : couponCode
	 *  @Return      : 
	 */	
	public void selectAvailable_Coupon(String couponCode) {
		Browser.clickOnTheElementByXpath("//a[text()='Select From Available Coupons']"); // selectPopup
		Browser.waitFortheElementXpath("//h5[text()='Available Coupons']"); // popUpHeader
		Browser.clickOnTheElementByXpath("//span[@class='coupon-code' and text()='" + couponCode + "']"); // selectCoupon
		Browser.clickOnTheElementByXpath("//button[text()='Apply Coupon']"); // ApplyCouponPopUp
		Browser.waitFortheElementXpath("//a[text()='Remove']"); // applyConfirm
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : Verify order summary
	 *  @Parameters  : applyPromo (Apply OR ""), packageName, pkgPrice, discValue_per
	 *  @Return      : 
	 */	
	public int diagnostic_checkout_orderSummary(String applyPromo, String packageName, int pkgPrice, int discValue_per) {
		int grandTotal;
		Browser.waitFortheElementXpath("//h3[text()='ORDER SUMMARY']");
		Browser.waitFortheElementXpath(
				"//div[@class='card-body order-summary']/div[@class='row'][1]/div/h5[text()='" + packageName + "']");
		Browser.waitFortheElementXpath(
				"//div[@class='card-body order-summary']/div[@class='row'][1]/div/h4[text()='₹ " + pkgPrice + "']"); // packagePrice
		Browser.waitFortheElementXpath(
				"//div[@class='card-body order-summary']/div[@class='row'][2]/div/h4[text()='₹ " + pkgPrice + "']"); // subTotalPrice
		if(applyPromo.equalsIgnoreCase("Apply")) {
			grandTotal = pkgPrice - (pkgPrice * discValue_per) / 100;
			Browser.waitFortheElementXpath(
					"//div[@class='card-body order-summary']/div[@class='row'][4]/div/h4[text()='₹ " + grandTotal + "']"); // grandTotalPrice
		} else {
			grandTotal = pkgPrice;
			Browser.waitFortheElementXpath(
					"//div[@class='card-body order-summary']/div[@class='row'][3]/div/h4[text()='₹ " + grandTotal + "']"); // grandTotalPrice
		}
		return grandTotal;
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : select paymentmode and click on place order
	 *  @Parameters  : paymentMode (online OR cod)
	 *  @Return      : 
	 */	
	public void diagnostic_checkout_selectPaymentMode_placeOrder(String paymentMode) {
		Browser.clickOnTheElementByXpath("//label[@for='" + paymentMode + "']"); // selectPayMode
		Browser.clickOnTheElementByXpath("//button/span[text()='Place Order']"); // placeOrderBtn
		if(paymentMode.equalsIgnoreCase("online")) {
			Browser.waitFortheElementXpath("//a[contains(., 'NET BANKING')]");
		} else {
			System.out.println("Paymode is cod check success page.");
		}
	}
	
	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to verify the amount on PG
	 * 
	 * @Parameters : transaction amount
	 * 
	 * @Return :
	 */
	public void verify_transactionAmount_PG(Double trnAmt) {
		Long y = Math.round(trnAmt);
//		Double trnAmt_Co = y.doubleValue();
//		String trnAmt_S = Browser.decimalFormat(trnAmt_Co);
		Browser.waitFortheElementXpath("//h4[text()='CHOOSE A PAYMENT METHOD']"); // heading
		Browser.waitFortheElementXpath("//p[text()='Amount to be paid ']/span[text()='₹ " + y + ".00']");
		System.out.println("Verified amount in PG");
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : This method verifies service detail page and clicks on book appointment
	 *  @Parameters  : packageName, finalPrice
	 *  @Return      : bookingID
	 */	
	public String diagnostic_bookingSuccessPage(String packageName, String timeslot) {
		String bookingID = null;
		Browser.waitFortheElementXpath("//span[contains(., 'BOOKING CONFIRMED')]"); // confirmHeader
//		Browser.waitFortheElementXpath("//p[text()='Package Name']/following-sibling::p[text()='" + packageName + "']"); // pkgName
		Browser.waitFortheElementXpath("//div/p[text()='" + timeslot + "']"); // aptTime
		bookingID = Browser.getTextByXpath_ParentElementOnly("//p[text()='YOUR BOOKING ID IS']/following-sibling::h5"); // bookingID

		return bookingID;

	}
	
}