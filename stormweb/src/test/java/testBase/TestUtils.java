package testBase;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.SubjectTerm;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import objectRepository.Elements_Recipients;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import io.codearte.jfairy.Fairy;
import io.codearte.jfairy.producer.company.Company;
import io.codearte.jfairy.producer.person.Person;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class TestUtils extends LoadProp{

	//FirefoxDriver browser = new FirefoxDriver();

	public final WebDriver driver;
	
	public TestUtils(WebDriver driver) {
		this.driver=driver;
		
	}
	
	/*	@Author: Sagar Sen
	 * 	@Description: This method returns the os code is running on
	 * 	@Parms: NA
	 * 	@Return: OS name
	 */
	public String getOSname() {
		String os = System.getProperty("os.name").toLowerCase();
		return os;
	}
	
	/*	@Author: Sagar Sen
	 * 	@Description: This method can be used to extract the image source using xpath
	 * 	@Parms: xpath
	 * 	@Return: src path
	 */
	public String getImageSrc(String xpath)
	{
		WebElement element = driver.findElement(By.xpath(xpath));
		String src = ((JavascriptExecutor)driver).executeScript("return arguments[0].attributes['src'].value;", element).toString();
		System.out.println("The image source extracted is: "+src);
		return src;
	}
	
	/*	@Author: Sagar Sen
	 * 	@Description: This method can be used to extract current date only
	 * 	@Parms: NA
	 * 	@Return: current date, example "1"
	 */
	public String getCurrentDate()
	{
		Date currentDate=new Date();
		DateFormat sdf = new SimpleDateFormat("d"); 
		String date=sdf.format(currentDate);
		System.out.println("Current Date is:"+date);
		return date;
	}
	
	/*	@Author: Sagar Sen
	 * 	@Description: This method can be used to extract modified date only
	 * 	@Parms: Value can be negative or positive
	 * 	@Return: Modified date, example "current date +/- value"
	 */
	public String getModifiedDate(int value)
	{
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, value);
		String modDate=new SimpleDateFormat("d").format(cal.getTime());
		System.out.println("Modified Dtae is:"+modDate);
		return modDate;
	}
	
	/*	@Author: Sagar Sen
	 * 	@Description: This method can be used to extract date in required format
	 * 	@Parms: dateFormate example yyyy-MM-dd, Modified date true or false, value example "current date +/- value"
	 * 	@Return: 
	 */
	public String getDate_format(String dateFormat, Boolean modifyDate, int Value){
		Date date = new Date();
		String DateFormat= new SimpleDateFormat(dateFormat).format(date);
		if(modifyDate=true){
			// convert date to calendar
	        Calendar cal = Calendar.getInstance();
	        cal.setTime(date);
	        // manipulate date
	        cal.add(Calendar.DATE, Value);
	        Date currentDatePlusOne = cal.getTime();
	        DateFormat modDate=new SimpleDateFormat(dateFormat);
	        String outPut = modDate.format(currentDatePlusOne);
	        return outPut;
		}else{
			return DateFormat;
		}
	}
	
	/*	@Author: Sagar Sen
	 * 	@Description: This method can be used to fetch first 3 char of current day
	 * 	@Parms:
	 * 	@Return: first 3 char of current day
	 */
	public String getCurrentDay(){
		SimpleDateFormat sdf = new SimpleDateFormat("EEE");
		Date d = new Date();
		String dayOfTheWeek = sdf.format(d);
		System.out.println("Day is : "+dayOfTheWeek);
		return dayOfTheWeek;
	}
	
	/*	@Author: Sagar Sen
	 * 	@Description: This method can be used to fetch first 3 char of current day
	 * 	@Parms:
	 * 	@Return: first 3 char of current day
	 */
	public String getCurrentDay_format_modifier(String format, int value){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, value);
		String modDay=new SimpleDateFormat(format, Locale.ENGLISH).format(cal.getTime());
		System.out.println("Modified Day is : "+modDay);
		return modDay;
		
	}
	
	/*	@Author: Sagar Sen
	 * 	@Description: This method can be used to fetch first 3 char of Modified day
	 * 	@Parms: Value can be negative or positive
	 * 	@Return: first 3 char of modified day
	 */
	public String getModDay(int Value){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, Value);
		String modDay=new SimpleDateFormat("EEE", Locale.ENGLISH).format(cal.getTime());
		System.out.println("Modified Day is : "+modDay);
		return modDay;
	}
	
	/*	@Author: Sagar Sen
	 * 	@Description: This method is used to modify the widith and height of the web page
	 * 	@Parms: int width, int height
	 * 	@Return: NA
	 */
	public void setScreen_dimentions(int width, int height) throws Exception {
		Dimension dm = new Dimension(width,height);
		Thread.sleep(2000);
        driver.manage().window().setSize(dm);
        Thread.sleep(2000);
	}
	
	/*	@Author:Ch.LakshmiKanth
	 * 	@Description: This method can be used to fetch the current month last date (i.e 30 or 31)
	 * 	@Parms:
	 * 	@Return:CurrentmonthlastDate :- it gives last date of the Current month
	 */
	
	public String GetCurrentMonthLastDate(){
		
		SimpleDateFormat dfs = new SimpleDateFormat("d");  
	    Calendar calendar = Calendar.getInstance();
	    int lastDate = calendar.getActualMaximum(Calendar.DATE);
	    calendar.set(Calendar.DATE, lastDate);
	    int lastDay = calendar.get(Calendar.DAY_OF_WEEK);
	    String CurrentmonthlastDate=dfs.format(calendar.getTime());
	    System.out.println("Last Date: " +CurrentmonthlastDate);
	    return CurrentmonthlastDate;
	    
	   
	}

	/*   @Autur : Ganesh Mandala
	 *   @Description: Sharing geolocation in zoylo map
	 *   @Recent Changes : 
	 *   @Comments : This method works only in Chrome
	 */

	public void shareGeoLocation() throws InterruptedException{
		driver.navigate().to("chrome://settings/content");
		Thread.sleep(2000);
		driver.switchTo().frame("settings"); 
		driver.findElement(By.xpath("//input[@name='location' and @value='allow']")).click();
		driver.findElement(By.xpath("//*[@id='content-settings-overlay-confirm']")).click();

	}


	//links
	public void clickLink(String link){
		driver.findElement(By.linkText(link)).click();
	}
	//button
	public void clickButtonWithName(String name){
		driver.findElement(By.name(name)).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 
	}
	//Open Browser
	public void openUrl(String name) throws InterruptedException{
		
			driver.get(name);

		System.out.println("Opened URL="+name);
		Reporter.log("Opened URL="+name);
		
		if(name.equalsIgnoreCase(EnvironmentURL)) {
			if(driver.findElements(By.xpath("//button[@class='No thanks']")).size() > 0) {
				clickOnTheElementByXpath("//button[@class='No thanks']");
			}
		}
		
		
		Thread.sleep(2000);
	}



	//Scroll by ID
	public void scrollbyID(String ID) throws InterruptedException
	{
		WebElement scroll = driver.findElement(By.id(ID));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll);
		Thread.sleep(1000);
		Reporter.log("scrolled to "+ID);
		
	}

	//Scroll by xpath
	public void scrollbyxpath(String xpath) throws InterruptedException
	{
		WebElement scroll = driver.findElement(By.xpath(xpath));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scroll);
		Thread.sleep(1000);
		System.out.println("scrolled to "+xpath);
		Reporter.log("scrolled to "+xpath);
	}

	//Scroll by name
	public void scrollbyName(String name)
	{
		WebElement scrollname = driver
				.findElement(By.name(name));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", scrollname);
		Reporter.log("scrolled to "+name);
	}

	
	/*   @Author : Sagar Sen
	 *   @Description: Clicks on element using js executor
	 *   @ Params: String xpath
	 *   @ Returns: NA
	 */
	public void clickOnElementByXpath_js(String xpath) {
		WebElement element = driver.findElement(By.xpath(xpath));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	}
	
	//horizontal scroll
	public void horizontalScroll()
	{
		JavascriptExecutor jse = (JavascriptExecutor) driver;     
		jse.executeScript("document.querySelector('table th:last-child').scrollIntoView();");
		Reporter.log("scrolled Horizontally");
	}
	
	// scroll UP
		public void ScrollUp()
		{
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,-500)", "");
			Reporter.log("scrolled UP");
		}
		
		// scroll Down
				public void ScrollDown()
				{
					
					JavascriptExecutor jse = (JavascriptExecutor)driver;
					jse.executeScript("window.scrollBy(0,500)", "");
					
					Reporter.log("scrolled Down");
					
				}

	//screen-shot
	public void capturescreenshot(String screenname) throws IOException{
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("screenshots\\"+screenname+".png"));

	}

	//TIME increment and send keys by Name
	public void dateTimeIncrement(int value, String name)
	{
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter= new SimpleDateFormat("MM/dd/YYYY HH:mm");
		currentDate.add(Calendar.MINUTE, value);
		String date = formatter.format(currentDate.getTime());
		driver.findElement(By.name(name)).sendKeys(date);
		//System.out.println(date);
	}

	//YEAR increment and send keys by Name
	public void yearIncrement(int value, String name)
	{
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter= new SimpleDateFormat("MM/dd/YYYY HH:mm");
		currentDate.add(Calendar.YEAR, value);
		String date = formatter.format(currentDate.getTime());
		driver.findElement(By.name(name)).sendKeys(date);
		//System.out.println(date);
	}

	//Wait for the ID
	public void waitFortheID(String ID){
		System.out.println("waiting for "+ID);
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(ID)));
		System.out.println("waited for "+ID);
	}

	//Wait for the Xpath Element
	public void waitFortheElementXpath(String xpath){
		System.out.println("waiting for "+xpath);
		WebDriverWait wait = (new WebDriverWait(driver, 100));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		System.out.println("waited for "+xpath);	 
	}
	
	//Wait for the css Element
		public void waitFortheElementCss(String css){
			System.out.println("waiting for "+css);
			WebDriverWait wait = (new WebDriverWait(driver, 100));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(css)));
			System.out.println("waited for "+css);	 
		}
	
	//Wait for the name Element
	public void waitforElementName(String name)
	{
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(name)));
	}
	
	
	/*	@Author: Sagar Sen
	 * 	@Description: This method can be used for decimal place rounding
	 * 	@Parms: double value and places in int
	 * 	@Return: first 3 char of modified day
	 */
	public String decimalFormat(Double value) {
		double roundOff = Math.round(value * 100.0) / 100.0;
		DecimalFormat df = new DecimalFormat("##,###.00");
		df.setGroupingUsed(true);
		df.setGroupingSize(3);
		String format = df.format(roundOff);
		return format;
	}

	/*	@Author: Sagar Sen
	 * 	@Description: This method can be used for fluent wait by xpath
	 * 	@Parms: xpath, long timeout and poolingIntervel
	 * 	@Return: first 3 char of modified day
	 */
	/*public void fluentWaitByXpath(String xpath, long timeout, long poolingIntervel){
		System.out.println("Fluent waiting invocked");
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(timeout, TimeUnit.SECONDS).pollingEvery(poolingIntervel, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
		WebElement f = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				System.out.println("Finding element with pooling intervel: "+ poolingIntervel);
				return driver.findElement(By.xpath(xpath));
			}
		});
	}
*/
	
	//Wait for text to be present by xpath
	public void waitforTextbyxpath(String xpath, String value)
	{
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		//wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(xpath), value));
		wait.until(ExpectedConditions.textToBePresentInElement(By.xpath(xpath), value));

	}

	//Wait for text to be present by ID
	public void waitforTextbyID(String ID, String value)
	{
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		//wait.until(ExpectedConditions.textToBePresentInElementValue(By.id(ID), value));
		wait.until(ExpectedConditions.textToBePresentInElement(By.id(ID), value));
	}

	//Actions by xpath
	public void actionbyXpath(String path, String value) throws InterruptedException
	{
		Actions qua = new Actions(driver);
		qua.moveToElement(driver.findElement(By.xpath(
				path)));
		qua.click();
		qua.sendKeys(value);
		Thread.sleep(2000);
		qua.sendKeys(Keys.ENTER);
		qua.build().perform();
	}

	//Actions by name
	public void actionbyname(String name, String value)
	{
		Actions qua = new Actions(driver);
		qua.moveToElement(driver.findElement(By.name(
				name)));
		qua.click();
		qua.sendKeys(value);
		qua.sendKeys(Keys.ENTER);
		qua.build().perform();
	}


	//Actions by id	
	public void actionbyid(String id, String value) throws Exception
	{
		Actions qua = new Actions(driver);
		qua.moveToElement(driver.findElement(By.id(id)));
		qua.click();
		//Thread.sleep(4000);
		qua.sendKeys(value);
		//Thread.sleep(2000);
		qua.sendKeys(Keys.ENTER);
		qua.build().perform();
	}

	//close second tab
	public void closeSecondTab()
	{
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		driver.close();
		driver.switchTo().window(tabs2.get(0));
	}

	//Click on the element ID
	public void clickOnTheElementByID(String ID)
	{
		System.out.println("waiting  for "+ID);
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(ID)));
		driver.findElement(By.id(ID)).click();
		System.out.println("Clicked on "+ID);
		Reporter.log("Clicked on the Element="+ID);
	}

	//Get text of the element by Xpath
	public String getTextByXpath(String xpath)
	{
		System.out.println("waiting  for "+xpath);
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		String text =driver.findElement(By.xpath(xpath)).getText();
		System.out.println("Text = "+text);
		return text;
	}
	
	/*	@Author: Sagar Sen
	 * 	@Description: return text of only parent element ignores child elements
	 * 	@Parms: xpath
	 * 	@Return: text
	 */
	public String getTextByXpath_ParentElementOnly(String xpath) {
		String s = null;
		System.out.println("waiting  for "+xpath);
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement myDiv = driver.findElement(By.xpath(xpath));
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		s = (String) jse.executeScript("return arguments[0].childNodes[0].nodeValue;", myDiv);
		System.out.println("Returning text : " + s);
		
		return s;
	}

	//Get text of the element by ID
	public String getTextByID(String ID)
	{
		System.out.println("waiting  for "+ID);
		WebDriverWait wait = (new WebDriverWait(driver, 90));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(ID)));
		String text =driver.findElement(By.id(ID)).getText();
		System.out.println("Text = "+text);
		return text;
	}

	//Click on the element Xpath
	public void clickOnTheElementByXpath(String Xpath)
	{
		System.out.println("Waiting for "+Xpath);
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Xpath)));
		driver.findElement(By.xpath(Xpath)).click();
		System.out.println("Clicked on "+Xpath);
		Reporter.log("Clicked on the Element="+Xpath);
	}

	// Enter text by id
	public void enterTextByID(String ID,String data) throws InterruptedException
	{
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(ID)));
		Thread.sleep(500);
		driver.findElement(By.id(ID)).clear();
		Thread.sleep(500);
		driver.findElement(By.id(ID)).sendKeys(data);
		System.out.println("Texted = "+data);
		Reporter.log("Text Entered="+data);
	}
	
	// Enter text by Name
		public void enterTextByName(String Name,String data) throws InterruptedException
		{
			WebDriverWait wait = (new WebDriverWait(driver, 60));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(Name)));
			Thread.sleep(500);
			driver.findElement(By.name(Name)).clear();
			Thread.sleep(500);
			driver.findElement(By.name(Name)).sendKeys(data);
			System.out.println("Texted = "+data);
			Reporter.log("Text Entered="+data);
		}

	//Enter text by  Xpath
	public void enterTextByXpath(String Xpath,String data) throws InterruptedException
	{
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Xpath)));
		Thread.sleep(1000);
		driver.findElement(By.xpath(Xpath)).clear();
		Thread.sleep(500);
		driver.findElement(By.xpath(Xpath)).sendKeys(data);
		System.out.println("Texted = "+data);
		Reporter.log("Text Entered="+data);
	}


	// select text by id
	public void selectIDbyText(String ID,String data)
	{
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(ID)));
		Select drp = new Select(driver.findElement(By.id(ID)));
		drp.selectByVisibleText(data);
		System.out.println("Selected = "+data);
	}
	
	// select value by id
	public void selectIDbyValue(String ID,String data)
	{
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(ID)));
		Select drp = new Select(driver.findElement(By.id(ID)));
		drp.selectByValue(data);
		System.out.println("Selected = "+data);
	}
	
	// select value by name
	public void selectNameByValue(String Name,String data)
	{
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(Name)));
		Select drp = new Select(driver.findElement(By.name(Name)));
		drp.selectByValue(data);
		System.out.println("Selected = "+data);
	}

	// select text by name
	public void selectNameByText(String Name,String data)
	{
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(Name)));
		Select drp = new Select(driver.findElement(By.name(Name)));
		drp.selectByVisibleText(data);
		System.out.println("Selected = "+data);
	}
		
	// select text by xpath
	public void selectXpathByText(String xpath,String data)
	{
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		Select drp = new Select(driver.findElement(By.xpath(xpath)));
		drp.selectByVisibleText(data);
		System.out.println("Selected = "+data);
	}
	
	// select Value by xpath
	public void selectXpathByValue(String xpath,String data)
	{
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		Select drp = new Select(driver.findElement(By.xpath(xpath)));
		drp.selectByValue(data);
		System.out.println("Selected = "+data);
	}

	//Wait Till int
	public void waitTill(int time) {

		driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
	}

	public void CheckNotificationMessage(String ExpectedNotificationMesg ){

		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[@class='zy-status-wrapper']")));
		String ActualNotification= driver.findElement(By.cssSelector("div.zy-status-wrapper")).getText();
		System.out.println("ActualNotificationMessage="+ActualNotification);
		Reporter.log("Verified Notification="+ActualNotification);
		Assert.assertEquals(ActualNotification,ExpectedNotificationMesg);
		driver.findElement(By.xpath("//div[@class='zy-status-wrapper']")).click();

	}
	
	/*
	 * @Author: Sagar Sen
	 * @Desc: Method is used to identify filed level error messages in admin module
	 * @Parm: Message
	 * @return: Null
	 */
	public void filedLevelMessage(String ExpectedNotificationMesg ){

		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//div[@class='input-group__messages']//div")));
		String ActualNotification= driver.findElement(By.xpath("//div[@class='input-group__messages']//div")).getText();
		System.out.println("ActualNotificationMessage="+ActualNotification);
		Reporter.log("Verified Notification="+ActualNotification);
		Assert.assertEquals(ActualNotification,ExpectedNotificationMesg);
	}
	
	/*
	 * @Author: Sagar Sen
	 * @Desc: Wait for notification message
	 * @Parm: text
	 * @return: NA
	 */
		public void zmt_notification(String text) throws Exception {
			WebDriverWait wait = (new WebDriverWait(driver, 60));
			wait.until(ExpectedConditions.textToBePresentInElementLocated(
					By.xpath("//div[@class='alert alert-success alert-dismissable']"), text));
			Thread.sleep(500);
			driver.findElement(By.xpath("//button[@data-dismiss='alert']")).click();
			Thread.sleep(1000);
		}


	/* 
	 * @Description:This function is used to get the appointment id from thank you page
	   @Return type appointment id
	 *
	 */

	public String getAppointmentID(){

		String AppointmentId = driver.findElement(By.xpath("(//div[@class='book-dtbox']/h3)[1]")).getText();
		System.out.println("before split id is "+AppointmentId);
		String APID[]=AppointmentId.split(":");
		String Apid=APID[1].replace(" ", "");
		System.out.println("After split id is="+Apid);
		return Apid;

	}

	/*
	 * @Author: Ganesh
	 * @Desc: This function will return the value of the textbox
	 * @Parm: ID
	 * @return: String Value of the textbox (ID)
	 */
	public String getTextBoxValueByID(String ID){
		
	JavascriptExecutor js = (JavascriptExecutor)driver;
    String inputValue = (String) js.executeScript("return document.getElementById('"+ID+"').value");       
    System.out.println("CURRENT VALUE : " + inputValue);
     
    return inputValue;    
	}

	/* 
	 * This function is used to read the emails from gmail with the subject name
       Ex:Browser.emailResponse(Recipient_DocUsername, Recipient_DocPassword, "Zoylo.com | Appointment registered");
	 *
	 */	
	public  String  emailResponse(String user,String password,String Subject) throws Exception{

		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");

		Session session = Session.getDefaultInstance(props, null);
		Store store = session.getStore("imaps");
		store.connect("imap.gmail.com", user, password);

		Folder folder = store.getFolder("INBOX");
		folder.open(Folder.READ_WRITE);

		System.out.println("Total Message:" + folder.getMessageCount());
		System.out.println("Unread Message:"
				+ folder.getUnreadMessageCount());

		Message[] messages = null;
		boolean isMailFound = false;
		Message mailFromGod= null;
		Thread.sleep(1000);
		//Search for mail from Zoylo
		for (int i = 0; i<=5; i++) {
			Thread.sleep(500);
			messages = folder.search(new SubjectTerm(Subject),folder.getMessages());

			//Wait for 10 seconds

			if (messages.length == 0) {
				System.out.println("mail not found");
				Thread.sleep(10000);
			}

		}

		//Search for unread mail from Zoylo
		//This is to avoid using the mail for which 
		//Registration is already done
		for (Message mail : messages) {
			if (!mail.isSet(Flags.Flag.SEEN)) {
				mailFromGod = mail;
				System.out.println("Message Count is: "
						+ mailFromGod.getMessageNumber());
				isMailFound = true;
			}
		}
		StringBuffer buffer =null;
		//Test fails if no unread mail was found from Zoylo
		if (!isMailFound) {
			// throw new Exception("Could not find new mail from Zoylo :-(");
			System.out.println("Mail Not Found Subject name="+Subject);
			//Read the content of mail and launch registration URL                
		} else {
			String line;
			buffer = new StringBuffer();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(mailFromGod
							.getInputStream()));
			while ((line = reader.readLine()) != null) {
				buffer.append(line);
			}
			System.out.println(buffer);


		}

		String  Email_response=null;

		if(buffer != null){
			Email_response=buffer.toString(); 
		} else {
			System.out.println("no response");
		}
		return Email_response;
	}

	/*	@Author: Ganesh
	 * 	@Description: This method can be used to read xl sheet with table name and sheet name
	 * 	@Parms: xlFilePath,  sheetName,  tableName
	 * 	@Return: Table array
	 */

	public static String[][] getTableArray(String xlFilePath, String sheetName, String tableName) throws Exception{
		String[][] tabArray=null;

		Workbook workbook = Workbook.getWorkbook(new File(xlFilePath));
		Sheet sheet = workbook.getSheet(sheetName); 
		int startRow,startCol, endRow, endCol,ci,cj;
		Cell tableStart=sheet.findCell(tableName);
		startRow=tableStart.getRow();
		startCol=tableStart.getColumn();

		Cell tableEnd= sheet.findCell(tableName, startCol+1,startRow+1, 64000, 64000,  false);                

		endRow=tableEnd.getRow();
		endCol=tableEnd.getColumn();

		System.out.println("startRow="+startRow+", endRow="+endRow+", " +
				"startCol="+startCol+", endCol="+endCol);
		tabArray=new String[endRow-startRow-1][endCol-startCol-1];
		ci=0;

		for (int i=startRow+1;i<endRow;i++,ci++){
			cj=0;
			for (int j=startCol+1;j<endCol;j++,cj++){
				tabArray[ci][cj]=sheet.getCell(j,i).getContents();

			}
		}


		return(tabArray);  
	}

	public String randomalphabets(){
		String alphabet= "abcdefghijklmnopqrstuvwxyz";
		String s = "";
		Random random = new Random();
		int randomLen = 1+random.nextInt(9);
		for (int i = 0; i < randomLen; i++) {
			char c = alphabet.charAt(random.nextInt(26));
			s+=c;

		}
		return s;

	}


	/*	@Author: Ganesh
	 * 	@Description: This method can be used to generate Random String
	 * 	@Parms: length of the return type
	 * 	@Return: RandomString
	 */
	public String generateRandomString(int length){
		return RandomStringUtils.randomAlphabetic(length);
	}

	/*	@Author: Ganesh
	 * 	@Description: This method can be used to generate Random Number
	 * 	@Parms: length of the return type
	 * 	@Return: Random Number
	 */	 
	public String generateRandomNumber(int length){
		return RandomStringUtils.randomNumeric(length);
	}
	/*	@Author: Ganesh
	 * 	@Description: This method can be used to generate Random AlphaNumeric
	 * 	@Parms: length of the return type
	 * 	@Return: Random AlphaNumeric
	 */	 
	public String generateRandomAlphaNumeric(int length){
		return RandomStringUtils.randomAlphanumeric(length);
	}
	/*	@Author: Ganesh
	 * 	@Description: This method can be used to generate random Email
	 * 	@Parms: length of the return type
	 * 	@Return: random Email
	 */	 
	public String generateEmail(int length) {
		String allowedChars="abcdefghijklmnopqrstuvwxyz" +   //alphabets
				"1234567890";
		String email="";
		String temp=RandomStringUtils.random(length,allowedChars);
		email=temp.substring(0,temp.length()-9)+"@automation.org";
		return email;
	}
	/*	@Author: Ganesh
	 * 	@Description: This method verify the string is integer / decimal or not
	 * 	@Parms: String
	 * 	@Return: boolean
	 */	
	public  boolean isInteger(String str) {
		try { 
			Double.parseDouble(str);
		} catch(NumberFormatException e) { 
			return false; 
		} 
		// only got here if we didn't return false
		return true;
	}
	
	/*	@Author: Sagar Sen
	 * 	@Description: This method will convert a string to title case
	 * 	@Parms: String
	 * 	@Return: String
	 */
	public String stringTo_titleCase(String text) {
		StringBuilder converted = new StringBuilder();
		 
	    boolean convertNext = true;
	    for (char ch : text.toCharArray()) {
	        if (Character.isSpaceChar(ch)) {
	            convertNext = true;
	        } else if (convertNext) {
	            ch = Character.toTitleCase(ch);
	            convertNext = false;
	        } else {
	            ch = Character.toLowerCase(ch);
	        }
	        converted.append(ch);
	    }
	    
	    System.out.println("Converted " + text + " to title case : "+ converted.toString());
	    return converted.toString();
	}
	
	
	
	
	/*	@Author: Ganesh
	 * 	@Description: This method verify the the element present or not
	 * 	@Parms: Element by xpath/css/id or any element tag
	 * 	@Return: boolean
	 */	
	public boolean isElementPresent(By by){
		try{
			driver.findElement(by);
			return true;
		}
		catch(NoSuchElementException e){
			return false;
		}
	}

	/*	@Author: Ganesh
	 * 	@Description: This method use to read PDF
	 * 	@Parms: String file path
	 * 	@Return: String
	 */	
	
	public String readPDF(String filepath) throws  IOException{

//		File fileDetails = new File(filepath);	
//		PDDocument document = PDDocument.load(fileDetails);
//		PDFTextStripper pdfStripper = new PDFTextStripper();
//		String text = pdfStripper.getText(document);
//		System.out.println(text);
//		document.close();
//		if(fileDetails.delete()) { 
//			System.out.println(fileDetails.getName() + " is deleted!");
//		} else {
//			System.out.println("Delete operation is failed.");
//		}
		String output;
		java.net.URL url = new java.net.URL(filepath);
		InputStream is = url.openStream();
		BufferedInputStream file = new BufferedInputStream(is);
		PDDocument document = null;
		try {
			document = PDDocument.load(file);
			output = new PDFTextStripper().getText(document);
			System.out.println("PDF data is : " + output);
		} finally {
			if (document != null) {
				document.close();
			}
		}
		return output;
	}
	
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to generate test data
	 * @ Parms: NA
	 * @ Returns: Person test data
	 */
	public Person personData() {
		Fairy f = Fairy.create();
		Person p = f.person();
		return p;
	}
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to generate test data
	 * @ Parms: NA
	 * @ Returns: Company test data
	 */
	public Company companyData() {
		Fairy f = Fairy.create();
		Company c = f.company();
		return c;
	}
	

	// >>>>>>>>>>>>>>>>>>>   MONGO DB	<<<<<<<<<<<<<<<<<<
	
	/* 
	 * This function is used to get the values from MongoDB based on the key and value
			   Ex:Browser.mongoDB_Response(String ServerAddress ,int Port ,String UserName, String Database ,String Password,String QueryKey,String QueryValue);
	 */
	public String mongoDB_Response(String Env, String collection, String QueryKey,String QueryValue) throws UnknownHostException{


		//Selecting the database
		DB db = mongoDB_Connect(Env);

		System.out.println("Connect to database successfully");

		//System.out.println(db.getStats());
		System.out.println(db.getCollectionNames());

		DBCollection coll = db.getCollection(collection);
		System.out.println("Collection mycol selected successfully");

		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put(QueryKey, QueryValue);

		DBCursor cursor = coll.find(searchQuery);

		String response=null;
		while (cursor.hasNext()) { 
			//System.out.println("Inserted Document: "+i); 
			response = cursor.next().toString();
			System.out.println(response); 


		} 
		// Assert.assertTrue(response.contains("ganesh@zoylo.com"));
		System.out.println("Asserted successfully");
		return response;

	}

	/*
	 * @ Description: This method is used to remove the document from DB
	 */
	public void mongoDB_Remove(String Env, String collectionName, String QueryKey,String QueryValue) throws UnknownHostException{

		try{
			DB db = mongoDB_Connect(Env);

			System.out.println("Connected to database successfully");

			//System.out.println(db.getStats());
			System.out.println(db.getCollectionNames());

			DBCollection coll = db.getCollection(collectionName);
			System.out.println(collectionName+" Collection selected successfully");

			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(QueryKey, QueryValue);

			coll.remove(searchQuery);
			System.out.println("Removed " +QueryValue+ " from " +collectionName+ " collection successfully ");

		}catch (Exception e) {
			System.out.println(QueryValue+ " from MongoDB is not removed");
		}

	}

	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to remove document from specific collection(s)
	 * @ Parms: Env, collectionName,  QueryKey,  QueryValue
	 * @ Returns: -
	 */
	public void remove_Document(String Env, String collectionName, String QueryKey, String QueryValue) throws Exception{
		
			mongoDB_Remove(Env, collectionName, QueryKey, QueryValue);
	}

//	public void mongoDB_isWhiteListHonoured(String ServerAddress ,int Port ,String UserName, String Database ,String Password,String collectionName, boolean isWhiteListHonouredValue) throws UnknownHostException{
//
//		//Connecting to the mongoDB instance
//		MongoClient mongoClient = null;
//		MongoCredential mongoCredential = MongoCredential.createScramSha1Credential("zoynpap","zoylo_zqa","apz0yl0_321".toCharArray());
//
//		mongoClient = new MongoClient(new ServerAddress("52.66.101.182", 27219), Arrays.asList(mongoCredential));
//		System.out.println("Connect to server successfully");   
//		//Selecting the database
//		DB db = mongoClient.getDB("zoylo_zqa");
//		System.out.println("Connect to database successfully");   
//
//		DBCollection dbCollection = db.getCollection("applicationProperties");
//
//		BasicDBObject newDocument = new BasicDBObject();
//		newDocument.append("$set", new BasicDBObject().append("propertyValue", isWhiteListHonouredValue));
//		BasicDBObject searchQuery = new BasicDBObject().append("propertyName", "isWhiteListHonoured");
//
//		dbCollection.update(searchQuery, newDocument);
//
//		System.out.println("Updated successfully");
//		mongoClient.close();
//
//	}

	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to get ID of a document
	 * @ Parms: Env, collectionName,  QueryKey,  QueryValue
	 * @ Returns: ID
	 */
	public String mongoDB_getID(String Env ,String collectionName, String QueryKey, String QueryValue) throws UnknownHostException{

		//Connecting to the mongoDB instance
		DB db = mongoDB_Connect(Env);
		
		System.out.println("Connect to database successfully");   

		DBCollection dbCollection = db.getCollection(collectionName);

		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put(QueryKey, QueryValue);
		BasicDBObject searchQuery1 = new BasicDBObject();
		searchQuery1.put("zoyloId", 1);

		DBCursor cursor = dbCollection.find(searchQuery,searchQuery1);

		String response=null;
		while (cursor.hasNext()) { 
			//System.out.println("Inserted Document: "+i); 
			response = cursor.next().get("zoyloId").toString();
			System.out.println(response); 
			System.out.println("Updated successfully");
			
			//System.out.println(resultValue);

		}
		return response;
	}
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to get document ID from specific collection(s)
	 * @ Parms: Env, collectionName,  QueryKey,  QueryValue
	 * @ Returns: document ID
	 */
	public String get_Document_ID(String Env, String collectionName, String QueryKey, String QueryValue) throws Exception{
		
		String id=mongoDB_getID(Env , collectionName, QueryKey, QueryValue);
			
		return id;
	}
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to post using restAssured and get response auth token
	 * @ Parms: Environment, username, password
	 * @ Returns: auth token in response header
	 */
	public String getLoginToken(String Environment, String username, String password) throws Exception{
		String response=null;
		URL url=null;
		if(Environment.contains("qa")){
			url = new URL(qaAdmin_API + "zoylogateway-0.0.1-SNAPSHOT/api/authenticate");
		}else{
			url = new URL(uatAdmin_API + "zoylogateway-0.0.1-SNAPSHOT/api/authenticate");
		}
		String Body="{"
				+ "\"username\":\""+username+"\","
				+ "\"password\":\""+password+"\""
						+ "}";
		Response respcode=RestAssured.given().header("Content-Type", "application/json").body(Body).post(url);
		int coderesponse=respcode.getStatusCode();
		if(coderesponse==200){
			response=respcode.getHeader("Authorization");
		}
		return response;
	}
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to put using restAssured for appointment API and get response code
	 * @ Parms: Environment, token, payload
	 * @ Returns: response code
	 */
	public int appointmentAPI(String Environment, String token, String payload) throws Exception{
		int responsecode = 0;
		URL url=null;
		
		if(Environment.contains("qa")){
			url = new URL(qaAdmin_API + "zoylogateway-0.0.1-SNAPSHOT/zoyloadmin/zoyloadmin-0.0.1-SNAPSHOT/api/doctor-appointments");
		}else{
			url = new URL(uatAdmin_API + "zoylogateway-0.0.1-SNAPSHOT/zoyloadmin/zoyloadmin-0.0.1-SNAPSHOT/api/doctor-appointments");
		}
		Response rex=(Response) RestAssured.given().header("Authorization", token).header("Content-Type", "application/json").body(payload).put(url);
		responsecode=rex.getStatusCode();
		return responsecode;
	}
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to post using restAssured for doctor enrollment API - serviceprovidingresourcelead
	 * @ Parms: Environment, token, payload
	 * @ Returns: response code
	 */
	public int serviceproviderAPI(String Environment, String Body) throws Exception{
		int responseCode=0;
		URL url=null;
		
		if(Environment.contains("qa")){
			url = new URL(qaAdmin_API + "zoylogateway-0.0.1-SNAPSHOT/zoyloadmin/zoyloadmin-0.0.1-SNAPSHOT/api/serviceprovidingresourcelead");
		}else{
			url = new URL(uatAdmin_API + "zoylogateway-0.0.1-SNAPSHOT/zoyloadmin/zoyloadmin-0.0.1-SNAPSHOT/api/serviceprovidingresourcelead");
		
		}
		Response rep=RestAssured.given().header("Content-Type", "application/json").body(Body).get(url);
		responseCode=rep.getStatusCode();
		return responseCode;
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : This method is used to wait until loader is disappered
	 *  @Parameters  : xpath
	 *  @Return      : 
	 */
	public void waitUntil_InvisiblityByXpath(String xpath){
		new WebDriverWait(driver, 5).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath)));
		System.out.println("Waited till invisibility of "+ xpath);
	}

	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to get OTP
	 * @ Parms: value example email ID
	 * @ Returns: otp
	 */ //Modified : Added UAT parameter env details
	public String getOtp(String value, String env) throws Exception
	{		
		String x=null;
		x=mongoDB_Response(env, "zoyloUser", "emailInfo.emailAddress", value);		
		String[] y=x.split("otp\" : ");		
		String[] otp=y[1].split(" ");		
		System.out.println("otp="+otp[0]);
		return otp[0];	
	}
	
	
	/*
	 * @ Author: Ch.LakshmiKanth
	 * @ Description: This method is used to get OTP
	 * @ Parms: value example MobileNumber with +91
	 * @ Returns: otp
	 */ //Modified : Added UAT parameter env details
	public String getOtp_Mobile(String value, String env) throws Exception
	{		
		String x=null;
		x=mongoDB_Response(env, "zoyloUser", "phoneInfo.phoneNumber", value);
		String[] y=x.split("otp\" : ");		
		String[] otp=y[1].split(" ");		
		System.out.println("otp="+otp[0]);
		return otp[0];	
		
	}
	
	
	
	
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to upload photo using ID
	 * @ Parms: ID, imageURL
	 * @ Returns:
	 */
	public void upload_Image_byID(String ID, String imageURL)
	{
		WebElement element = driver.findElement(By.id(ID));
		LocalFileDetector detector = new LocalFileDetector();
		String path = imageURL;
		File f = detector.getLocalFile(path);
		((RemoteWebElement)element).setFileDetector(detector);
		element.sendKeys(f.getAbsolutePath());
	}
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to upload photo using xpath
	 * @ Parms: ID, imageURL
	 * @ Returns:
	 */
	public void upload_Image_byXpath(String xpath, String imageURL)
	{
		WebElement element = driver.findElement(By.xpath(xpath));
		LocalFileDetector detector = new LocalFileDetector();
		String path = imageURL;
		File f = detector.getLocalFile(path);
		((RemoteWebElement)element).setFileDetector(detector);
		element.sendKeys(f.getAbsolutePath());
	}
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to remove readOnly attribute for an xpath
	 * @ Parms: xpath
	 * @ Returns: Null
	 */
	public void removeReadOnly_byXpath(String xpath)
	{
		WebElement data = driver.findElement(By.xpath(xpath));
		((JavascriptExecutor)driver).executeScript("arguments[0].removeAttribute('readonly','readonly')", data);
	}
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to remove specific attribute for an xpath
	 * @ Parms: xpath
	 * @ Returns: Null
	 */
	public void removeAttribute_byXpath(String xpath, String attributeName) {
		WebElement data = driver.findElement(By.xpath(xpath));
		((JavascriptExecutor)driver).executeScript("arguments[0].removeAttribute('"+attributeName+"','"+attributeName+"')", data);
	}
	
	
	public void removeAttributeNameValue_byXpath(String xpath, String attributeName, String attributeValue) {
		WebElement data = driver.findElement(By.xpath(xpath));
		((JavascriptExecutor)driver).executeScript("arguments[0].removeAttribute('"+attributeName+"','"+attributeValue+"')", data);
	}
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to enter test by xpath using js Executer
	 * @ Parms: xpath, value
	 * @ Returns: Null
	 */
	public void enterTextByXpath_jsExecuter(String xpath, String value) {
		System.out.println("Waiting for "+xpath);
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		WebElement ele = driver.findElement(By.xpath(xpath));
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("arguments[0].value='"+value+"';", ele);
		System.out.println("Entered text "+value);
	}
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to enter test by id using js Executer
	 * @ Parms: xpath, value
	 * @ Returns: Null
	 */
	public void enterTextByID_jsExecuter(String id, String value) {
		System.out.println("Waiting for "+id);
		WebDriverWait wait = (new WebDriverWait(driver, 60));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
		WebElement ele = driver.findElement(By.id(id));
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("arguments[0].value='"+value+"';", ele);
		System.out.println("Entered text "+value);
	}
	
	
	public void quit() throws InterruptedException
	{		
		 Thread.sleep(5000);
	     driver.quit();
	     System.out.println("BROWSER QUIT");
	}
	
	
	
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to get ID of a document
	 * @ Parms: Env, collectionName,  QueryKey,  QueryValue, Attribute
	 * @ Returns: Attribute value
	 */
	public String mongoDB_getAttribute(String Env, String collectionName, String QueryKey, String QueryValue,String Attribute) throws UnknownHostException{

		//Connecting to the mongoDB instance
		DB db = mongoDB_Connect(Env);
		System.out.println("Connect to database successfully");   

		DBCollection dbCollection = db.getCollection(collectionName);

		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put(QueryKey, QueryValue);
		BasicDBObject searchQuery1 = new BasicDBObject();
		searchQuery1.put(Attribute, 1);

		DBCursor cursor = dbCollection.find(searchQuery,searchQuery1);

		String response=null;
		while (cursor.hasNext()) { 
			//System.out.println("Inserted Document: "+i); 
			response = cursor.next().get(Attribute).toString();
			System.out.println(response); 
			System.out.println("Updated successfully");
			
			//System.out.println(resultValue);

		}
		return response;
	}
	
	/*
	 * @ Author: Ch.LakshmiKanth
	 * @ Description: This method is used to get document ID from specific collection(s)
	 * @ Parms: Env, collectionName,  QueryKey,  QueryValue,Attribute
	 * @ Returns: document ID
	 */
	public String get_Document_Attribute(String Env, String collectionName, String QueryKey, String QueryValue,String Attribute) throws Exception{
		
		String id=mongoDB_getAttribute(Env, collectionName, QueryKey, QueryValue,Attribute);
		return id;
		
	}

	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to get ID of duplicate document by skip and limit
	 * @ Parms: Env , collectionName,  QueryKey,  QueryValue, Attribute
	 * @ Returns: ID
	 */
	public String mongoDB_getAttribute_bySkipLimit(String Env , String collectionName, String QueryKey, String QueryValue,String Attribute, int skipDocuments, int ofLimit) throws UnknownHostException{

		//Connecting to the mongoDB instance
		DB db = mongoDB_Connect(Env);
		System.out.println("Connect to database successfully");   

		DBCollection dbCollection = db.getCollection(collectionName);

		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put(QueryKey, QueryValue);
		BasicDBObject searchQuery1 = new BasicDBObject();
		searchQuery1.put(Attribute, 1);

		DBCursor cursor = dbCollection.find(searchQuery,searchQuery1).skip(skipDocuments).limit(ofLimit);

		String response=null;
		while (cursor.hasNext()) { 
			//System.out.println("Inserted Document: "+i); 
			response = cursor.next().get(Attribute).toString();
			System.out.println(response); 
			System.out.println("Updated successfully");
			
			//System.out.println(resultValue);

		}
		return response;
	}
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to get ID of duplicate document by skip and limit by environment condition
	 * @ Parms: Env, collectionName,  QueryKey,  QueryValue,Attribute, skipDocuments, ofLimit
	 * @ Returns: ID
	 */
	public String get_Document_Attribute_bySkipLimit(String Env, String collectionName, String QueryKey, String QueryValue,String Attribute, int skipDocuments, int ofLimit) throws Exception{
		
		String id=mongoDB_getAttribute_bySkipLimit(Env, collectionName, QueryKey, QueryValue,Attribute,skipDocuments,ofLimit);
		
		return id;
	}
	
	/*
	 * @ Author: Ch.LakshmiKanth
	 * @ Description: This method is used to get value of Array Attribute
	 * @ Parms: Env, collectionName, QueryKey, QueryValue,ArrayAttribute,Arrayvalueindex
	 * @ Returns: Arrayindexvalue
	 */
	public String mongo_getArrayAttribute(String Env, String collectionName,String QueryKey, String QueryValue,String ArrayAttribute,int Arrayvalueindex){
		
		//Connecting to the mongoDB instance
				
		//Selecting the database
				MongoDatabase database;
				if (Env.equals("qa")) {
					//Connecting to the mongoDB instance
					MongoClientURI uri = new MongoClientURI(
						    "mongodb://"+qa_userName+":"+qa_password+"@"+qa_clusterone+","+qa_clustertwo+","+qa_clusterthree+"/"+qa_dbName+"?ssl=true&replicaSet="+qa_replicaSet+"");
					MongoClient mongoClient = new MongoClient(uri);
					database = mongoClient.getDatabase(qa_dbName);
				} else {
					MongoClientURI uri = new MongoClientURI(
						    "mongodb://"+uat_userName+":"+uat_password+"@"+uat_clusterone+","+uat_clustertwo+","+uat_clusterthree+"/"+uat_dbName+"?ssl=true&replicaSet="+uat_replicaSet+"");
					MongoClient mongoClient = new MongoClient(uri);
					database = mongoClient.getDatabase(uat_dbName);
				}
		
				  //Selecting the collection
				MongoCollection<Document> collection = (MongoCollection<Document>) database.getCollection(collectionName);
				//Setting search query with the required key-value pair
				BasicDBObject searchQuery = new BasicDBObject();
				searchQuery.put(QueryKey, QueryValue);
				//DBCursor with the find query result
				//Fetching the response
				
				 	MongoCursor<Document> cursor= collection.find(searchQuery).iterator();
				 	String Arrayindexvalue=null;
				 	try{
				 	while (cursor.hasNext()) {
					Document doc = (Document) cursor.next().get(ArrayAttribute);
					System.out.println(doc);
					List list = new ArrayList(doc.values());
		            System.out.print("String value :"+list.get(Arrayvalueindex));
		            Arrayindexvalue =list.get(Arrayvalueindex).toString();
					
				 	}//Ends While loop
				
				 	}finally {
				    cursor.close();
				 	}
				 	return	 Arrayindexvalue;	

					} 	
	
	
	/*
	 * @ Author: Ch.LakshmiKanth
	 * @ Description: This method is used to Update value of Array Attribute
	 * @ Parms: ServereAddress,Port,Database, UserName, Password, collectionName, QueryKey, QueryValue,UpdateAttribute,Updatevalue
	 * @ Returns:
	 */
	
	public void mongo_updateAttribute(String Env, String collectionName,String QueryKey, String QueryValue,String UpdateAttribute,String Updatevalue){

		//Selecting the database
		MongoDatabase database;
		if (Env.equals("qa")) {
			//Connecting to the mongoDB instance
			MongoClientURI uri = new MongoClientURI(
				    "mongodb://"+qa_userName+":"+qa_password+"@"+qa_clusterone+","+qa_clustertwo+","+qa_clusterthree+"/"+qa_dbName+"?ssl=true&replicaSet="+qa_replicaSet+"");
			MongoClient mongoClient = new MongoClient(uri);
			database = mongoClient.getDatabase(qa_dbName);
		} else {
			MongoClientURI uri = new MongoClientURI(
				    "mongodb://"+uat_userName+":"+uat_password+"@"+uat_clusterone+","+uat_clustertwo+","+uat_clusterthree+"/"+uat_dbName+"?ssl=true&replicaSet="+uat_replicaSet+"");
			MongoClient mongoClient = new MongoClient(uri);
			database = mongoClient.getDatabase(uat_dbName);
		}
		//Selecting the collection
		MongoCollection<Document> collection = database.getCollection(collectionName);
		System.out.println("Connected to Database");
		Document found = (Document) collection.find(new Document(QueryKey, QueryValue)).first();
		if(found!=null){
		System.out.println("User found");
		Bson updatedvalue = new Document(UpdateAttribute, Updatevalue);
		Bson updateoperation = new Document("$set",updatedvalue);
		collection.updateOne(found, updateoperation);
		System.out.println("User Updated");
		}
	
	}
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to Update boolean attribute
	 * @ Parms: ServereAddress,Port,Database, UserName, Password, collectionName, QueryKey, QueryValue,UpdateAttribute,Updatevalue
	 * @ Returns:
	 */
	
	public void mongo_update_booleanAttribute(String Env, String collectionName,String QueryKey, String QueryValue,String UpdateAttribute,boolean Updatevalue){

		//Selecting the database
		MongoDatabase database;
		if (Env.equals("qa")) {
			//Connecting to the mongoDB instance
			MongoClientURI uri = new MongoClientURI(
				    "mongodb://"+qa_userName+":"+qa_password+"@"+qa_clusterone+","+qa_clustertwo+","+qa_clusterthree+"/"+qa_dbName+"?ssl=true&replicaSet="+qa_replicaSet+"");
			MongoClient mongoClient = new MongoClient(uri);
			database = mongoClient.getDatabase(qa_dbName);
		} else {
			MongoClientURI uri = new MongoClientURI(
				    "mongodb://"+uat_userName+":"+uat_password+"@"+uat_clusterone+","+uat_clustertwo+","+uat_clusterthree+"/"+uat_dbName+"?ssl=true&replicaSet="+uat_replicaSet+"");
			MongoClient mongoClient = new MongoClient(uri);
			database = mongoClient.getDatabase(uat_dbName);
		}
		//Selecting the collection
		MongoCollection<Document> collection = database.getCollection(collectionName);
		System.out.println("Connected to Database");
		Document found = (Document) collection.find(new Document(QueryKey, QueryValue)).first();
		if(found!=null){
		System.out.println("User found");
		Bson updatedvalue = new Document(UpdateAttribute, Updatevalue);
		Bson updateoperation = new Document("$set",updatedvalue);
		collection.updateOne(found, updateoperation);
		System.out.println("User Updated");
		}
	
	}
	
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to Update double attribute
	 * @ Parms: ServereAddress,Port,Database, UserName, Password, collectionName, QueryKey, QueryValue,UpdateAttribute,Updatevalue
	 * @ Returns:
	 */
	
	public void mongo_update_doubleAttribute(String Env, String collectionName,String QueryKey, String QueryValue,String UpdateAttribute,double Updatevalue){

		//Selecting the database
		MongoDatabase database;
		if (Env.equals("qa")) {
			//Connecting to the mongoDB instance
			MongoClientURI uri = new MongoClientURI(
				    "mongodb://"+qa_userName+":"+qa_password+"@"+qa_clusterone+","+qa_clustertwo+","+qa_clusterthree+"/"+qa_dbName+"?ssl=true&replicaSet="+qa_replicaSet+"");
			MongoClient mongoClient = new MongoClient(uri);
			database = mongoClient.getDatabase(qa_dbName);
		} else {
			MongoClientURI uri = new MongoClientURI(
				    "mongodb://"+uat_userName+":"+uat_password+"@"+uat_clusterone+","+uat_clustertwo+","+uat_clusterthree+"/"+uat_dbName+"?ssl=true&replicaSet="+uat_replicaSet+"");
			MongoClient mongoClient = new MongoClient(uri);
			database = mongoClient.getDatabase(uat_dbName);
		}
		//Selecting the collection
		MongoCollection<Document> collection = database.getCollection(collectionName);
		System.out.println("Connected to Database");
		Document found = (Document) collection.find(new Document(QueryKey, QueryValue)).first();
		if(found!=null){
		System.out.println("User found");
		Bson updatedvalue = new Document(UpdateAttribute, Updatevalue);
		Bson updateoperation = new Document("$set",updatedvalue);
		collection.updateOne(found, updateoperation);
		System.out.println("User Updated");
		}
	
	}
	
	
	/*
	 * @ Author: Sagar
	 * @ Description: This method is used to Update value of Array Attribute specific to environment
	 * @ Parms: ServereAddress,Port,Database, UserName, Password, collectionName, QueryKey, QueryValue,UpdateAttribute,Updatevalue
	 * @ Returns:
	 */
	public void update_MongoAttribute(String Env, String collectionName, String QueryKey, String QueryValue,String updateAttribute, String updateValue) throws Exception{
		
			mongo_updateAttribute(Env, collectionName, QueryKey, QueryValue, updateAttribute, updateValue);
			
	}
	
	/*
	 * @ Author: Sagar
	 * @ Description: This method is used to Update value of boolean attribute specific to environment
	 * @ Parms: ServereAddress,Port,Database, UserName, Password, collectionName, QueryKey, QueryValue,UpdateAttribute,Updatevalue
	 * @ Returns:
	 */
	public void update_Boolean_MongoAttribute(String Env, String collectionName, String QueryKey, String QueryValue,String updateAttribute, boolean updateValue) throws Exception{
		
		mongo_update_booleanAttribute(Env, collectionName, QueryKey, QueryValue, updateAttribute, updateValue);
			
	}
	
	/*
	 * @ Author: Sagar
	 * @ Description: This method is used to Update value of double attribute specific to environment
	 * @ Parms: ServereAddress,Port,Database, UserName, Password, collectionName, QueryKey, QueryValue,UpdateAttribute,Updatevalue
	 * @ Returns:
	 */
	public void update_double_MongoAttribute(String Env, String collectionName, String QueryKey, String QueryValue,String updateAttribute, double updateValue) throws Exception{
		
		mongo_update_doubleAttribute(Env, collectionName, QueryKey, QueryValue, updateAttribute, updateValue);
			
	}
	
	
	/*
	 * @ Author: Sagar
	 * @ Description: This method is used to Update value of int attribute specific to environment
	 * @ Parms: ServereAddress,Port,Database, UserName, Password, collectionName, QueryKey, QueryValue,UpdateAttribute,Updatevalue
	 * @ Returns:
	 */
	public void mongo_update_intAttribute(String Env, String collectionName,String QueryKey, String QueryValue,String UpdateAttribute,int Updatevalue){

		//Selecting the database
		MongoDatabase database;
		if (Env.equals("qa")) {
			//Connecting to the mongoDB instance
			MongoClientURI uri = new MongoClientURI(
				    "mongodb://"+qa_userName+":"+qa_password+"@"+qa_clusterone+","+qa_clustertwo+","+qa_clusterthree+"/"+qa_dbName+"?ssl=true&replicaSet="+qa_replicaSet+"");
			MongoClient mongoClient = new MongoClient(uri);
			database = mongoClient.getDatabase(qa_dbName);
		} else {
			MongoClientURI uri = new MongoClientURI(
				    "mongodb://"+uat_userName+":"+uat_password+"@"+uat_clusterone+","+uat_clustertwo+","+uat_clusterthree+"/"+uat_dbName+"?ssl=true&replicaSet="+uat_replicaSet+"");
			MongoClient mongoClient = new MongoClient(uri);
			database = mongoClient.getDatabase(uat_dbName);
		}
		//Selecting the collection
		MongoCollection<Document> collection = database.getCollection(collectionName);
		System.out.println("Connected to Database");
		Document found = (Document) collection.find(new Document(QueryKey, QueryValue)).first();
		if(found!=null){
		System.out.println("Document found");
		Bson updatedvalue = new Document(UpdateAttribute, Updatevalue);
		Bson updateoperation = new Document("$set",updatedvalue);
		collection.updateOne(found, updateoperation);
		System.out.println("Document Updated");
		}
	
	}
	
	
	/*
	 * @ Author: Ch.LakshmiKanth
	 * @ Description: This method is used to get value of Array Attribute
	 * @ Parms: ServereAddress,Port,Database, UserName, Password, collectionName, QueryKey, QueryValue,ArrayAttribute,Arrayvalueindex
	 * @ Returns: Arrayindexvalue
	 */
	public String Mongo_GetArrayAttribute_Value(String Env,String collectionName,String QueryKey, String QueryValue,String ArrayAttribute,int Arrayvalueindex){
	
	String arrayvalue= mongo_getArrayAttribute(Env ,collectionName,QueryKey, QueryValue, ArrayAttribute, Arrayvalueindex);
	return arrayvalue;
	
	}
	
	/*
	 * @ Author: Sagar
	 * @ Description: This method is used to connect to mongo DB based on env
	 * @ Parms: ServereAddress,Port,Database, UserName, Password
	 * @ Returns: DB connection
	 */
	public DB mongoDB_Connect(String Env) {
		DB db;
		if (Env.equals("qa")) {
			MongoClientURI uri = new MongoClientURI(
				    "mongodb://"+qa_userName+":"+qa_password+"@"+qa_clusterone+","+qa_clustertwo+","+qa_clusterthree+"/"+qa_dbName+"?ssl=true&replicaSet="+qa_replicaSet+"");
			MongoClient mongoClient = new MongoClient(uri);
			db = mongoClient.getDB(qa_dbName);
			System.out.println("Connected to QA DB.");
			return db;
		} else {
			MongoClientURI uri = new MongoClientURI(
				    "mongodb://"+uat_userName+":"+uat_password+"@"+uat_clusterone+","+uat_clustertwo+","+uat_clusterthree+"/"+uat_dbName+"?ssl=true&replicaSet="+uat_replicaSet+"");
			MongoClient mongoClient = new MongoClient(uri);
			db = mongoClient.getDB(uat_dbName);
			System.out.println("Connected to UAT DB.");
			return db;
		}
	}
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method is used to get count of document based on query search
	 * @ Parms: Env, collectionName,  QueryKey,  QueryValue
	 * @ Returns: Count of documents
	 */
	public int mongoDB_getDocumentCount(String Env, String collectionName, String QueryKey, String QueryValue) throws UnknownHostException{

		MongoDatabase database;
		int response;
		if (Env.equals("qa")) {
			//Connecting to the mongoDB instance
			MongoClientURI uri = new MongoClientURI(
				    "mongodb://"+qa_userName+":"+qa_password+"@"+qa_clusterone+","+qa_clustertwo+","+qa_clusterthree+"/"+qa_dbName+"?ssl=true&replicaSet="+qa_replicaSet+"");
			MongoClient mongoClient = new MongoClient(uri);
			database = mongoClient.getDatabase(qa_dbName);
		} else {
			MongoClientURI uri = new MongoClientURI(
				    "mongodb://"+uat_userName+":"+uat_password+"@"+uat_clusterone+","+uat_clustertwo+","+uat_clusterthree+"/"+uat_dbName+"?ssl=true&replicaSet="+uat_replicaSet+"");
			MongoClient mongoClient = new MongoClient(uri);
			database = mongoClient.getDatabase(uat_dbName);
		}
		System.out.println("Connect to database successfully");   

		MongoCollection<Document> collection = database.getCollection(collectionName);
		long found = collection.count(new Document(QueryKey, QueryValue));
		response = (int) found;
		return response;
	}
	
	// SQL DB
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method will execute where query in specified table
	 * @ Parms: tableName, whereQuery, valueOf
	 * @ Returns: attribute value
	 */
	public String sqlDB_selectFromQuery(String tableName, String whereQuery, String valueOf) throws Exception {
		String res = null;
		Connection con = DriverManager.getConnection(sql_dbURL, sql_userName, sql_userPw);
		System.out.println("Connected to sql DB...");
		Statement stm = con.createStatement();
		System.out.println("Created statement object..");
		ResultSet result = stm.executeQuery("select * from "+tableName+" where "+whereQuery+";");
		while (result.next()) {
		    res = result.getString(valueOf);
		}
		con.close();
		return res;
	}
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method will delete a row form table
	 * @ Parms: tableName, whereQuery
	 * @ Returns: 
	 */
	public void sqlDB_deleteQuery(String tableName, String whereQuery) throws Exception {
		Connection con = DriverManager.getConnection(sql_dbURL, sql_userName, sql_userPw);
		System.out.println("Connected to sql DB...");
		Statement stm = con.createStatement();
		System.out.println("Created statement object..");
		stm.executeUpdate("delete from "+tableName+" where "+whereQuery+";");
		con.close();
		System.out.println("Deleted "+whereQuery+" form table " +tableName);
	}
	
	/*
	 * @ Author: Sagar Sen
	 * @ Description: This method will update a field form table
	 * @ Parms: tableName, whereQuery, updateQuery
	 * @ Returns: 
	 */
	public void sqlDB_updateFieldQuery(String tableName, String whereQuery, String updateQuery) throws Exception {
		Connection con = DriverManager.getConnection(sql_dbURL, sql_userName, sql_userPw);
		System.out.println("Connected to sql DB...");
		Statement stm = con.createStatement();
		System.out.println("Created statement object..");
		stm.executeUpdate("UPDATE " + tableName + " SET " + updateQuery + " WHERE " + whereQuery + ";");
		con.close();
		System.out.println("Updated "+updateQuery+" form table " +tableName + "for " + whereQuery);
	}
	
	
}