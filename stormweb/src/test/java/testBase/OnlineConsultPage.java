package testBase;
import objectRepository.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

public class OnlineConsultPage  {
	//FirefoxDriver Mobile = new FirefoxDriver();
	public   WebDriver driver;
	public TestUtils Browser;
	

	public OnlineConsultPage(WebDriver driver) throws Exception {
		this.driver=driver;

		Elements_OnlineConsult.Users_PageProperties();
		Browser= new TestUtils(driver);  
	}
	
	/*  
	 *  @Author      : Ganesh kumar.M
	 *  @Description : This method is used to login into Med app
	 *  @Parameters  : Username, Password ..
	 *  @Return      : 
	 */

	public void customerLogin(String email, String password) throws InterruptedException{

		Browser.enterTextByID(Elements_OnlineConsult.SignIn_Email, email);	
		System.out.println("Entered Email");
		Browser.enterTextByID(Elements_OnlineConsult.SignIn_Password, password);
		System.out.println("Entered password");
		Browser.clickOnTheElementByID(Elements_OnlineConsult.SignIn_Login);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
		
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : This method is used to click consult online on home page
	 *  @Parameters  : NA
	 *  @Return      : NA
	 */
	public void homePage_consultOnline_click(){
		Browser.clickOnTheElementByXpath(Elements_OnlineConsult.homePage_consultOnline);
		Browser.waitFortheElementXpath(Elements_OnlineConsult.onlineListPage_banner);
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : This method is used to select doctor and click on mode of online consultation on list page
	 *  @Parameters  : DoctorName, Consultation type - Chat or Video
	 *  @Return      : NA
	 */
	public void onlineListPage_selectDoctor_ConsultType_Click(String doctorName, String consultationType) throws Exception{
		
		while(driver.findElements(By.xpath("//div[@class='left-center-location' and contains(., '"+doctorName+"')]")).size()==0)
		{
			System.out.println("exists in while");
			Browser.scrollbyxpath("(//img[@alt='Fb'])[1]");
			if(driver.findElements(By.xpath("//div[@class='left-center-location' and contains(., '"+doctorName+"')]")).size()!=0){
				System.out.println("Scrolling to specific doc.");
				Browser.scrollbyxpath("//div[@class='left-center-location' and contains(., '"+doctorName+"')]");
			}
		}
		
		System.out.println("out of while");
		Browser.clickOnTheElementByXpath("//div[@class='left-center-location' and contains(., '"+doctorName+"')]/following-sibling::div/div[@class='doctor-booking-detail']/a[contains(., '"+consultationType+"')]");
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : This method is used to select doctor and click on mode of online consultation on profile page
	 *  @Parameters  : DoctorName, Consultation type - Chat or Video
	 *  @Return      : NA
	 */
	public void onlineProfilePage_selectDoctor_ConsultType_Click(String doctorName, String consultationType) throws Exception{
		Browser.scrollbyxpath("//div[@class='doctor-online-container' and contains(., '"+doctorName+"')]");
		Browser.clickOnTheElementByXpath("//div[@class='left-center-location']/div/div[@class='profile-detail' and contains(., '"+doctorName+"')]/preceding-sibling::div[@class='user-image-section mobile-hide']/div[@class='user-image']/a/span");
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath("//div[@class='doctor-booking-detail']/a[contains(., '"+consultationType+"')]");
	}	
}