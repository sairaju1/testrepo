package testBase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import objectRepository.Elements_DocPMS;

public class DoctorPMSPage {
	public WebDriver driver;
	public TestUtils Browser;
	
	public DoctorPMSPage(WebDriver driver){
		this.driver=driver;
		Elements_DocPMS.DocPMS_PageProperties();
		Browser= new TestUtils(driver);
	}
	
	//PMS METHODS
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : This method is used to check choose clinic pop up and select a clinic
	 *  @Parameters  : clinic name
	 *  @Return      : 
	 */
	public void chooseClinicPopUp_Check(String clinicName) throws Exception{
		if(driver.findElements(By.xpath(Elements_DocPMS.chooseClinic_PopUp)).size()!=0){
			System.out.println("Selecting Online and "+clinicName);
			Browser.clickOnTheElementByXpath("//label[@for='online' and contains(., 'Online')]"); //Selecting online
			Browser.clickOnTheElementByXpath("//label[@for='doctor' and contains(., '"+clinicName+"')]");
			Browser.clickOnTheElementByXpath(Elements_DocPMS.chooseClinic_PopUp_SelectBtn);
		}else{
			System.out.println("No Clinics to choose.");
		}
		Browser.waitFortheElementXpath(Elements_DocPMS.menu_dashboard);
		Thread.sleep(1000);
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : This method is used to select month view of calendar and select available appointment
	 *  @Parameters  :
	 *  @Return      : 
	 */
	public void calendar_SelectMonth_chooseApt() throws Exception{
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.dashboard_calendar_monthBtn);
		Browser.waitFortheElementXpath(Elements_DocPMS.dashboard_calendar_DayHeader);
		String Date=Browser.getCurrentDate();
		int currentDate=Integer.parseInt(Date);
		if(currentDate>23){
			Browser.ScrollDown();
		}
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.dashboard_calendar_appointmentCard);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.dashboard_calendar_aptPopUp);
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : This method is used to click start consultation btn in apt details pop up
	 *  @Parameters  :
	 *  @Return      : 
	 */
	public void aptDetails_StartConsultationBtn_Click(){
		Browser.waitFortheElementXpath(Elements_DocPMS.pms_aptDetailsPopUp_header);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.pms_aptDetailsPopUp_startConsultationBtn);
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : This method is used to click view prescription btn in apt details pop up
	 *  @Parameters  :
	 *  @Return      : 
	 */
	public void aptDetails_ViewPrescriptionBtn_Click(){
		Browser.waitFortheElementXpath(Elements_DocPMS.pms_aptDetailsPopUp_header);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.pms_aptDetailsPopUp_viewPrescription);
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : This method is used to generate the prescription of an appointment
	 *  @Parameters  : clinic name
	 *  @Return      : 
	 */
	public void ePrescription_generation(String temperature) throws Exception{
		Browser.waitFortheID(Elements_DocPMS.prescription_heading);
		Browser.scrollbyxpath(Elements_DocPMS.prescription_saveBtn);
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : This method is used to add appointment from PMS dahboard
	 *  @Parameters  : DocName, timeslot, Phone Number
	 *  @Return      : 
	 */
	public void dashboard_AddAppointment(String docName, String timeslot, String phoneNumber) throws Exception{
		Browser.clickOnTheElementByXpath(Elements_DocPMS.dashboard_addAppointment);
		Browser.waitFortheElementXpath("(//h1[contains(., 'ADD NEW APPOINTMENT')])[1]");
		Browser.selectNameByText(Elements_DocPMS.addAppointment_ChooseDoctor, docName);
		Browser.selectNameByText(Elements_DocPMS.addAppointment_timeSlot, timeslot);
		Browser.selectNameByText(Elements_DocPMS.addAppointment_consultationType, "General");
		Browser.clickOnTheElementByXpath(Elements_DocPMS.addAppointment_NextBtn);
		Browser.waitFortheElementXpath("//li/span[contains(., '"+docName+"')]");
		Browser.scrollbyName(Elements_DocPMS.addAppointment_patientMobile);
		Browser.enterTextByName(Elements_DocPMS.addAppointment_patientMobile, phoneNumber);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.addAppointment_patientAddress);
		Browser.scrollbyxpath(Elements_DocPMS.addAppointment_confirm);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.addAppointment_confirm);
		Browser.waitFortheElementXpath(Elements_DocPMS.addAppointment_successMessage);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.addAppointment_successMessageOKbtn);
		Browser.scrollbyxpath(Elements_DocPMS.dashboard_addAppointment);
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : This method is used to generate prescription with data
	 *  @Parameters  : vital_temprature, vital_pulse, vital_respiration, vital_bp, vital_spo2, vital_height, vital_weight
	 *  @Return      : 
	 */
	public void generatePrescription(String vital_temprature, String vital_pulse, String vital_respiration, String vital_bp, 
			String vital_spo2, String vital_height, String vital_weight, String complaint_description, String complaint_since,
			String complaint_problemSince, String complaint_severity, String diseases_discription, String disease_nature,
			String investigation, String medicine_name, String medicine_Brand, String medicine_dosage, String medicine_frequency,
			String medicine_duration, String medicine_durationType, String notes) throws Exception{
		//Vitals
		Browser.waitFortheID(Elements_DocPMS.prescription_heading);
		Browser.enterTextByID(Elements_DocPMS.prescription_vital_temperature, vital_temprature);
		Browser.enterTextByID(Elements_DocPMS.prescription_vital_pulse, vital_pulse);
		Browser.enterTextByID(Elements_DocPMS.prescription_vital_respiration, vital_respiration);
		Browser.enterTextByID(Elements_DocPMS.prescription_vital_bloodpressure, vital_bp);
		Browser.enterTextByID(Elements_DocPMS.prescription_vital_sp, vital_spo2);
		Browser.enterTextByID(Elements_DocPMS.prescription_vital_height, vital_height);
		Browser.enterTextByID(Elements_DocPMS.prescription_vital_weight, vital_weight);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.prescription_saveBtn); //Vitals Save
		Browser.waitFortheElementXpath(Elements_DocPMS.prescription_vitals_saveNotification); //Vitals Saved
		//Complaints
		Browser.enterTextByID(Elements_DocPMS.prescription_complaints_complaint_diseases, complaint_description);
		Browser.selectIDbyText(Elements_DocPMS.prescription_complaints_sinceValue, complaint_since);
		Browser.selectIDbyText(Elements_DocPMS.prescription_complaints_sinceCode, complaint_problemSince);
		Browser.selectIDbyText(Elements_DocPMS.prescription_complaints_severity, complaint_severity);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.prescription_saveBtn); //Complaint Save
		Browser.waitFortheElementXpath(Elements_DocPMS.prescription_complaints_saveNotification); //Complaint Saved
		//Diseases
		Browser.enterTextByID(Elements_DocPMS.prescription_complaints_complaint_diseases, diseases_discription);
		Browser.selectIDbyText(Elements_DocPMS.prescription_diseases_nature, disease_nature);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.prescription_saveBtn); //Diseases Save
		Browser.waitFortheElementXpath(Elements_DocPMS.prescription_diseases_saveNotification); //Diseases saved
		//Investigation
		Browser.enterTextByID(Elements_DocPMS.prescription_investigation_text, investigation);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.prescription_investigation_saveBtn); //Investigation Save
		Browser.waitFortheElementXpath(Elements_DocPMS.prescription_investigation_saveNotification); //Investigation Saved
		//Medication
		Browser.enterTextByXpath(Elements_DocPMS.prescription_medication_formulationName, medicine_name);
		Browser.enterTextByXpath(Elements_DocPMS.prescription_medication_brandName, medicine_Brand);
		Browser.enterTextByXpath(Elements_DocPMS.prescription_medication_dosage, medicine_dosage);
		Browser.selectXpathByText(Elements_DocPMS.prescription_medication_frequency, medicine_frequency);
		Browser.enterTextByXpath(Elements_DocPMS.prescription_medication_dosageDuration, medicine_duration);
		Browser.selectXpathByText(Elements_DocPMS.prescription_medication_durationType, medicine_durationType);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.prescription_saveBtn); //Medication Save
		Browser.waitFortheElementXpath(Elements_DocPMS.prescription_medication_saveNotification); //Medication Saved
		//Notes
		Browser.enterTextByID(Elements_DocPMS.prescription_notesText, notes);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.prescription_saveBtn); //Notes Save
		Browser.waitFortheElementXpath(Elements_DocPMS.prescription_notes_saveNotification); //Notes saved
		Thread.sleep(5000);
		Browser.scrollbyxpath(Elements_DocPMS.prescription_endBtn);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.prescription_endBtn);
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : This method is used to generate prescription with data
	 *  @Parameters  : menuName - Dashboard, Online Consultation etc,.
	 *  @Return      : 
	 */
	public void Dashboard_menuClick(String menuName){
		Browser.clickOnTheElementByXpath("//li/a/span[contains(., '"+menuName+"')]");
	}
	
	/*  
	 *  @Author      : Sagar Sen
	 *  @Description : This method is used to logout from PMS
	 *  @Parameters  : 
	 *  @Return      : 
	 */
	public void pms_loguot(){
		Browser.clickOnTheElementByXpath(Elements_DocPMS.dashboard_userIcon);
		Browser.clickOnTheElementByXpath(Elements_DocPMS.dashboard_userIcon_logoutBtn);
	}
}
