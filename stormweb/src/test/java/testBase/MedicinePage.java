package testBase;

import objectRepository.*;

import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import io.codearte.jfairy.producer.person.Address;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class MedicinePage {
	// FirefoxDriver Mobile = new FirefoxDriver();
	public WebDriver driver;
	public TestUtils Browser;

	public MedicinePage(WebDriver driver) throws Exception {
		this.driver = driver;

		Elements_Medicines.Medicine_PageProperties();
		Browser = new TestUtils(driver);
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will wait until medicine page is loaded completely
	 * 
	 * @ Parms:
	 * 
	 * @ Returns:
	 */
	public void medicinePage_loadWait() throws InterruptedException {
		Browser.waitFortheElementXpath(Elements_Medicines.homePage_uploadPrescriptionBtn); // uploadPresIcon
		if (driver.findElements(By.xpath(Elements_Medicines.pushNotification_popUp_NoBtn)).size() > 0) {
			Browser.clickOnTheElementByXpath(Elements_Medicines.pushNotification_popUp_NoBtn);
		}
		Thread.sleep(5000);
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will click on required top menu
	 * 
	 * @ Parms: Medicine, Helathcare Products, Surgical and Devices, Ayurveda,
	 * Homeopathy, Knowledge Base, Offers
	 * 
	 * @ Returns:
	 */
	public void medicineTopMenu_click(String menuName, String submenuName) throws Exception {
		if (submenuName.length() > 1) {
			Actions action = new Actions(driver);
			WebElement we = driver.findElement(By.xpath("//span[text()='" + menuName + "']"));
			Thread.sleep(2000);
			action.moveToElement(we).moveToElement(driver.findElement(By.xpath(
					"//li[contains(., 'Healthcare Products')]/div/div/div/ul[@id='submenu-menu']/li/ul/li/a[@title='"
							+ submenuName + "']/span")))
					.click().build().perform();
			Browser.waitFortheElementXpath("//ul[@class='items']/li[4]/strong[text()='" + submenuName + "']");
		} else {
			Browser.clickOnTheElementByXpath("//span[text()='" + menuName + "']");
		}
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will click on required top menu in responsive
	 * window
	 * 
	 * @ Parms: String menuName, String submenuName, String childmenuName
	 * 
	 * @ Returns:
	 */
	public void responsive_medicineTopMenu_click(String menuName, String submenuName, String childmenuName)
			throws Exception {
		Browser.clickOnTheElementByXpath("//span[contains(text(),'Shop By Category')]");
		if (submenuName.length() > 1) {
			Browser.clickOnTheElementByXpath("//span[text()='" + menuName + "']");
			Browser.waitFortheElementXpath("//span[contains(text(),'" + menuName + "')]");
			if (childmenuName.length() > 1) {
				Browser.clickOnTheElementByXpath("(//li[@class='ui-menu-item level1 parent expand' and contains(., '"
						+ submenuName + "')]/div)[1]");
				Browser.clickOnTheElementByXpath(
						"//li[@class='ui-menu-item level2 ' and contains(., '" + childmenuName + "')]");
			} else {
				Browser.clickOnTheElementByXpath(
						"(//li[@class='ui-menu-item level1 parent expand' and contains(., '" + submenuName + "')])[1]");
			}
		} else {
			Browser.clickOnTheElementByXpath("//span[text()='" + menuName + "']");
		}
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will return product price and click on add to cart
	 * in medicine plp page
	 * 
	 * @ Parms: product name
	 * 
	 * @ Returns: price in double
	 */
	public Double medicine_PLPpage_getProductPrice_addToCartBtn_click(String productName) throws Exception {
		Double PLPprice = Double
				.parseDouble(Browser
						.getTextByXpath("//div[@class='product details product-item-details' and contains(., '"
								+ productName + "')]/div[@data-role='priceBox']")
						.replaceAll("₹", "").replaceAll(",", ""));
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath("//div[@class='product details product-item-details' and contains(., '"
				+ productName + "')]/div/div/div/form/button");
		Thread.sleep(2000);

//		while (driver
//				.findElements(By
//						.xpath("//div[@class='page messages' and contains(.,'You added " + productName + " to your')]"))
//				.size() == 0) {
//			if (driver
//					.findElements(By.xpath(
//							"//div[@class='page messages' and contains(.,'You added " + productName + " to your')]"))
//					.size() == 0) {
//				driver.navigate().refresh();
//				Browser.waitFortheElementXpath(
//						"//div[@class='page messages' and contains(.,'You added " + productName + " to your')]");
//			} else {
//				continue;
//			}
//		}
		return PLPprice;
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will return product final price in cart and set
	 * product quantity in cart page
	 * 
	 * @ Parms: product name, int quantity , boolean modify qty or not and PLPprice
	 * from medicine_PLPpage_getProductPrice_addToCartBtn_click method
	 * 
	 * @ Returns: productSubTotal in double
	 */
	public Double checkout_cart_itemQtyInfo(String productName, Double PLPprice, int qty) throws Exception {
		Double productSubTotal_item = null;
		String productSubTotal_itemString;
//		String PLPpriceString = Browser.decimalFormat(PLPprice);
		Browser.waitFortheElementXpath(Elements_Medicines.cart_prescriptionHeader);
//		Browser.waitFortheElementXpath("//td[@data-th='Item' and contains(., '" + productName
//				+ "')]/following-sibling::td[@class='col subtotal']/span/span/span[text()='₹" + PLPpriceString + "']");
		Double cartItemPrice = PLPprice;
		if (qty > 1) {
			// Qty Increase and calculation
			Browser.enterTextByXpath("//td[@data-th='Item' and contains(., '" + productName
					+ "')]/following-sibling::td[@class='col qty']/div/div/input", Integer.toString(qty));
			Thread.sleep(15000);
//			Browser.waitUntil_InvisiblityByXpath("//div[@class='loader']/img[@alt='Loading...']");
			productSubTotal_item = cartItemPrice * qty;
			productSubTotal_itemString = Browser.decimalFormat(productSubTotal_item);
			Browser.waitFortheElementXpath("//td[@data-th='Item' and contains(., '" + productName
					+ "')]/following-sibling::td[@class='col subtotal']/span/span/span[text()='₹"
					+ productSubTotal_itemString + "']");
		} else {
			productSubTotal_item = cartItemPrice;
			productSubTotal_itemString = Browser.decimalFormat(productSubTotal_item);
			Browser.waitFortheElementXpath("//td[@data-th='Item' and contains(., '" + productName
					+ "')]/following-sibling::td[@class='col subtotal']/span/span/span[text()='₹"
					+ productSubTotal_itemString + "']");
		}
		return productSubTotal_item;
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will enter coupon code if required and check
	 * summary
	 * 
	 * @ Parms: isCoupon (Apply OR ""), couponName and productSubTotal_item from
	 * cartPage_itemQtyInfo method
	 * 
	 * @ Returns: Double orderTotal
	 */
	public Double checkout_coupon_summary(String isCoupon, String couponName, Double cartSubTotal) throws Exception {
		Double discountAmt, discountFinalPrice, orderTotal_checkout, shippingAmt;
		String productSubTotalString, orderTotalString;
		productSubTotalString = Browser.decimalFormat(cartSubTotal);
		Browser.waitFortheElementXpath("//tr[@class='totals sub']/td/span[text()='₹" + productSubTotalString + "']");
		Thread.sleep(3000);
		if (cartSubTotal < 500.00) {
			try {
				Browser.waitFortheElementXpath(Elements_Medicines.checkout_shippingAmtWait);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		shippingAmt = Double
				.parseDouble(Browser.getTextByXpath(Elements_Medicines.checkout_shippingAmt).replaceAll("₹", ""));
		if (isCoupon.equalsIgnoreCase("Apply")) {
			Browser.enterTextByXpath(Elements_Medicines.checkout_couponCode, couponName);
			Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_couponApplyBtn);
			if (driver.findElements(By.xpath("//div[@data-bind='html: message.text' and contains(., 'The coupon code \""
					+ couponName + "\" is not valid.')]")).size() > 0) {
				System.out.println("Coupon code is not valid");
				orderTotal_checkout = cartSubTotal + shippingAmt;
				orderTotalString = Browser.decimalFormat(orderTotal_checkout);
				Browser.waitFortheElementXpath(
						"//td[@class='amount']/strong/span[@class='price' and text()='₹" + orderTotalString + "']");
			} else {
//				Browser.waitFortheElementXpath(
//						"//div[@data-bind='html: message.text' and contains(., 'You used coupon code \"" + couponName
//								+ "\"')]");
				int discountPer = Integer.parseInt(couponName.replaceAll("[^0-9]", ""));
				discountAmt = (cartSubTotal * discountPer) / 100;

				String discountAmtStr = Browser.decimalFormat(discountAmt);

				discountFinalPrice = cartSubTotal - Double.parseDouble(discountAmtStr);
				Browser.waitFortheElementXpath(
						"//th[contains(., 'Discount')]/following-sibling::td[@data-th='Discount' and contains(., '"
								+ discountAmtStr + "')]");
				orderTotal_checkout = discountFinalPrice + shippingAmt;
				orderTotalString = Browser.decimalFormat(orderTotal_checkout);
				Browser.waitFortheElementXpath(
						"//td[@class='amount']/strong/span[@class='price' and text()='₹" + orderTotalString + "']");
				Browser.scrollbyxpath(
						"//td[@class='amount']/strong/span[@class='price' and text()='₹" + orderTotalString + "']");
			}
		} else {
			orderTotal_checkout = cartSubTotal + shippingAmt;
			orderTotalString = Browser.decimalFormat(orderTotal_checkout);
			Browser.waitFortheElementXpath(
					"//td[@class='amount']/strong/span[@class='price' and text()='₹" + orderTotalString + "']");
		}
		Thread.sleep(10000);
		Browser.scrollbyxpath(Elements_Medicines.checkout_Button);
		Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_Button);
		return orderTotal_checkout;
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will enter coupon code if required and check
	 * summary
	 * 
	 * @ Parms: isCoupon true or false, couponName and productSubTotal_item from
	 * cartPage_itemQtyInfo method
	 * 
	 * @ Returns: Double orderTotal
	 */
	public Double checkout_coupon_summary_newSignUp(Boolean isCoupon, String couponName, Double cartSubTotal)
			throws Exception {
		Double orderTotal_checkout;
		String productSubTotalString, orderTotalString;
		productSubTotalString = Browser.decimalFormat(cartSubTotal);
		Browser.waitFortheElementXpath("//tr[@class='totals sub']/td/span[text()='₹" + productSubTotalString + "']");
		Thread.sleep(3000);
//		Browser.waitFortheElementXpath("//span[@data-th='Shipping' and text()='₹0.00']");
		orderTotal_checkout = cartSubTotal;
		orderTotalString = Browser.decimalFormat(orderTotal_checkout);
		Browser.waitFortheElementXpath(
				"//td[@class='amount']/strong/span[@class='price' and text()='₹" + orderTotalString + "']");
		Thread.sleep(15000);
		Browser.scrollbyxpath(Elements_Medicines.checkout_Button);
		Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_Button);
		return orderTotal_checkout;
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: Will select upload prescription or later
	 * 
	 * @ Parms: boolean uploadPrescription
	 * 
	 * @ Returns:
	 */
	public void checkout_prescriptionPopUp(boolean uploadPrescription) throws Exception {
		Browser.waitFortheElementXpath(Elements_Medicines.homePage_uploadPrescriptionBtn);
		if (uploadPrescription == true) {
			Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_prescriptionPopUp_Ready); // prescriptionReady
			Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_prescriptionPopUp_proceed); // popupproceed
			uploadPrescriptionPage(false, "", "");
		} else {
			Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_prescriptionPopUp_Later); // laterCheckBox
			Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_prescriptionPopUp_proceed); // popupproceed
		}
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: upload prescription functionality
	 * 
	 * @ Parms: boolean newPrescriptionUpload
	 * 
	 * @ Returns:
	 */
	public void uploadPrescriptionPage(boolean newPrescriptionUpload, String filePath, String fileName)
			throws Exception {
		Browser.waitFortheElementXpath(Elements_Medicines.prescription_chooseFile);
		if (newPrescriptionUpload == true) {
//			Thread.sleep(20000);
//			driver.findElement(By.xpath("//input[@name='up']")).sendKeys(filePath);
//			System.out.println(filePath);
//			Browser.clickOnTheElementByXpath("//form[@action='https://dev.zoylo.com/medicines/uploadprescription/index/upload']");
//			Thread.sleep(20000);
		} else {
			Browser.clickOnTheElementByXpath(Elements_Medicines.prescription_First_RecentFile); // firstCheckBox
		}
		Browser.waitUntil_InvisiblityByXpath(Elements_Medicines.prescription_continueDisable); // disabledBtn
		Browser.clickOnTheElementByXpath(Elements_Medicines.prescription_continueBtn); // continueBtn
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: upload prescription specifications
	 * 
	 * @ Parms: boolean specifyMedicines, int numberOfMedicines
	 * 
	 * @ Returns:
	 */
	public void uploadPrescription_specifications(boolean specifyMedicines, int numberOfMedicines) throws Exception {
//		Thread.sleep(5000);
		Browser.waitFortheElementXpath(Elements_Medicines.prescription_selectItemHeader); // itemsHeading
		if (specifyMedicines == true) {
			Browser.clickOnTheElementByXpath(Elements_Medicines.prescription_specifyBtn); // specifyChkBox
			for (int x = 1; x <= numberOfMedicines; x++) {
				Browser.enterTextByXpath("(//input[@name='medicine_name[]'])[" + x + "]",
						Browser.generateRandomString(5).toLowerCase()); // MedName
				Browser.enterTextByXpath("(//input[@name='medicine_qty[]'])[" + x + "]", "1"); // QTY
				Browser.clickOnTheElementByXpath("//button[@name='add']"); // AddMoreBtn
			}
			Thread.sleep(1000);
		} else {
			Browser.enterTextByName("order_comment", "This is order comment"); // orderComment
			Thread.sleep(1000);
		}
		Browser.clickOnTheElementByXpath(Elements_Medicines.prescription_addMedProceed); // proceedBtn

	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: verify order summary
	 * 
	 * @ Parms: Double orderTotal_checkout from checkout_coupon_summary method,
	 * comment, couponName and isCoupon (Apply OR "")
	 * 
	 * @ Returns:
	 */
	public Double shippingPage_orderSummary(String isCoupon, String couponName, Double orderTotal_checkout,
			String comment) throws Exception {
		Browser.waitFortheElementXpath(Elements_Medicines.shippingAddressHeader);
		Double shipping_orderTotal = null;
		Thread.sleep(8000);
		String orderTotal_shippingString = null;
		Double orderTotal_discount, discountAmt, cartSubTotal, shippingAmt;
		if (driver.findElements(By.xpath(Elements_Medicines.checkout_paymentGroup)).size() != 0) {
			System.out.println("Payment options are available.");
			Browser.waitFortheElementXpath(Elements_Medicines.checkout_paymentGroup);
			if (isCoupon.equalsIgnoreCase("Apply")) {
				Browser.enterTextByXpath(Elements_Medicines.shipping_enterCouponName, couponName);
				Browser.clickOnTheElementByXpath("(" + Elements_Medicines.shipping_couponApplyBtn + ")[2]");
				Browser.waitUntil_InvisiblityByXpath("//img[@alt='Loading...']");
				if (driver.findElements(By.xpath(Elements_Medicines.shipping_invalidCouponMessage))
						.size() > 0) {
					System.out.println("Coupon code is not valid");
					shipping_orderTotal = orderTotal_checkout;
					orderTotal_shippingString = Browser.decimalFormat(orderTotal_checkout);
				} else {
					int discountPer = Integer.parseInt(couponName.replaceAll("[^0-9]", ""));
					cartSubTotal = Double.parseDouble(
							Browser.getTextByXpath(Elements_Medicines.shipping_cartSubTotal).replaceAll("₹", ""));
					shippingAmt = Double.parseDouble(
							Browser.getTextByXpath(Elements_Medicines.checkout_shippingAmt).replaceAll("₹", ""));
					discountAmt = (cartSubTotal * discountPer) / 100;
					orderTotal_discount = (cartSubTotal + shippingAmt)
							- Double.parseDouble(Browser.decimalFormat(discountAmt));
					orderTotal_shippingString = Browser.decimalFormat(orderTotal_discount);
					shipping_orderTotal = orderTotal_discount;
				}
			} else {
				shipping_orderTotal = orderTotal_checkout;
				orderTotal_shippingString = Browser.decimalFormat(orderTotal_checkout);
			}
			Browser.waitFortheElementXpath(
					"//tr[@class='grand totals']/td/strong/span[text()='₹" + orderTotal_shippingString + "']");
			Browser.enterTextByXpath(Elements_Medicines.checkout_comments, comment);
		} else {
			Browser.waitFortheElementXpath(Elements_Medicines.payment_COD2);
		}
		return shipping_orderTotal;
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: choose a payment method
	 * 
	 * @ Parms: payonline , cashondelivery
	 * 
	 * @ Returns:
	 */
	public void shippingPage_paymentMethod_placeOrderClick(String paymode) throws Exception {
		Thread.sleep(8000);
		if (driver.findElements(By.xpath(Elements_Medicines.payment_COD_radioBtn)).size() != 0) {
			Browser.clickOnTheElementByXpath("(//input[@value='" + paymode + "'])[2]");
			Browser.scrollbyxpath(Elements_Medicines.checkout_placeOrderBtn);
			Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_placeOrderBtn);
			if (paymode.equalsIgnoreCase("payonline")) {
				Browser.waitFortheElementXpath("//a[contains(., 'NET BANKING')]");
			} else {
				System.out.println("Paymode is COD check confirmation page.");
			}

		} else {
			System.out.println("No payment mode needed. Proceed to checkout..");
			Browser.scrollbyxpath(Elements_Medicines.checkout_comments);
			Browser.scrollbyxpath(Elements_Medicines.checkout_placeOrderBtn);
			Browser.clickOnTheElementByXpath(Elements_Medicines.checkout_placeOrderBtn);
		}
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: Verify order confirmation page
	 * 
	 * @ Parms: String productName, Double orderTotal, String paymode
	 * 
	 * @ Returns: orderID
	 */
	public String order_confirmationPage(String productName, Double orderTotal, String paymode) {
		String orderID, orderTotalString;
		orderTotalString = Browser.decimalFormat(orderTotal);
		Browser.waitFortheElementXpath(Elements_Medicines.confirmation_heading);
		orderID = Browser.getTextByXpath(Elements_Medicines.confirmation_getOrderId).replaceAll("[^0-9]", "");
		int prescriptionAdded = driver.findElements(By.xpath(Elements_Medicines.confirmation_prescriptionAdded)).size();
		int walletCount = driver.findElements(By.xpath("//div/p[text()='zoylowallet']")).size();
		int wallet_payOl = driver.findElements(By.xpath("//div/p[text()='zoylowallet_juspay']")).size();
		if (prescriptionAdded >= 0 && orderTotal > 0) {
			Browser.waitFortheElementXpath(
					"//div[contains(., 'Items in Order')]/table/tbody/tr/td[contains(., '" + productName + "')]");
			Browser.waitFortheElementXpath(
					"//h5[@class='grand-total-rightl' and contains(., '" + orderTotalString + "')]");
			if (prescriptionAdded != 0) {
				Browser.waitFortheElementXpath(Elements_Medicines.confirmation_prescriptionAdded);
			}
			if (paymode.equalsIgnoreCase("cashondelivery")) {
				Browser.waitFortheElementXpath(Elements_Medicines.confirmation_paymentCOD);
			} else if (paymode.equalsIgnoreCase("payonline")) {
				if (walletCount != 0) {
					Browser.waitFortheElementXpath("//div/p[text()='zoylowallet']");
				} else if (wallet_payOl != 0) {
					Browser.waitFortheElementXpath("//div/p[text()='zoylowallet_juspay']");
				} else {
					Browser.waitFortheElementXpath("//div/p[contains(., 'Razorpay')]");
				}
			} else {
				Browser.waitFortheElementXpath(Elements_Medicines.confirmation_paymentPayLater);
			}
		} else {
			Browser.waitFortheElementXpath(Elements_Medicines.confirmation_prescriptionAdded);
			Browser.waitFortheElementXpath(Elements_Medicines.confirmation_paymentCOD);
		}
		return orderID;
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: ecom home page, searchs product and clicks on first list OR
	 * adds the first product to cart
	 * 
	 * @ Parms: productName, searchAddToCart (addtocart)
	 * 
	 * @ Returns:
	 */
	public Double medicineHomePage_search(String productName, String searchAddToCart) throws Exception {
		Double searchListPrice = null;
		Browser.enterTextByID("search", productName);
		Browser.waitFortheElementXpath(Elements_Medicines.genericSearch_resultWait); // searchResult
		Browser.waitFortheElementXpath("//a[contains(., '" + productName + "')]");
		Thread.sleep(2000);
		searchListPrice = Double.parseDouble(Browser.getTextByXpath(Elements_Medicines.genericSearch_firstProduct_Price).replaceAll("₹", ""));
		if (searchAddToCart.equalsIgnoreCase("addtocart")) {
			Browser.clickOnTheElementByXpath(Elements_Medicines.searchResult_FirstAddToCart);
		} else {
			Browser.clickOnTheElementByXpath("//a[@class='title' and contains(., '" + productName + "')]"); // searchProduct
		}

		return searchListPrice;

	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: PDP product verify, modify qty and next action is defined
	 * 
	 * @ Parms: String ProductName, Boolean modQty, int Qty, pdp_action = Add to
	 * Cart or Buy
	 * 
	 * @ Returns:
	 */
	public void PDP_itemQtyInfo(String productName, int Qty, String pdp_action) throws Exception {
		Browser.waitFortheElementXpath("//div[@class='pro-name']/h1[contains(., '" + productName + "')]");
		if (Qty > 1) {
			Browser.enterTextByID("qty", Integer.toString(Qty));
		}
		if (pdp_action.equalsIgnoreCase("Buy")) {
			Browser.clickOnTheElementByXpath("//div[@class='actions']/div/button[@title='" + pdp_action + "']");
		} else {
			Browser.clickOnTheElementByXpath("//div[@class='actions']/button[@title='" + pdp_action + "']");
		}
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: Returns Coupon name from PDP page
	 * 
	 * @ Parms: String ProductName
	 * 
	 * @ Returns: Coupon name
	 */
	public String PDP_getCouponName(String productName) throws Exception {
		String couponName = null;
		Browser.waitFortheElementXpath("//div[@class='pro-name']/h1[contains(., '" + productName + "')]");

		if (driver.findElements(By.xpath(Elements_Medicines.PDP_discountBlock)).size() > 0) { // DiscountBlock

//			Browser.scrollbyxpath("//div[@class='get-additional-block']"); // DiscountBlock
			couponName = Browser.getTextByXpath(Elements_Medicines.PDP_discountName); // CouponName

		}

		return couponName;
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: Returns Coupon name from PD responsive page
	 * 
	 * @ Parms: String ProductName
	 * 
	 * @ Returns: Coupon name
	 */
	public String PDP_getCouponName_responsive(String productName) throws Exception {
		String couponName = null;
		Browser.waitFortheElementXpath("//div[@class='pro-name']/h1[contains(., '" + productName + "')]");

		if (driver.findElements(By.xpath(Elements_Medicines.PDP_discountBlock)).size() > 0) { // DiscountBlock
			couponName = Browser.getTextByXpath(Elements_Medicines.PDP_discountName); // CouponName

		}

		return couponName;
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: Returns Coupon discount val from PDP page
	 * 
	 * @ Parms: String ProductName
	 * 
	 * @ Returns: discountVal name
	 */
	public int PDP_getCouponValue(String productName) throws Exception {
		int couponVal = 0;
		Browser.waitFortheElementXpath("//div[@class='pro-name']/h1[contains(., '" + productName + "')]");

		if (driver.findElements(By.xpath(Elements_Medicines.PDP_discountBlock)).size() > 0) { // DiscountBlock

			Browser.scrollbyxpath(Elements_Medicines.PDP_discountBlock); // DiscountBlock
			couponVal = Integer
					.parseInt(Browser.getTextByXpath(Elements_Medicines.PDP_discountValue).replaceAll("[^0-9]", "")); // CouponVal

		}

		return couponVal;
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: Mini cart qty per item and item sub total
	 * 
	 * @ Parms: Boolean miniCart_Click, String ProductName, int qty, Double
	 * productListPrice
	 * 
	 * @ Returns: itemSubTotal
	 */
	public Double miniCart_qty_itemSubTotal(Boolean miniCart_Click, String productName, Double productListPrice,
			int qty) throws Exception {
		Double itemSubTotal = null;
		String productListPriceString = Browser.decimalFormat(productListPrice);

		if (miniCart_Click == true) {
			while (driver.findElements(By.xpath(Elements_Medicines.header_miniCartViewBtn)).size() == 0) {
				Browser.clickOnTheElementByXpath(Elements_Medicines.header_miniCartIcon); // minicartIcon
			}
		}
		Browser.waitFortheElementXpath(Elements_Medicines.header_miniCartViewBtn); // viewCartBtn
		Browser.waitFortheElementXpath("//strong[@class='product-item-name' and contains(., '" + productName
				+ "')]/following-sibling::div/div[@class='product-item-pricing']/div/span/span/span/span[@class='price' and contains(.,'₹"
				+ productListPriceString + "')]");
		if (qty > 1) {
			Browser.clickOnTheElementByXpath("//td[contains(., '" + productName
					+ "')]/following-sibling::td/div[@class='details-qty qty']/input");
			Thread.sleep(2000);

//			Browser.enterTextByXpath("//td[contains(., '" + productName
//					+ "')]/following-sibling::td/div[@class='details-qty qty']/input", Integer.toString(qty));
			driver.findElement(By.xpath("//td[contains(., '" + productName
					+ "')]/following-sibling::td/div[@class='details-qty qty']/input")).sendKeys(Keys.ARROW_RIGHT);
			Thread.sleep(1000);
			driver.findElement(By.xpath("//td[contains(., '" + productName
					+ "')]/following-sibling::td/div[@class='details-qty qty']/input")).sendKeys(Keys.BACK_SPACE);
			driver.findElement(By.xpath("//td[contains(., '" + productName
					+ "')]/following-sibling::td/div[@class='details-qty qty']/input")).sendKeys(Integer.toString(qty));

			Browser.clickOnTheElementByXpath("//strong[@class='product-item-name' and contains(., '" + productName
					+ "')]/following-sibling::div/div[@class='product-item-pricing']/div/span/span/span/span[@class='price' and contains(.,'₹"
					+ productListPriceString + "')]");
			Thread.sleep(2000);
			itemSubTotal = productListPrice * qty;

		} else {
			itemSubTotal = productListPrice;
		}

		return itemSubTotal;
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: Verifies mini cart sub total and clicks on view cart btn
	 * 
	 * @ Parms: Boolean miniCart_Click, itemSubTotal from miniCart_qty_itemSubTotal
	 * method
	 * 
	 * @ Returns: miniCart_subTotal
	 */
	public Double miniCart_subTotal(Boolean miniCart_Click, Double itemSubTotal) {
		if (miniCart_Click == true) {
			Browser.clickOnTheElementByXpath(Elements_Medicines.header_miniCartIcon); // minicartIcon
		}
		Double miniCart_subTotal;
		String itemSubTotalString = Browser.decimalFormat(itemSubTotal);
		Browser.waitFortheElementXpath(
				"//span[contains(., 'Cart Subtotal')]/following-sibling::div/span/span[contains(., '₹"
						+ itemSubTotalString + "')]");

		miniCart_subTotal = itemSubTotal;

		return miniCart_subTotal;

	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will add address for new user
	 * 
	 * @ Parms: String AddressType (home,work,others), Address Address, String
	 * State, String pinCode, String mobileNumber, String City
	 * 
	 * @ Returns:
	 */
	public void shippingPage_addAddress(String AddressType, Address Address, String State, String pinCode,
			String mobileNumber, String City) throws Exception {
		Browser.waitFortheElementXpath(Elements_Medicines.shippingAddressHeader);
		Browser.selectXpathByValue(Elements_Medicines.shippingAddress_type, AddressType);
		Browser.enterTextByXpath(Elements_Medicines.shippingAddress_Street, Address.toString());
		Browser.enterTextByName(Elements_Medicines.shippingAddress_pin, pinCode);
//		Browser.selectXpathByText(Elements_Medicines.shippingAddress_state, State);
//		Browser.enterTextByName(Elements_Medicines.shippingAddress_city, City);
		Browser.enterTextByName(Elements_Medicines.shippingAddress_phone, mobileNumber);
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will return order status from api
	 * 
	 * @ Parms: String OrderID
	 * 
	 * @ Returns: orderStatus
	 */
	public String getOrderStatus_postApi(String orderId) throws Exception {
		System.out.println("Getting order status for order ID: " + orderId);
		String orderStatus = null;
		Response result = RestAssured.given().multiPart("id", orderId)
				.post(LoadProp.EnvironmentURL + LoadProp.getOrder_StatusURL);
		orderStatus = result.getBody().asString();
		System.out.println("API response is : " + orderStatus);
		return orderStatus;
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will login into magento admin
	 * 
	 * @ Parms: String userName and password
	 * 
	 * @ Returns: NA
	 */
	public void Magento_Login(String userName, String password) throws Exception {
		Browser.waitFortheElementXpath("//img[@title='Magento Admin Panel']");
		Browser.enterTextByID("username", userName);
		Browser.enterTextByID("login", password);
		Browser.clickOnTheElementByXpath("//button/span[text()='Sign in']"); // signInBtn
		Thread.sleep(3000);
		if (driver.findElements(By.xpath("//button[@class='action-close']")).size() > 0) {
			Browser.clickOnTheElementByXpath("//button[@class='action-close']"); // popUpCloseBtn
			Browser.waitFortheElementXpath("(//a/span[text()='Dashboard'])[1]"); // dashboardMenu
		}
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will click on side menu of magento and choose
	 * subMenu
	 * 
	 * @ Parms: String menuName and subMenuName
	 * 
	 * @ Returns: NA
	 */
	public void Magento_sideMenu_click(boolean menuScroll, String menuName, String subMenuName) throws Exception {
		if (menuScroll = true) {
			Browser.scrollbyxpath("(//a/span[text()='" + menuName + "'])[1]");
			Browser.clickOnTheElementByXpath("(//a/span[text()='" + menuName + "'])[1]");
			Browser.scrollbyxpath("//strong[text()='" + menuName + "']");
			Browser.clickOnTheElementByXpath("(//div[@class='submenu']/ul/li/a/span[text()='" + subMenuName + "'])[1]");
		} else {
			Browser.clickOnTheElementByXpath("(//a/span[text()='" + menuName + "'])[1]");
			Browser.clickOnTheElementByXpath("(//div[@class='submenu']/ul/li/a/span[text()='" + subMenuName + "'])[1]");
		}
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will enter POS customer details to place order
	 * 
	 * @ Parms: String number, addressValue
	 * 
	 * @ Returns: NA
	 */
	public void Magento_POS_customerdetails(String number, String addressValue) throws Exception {
		Browser.enterTextByName("mobile", number);
		Browser.clickOnTheElementByXpath("//th[text()='Customer Mobile Number : ']"); // focusOut
		Thread.sleep(2000);
		Browser.waitFortheElementXpath("//select/option[text()='SELECT AN ADDRESS']"); // addressLoad
		Browser.selectNameByValue("customer_address", addressValue);
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will enter POS product / item details to place
	 * order
	 * 
	 * @ Parms: String productName, qty
	 * 
	 * @ Returns: NA
	 */
	public void Magento_POS_productDetails(String productName, String qty) throws Exception {
		Browser.enterTextByID("skuname-0", productName);
		Browser.clickOnTheElementByXpath("//li[@class='ui-menu-item']/a[text()='" + productName + "']");
		Browser.enterTextByID("qty-0", qty);
		Browser.clickOnTheElementByXpath("//span[text()='Price: ']"); // focusOut
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will click on order status and get details table
	 * 
	 * @ Parms: String orderId, orderStatus
	 * 
	 * @ Returns: NA
	 */
	public void Magento_QuickOrder_OrderStatus(String orderId, String orderStatus) throws Exception {
		Browser.clickOnTheElementByXpath("//button[text()='Order Status']"); // statusTab
		Browser.enterTextByID("order_id", orderId); // enterOrderId
		Browser.clickOnTheElementByID("check-status"); // checkBtn
		// Order details table
		Browser.waitFortheElementXpath("//div[@id='idorderdetailstable']/table/tbody/tr/td[text()='" + orderId
				+ "']/following-sibling::td[text()='" + orderStatus + "']");
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will click on order status and perform table
	 * action
	 * 
	 * @ Parms: String orderId, Action (View OR Re-Order)
	 * 
	 * @ Returns: NA
	 */
	public void Magento_QuickOrder_OrderStatus_table_Action(String orderId, String Action) {
		Browser.clickOnTheElementByXpath("//div[@id='idorderdetailstable']/table/tbody/tr/td[text()='" + orderId
				+ "']/following-sibling::td/a[contains(., '" + Action + "')]");

		// Switch Window
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		if (tabs.size() == 2) {
			driver.switchTo().window(tabs.get(1));
		} else {
			System.out.println("Action to the order did not work...");
		}

		if (Action.contains("View")) {
			Browser.waitFortheElementXpath("//h1[text()='#" + orderId + "']");
		} else {
			Browser.waitFortheElementXpath("//h1[contains(., 'Create New Order')]");
		}
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method will change status from dropdown in view order
	 * 
	 * @ Parms: String status
	 * 
	 * @ Returns: NA
	 */
	public void Magento_QuickOrder_View_ChangeStatus(String status) throws Exception {
		Browser.scrollbyxpath("//div[@class='admin__page-section-title']/span[text()='Order Total']"); // scrollToStatus
		Browser.selectIDbyText("history_status", status);
		Browser.enterTextByID("history_comment", status);
		Browser.clickOnTheElementByXpath("//button[@title='Submit Comment']/span"); // submitBtn
		Browser.scrollbyxpath("//span[text()='Order & Account Information']"); // scrollToTop
		Browser.waitFortheElementXpath(
				"//tr[contains(., 'Order Status')]/td/span[@id='order_status' and contains(., '" + status + "')]");
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method is used to scroll down and select payment method
	 * in reorder page
	 * 
	 * @ Parms: String pMethod (paylater OR cashondelivery)
	 * 
	 * @ Returns: NA
	 */
	public void Magento_QuickOrder_ReOrder_PaymentMethod(String pMethod) throws Exception {
//		int eleSize = driver.findElements(By.xpath("//span[text()='Payment & Shipping Information']")).size(); // paymentTitle
//		while (eleSize == 1) {
//			Browser.ScrollDown();
//			if (driver.findElement(By.xpath("//span[text()='Payment & Shipping Information']")).isDisplayed()) {
//				Browser.scrollbyxpath("//span[text()='Payment & Shipping Information']"); // paymentTitle
//				eleSize = 1;
//			}
//		}
		Browser.scrollbyxpath("//span[text()='Payment & Shipping Information']"); // paymentTitle
		Browser.clickOnTheElementByXpath("//input[@value='" + pMethod + "']");
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method is used to search orders in sales
	 * 
	 * @ Parms: data (order Id or mobile number)
	 * 
	 * @ Returns: NA
	 */
	public void Magento_Sales_Orders_search(String data) throws Exception {
		Browser.waitFortheElementXpath("//h1[text()='Orders']"); // header
		Thread.sleep(2000);
		if (driver.findElements(By.xpath("(//button[@class='action-remove'])[1]")).size() > 0) {
			Browser.clickOnTheElementByXpath("(//button[text()='Clear all'])[1]");
			Thread.sleep(10000);
		}
		Browser.enterTextByXpath("(//label[@title='Search']/following-sibling::input)[1]", data); // searchInput
		Browser.clickOnTheElementByXpath(
				"(//label[@title='Search']/following-sibling::button[@class='action-submit'])[1]"); // searchBtn
		Browser.waitFortheElementXpath("(//td/div[text()='" + data + "'])[1]");
	}

	/*
	 * @ Author: Sagar Sen
	 * 
	 * @ Description: This method is used to view order details in sales
	 * 
	 * @ Parms: data (order Id or mobile number)
	 * 
	 * @ Returns: NA
	 */
	public void Magento_Sales_Orders_view(String data) throws Exception {
		Browser.clickOnTheElementByXpath(
				"(//td[contains(., '" + data + "')])[1]/following-sibling::td/a[text()='View']"); // viewBtn
		Browser.waitFortheElementXpath("//h1[text()='#" + data + "']"); // header
	}

}