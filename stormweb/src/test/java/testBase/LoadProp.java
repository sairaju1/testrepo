package testBase;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.net.URL;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.Reporter;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LoadProp {
	// Environment
	public static String browser_name, Environment, linux_imagepath, EnvironmentURL, EnvironmentAdminURL, magentoAdminURL;
	// DB credentials
	public static String qa_address, qa_userName, qa_dbName, qa_password, qa_clusterone, qa_clustertwo, qa_clusterthree, qa_replicaSet, uat_address, uat_userName, uat_dbName,
			uat_password, uat_clusterone, uat_clustertwo, uat_clusterthree, uat_replicaSet;
	public static int qa_port, uat_port;
	// URL
	public static String qa_url, qa_admin_url, uat_url, uat_affiliates, uat_admin_url, uat_magento_URL;

	// Doctor Details
	public static String Doctor_Username, Doctor_Password, Doctor_Name, Doctor_Location, doctor_location2,Appointment_Sucessful_Message,
			Reschedule_Message, Promo_Code, Wellness_Location, Wellness_Center,doctor_name_email,doctor_name,Clinic_PromoCode,Wellness_Promocode;

	// PMS Details
	public static String PMS_Username, PMS_Password,PMS_Doctor, OC_Promocode, OC_Wallet_UserName, OC_Wallet_Password,OC_Username,OC_Password,OC_DoctorEmail,OC_ClinicEmail,OC_User_DB_Phone,OC_DoctorName,
	OC_Doctor_Email,OC_Doctor_Mobile,OC_Doctor_Clinic,PMS_URL,Home_URL,OC_Promocodetwo;

	// Clinic
	public static String Clinic_Name,doctor_clinic2,hospital_name;

	// Admin
	public static String Admin_Username, Admin_Password;
	
	// Screen dimentions
	public static int width, height;

	// To Run On Cloud - Sauce Labs
	public static final String USERNAME = "ganeshmandala123";
	public static final String ACCESS_KEY = "54f5beb0-8191-4184-9094-ec209f9b300c";
	public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";
	// To Run On Cloud - Gridlastics
	public static final String GridUSERNAME = "0Xb3ucxZdPKuRNKetwPJXSZqb2mSRxkI";
	public static final String GridACCESS_KEY = "llBGoZvrE5LmJmq2J6BxPrNQj0z1oJAA";
	public static final String GridSUBDOMAIN = "7dgrd43a";
	public static final String GridURL = "http://" + GridUSERNAME + ":" + GridACCESS_KEY + "@" + GridSUBDOMAIN
			+ ".gridlastic.com:80/wd/hub";

	// SQL Info
	public static String sql_dbURL, sql_dbName, sql_userName, sql_userPw;

	// MED user
	public static String user_id, user_pw, user_mail;

	// MED Info
	public static String productName_rx, cod, online, payLater, productName_nonRX, manufacturer, brands, orderSuccess_Status, orderFail_Status, orderCancel_Status, 
	orderPendingPayment_Status, getOrder_StatusURL, shopBrands, Magento_user, Magento_pwd, Med_Promo;

	// Admin user
	public static String adminuser_id, adminuser_pw, qaAdmin_API, uatAdmin_API;

	// Zoylo Lab
	public static String partner_name, allcitieszones_pinAPI, zoneSlots_API, zonePin_API, qualification, operatingCity_1, operatingCity_2, test_Name,
	profile_Name, package_Name, package_NameTwo, rec_packageName, dcPay_online, dcPay_cod, dc_promo;
	
	//PMS Test Data
	public static String PreConsultation_Age, PreConsultation_Gender, PreConsultation_Username,PreConsultation_Symptom,PreConsultation_diseaseOne,PreConsultation_diseaseTwo,PreConsultation_diseaseThree;

	public static WebDriver driver;

	public static WebDriver LoadBrowserProperties() throws IOException {
		FileInputStream inStream;
		inStream = new FileInputStream(new File("ConfigFiles/Setup-Details.txt"));
		Properties prop = new Properties();
		prop.load(inStream);

		linux_imagepath = prop.getProperty("linux.imagepath");
		browser_name = prop.getProperty("browser.name");

		// DB credentials
		uat_address = prop.getProperty("uat.address");
		uat_port = Integer.parseInt(prop.getProperty("uat.port"));
		uat_userName = prop.getProperty("uat.userName");
		uat_dbName = prop.getProperty("uat.dbName");
		uat_password = prop.getProperty("uat.password");
		uat_clusterone = prop.getProperty("uat.clusterone");
		uat_clustertwo = prop.getProperty("uat.clustertwo");
		uat_clusterthree = prop.getProperty("uat.clusterthree");
		uat_replicaSet = prop.getProperty("uat.replicaSet");
		qa_userName = prop.getProperty("qa.userName");
		qa_dbName = prop.getProperty("qa.dbName");
		qa_password = prop.getProperty("qa.password");
		qa_clusterone = prop.getProperty("qa.clusterone");
		qa_clustertwo = prop.getProperty("qa.clustertwo");
		qa_clusterthree = prop.getProperty("qa.clusterthree");
		qa_replicaSet = prop.getProperty("qa.replicaSet");

		// SQL DB credentials
		sql_dbURL = prop.getProperty("sql.dbURL");
		sql_dbName = prop.getProperty("sql.dbName");
		sql_userName = prop.getProperty("sql.userName");
		sql_userPw = prop.getProperty("sql.userPw");

		// URL'S
		qa_url = prop.getProperty("qa.url");
		qa_admin_url = prop.getProperty("qa.admin.url");
		uat_url = prop.getProperty("uat.url");
		uat_affiliates = prop.getProperty("uat.affiliates");
		uat_admin_url = prop.getProperty("uat.admin.url");
		uat_magento_URL = prop.getProperty("uat.magento");

		// MED USer
		user_id = prop.getProperty("user.id");
		user_pw = prop.getProperty("user.pw");
		user_mail = prop.getProperty("user.mail");

		// Admin User
		adminuser_id = prop.getProperty("adminuser.id");
		adminuser_pw = prop.getProperty("adminuser.pw");
		qaAdmin_API = prop.getProperty("qaAdmin.API");
		uatAdmin_API = prop.getProperty("uatAdmin.API");

		// Zoylo lab
		partner_name = prop.getProperty("partner.name");
		qualification = prop.getProperty("qualification");
		operatingCity_1 = prop.getProperty("operatingCity1");
		operatingCity_2 = prop.getProperty("operatingCity2");
		test_Name = prop.getProperty("testName");
		profile_Name = prop.getProperty("profileName");
		package_Name = prop.getProperty("packageName");
		package_NameTwo = prop.getProperty("packageNameTwo");
		rec_packageName = prop.getProperty("rec.packageName");
		dcPay_online = prop.getProperty("dcPay.online");
		dcPay_cod = prop.getProperty("dcPay.cod");
		dc_promo = prop.getProperty("dc.promo");

		// Doctor User
		Doctor_Username = prop.getProperty("Doctor.userid");
		Doctor_Password = prop.getProperty("Doctor.password");
		Doctor_Name = prop.getProperty("Doctor.Name");
		Doctor_Location = prop.getProperty("Doctor.Location");
		Appointment_Sucessful_Message = prop.getProperty("Appointment.Sucessful.Message");
		Reschedule_Message = prop.getProperty("Rechedule.Message");
		Promo_Code = prop.getProperty("Promo.Code");
		Wellness_Location = prop.getProperty("Wellness.Location");
		Wellness_Center = prop.getProperty("Wellness.Center");
		Wellness_Promocode=prop.getProperty("Wellness.Promo");
		doctor_location2=prop.getProperty("doctor.location2");
		doctor_name_email=prop.getProperty("doctor.name.email");
		doctor_name=prop.getProperty("doctor.name");

		// PMS
		PMS_Username = prop.getProperty("PMS.Username");
		PMS_Password = prop.getProperty("PMS.Password");
		PMS_Doctor=prop.getProperty("PMS.Doctor");
		OC_Promocode = prop.getProperty("OC.Promocode");
		Clinic_PromoCode=prop.getProperty("Clinic.Promo");
		OC_Wallet_UserName = prop.getProperty("OC.Wallet.User");
		OC_Wallet_Password = prop.getProperty("OC.Wallet.Password");
		OC_Username=prop.getProperty("OC.Username");
		OC_Password=prop.getProperty("OC.Password");
		OC_DoctorEmail=prop.getProperty("OC.DoctorEmail");
		OC_ClinicEmail=prop.getProperty("OC.ClinicEmail");
		OC_User_DB_Phone=prop.getProperty("OC.DB.Phone");
		OC_DoctorName=prop.getProperty("OC.DoctorName");
		OC_Doctor_Email=prop.getProperty("OC.Doctor.Email");
		OC_Doctor_Mobile=prop.getProperty("OC.Doctor.Mobile");
		OC_Doctor_Clinic=prop.getProperty("OC.Doctor.Clinic");
		PMS_URL=prop.getProperty("PMS.URL");
		Home_URL=prop.getProperty("Home.URL");
		OC_Promocodetwo=prop.getProperty("OC.Promocodetwo");
		
		//PMS Test Data
		PreConsultation_Age=prop.getProperty("PreConsultation.Age");;
		PreConsultation_Gender=prop.getProperty("PreConsultation.Gender");; 
		PreConsultation_Username=prop.getProperty("PreConsultation.Username");;
		PreConsultation_Symptom=prop.getProperty("PreConsultation.Symptom");;
		PreConsultation_diseaseOne=prop.getProperty("PreConsultation.diseaseOne");;
		PreConsultation_diseaseTwo=prop.getProperty("PreConsultation.diseaseTwo");;
		PreConsultation_diseaseThree=prop.getProperty("PreConsultation.diseaseThree");;

		// Clinic
		Clinic_Name = prop.getProperty("Clinic.Name");
		doctor_clinic2=prop.getProperty("doctor.clinic2");
		hospital_name=prop.getProperty("hospital.name");

		// Admin
		Admin_Username = prop.getProperty("Admin.Username");
		Admin_Password = prop.getProperty("Admin.Password");
		
		// Screen dimentions
		width = Integer.parseInt(prop.getProperty("width"));
		height = Integer.parseInt(prop.getProperty("height"));

		// Sauce Labs - Capabilities

		/*
		 * DesiredCapabilities caps = DesiredCapabilities.chrome();
		 * caps.setCapability("platform", "macOS 10.12"); caps.setCapability("version",
		 * "58.0"); driver = new RemoteWebDriver(new URL(URL), caps);
		 */

		// MED Info
		productName_rx = prop.getProperty("productName.rx");
		productName_nonRX = prop.getProperty("productName.nonRX");
		cod = prop.getProperty("cod");
		online = prop.getProperty("online");
		payLater = prop.getProperty("payLater");
		manufacturer = prop.getProperty("manufacturer");
		brands = prop.getProperty("brands")+" ";
		orderSuccess_Status = prop.getProperty("orderSuccess_status");
		orderFail_Status = prop.getProperty("orderFail_Status");
		orderCancel_Status = prop.getProperty("orderCancel_Status");
		orderPendingPayment_Status = prop.getProperty("orderPendingPayment_Status");
		getOrder_StatusURL = prop.getProperty("getOrder_StatusURL");
		shopBrands = prop.getProperty("shopBrands")+" ";
		Magento_user = prop.getProperty("Magento.user");
		Magento_pwd = prop.getProperty("Magento.pwd");
		Med_Promo = prop.getProperty("Med.Promo");

		// Env URL Handler
		Environment = prop.getProperty("Environment");
		if (Environment.contains("qa")) {
			EnvironmentURL = qa_url;
			EnvironmentAdminURL = qa_admin_url;
			System.out.println("Environment set to : " + Environment);
			allcitieszones_pinAPI = prop.getProperty("allcitieszones.pinQaAPI");
			zoneSlots_API = prop.getProperty("zoneSlots.QaAPI");
			zonePin_API = prop.getProperty("zonePin.QaAPI");
		} else {
			EnvironmentURL = uat_url;
			EnvironmentAdminURL = uat_admin_url;
			magentoAdminURL = uat_magento_URL;
			System.out.println("Environment set to : " + Environment);
			allcitieszones_pinAPI = prop.getProperty("allcitieszones.pinUatAPI");
			zoneSlots_API = prop.getProperty("zoneSlots.UatAPI");
			zonePin_API = prop.getProperty("zonePin.UatAPI");
		}

		if (browser_name.equals("chrome")) {
			String os = System.getProperty("os.name").toLowerCase(); // Added to verify the OS
			System.out.println("OS=" + os);
			if (os.contains("mac")) {

				System.out.println("launching chrome browser");
				// WebDriver manager Details
				
//				WebDriverManager.chromedriver().setup(); 								// Takes latest version automatically
				WebDriverManager.chromedriver().version("76.0.3809.68").setup(); 		// Set Version manually
				
				ChromeOptions options = new ChromeOptions();
				options.addArguments("disable-infobars");
				options.addArguments("disable-extensions");
				options.addArguments("--use-fake-ui-for-media-stream=1"); // Added for camera
				//options.addArguments("--enable-geolocation");
				//options.addArguments("--enable-strict-powerful-feature-restrictions");
				driver = new ChromeDriver(options);
				driver.manage().window().fullscreen();

				/*
				 * //System.setProperty("java.awt.headless", "true");
				 * System.setProperty("webdriver.chrome.driver", "BrowserDrivers/chromedriver");
				 * ChromeOptions options = new ChromeOptions();
				 * //options.addArguments("--headless","--disable-gpu"); //Added to launch
				 * chrome without GUI options.addArguments("disable-infobars"); // Added to
				 * remove new chrome warning message
				 * options.addArguments("--use-fake-ui-for-media-stream=1"); // Added to allow
				 * camera //options.addArguments("--kiosk"); // Added to Maximize window driver
				 * = new ChromeDriver(options); //driver.manage().window().maximize();
				 * 
				 * GraphicsDevice gd =
				 * GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
				 * int width = gd.getDisplayMode().getWidth(); System.out.println("width :"
				 * +width); int height = gd.getDisplayMode().getHeight();
				 * System.out.println("Height :"+height); driver.manage().window().setSize(new
				 * Dimension(width, height));
				 */

			} else {
				Environment = System.getProperty("Environment");
				if (Environment.contains("qa")) {
					EnvironmentURL = qa_url;
					EnvironmentAdminURL = qa_admin_url;
					System.out.println("Environment set to : " + Environment);
				} else {
					EnvironmentURL = uat_url;
					EnvironmentAdminURL = uat_admin_url;
					magentoAdminURL = uat_magento_URL;
					System.out.println("Environment set to : " + Environment);
				}
				System.out.println("launching chrome browser in Linux");
				// System.setProperty("java.awt.headless", "true");
				// System.setProperty("webdriver.chrome.driver",
				// "BrowserDrivers/chromedriverL2");
				WebDriverManager.chromedriver().version("73.0.3683.68").setup(); 
				ChromeOptions options = new ChromeOptions();
				// options.addArguments("--headless","--disable-gpu"); //Added to launch chrome
				// without GUI
				options.addArguments("disable-infobars"); // Added to remove new chrome warning message
				options.addArguments("--use-fake-ui-for-media-stream=1"); // Added to allow camera
				// options.addArguments("--kiosk"); // Added to Maximize window
				options.addArguments("--no-sandbox"); // https://groups.google.com/forum/#!topic/chromedriver-users/8yZDv_h8feg
				driver = new ChromeDriver(options);
				// driver.manage().window().maximize();
				driver.manage().window().fullscreen();
				driver.manage().window().maximize();
//				GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
//				int width = gd.getDisplayMode().getWidth();
//				System.out.println("width :" + width);
//				int height = gd.getDisplayMode().getHeight();
//				System.out.println("Height :" + height);
				//driver.manage().window().setSize(new Dimension(1044, 810));
				driver.manage().window().setSize(new Dimension(height,width));
				System.out.println("Height :" + height);
				System.out.println("width :" + width);
			}

		} else if (browser_name.equals("firefox")) {

			System.out.println("launching Firefox browser");
			WebDriverManager.firefoxdriver().setup();

			FirefoxOptions capabilities = new FirefoxOptions();
			capabilities.setCapability("marionette", true);
			capabilities.setCapability("geo.prompt.testing", true);
			capabilities.setCapability("geo.prompt.testing.allow", true);
			capabilities.setCapability("media.navigator.permission.disabled", true);
			capabilities.setCapability("pdfjs.disabled", true);
			capabilities.setCapability("browser.helperApps.neverAsk.saveToDisk", "application/pdf");

			driver = new FirefoxDriver(capabilities);
			driver.manage().window().maximize();
		} else if (browser_name.equals("safari")) {
			System.out.println("launching Safari browser");
			driver = new SafariDriver();
			driver.manage().window().maximize();

		} else if (browser_name.equals("grid")) {
			System.out.println("launching Grid");

			String platform_name = "linux";
			String browser_name = "chrome";
			String browser_version = "latest";

			// optional video recording
			String record_video = "True";

			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("ie.fileUploadDialogTimeout", 10000);
			if (platform_name.equalsIgnoreCase("win7")) {
				capabilities.setPlatform(Platform.VISTA);
			}
			if (platform_name.equalsIgnoreCase("win8")) {
				capabilities.setPlatform(Platform.WIN8);
			}
			if (platform_name.equalsIgnoreCase("win8_1")) {
				capabilities.setPlatform(Platform.WIN8_1);
			}
			if (platform_name.equalsIgnoreCase("win10")) {
				capabilities.setPlatform(Platform.WIN10);
			}
			if (platform_name.equalsIgnoreCase("linux")) {
				capabilities.setPlatform(Platform.LINUX);
			}
			capabilities.setBrowserName(browser_name);
			capabilities.setVersion(browser_version);

			// video record
			if (record_video.equalsIgnoreCase("True")) {
				capabilities.setCapability("video", "True"); // NOTE: "True" is a case sensitive string, not boolean.
			} else {
				capabilities.setCapability("video", "False"); // NOTE: "False" is a case sensitive string, not boolean.
			}

			// Chrome specifics
			if (browser_name.equalsIgnoreCase("chrome")) {
				ChromeOptions options = new ChromeOptions();
				options.addArguments("disable-infobars"); // starting from Chrome 57 the info bar displays with "Chrome
															// is being controlled by automated test software."
				// On Linux start-maximized does not expand browser window to max screen size.
				// Always set a window size and window position.
				if (platform_name.equalsIgnoreCase("linux")) {
					options.addArguments(Arrays.asList("--window-position=0,0"));
					options.addArguments(Arrays.asList("--window-size=1920,1080"));
				} else {
					options.addArguments(Arrays.asList("--start-maximized"));
				}
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			}

			// Firefox specifics
			if (browser_name.equalsIgnoreCase("firefox")) {
				// If you are using selenium 3 and test Firefox versions below version 48
				if (Integer.parseInt(browser_version) < 48) {
					capabilities.setCapability("marionette", false);
				}
			}

			driver = new RemoteWebDriver(new URL(GridURL), capabilities);
			// driver.setFileDetector(new LocalFileDetector());
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

			// On LINUX/FIREFOX the "driver.manage().window().maximize()" option does not
			// expand browser window to max screen size. Always set a window size.
			if (platform_name.equalsIgnoreCase("linux") && browser_name.equalsIgnoreCase("firefox")) {
				driver.manage().window().setSize(new Dimension(1920, 1080));
			}

			
		}

		return driver;
	}

}
