package testBase;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.tools.ant.taskdefs.Echo.EchoLevel;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import objectRepository.Elements_Admin;
import objectRepository.Elements_Recipients;

public class AdminPage {
	public WebDriver driver;
	public TestUtils Browser;

	public AdminPage(WebDriver driver) throws Exception {
		this.driver = driver;
		Elements_Admin.Admin_PageProperties();
		Elements_Recipients.Recipients_PageProperties();
		Browser = new TestUtils(driver);
	}

	// ADMIN METHODS:
	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to login into zoylo admin app
	 * 
	 * @Parameters : Username, Password
	 * 
	 * @Return :
	 */
	public void adminLogin(String MobNum, String password) throws Exception {
		Browser.enterTextByName(Elements_Admin.SignIn_Mobile, MobNum);
		System.out.println("Entered mobile number");
		Browser.enterTextByName(Elements_Admin.SignIn_Password, password);
		System.out.println("Entered password");
		Browser.clickOnTheElementByXpath(Elements_Admin.SignIn_Login);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		Thread.sleep(2000);
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to wait until loader is disappered
	 * 
	 * @Parameters : xpath
	 * 
	 * @Return :
	 */
	public void waitUntil_LoaderInvisible(String xpath) {
		new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath)));
		System.out.println("Load wait completed");
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to logout from admin app
	 * 
	 * @Parameters :
	 * 
	 * @Return :
	 */
	public void adminLogout() {
		Browser.clickOnTheElementByXpath(Elements_Admin.elipse);
		Browser.clickOnTheElementByXpath(Elements_Admin.logout);
		Browser.waitforElementName(Elements_Admin.SignIn_Mobile);
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to check availability of doctor in front
	 * end
	 * 
	 * @Parameters : Doctor name
	 * 
	 * @Return :
	 */
	public void checkDoctorVisibility_RecipientEnd(String doctorName) throws Exception {
		//Browser.waitFortheElementXpath("(//div[@class='profile-detail']/h1/a)[1]");
		Browser.waitFortheElementXpath("//ul[@class='search-list']/li/div/div//h2//a");
		
		
		Thread.sleep(5000);
		int profileCount = driver.findElements(By.xpath("//ul[@class='search-list']/li/div/div//h2//a")).size();
		String pCount = Integer.toString(profileCount);
		System.out.println("Number of doctors are :" + pCount);
		for (int i = 1; i <= profileCount; i++) {
			String available_DocName = Browser.getTextByXpath("(//ul[@class='search-list']/li/div/div//h2)[" + i + "]");
			if (available_DocName.contains(doctorName)) {
				System.out.println(doctorName + " is available in front end");
			} else {
				System.out.println(doctorName + " is NOT AVAILABLE in front end");
			}
		}
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to choose sub menu under kellton menu
	 * 
	 * @Parameters : MainMenu Name to click, scrollToSubTabName= True OR false,
	 * SubTabName
	 * 
	 * @Return :
	 */
	public void AdminMenu_subMenu(String MainMenuName, String scrollToSubTabName, String SubTabNameName)
			throws Exception {
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + MainMenuName + "')]"); // Kellton
																														// Menu
																														// Click
		if (scrollToSubTabName.contains("True")) {
			Browser.scrollbyxpath("//div[@class='list__tile__title' and contains(., '" + SubTabNameName + "')]");
		}
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + SubTabNameName + "')]");
	}

	/*
	 * @Author : Guru charan
	 * 
	 * @Description : This method is used to Add Clinic
	 * 
	 * @Parameters :
	 * 
	 * @Return :
	 */

	public void Clinic_Hospital_registration(String Clinicname, String Mobnum, String Email, String Country,
			String State, String City, String Address1, String Address2, String Nearest, String Location, String PIN,
			String Lat, String Long, String FirstName, String MiddleName, String LastName, String MobileNumber,
			String Email1, String Password, String Confirmpassword) throws Exception {

		// About Clinic/Hospital
//		Browser.enterTextByXpath("(//div[@class='input-group__selections'])[1]", Type);
//		Browser.clickOnTheElementByXpath("(//div[@class='list__tile__title' and contains(., '"+Type+"')])[1]");
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Name, Clinicname);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_MobileNumber, Mobnum);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Email, Email);
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_About_Country);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + Country + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_About_State);
//    	Browser.enterTextByXpath("(//input[@class='input-group--select__autocomplete'])[1]", "Telangana");
//    	driver.findElement(By.xpath("(//input[@class='input-group--select__autocomplete'])[1]")).sendKeys(Keys.ARROW_DOWN);
//		driver.findElement(By.xpath("(//input[@class='input-group--select__autocomplete'])[1]")).sendKeys(Keys.ENTER);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__content']/div[contains(., '" + State + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_About_City);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + City + "')]");
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Address1, Address1);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Address2, Address2);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Nearest, Nearest);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Location, Location);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_PIN, PIN);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Lat, Lat);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Long, Long);
		Browser.scrollbyxpath(Elements_Admin.Clinic_About_Save);
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_About_Save);
		String abc = Browser.getTextByXpath("//div[@class='custom-success alert alert--dismissible']//p");
		System.out.println("suceess" + abc);
		Thread.sleep(4000);

		// Facilities
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_Facilities);
		Thread.sleep(4000);
		for (int i = 2; i <= 11; i++) {
			System.out.println("Value of x:" + i);
			Browser.clickOnTheElementByXpath("(//div[@class='input-group--selection-controls__ripple'])[" + i + "]");
		}
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_Facility_Save);
		Thread.sleep(4000);

		// SEO
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_SEO);
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_SEO_Save);
		Thread.sleep(3000);

		// Add Admin User
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_AddAdminuser);
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath("(//button[@class='synm-btn btn btn--raised theme--dark primary'])[1]");
		Browser.enterTextByXpath(Elements_Admin.Clinic_Adminuser_firstname, FirstName);
		Browser.enterTextByXpath(Elements_Admin.Clinic_Adminuser_middlename, MiddleName);
		Browser.enterTextByXpath(Elements_Admin.Clinic_Adminuser_lastname, LastName);
		Browser.enterTextByXpath(Elements_Admin.Clinic_Adminuser_mobnum, MobileNumber);
		Browser.enterTextByXpath(Elements_Admin.Clinic_Adminuser_Email1, Email1);
		Browser.enterTextByXpath(Elements_Admin.Clinic_Adminuser_pwd, Password);
		Browser.enterTextByXpath(Elements_Admin.Clinic_Adminuser_confirmpwd, Confirmpassword);
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_Adminuser_roleField); // Role field
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_Adminuser_docClinicRole); // Role select from dropdwn
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_Adminuser_Save);
	}

	/*
	 * @Author : Guru charan
	 * 
	 * @Description : This method is used to Add Hospital
	 * 
	 * @Parameters :
	 * 
	 * @Return :
	 */

	public void Hospital_registration(String Clinicname, String Mobnum, String Email, String Country, String State,
			String City, String Address1, String Address2, String Nearest, String Location, String PIN, String Lat,
			String Long, String FirstName, String MiddleName, String LastName, String MobileNumber, String Email1,
			String Password, String Confirmpassword) throws Exception {

		// About Hospital
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Name, Clinicname);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_MobileNumber, Mobnum);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Email, Email);
		Browser.clickOnTheElementByXpath(Elements_Admin.hop_About_Country);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + Country + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.hop_About_State);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__content']/div[contains(., '" + State + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.hop_About_City);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + City + "')]");
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Address1, Address1);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Address2, Address2);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Nearest, Nearest);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Location, Location);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_PIN, PIN);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Lat, Lat);
		Browser.enterTextByXpath(Elements_Admin.Clinic_About_Long, Long);
		Browser.scrollbyxpath(Elements_Admin.Clinic_About_Save);
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_About_Save);
		String abc = Browser.getTextByXpath("//div[@class='custom-success alert alert--dismissible']//p");
		System.out.println("suceess" + abc);
		Thread.sleep(4000);

		// Facilities
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_Facilities);
		Thread.sleep(4000);
		for (int i = 2; i <= 11; i++) {
			System.out.println("Value of x:" + i);
			Browser.clickOnTheElementByXpath("(//div[@class='input-group--selection-controls__ripple'])[" + i + "]");
		}
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_Facility_Save);
		Thread.sleep(4000);

		// SEO
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_SEO);
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_SEO_Save);
		Thread.sleep(3000);

		// Add Admin User
		Browser.clickOnTheElementByXpath(Elements_Admin.Clinic_AddAdminuser);
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath("(//button[@class='synm-btn btn btn--raised theme--dark primary'])[1]");
		Browser.enterTextByXpath(Elements_Admin.Clinic_Adminuser_firstname, FirstName);
		Browser.enterTextByXpath(Elements_Admin.Clinic_Adminuser_middlename, MiddleName);
		Browser.enterTextByXpath(Elements_Admin.Clinic_Adminuser_lastname, LastName);
		Browser.enterTextByXpath(Elements_Admin.Clinic_Adminuser_mobnum, MobileNumber);
		Browser.enterTextByXpath(Elements_Admin.Clinic_Adminuser_Email1, Email1);
		Browser.enterTextByXpath(Elements_Admin.Clinic_Adminuser_pwd, Password);
		Browser.enterTextByXpath(Elements_Admin.Clinic_Adminuser_confirmpwd, Confirmpassword);
		Browser.clickOnTheElementByXpath("(//div[@class='input-group__selections'])[4]"); // role field
		Browser.clickOnTheElementByXpath(
				"//div[@class='list__tile__title' and contains(.,'Doctor Clinic Administrator')]"); // role dropdwn
		Browser.clickOnTheElementByXpath(Elements_Admin.hop_Adminuser_Save);
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to select dropdown value
	 * 
	 * @Parameters : XpathName: Language,Professional Tag,Area Of
	 * Specialization,Line Of Practice, Select Council Type, Doctor Qualification,
	 * Select Medical State Council
	 * 
	 * @Return :
	 */
	public void docRegistration_Dropdowns(String xpathName, String value) throws Exception {
		Browser.clickOnTheElementByXpath("//label[contains(., '" + xpathName + "')]/following-sibling::div//i"); // Language
																													// dropdown
																													// click
		Browser.enterTextByXpath("//label[contains(., '" + xpathName + "')]/following-sibling::div[1]/div/div/input",
				value);
		Thread.sleep(500);
		driver.findElement(
				By.xpath("//label[contains(., '" + xpathName + "')]/following-sibling::div[1]/div/div/input"))
				.sendKeys(Keys.ARROW_DOWN);
		driver.findElement(
				By.xpath("//label[contains(., '" + xpathName + "')]/following-sibling::div[1]/div/div/input"))
				.sendKeys(Keys.ENTER);
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to enter primary info details of doctor
	 * 
	 * @Parameters : Language, FirstName, LastName, Email, mobileNumber, YOS,
	 * Gender, tag, Specialization, LOP, imgPath
	 * 
	 * @Return :
	 */
	public void doctor_PrimaryInformation(String Language, String FirstName, String LastName, String Email,
			String mobileNumber, String YOS, String Gender, String tag, String Specialization, String LOP,
			String imgPath) throws Exception {
		docRegistration_Dropdowns("Language", Language);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PrimaryInfo_salutation);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PrimaryInfo_salutationName);
		Browser.clickOnTheElementByID(Elements_Admin.doc_PrimaryInfo_FirstName); // To close language dropdown
		Browser.enterTextByID(Elements_Admin.doc_PrimaryInfo_FirstName, FirstName);
		Browser.enterTextByID(Elements_Admin.doc_PrimaryInfo_lastName, LastName);
		Browser.enterTextByID(Elements_Admin.doc_PrimaryInfo_Email, Email);
		Browser.enterTextByID(Elements_Admin.doc_PrimaryInfo_MobileNumber, mobileNumber);
		Browser.removeReadOnly_byXpath(Elements_Admin.doc_PrimaryInfo_YearsOfExperience); // YOS readonly remove
		Browser.enterTextByXpath(Elements_Admin.doc_PrimaryInfo_YearsOfExperience, YOS); // YYYY-MM-DD
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PrimaryInfo_GenderBtn); // Gender dropdown click
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + Gender + "')]"); // Click
																													// selected
																													// Gender
		docRegistration_Dropdowns("Professional Tag", tag);
		docRegistration_Dropdowns("Area Of Specialization", Specialization);
		docRegistration_Dropdowns("Line Of Practice", LOP);
		Browser.clickOnTheElementByXpath("//textarea"); // To close active dropdown list.
		driver.findElement(By.id("file")).sendKeys(System.getProperty("user.dir") + imgPath);
		Browser.waitFortheElementXpath("//img[@alt='image']"); // Image display on page.
		Browser.scrollbyxpath(Elements_Admin.doc_PrimaryInfo_Save); // Scroll to Save
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PrimaryInfo_Save); // Click on save
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to enter registration verification info
	 * details of doctor
	 * 
	 * @Parameters : councilName, Qualification, stateCouncilName, medNum
	 * 
	 * @Return :
	 */
	public void doctor_RegistrationVerification(String councilName, String Qualification, String stateCouncilName,
			String medNum) throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_RegistrationTab); // Clicks on Registration tab
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_RegistrationTab);
		Thread.sleep(1000);
		docRegistration_Dropdowns("Select Council Type", councilName);
		docRegistration_Dropdowns("Doctor Qualification", Qualification);
		docRegistration_Dropdowns("Select Medical State Council", stateCouncilName);
		Browser.clickOnTheElementByID(Elements_Admin.doc_Registration_MedRegNum); // Click to close active dropdown
		Browser.enterTextByID(Elements_Admin.doc_Registration_MedRegNum, medNum);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_Registration_Save); // Click on save
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to enter Practice info details of doctor
	 * 
	 * @Parameters : clinicName, roleName
	 * 
	 * @Return :
	 */
	public void doctor_PracticeInfo(String roleName, String clinicName) throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PracticeInfoTab); // Clicks on Practice Info Tab
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PracticeInfo_AddOtherClinicBtn); // Clicks on add other
																								// clinic button
		waitUntil_LoaderInvisible("(//img[@class='loaderImage'])[1]");
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PracticeInfo_roleBtn); // role dropdown click
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + roleName + "')]"); // Click
																													// selected
																													// Gender
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PracticeInfo_ClinicName);

		for (int i = 0; i <= clinicName.length() - 1; i++) {
			char key = clinicName.charAt(i);
			WebDriverWait wait = (new WebDriverWait(driver, 60));
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(Elements_Admin.doc_PracticeInfo_ClinicName)));
			Thread.sleep(500);
			driver.findElement(By.xpath(Elements_Admin.doc_PracticeInfo_ClinicName)).sendKeys(Character.toString(key));
			System.out.println("Texted = " + clinicName);
			Reporter.log("Text Entered=" + clinicName);
			Thread.sleep(600);
		}

		// Browser.enterTextByXpath(Elements_Admin.doc_PracticeInfo_ClinicName,
		// clinicName);
		Thread.sleep(500);
		driver.findElement(By.xpath(Elements_Admin.doc_PracticeInfo_ClinicName)).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(By.xpath(Elements_Admin.doc_PracticeInfo_ClinicName)).sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		// Browser.clickOnTheElementByXpath("//div[@class='card__text']/div/input");
		Browser.clickOnTheElementByXpath("//div[@class='card__text']//div[contains(., '" + clinicName + "')]//input"); // clinic
																														// radio
																														// button
																														// click
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PracticeInfo_AddClinic_SaveBtn); // Clinic pop up save click
//		Browser.waitFortheElementXpath("(//div[@class='card__text']/div/input)[2]"); // Wait for load invisible
//		Browser.scrollbyxpath("(//button[@class='btn btn--raised theme--dark error'])[1]"); //Scroll to close button
//		Browser.clickOnTheElementByXpath(Elements_Admin.doc_PracticeInfo_PopUP_CancelBtn); // Click on cancel to close popup
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to split time by comma for work days info
	 * of doctor
	 * 
	 * @Parameters : Value
	 * 
	 * @Return : array of values
	 */
	public String[] getTime(String value) {
		String a = value;
		String[] b = a.split(",");
		return b;
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to enter work days info of doctor
	 * 
	 * @Parameters : MON_Morning_Start, MON_Afternoon_Start, MON_Evening_Start,
	 * MON_Night_Start, MON_Morning_End, MON_Afternoon_End, MON_Evening_End,
	 * MON_Night_End
	 * 
	 * @Return :
	 */
	public void doctor_workDaysAndTimings(String clinicName, String MON_Morning_Start, String MON_Morning_End,
			String MON_Afternoon_Start, String MON_Afternoon_End, String MON_Evening_Start, String MON_Evening_End,
			String MON_Night_Start, String MON_Night_End) throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_WorkDaysTab); // Clicks on Timings tab
		Browser.waitFortheElementXpath("//span[@class='black--text' and contains(., '" + clinicName + "')]"); // Wait
																												// for
																												// clinic
																												// name
		Thread.sleep(4000);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_WorkDays_Weekly); // Click weekly radio button
		Browser.waitFortheElementXpath("(//span[contains(., 'DAYS')])[1]");

		// ALL DAYS CHECK BOX
		driver.findElement(By.xpath("//label[@for='MON']")).click(); // Click on monday
		driver.findElement(By.xpath("//label[@for='TUE']")).click(); // Click on Tuesday
		driver.findElement(By.xpath("//label[@for='WED']")).click(); // Click on Wednesday
		driver.findElement(By.xpath("//label[@for='THU']")).click(); // Click on Thursday
		driver.findElement(By.xpath("//label[@for='FRI']")).click(); // Click on Friday
		driver.findElement(By.xpath("//label[@for='Timings']")).click(); // Timings same for all days checkbox

		// LOOP TO ENTER TIME SLOTS
		String[] DayStarts = { "MON_MORNING_start_DOCTOR_CONSULTATION_CLINIC",
				"MON_AFTERNOON_start_DOCTOR_CONSULTATION_CLINIC", "MON_EVENING_start_DOCTOR_CONSULTATION_CLINIC",
				"MON_NIGHT_start_DOCTOR_CONSULTATION_CLINIC" }; // Start Xpaths
		String[] DayEnds = { "MON_MORNING_end_DOCTOR_CONSULTATION_CLINIC",
				"MON_AFTERNOON_end_DOCTOR_CONSULTATION_CLINIC", "MON_EVENING_end_DOCTOR_CONSULTATION_CLINIC",
				"MON_NIGHT_end_DOCTOR_CONSULTATION_CLINIC" }; // End Xpaths
		String[] day_Start_Data = { MON_Morning_Start, MON_Afternoon_Start, MON_Evening_Start, MON_Night_Start }; // Start
																													// data
		String[] day_End_Data = { MON_Morning_End, MON_Afternoon_End, MON_Evening_End, MON_Night_End }; // End data
		String[] session_Start, session_end;
		String s_hrs, s_Min, s_Session, e_hrs, e_Min, e_Session;
		for (int i = 0; i <= DayStarts.length - 1 && i <= DayEnds.length - 1; i++) {
			session_Start = getTime(day_Start_Data[i]);
			s_hrs = session_Start[0];
			s_Min = session_Start[1];
			s_Session = session_Start[2];
			session_end = getTime(day_End_Data[i]);
			e_hrs = session_end[0];
			e_Min = session_end[1];
			e_Session = session_end[2];
			// driver.findElement(By.xpath(".//*[@id='physical_consult']/div[1]/div/div[2]/div[3]/div/table/tbody/tr["+i+1+"]/td[2]/div/div[1]/div/div[2]"));
			driver.findElement(By.xpath("//input[@id='" + DayStarts[i] + "']")).click();
			Browser.waitFortheElementXpath(
					"//input[@id='" + DayStarts[i] + "']/following-sibling::div[@class='dropdown']");
			Browser.clickOnTheElementByXpath("//input[@id='" + DayStarts[i]
					+ "']/following-sibling::div/div/ul[@class='hours']/li[contains(., '" + s_hrs + "')]");
			Browser.clickOnTheElementByXpath("//input[@id='" + DayStarts[i]
					+ "']/following-sibling::div/div/ul[@class='minutes']/li[contains(., '" + s_Min + "')]");
			Browser.clickOnTheElementByXpath("//input[@id='" + DayStarts[i]
					+ "']/following-sibling::div/div/ul[@class='apms']/li[contains(., '" + s_Session + "')]");
			driver.findElement(By.cssSelector("div.time-picker-overlay")).click();
			System.out.println("Entered start slots of " + DayStarts[i]);
			driver.findElement(By.xpath("//input[@id='" + DayEnds[i] + "']")).click();
			Browser.waitFortheElementXpath(
					"//input[@id='" + DayEnds[i] + "']/following-sibling::div[@class='dropdown']");
			Browser.clickOnTheElementByXpath("//input[@id='" + DayEnds[i]
					+ "']/following-sibling::div/div/ul[@class='hours']/li[contains(., '" + e_hrs + "')]");
			Browser.clickOnTheElementByXpath("//input[@id='" + DayEnds[i]
					+ "']/following-sibling::div/div/ul[@class='minutes']/li[contains(., '" + e_Min + "')]");
			Browser.clickOnTheElementByXpath("//input[@id='" + DayEnds[i]
					+ "']/following-sibling::div/div/ul[@class='apms']/li[contains(., '" + e_Session + "')]");
			driver.findElement(By.cssSelector("div.time-picker-overlay")).click();
			System.out.println("Entered end slots of " + DayEnds[i]);
		}

		Browser.scrollbyxpath("(//button[@class='#1b5e20 green darken-2 white--text btn btn--raised'])[1]"); // TO save
																												// button
		Browser.clickOnTheElementByXpath("(//button[@class='#1b5e20 green darken-2 white--text btn btn--raised'])[1]"); // Click
																														// save
																														// button
		Browser.waitforTextbyxpath("//div[@class='alert alert-success' and contains(., 'Posted Succesfully!')]",
				"Posted Succesfully!"); // Success message
		Thread.sleep(5000);
		//Browser.scrollbyxpath("//h5[contains(., 'Doctor Registration')]"); // Scroll to menu options
		JavascriptExecutor js = (JavascriptExecutor) driver;
		 js.executeScript("window.scrollBy(0,-350)", "");
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to enter fee info of doctor
	 * 
	 * @Parameters : clinicName, ZFC, followup Days, general fee, followup fee, ivf
	 * Fee, ivf follow up fee
	 * 
	 * @Return :
	 */
	public void doctor_feeStructure(String clinicName, String zfc, String followUpDays, String generalFee,
			String followUpFee, String ivfFee, String ivfFollowUpFee) throws Exception {
		//waitUntil_LoaderInvisible("(//img[@class='loaderImage'])[1]");
		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_FeeStructureTab); // Click on fee tab
		Browser.waitFortheElementXpath("(//td[contains(., '" + clinicName + "')])[2]"); // Wait for clinic name in fee
																						// window
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath("(//td[contains(., '" + clinicName + "')])[2]/following-sibling::td/i"); // Click
																													// on
																													// edit
																													// button
		Browser.waitFortheElementXpath("//div[@class='clinic-top-section']/h4[contains(., '" + clinicName + "')]"); // Wait
																													// for
																													// clinic
																													// name
																													// on
																													// fee
																													// pop
																													// up
		Browser.enterTextByXpath(Elements_Admin.doc_FeeStructure_ZFC, zfc); // Enter ZFC
		Browser.enterTextByName(Elements_Admin.doc_FeeStructure_FollupDays, followUpDays); // Enter follow up days
		Browser.enterTextByXpath(Elements_Admin.doc_FeeStructure_GeneralFee, generalFee); // Enter general fee
		Browser.enterTextByXpath(Elements_Admin.doc_FeeStructure_FollupFee, followUpFee); // Enter followup fee
		Browser.enterTextByXpath(Elements_Admin.doc_FeeStructure_ivfFee, ivfFee); // Enter ivf fee
		Browser.enterTextByXpath(Elements_Admin.doc_FeeStructure_ivfFollowUpFee, ivfFollowUpFee); // Enter followup ivf
																									// fee
		Browser.scrollbyxpath(Elements_Admin.doc_FeeStructure_ivfFollowUpFee);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_FeeStructure_Save); // Click on save fee button
		Browser.scrollbyxpath("(//td[contains(., '" + clinicName + "')])[2]");
		Browser.waitFortheElementXpath("//p[contains(., 'physical fee saved success.')]"); // Success message wait
		/*Browser.scrollbyxpath(Elements_Admin.doc_FeeStructure_Close);
		
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_FeeStructure_Close); // Click close button to close pop up
		
		Browser.scrollbyxpath("//h5[contains(., 'Doctor Registration')]"); // Scroll to menu options
*/	
		}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used click seo save and generate slug
	 * 
	 * @Parameters :
	 * 
	 * @Return :
	 */
	public void doctor_SEO() throws Exception {
		Thread.sleep(4000);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_SEOtab);
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_SEO_Save);
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used click additional info and validate and
	 * move to que
	 * 
	 * @Parameters :
	 * 
	 * @Return :
	 */
	public void doctor_AdditionalInformation() throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_AdditionalInfoTab); // Click on additional info tab
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_AdditionalInfo_AwardsTab); // Click on awards tab
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_AdditionalInfo_AwardsTab_ValidateBtn); // Click on validate
																									// button
		Browser.waitforTextbyxpath(
				"//div[@class='alert alert-success' and contains(., 'Validate and move queue Updated Successfully.')]",
				"Validate and move queue Updated Successfully."); // Success confirmation
		Thread.sleep(5000);
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to add user in user management.
	 * 
	 * @Parameters : firstname, lastname, email, number, password and role
	 * 
	 * @Return :
	 */
	public void userManagement_AddUser(String firstname, String lastname, String email, String number, String password,
			String Role) throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.userManagement_addUser);
		Browser.enterTextByXpath(Elements_Admin.userManagement_firstName, firstname);
		Browser.enterTextByXpath(Elements_Admin.userManagement_lastName, lastname);
		Browser.enterTextByXpath(Elements_Admin.userManagement_Email, email);
		Browser.enterTextByXpath(Elements_Admin.userManagement_mobileNumber, number);
		Browser.enterTextByXpath(Elements_Admin.userManagement_password, password);
		Browser.scrollbyxpath(Elements_Admin.userManagement_password);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//div[@id='" + Role + "']/div/div");
		Browser.clickOnTheElementByXpath(Elements_Admin.userManagement_Add);
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to make ZoyloEmployee true for user added
	 * in user management.
	 * 
	 * @Parameters : email
	 * 
	 * @Return :
	 */
	public void usersTab_makeZoyloUser(String number, String email) throws Exception {
		AdminMenu_subMenu("False", "True", "Users");
		Browser.enterTextByXpath(Elements_Admin.users_SearchBar, number);
		Thread.sleep(500);
		driver.findElement(By.xpath(Elements_Admin.users_SearchBar)).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(By.xpath(Elements_Admin.users_SearchBar)).sendKeys(Keys.ENTER);
		Browser.scrollbyxpath(Elements_Admin.users_SearchBar);
		String tableEmail = Browser.getTextByXpath("//tbody/tr/td[7]"); // verify email id on table
		Assert.assertTrue(tableEmail.contains(email));
		Browser.scrollbyxpath(Elements_Admin.users_editBtn);
		Browser.scrollbyxpath(Elements_Admin.users_SearchBar);
		Browser.clickOnTheElementByXpath(Elements_Admin.users_editBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.users_zoyloEmpBtn);
		Browser.clickOnTheElementByXpath(Elements_Admin.users_updateBtn);
		Browser.waitFortheElementXpath(Elements_Admin.users_SearchBar);
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to select session for reschedule.
	 * 
	 * @Parameters : Session - Morning, Afternoon, Evening, Night
	 * 
	 * @Return :
	 */
	public void reschedule_sessionSelect(String session) {
		Browser.waitFortheElementXpath(Elements_Admin.doc_appointmentLog_rescheduleSessions); // Time slot tab
		if (session.contains("Morning")) {
			Browser.clickOnTheElementByXpath(
					"(//div[@class='layout' and @data-v-72ae89aa='']//input[@data-v-72ae89aa=''])[1]"); // Morning
		} else if (session.contains("Afternoon")) {
			Browser.clickOnTheElementByXpath(
					"(//div[@class='layout' and @data-v-72ae89aa='']//input[@data-v-72ae89aa=''])[2]"); // Afternoon
		} else if (session.contains("Evening")) {
			Browser.clickOnTheElementByXpath(
					"(//div[@class='layout' and @data-v-72ae89aa='']//input[@data-v-72ae89aa=''])[3]"); // Evening
		} else if (session.contains("Night")) {
			Browser.clickOnTheElementByXpath(
					"(//div[@class='layout' and @data-v-72ae89aa='']//input[@data-v-72ae89aa=''])[4]"); // Night
		}
		Browser.clickOnTheElementByXpath("(//div[@id='checbox-container']/ul/li/input[@data-v-72ae89aa=''])[1]"); // Session
																													// time
																													// slot
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_appointmentLog_recSave);
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to search and click doctor name in linking
	 * queue.
	 * 
	 * @Parameters : doctorName
	 * 
	 * @Return :
	 */
	public void linkingQueue_Search_click_Doctor(String doctorName) throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.AppManagement_LinkingQueTab);
		Thread.sleep(1000);
		if (driver.findElements(By.xpath("//img[@class='loaderImage']")).size() > 0) {
			waitUntil_LoaderInvisible("//img[@class='loaderImage']");
		}

		for (int i = 0; i <= doctorName.length() - 1; i++) {
			char key = doctorName.charAt(i);
			WebDriverWait wait = (new WebDriverWait(driver, 60));
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(Elements_Admin.AppManagement_LinkingQue_SearchBar)));
			Thread.sleep(500);
			driver.findElement(By.xpath(Elements_Admin.AppManagement_LinkingQue_SearchBar))
					.sendKeys(Character.toString(key));
			System.out.println("Texted = " + doctorName);
			Reporter.log("Text Entered=" + doctorName);
			Thread.sleep(600);
		}

		driver.findElement(By.xpath(Elements_Admin.AppManagement_LinkingQue_SearchBar)).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(By.xpath(Elements_Admin.AppManagement_LinkingQue_SearchBar)).sendKeys(Keys.ENTER);
		Browser.waitFortheElementXpath("(//td[contains(., '" + doctorName + "')])[1]"); // Wait for table of doctor
		Browser.clickOnTheElementByXpath("(//td[contains(., '" + doctorName + "')])[1]");
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to search by clinic name in clinic tab of
	 * app management.
	 * 
	 * @Parameters : clinicName
	 * 
	 * @Return :
	 */
	public void clinicTab_search(String clinicName) throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.AppManagement_ClinicTab);

		for (int i = 0; i <= clinicName.length() - 1; i++) {
			char key = clinicName.charAt(i);
			WebDriverWait wait = (new WebDriverWait(driver, 60));
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath(Elements_Admin.AppManagement_ClinicTab_Search)));
			Thread.sleep(500);
			driver.findElement(By.xpath(Elements_Admin.AppManagement_ClinicTab_Search))
					.sendKeys(Character.toString(key));
			System.out.println("Texted = " + clinicName);
			Reporter.log("Text Entered=" + clinicName);
			Thread.sleep(600);
		}
		driver.findElement(By.xpath(Elements_Admin.AppManagement_ClinicTab_Search)).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(By.xpath(Elements_Admin.AppManagement_ClinicTab_Search)).sendKeys(Keys.ENTER);
		Browser.waitFortheElementXpath("(//td[contains(., '" + clinicName + "')])[1]"); // Wait for table of clinic
		Browser.clickOnTheElementByXpath("(//td[contains(., '" + clinicName + "')])[1]");
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to search by hospital name in hospital tab
	 * of app management.
	 * 
	 * @Parameters : hospitalName
	 * 
	 * @Return :
	 */
	public void hospitalTab_search(String hospitalName) throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.AppManagement_HospitalTab);

		for (int i = 0; i <= hospitalName.length() - 1; i++) {
			char key = hospitalName.charAt(i);
			WebDriverWait wait = (new WebDriverWait(driver, 60));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(.//*[@id='locationSearch'])[5]")));
			Thread.sleep(500);
			driver.findElement(By.xpath("(.//*[@id='locationSearch'])[5]")).sendKeys(Character.toString(key));
			System.out.println("Texted = " + hospitalName);
			Reporter.log("Text Entered=" + hospitalName);
			Thread.sleep(600);
		}
		driver.findElement(By.xpath("(.//*[@id='locationSearch'])[5]")).sendKeys(Keys.ARROW_DOWN);
		driver.findElement(By.xpath("(.//*[@id='locationSearch'])[5]")).sendKeys(Keys.ENTER);
		Browser.waitFortheElementXpath("(//td[contains(., '" + hospitalName + "')])[1]"); // Wait for table of clinic
		Browser.clickOnTheElementByXpath("(//td[contains(., '" + hospitalName + "')])[1]");
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to select payment type in payment reports
	 * tab.
	 * 
	 * @Parameters : paymentType
	 * 
	 * @Return :
	 */
	public void paymentReports_paymentTypeClick(String paymentType) {
		Browser.clickOnTheElementByXpath(Elements_Admin.payment_Type);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + paymentType + "')]");
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to add doctor appointment in appointment
	 * logs
	 * 
	 * @Parameters : number, firstName, lastName, email, entityName, date, session,
	 * time, paymentMode
	 * 
	 * @Return :
	 */
	public void appointmentLogs_addAppointment_Doctor(String number, String firstName, String lastName, String email,
			String location, String doctorName, String entityName, String date, String session, String time,
			String paymentMode) throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_appointmentLog_addAppointmentBtn);
		Browser.waitFortheElementXpath(Elements_Admin.doc_appointmentLog_addAppointmentHeader);
		Browser.enterTextByID(Elements_Admin.doc_appointmentLog_addAppointment_phoneNumber, number);
		Browser.enterTextByID(Elements_Admin.doc_appointmentLog_addAppointment_firstName, firstName);
		Browser.enterTextByID(Elements_Admin.doc_appointmentLog_addAppointment_lastName, lastName);
		Browser.enterTextByID(Elements_Admin.doc_appointmentLog_addAppointment_email, email);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_appointmentLog_addAppointmentGender);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains (., 'Male')]");
		Thread.sleep(1000);
		Browser.enterTextByID(Elements_Admin.doc_appointmentLog_addAppontment_location, location);
		Browser.clickOnTheElementByXpath(Elements_Recipients.home_locationSearch_Suggestion);
		Thread.sleep(4000);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_appointmentLog_addAppointment_doctor);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("(//ul[@class='list'])[1]/li[contains(., '" + doctorName + "')]");
		Browser.clickOnTheElementByID(Elements_Admin.doc_appointmentLog_addAppointment_search);
		System.out.println("Selected doctor and clicked on search to fetch practice locations.");

		// Select provider location and time
		Browser.waitFortheElementXpath(Elements_Admin.doc_appointmentLog_addAppointmentClinicHeader);
		Browser.clickOnTheElementByXpath("//div/span[contains(., '" + entityName + "')]/preceding-sibling::input");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_appointmentLog_addAppointmentDate);
		Thread.sleep(1000);
		Browser.waitFortheElementXpath(Elements_Admin.doc_qppointmentLog_addAppointmentDatePicker);
		Browser.clickOnTheElementByXpath(
				"(//div[@class='vdp-datepicker__calendar'])[1]/div/span[contains(., '" + date + "')]");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_appointmentLog_addAppointmentSession);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + session + "')]");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_appointmentLog_addAppointmentTimeSlot);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains (., '" + time + "')]");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//input[@value='" + paymentMode + "']");
		Browser.clickOnTheElementByXpath(Elements_Admin.doc_appointmentLog_addAppointmentSave);
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to select non kellton appointments menu
	 * and search doctor appointment.
	 * 
	 * @Parameters : filterValue (Booking Date or Appointment Date), Status (Waiting
	 * etc)
	 * 
	 * @Return :
	 */
	public void doctorAppointmentSearch_appointmentsMenu(String filterBy, String status) throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_AppointmentMenu);
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Appointments_Doctor);
		// Filter By
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Appointments_Doctor_FilterBy);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + filterBy + "')]");
		Thread.sleep(1000);
		// Status
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Appointments_Doctor_Status);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + status + "')]");
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Appointments_Doctor_Search);
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to navigate to doctor enrolment listing
	 * page
	 * 
	 * @Parameters :
	 * 
	 * @Return :
	 */
	public void doctorEnrolmentListingPage() {
		Browser.clickOnTheElementByXpath(Elements_Admin.enrolmentListing_Menu);
		Browser.clickOnTheElementByXpath(Elements_Admin.enrolmentListing_doctorTab);
	}

	// ADMIN Diagnostic Methods Start

	/*
	 * @Author : Ch.LakshmiKanth
	 * 
	 * @Description : This method is used to Logout the admin
	 * 
	 * @Parameters :
	 * 
	 * @Return :
	 */

	public void Admin_Logout() {

		Browser.clickOnTheElementByXpath("(//div[@class='toolbar__title'])[2]");
		Browser.clickOnTheElementByXpath("(//div[contains(., 'Logout')])[5]");
	}

	/*
	 * @Author : Ch.LakshmiKanth
	 * 
	 * @Description : This method is used to add Cashback ,Promotional, refund
	 * amount in admin
	 * 
	 * @Parameters : RecipientPhno, CreditType, appointmentId, amount, comments
	 * 
	 * @Return :
	 */

	public void Wallet_CreditWallet(String RecipientPhno, String CreditType, String appointmentId, String amount,
			String comments) throws Exception {
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Wallet_CreditWalletMenu);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Wallet_CreditWallet_Search);
		Browser.enterTextByXpath(Elements_Admin.Admin_Wallet_CreditWallet_Search, RecipientPhno);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Wallet_CreditWallet_Search_Suggession);
		Browser.waitFortheElementXpath("//div[@class='flex xs2' and contains(., '" + RecipientPhno + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Wallet_CreditWallet_CreditType);
		Browser.clickOnTheElementByXpath(
				"(//div[@class='list__tile__content']/div[contains(., '" + CreditType + "')])[1]");
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Wallet_CreditWallet_AppointmentID);
		Browser.enterTextByXpath(Elements_Admin.Admin_Wallet_CreditWallet_AppointmentID, appointmentId);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Wallet_CreditWallet_Amount);
		Browser.enterTextByXpath(Elements_Admin.Admin_Wallet_CreditWallet_Amount, amount);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Wallet_CreditWallet_Comments);
		Browser.enterTextByXpath(Elements_Admin.Admin_Wallet_CreditWallet_Comments, comments);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Wallet_CreditWallet_Save);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Wallet_CreditWallet_Save_Confirm);
		Thread.sleep(2000);
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to fetch user info in wallet users
	 * 
	 * @Parameters : userPhoneNum
	 * 
	 * @Return :
	 */
	public void WalletUsers(String userPhoneNum) throws Exception {
		Browser.ScrollUp();
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Wallet_WalletUsersMenu);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Wallet_WalletUsers_Search);
		Browser.enterTextByXpath(Elements_Admin.Admin_Wallet_WalletUsers_Search, userPhoneNum);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Wallet_CreditWallet_Search_Suggession);
		Browser.waitFortheElementXpath("//div[@class='flex xs2' and contains(., '" + userPhoneNum + "')]");
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used to search user and enter transaction ID
	 * 
	 * @Parameters : userPhoneNum, transactionID
	 * 
	 * @Return :
	 */
	public void Wallet_Debit(String userPhoneNum, String transactionID) throws Exception {
		Browser.ScrollUp();
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Wallet_DebitMenu);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_Wallet_DebitSearch);
		Browser.enterTextByXpath(Elements_Admin.Admin_Wallet_DebitSearch, userPhoneNum);
		Browser.clickOnTheElementByXpath(".//*[@id='debitWallet']/div/div[2]/div[3]/div/ul/li");
		Browser.waitFortheElementXpath(
				"//div[@id='debitWallet']//div[@class='flex xs2' and contains(., '" + userPhoneNum + "')]");
		Browser.enterTextByXpath(Elements_Admin.Admin_Wallet_DebitTrnsID, transactionID);
	}

	// Methods for Creation of Wellness Center

	/*
	 * @Author : Ch.LakshmiKanth
	 * 
	 * @Description : This method is used to add about Welnees Center Details
	 * 
	 * @Parameters : wellnessname,mobileno,email
	 * 
	 * @Return :
	 */

	public void AboutWellnesscenter(String wellnessname, String mobileno, String email) throws Exception {

		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_Wellnessname, wellnessname);
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_Mobileno, mobileno);
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_email, email);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_ClickCountry);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_SelectCountry);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_ClickState);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_SelectState);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_ClickCity);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_SelectCity);
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_AddressLineOne,
				"White House, 15, Subhash Road");
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_AddresssLineTwo,
				"Behind St. Joseph's Academy");
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_LandMark,
				"Behind St. Joseph's Academy");
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_SubArea, "Uttarakhand");
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_Pincode, "248001");
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_Latitude, "30.3165");
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_Longitude, "78.0322");
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_AboutWellnessMenu_Save);
	}

	/*
	 * @Author : Ch.LakshmiKanth
	 * 
	 * @Description : This method is used to add Facilities in Welnees Center
	 * 
	 * @Parameters :
	 * 
	 * @Return :
	 */

	public void FacilitiesInWellnessCenter() {

		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_FacilitiesMenu_CarParking);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_FacilitiesMenu_BikeParking);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_FacilitiesMenu_Ambulance);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_FacilitiesMenu_CreditCard);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_FacilitiesMenu_DeditCard);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_FacilitiesMenu_Save);

	}

	/*
	 * @Author : Ch.LakshmiKanth
	 * 
	 * @Description : This method is used to add Timings in Welnees Center
	 * 
	 * @Parameters : Timings_Start,Timings_End
	 * 
	 * @Return :
	 */
	public void AddTimingsInWellnessCenter(String Timings_Start, String Timings_End) {
		Browser.waitFortheElementXpath("(//label[contains(., 'Active')])[2]");
		Browser.clickOnTheElementByXpath("(//label[contains(., 'Active')])[2]");
		Browser.clickOnTheElementByXpath("//label[contains(., 'Timings same for every day')]");
		String[] HomeVisit_days = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
		for (int i = 0; i <= HomeVisit_days.length - 1; i++) {
			Browser.clickOnTheElementByXpath(
					"(//div[@aria-label='" + HomeVisit_days[i] + "']//following-sibling::div/div)[1]");
		}

		// Timings MON Start Split and Entering
		String[] Timings_MON_Start = { Timings_Start, Timings_End };
		String Timings_OPEN = Timings_MON_Start[0];
		String[] HomeVisit_time = Timings_OPEN.split(",");
		String Timings_OPEN_Hours = HomeVisit_time[0];
		String Timings_OPEN_Mints = HomeVisit_time[1];
		String Timings_OPEN_Session = HomeVisit_time[2];
		Browser.clickOnTheElementByXpath("//input[@id='labMON_start_clinic']");
		Browser.clickOnTheElementByXpath(
				"//input[@id='labMON_start_clinic']/following-sibling::div/div/ul[@class='hours']/li[contains(., '"
						+ Timings_OPEN_Hours + "')]");
		Browser.clickOnTheElementByXpath(
				"//input[@id='labMON_start_clinic']/following-sibling::div/div/ul[@class='minutes']/li[contains(., '"
						+ Timings_OPEN_Mints + "')]");
		Browser.clickOnTheElementByXpath(
				"//input[@id='labMON_start_clinic']/following-sibling::div/div/ul[@class='apms']/li[contains(., '"
						+ Timings_OPEN_Session + "')]");
		driver.findElement(By.cssSelector("div.time-picker-overlay")).click();

		// Timings MON End Time Split and Entering
		String Timings_CLOSE = Timings_MON_Start[1];
		String[] Timings_CLOSE_time = Timings_CLOSE.split(",");
		String Timings_CLOSE_Hours = Timings_CLOSE_time[0];
		String Timings_CLOSE_Mints = Timings_CLOSE_time[1];
		String Timings_CLOSE_Session = Timings_CLOSE_time[2];
		Browser.clickOnTheElementByXpath("//input[@id='labMON_end_clinic']");
		Browser.clickOnTheElementByXpath(
				"//input[@id='labMON_end_clinic']/following-sibling::div/div/ul[@class='hours']/li[contains(., '"
						+ Timings_CLOSE_Hours + "')]");
		Browser.clickOnTheElementByXpath(
				"//input[@id='labMON_end_clinic']/following-sibling::div/div/ul[@class='minutes']/li[contains(., '"
						+ Timings_CLOSE_Mints + "')]");
		Browser.clickOnTheElementByXpath(
				"//input[@id='labMON_end_clinic']/following-sibling::div/div/ul[@class='apms']/li[contains(., '"
						+ Timings_CLOSE_Session + "')]");
		driver.findElement(By.cssSelector("div.time-picker-overlay")).click();
		Browser.clickOnTheElementByXpath("(//div[text()='Save'])[3]");

	}

	/*
	 * @Author : Ch.LakshmiKanth
	 * 
	 * @Description : This method is used to add Admin User in Welnees Center
	 * 
	 * @Parameters : firstname,middlename,lastname, mobile,email,
	 * password,confirmpassword
	 * 
	 * @Return :
	 */
	public void WellnessCenter_AddAdminUser(String firstname, String middlename, String lastname, String mobile,
			String email, String password, String confirmpassword) throws Exception {

		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AddAdminUserMenu_firstname, firstname);
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AddAdminUserMenu_middlename, middlename);
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AddAdminUserMenu_lastname, lastname);
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AddAdminUserMenu_mobile, mobile);
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AddAdminUserMenu_email, email);
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AddAdminUserMenu_Password, password);
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_AddAdminUserMenu_ConfirmPassword, confirmpassword);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_AddAdminUserMenu_ClickRolename);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_AddAdminUserMenu_SelectRoleName);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_AddAdminUserMenu_Save);
	}

	/*
	 * @Author : Ch.LakshmiKanth
	 * 
	 * @Description : This method is used to add Packages in Welnees Center
	 * 
	 * @Parameters :
	 * 
	 * @Return :
	 */
	public void WellnessCenter_Packages(String Packagename, String desc, String price, String zfc, String Percentage)
			throws Exception {

		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_PackagesMenu_Packagename, Packagename);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_PackagesMenu_ClickPackageCategory);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_PackagesMenu_SelectPackageCategory);
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_PackagesMenu_Description, desc);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_PackagesMenu_SelectFixedPrice);
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_PackagesMenu_EnterFixedPrice, price);
		Browser.enterTextByXpath(Elements_Admin.Admin_WellnessTab_PackagesMenu_ZFC, zfc);
		Browser.enterTextByXpath("//label[contains(text(),'Discount Percentage')]/following-sibling::div[1]/input",
				Percentage);
		Browser.clickOnTheElementByXpath(Elements_Admin.Admin_WellnessTab_PackagesMenu_Save);

	}

	/*
	 * @Author : Ch.LakshmiKanth
	 * 
	 * @Description : This method is to Select the Package Name of Wellness Add
	 * Appointment
	 * 
	 * @Parameters : PackageName
	 * 
	 * @Return :
	 */

	public void Wellness_Search_PackageName(String PackageName, String PaymentType) throws Exception {

		Browser.enterTextByXpath("//input[@placeholder='search package']", PackageName);
		Browser.clickOnTheElementByXpath("//div[@class='flex sm3']//div//div[@class='btn__content']");
		String date = Browser.getModifiedDate(1);
		Browser.clickOnTheElementByXpath("//span[text()='" + date + "']");
		Browser.clickOnTheElementByXpath("//a[@class='re-green-btn popup-btn']");
		Browser.clickOnTheElementByXpath("//input[@value='" + PaymentType + "']");
		Browser.clickOnTheElementByXpath("//span[contains(@class,'re-green-btn confirm')]");
	}

	// ********************************* Zoylo Lab Methods
	// *********************************

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method will navigte to add appointment list page
	 * 
	 * @Parameters : NA
	 * 
	 * @Return : NA
	 */
	public void lab_addAppointmentList_navigate() throws Exception {
		Browser.waitFortheElementXpath("//div[text()='Zoylo Admin']"); // AdminTag
		Browser.openUrl(LoadProp.EnvironmentAdminURL + "/admin/zoyloLabAppointmentList");
		Browser.waitFortheElementXpath(Elements_Admin.lab_addAppointment_addBtn);
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method will click on add Appointment button and wait for
	 * listing header
	 * 
	 * @Parameters : NA
	 * 
	 * @Return : NA
	 */
	public void lab_addAppointmentButton_click() throws Exception {
		Thread.sleep(2000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_addBtn);
		if (driver.findElements(By.xpath(Elements_Admin.lab_addAppointment_customerTab)).size() == 0) {
			Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_addBtn);
		}
		Browser.waitFortheElementXpath(Elements_Admin.lab_addAppointment_customerTab);
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method will enter customer info in add lab appointments
	 * 
	 * @Parameters : firstName, email, gender, age, city
	 * 
	 * @Return : pincode
	 */
	public String lab_addAppointment_customerInfo(String firstName, String number, String email, String gender,
			String age, String city) throws Exception {
		String pincode = null;
		Browser.enterTextByXpath(Elements_Admin.lab_addAppointment_customerFisrtName, firstName);
		Browser.enterTextByXpath(Elements_Admin.lab_addAppointment_customerLastName,
				"L" + Browser.generateRandomString(5).toLowerCase());
		Browser.enterTextByXpath(Elements_Admin.lab_addAppointment_customerNumber, number);
		Browser.enterTextByXpath(Elements_Admin.lab_addAppointment_customerEmail, email);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_genderDrp);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + gender + "')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_Source);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., 'Internal Employee')]");
		Browser.enterTextByXpath(Elements_Admin.lab_addAppointment_customerAge, age);
		Browser.enterTextByXpath(Elements_Admin.lab_addAppointment_customerAddress,
				Browser.generateRandomAlphaNumeric(10));
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_cityDrp);
		Browser.enterTextByXpath(Elements_Admin.lab_addAppointment_cityInput, city);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + city + "')]"); // DrpSelect
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_customerPinDrp);
		pincode = Browser.getTextByXpath(Elements_Admin.lab_firstDrpDwnValue); // firstPincodeValue
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and contains(., '" + pincode + "')]");
		Browser.enterTextByXpath(Elements_Admin.docEnrol_landmark, "Landmark");

		return pincode;
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method will choose the next serviceable date and select
	 * slot
	 * 
	 * @Parameters : NA
	 * 
	 * @Return : NA
	 */
	public void lab_addAppointment_serviceDateSlot() throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_serviceDate);
		String currentDate = Browser.getCurrentDate();
		String modDate;
		if (Integer.parseInt(currentDate) >= 30) {
			Thread.sleep(500);
			Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_serviceNextMonthBtn);
			modDate = Browser.getModifiedDate(2);
			Thread.sleep(500);
			Browser.clickOnTheElementByXpath("//td/button/span[text()='" + modDate + "']");
		} else {
			modDate = Browser.getModifiedDate(1);
			Thread.sleep(500);
			Browser.clickOnTheElementByXpath("//td/button/span[text()='" + modDate + "']");
		}

		Thread.sleep(5000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_serviceSlotDrp);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_slotDrp);
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method will add patient and save to patient list in the
	 * add patient popup
	 * 
	 * @Parameters : patient_FirstName, gender
	 * 
	 * @Return : NA
	 */
	public void lab_addAppointment_addPatient(String patient_FirstName, String gender) throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_addPatientBtn);
		Browser.waitFortheElementXpath(Elements_Admin.lab_addAppointment_PatientHeader);
		Browser.enterTextByXpath(Elements_Admin.lab_addAppointment_PatientFirstName, patient_FirstName);
		Browser.enterTextByXpath(Elements_Admin.lab_addAppointment_PAtientLastName,
				Browser.generateRandomString(5).toLowerCase());
		Thread.sleep(500);
		Browser.clickOnTheElementByXpath("//input[@name='Gender' and @value='" + gender + "']");
		Browser.enterTextByXpath(Elements_Admin.lab_addAppointment_PatientEmail,
				Browser.generateRandomString(8).toLowerCase() + "@gmail.com");
		Browser.enterTextByXpath(Elements_Admin.lab_addAppointment_PatientNum, "9" + Browser.generateRandomNumber(9));
		Browser.enterTextByXpath(Elements_Admin.lab_addAppointment_PatientAge, "29");
		Browser.clickOnTheElementByXpath("(" + Elements_Admin.operatingCities_saveBtn + ")[1]");
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method will click on add test/profile and select
	 * test/profile
	 * 
	 * @Parameters : testName or profileName
	 * 
	 * @Return : String servicecost
	 */
	public void lab_addAppointment_selectTestProfile(String serviceName) throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_addTestProfileBtn);
		Browser.waitFortheElementXpath(Elements_Admin.lab_addAppointment_testProfileHeader);
		Browser.enterTextByXpath(Elements_Admin.lab_addAppointment_testProfileSearch, serviceName);
		Browser.waitFortheElementXpath("//tr/td[text()='" + serviceName + "']");
		Thread.sleep(2000);
//		if (driver
//				.findElements(By.xpath(
//						"//tr/td[text()='" + serviceName + "']/following-sibling::td[5]/button/div[text()='Selected']"))
//				.size() != 0) {
//			Browser.clickOnTheElementByXpath(
//					"//tr/td[text()='" + serviceName + "']/following-sibling::td[5]/button/div[text()='Selected']");
//		}
		Browser.clickOnTheElementByXpath(
				"//tr/td[text()='" + serviceName + "']/following-sibling::td[5]/button/div[text()='Select']");
		Browser.waitFortheElementXpath(
				"//tr/td[text()='" + serviceName + "']/following-sibling::td[5]/button/div[text()='Selected X']");
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_appointment_submitBtn);
		Browser.waitFortheElementXpath("//tr/td[text()='" + serviceName + "']");
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method will click on add package and select test
	 * 
	 * @Parameters : packagename
	 * 
	 * @Return : packagecost
	 */
	public String lab_addAppointment_selectPackage(String packageName) throws Exception {
		String packagecost = null;
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_addPackageBtn);
		Browser.waitFortheElementXpath(Elements_Admin.lab_addAppointment_packageHeader);
		Browser.enterTextByXpath(Elements_Admin.lab_addAppointment_serviceSearch, packageName);
		Thread.sleep(1000);
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + packageName + "')]");
		packagecost = Browser.getTextByXpath("//tr/td[contains(., '" + packageName + "')]/following-sibling::td[3]");
//		if (driver.findElements(By.xpath("//tr/td[contains(., '" + packageName
//				+ "')]/following-sibling::td[4]/button/div[contains(., 'Selected')]")).size() != 0) {
//			Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + packageName
//					+ "')]/following-sibling::td[4]/button/div[contains(., 'Selected')]");
//		}
		Browser.clickOnTheElementByXpath("//tr/td[contains(., '" + packageName
				+ "')]/following-sibling::td[4]/button/div[contains(., 'Select')]");
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + packageName
				+ "')]/following-sibling::td[4]/button/div[contains(., 'Selected X')]");
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_appointment_submitBtn);
		return packagecost;
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method will choose payment mode and click on confirm
	 * booking
	 * 
	 * @Parameters : testName or profileName
	 * 
	 * @Return : NA
	 */
	public void lab_addAPpointment_selectPayMode_ConfirmBooking(String payMode) throws Exception {
		Browser.clickOnTheElementByXpath("//input[@value='" + payMode + "']");
		Thread.sleep(3000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_confirmBooking);
		if (driver.findElements(By.xpath(Elements_Admin.lab_addAppointment_addBtn)).size() == 0) {
			Browser.clickOnTheElementByXpath(Elements_Admin.lab_addAppointment_confirmBooking);
		}
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method will search an appointment in list page
	 * 
	 * @Parameters : search data
	 * 
	 * @Return : NA
	 */
	public void lab_addAppointment_searchList(String parameter, String data) throws Exception {
		
		Thread.sleep(3000);

		Browser.clickOnTheElementByXpath(Elements_Admin.labAppointments_magnifierIcon); // magnifierIcon

		Browser.clickOnTheElementByXpath(Elements_Admin.labAppointments_searchParameterDrp); // selectParameter
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='" + parameter + "']"); // chooseParameter

		Browser.enterTextByXpath(Elements_Admin.lab_addAppointment_search, data);
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath(Elements_Admin.lab_appointment_searchSubmitBtn);
		Thread.sleep(1000);
		Browser.waitFortheElementXpath("//tr/td[contains(., '" + data + "')]");
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method will select location and service type in location
	 * based screen
	 * 
	 * @Parameters : city and service type
	 * 
	 * @Return : NA
	 */
	public void locationBased_select_city_servicetype(String location, String serviceType) throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.labLocation_cityDrp); // cityDrp
		Browser.enterTextByXpath(Elements_Admin.labLocation_cityDrpInput, location);
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='" + location + "']");
		Browser.clickOnTheElementByXpath(Elements_Admin.labLocation_serviceDrp); // serviceDrp
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='" + serviceType + "']");
		Browser.clickOnTheElementByXpath(Elements_Admin.labLocation_searchBtn); // searchBtn
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used for searching servicename in location
	 * based screen
	 * 
	 * @Parameters : serviceName
	 * 
	 * @Return : NA
	 */
	public void locationBased_search(String serviceName) throws Exception {
		Browser.clickOnTheElementByXpath(Elements_Admin.labLocation_SearchDrp);
		Browser.enterTextByXpath(Elements_Admin.labLocation_SearchInput, serviceName); // search
		Browser.clickOnTheElementByXpath("//div[@class='list__tile__title' and text()='" + serviceName + "']");
		Browser.waitFortheElementXpath("//div[@class='servicesBlock']/div/div/label[text()='" + serviceName + "']");
	}

	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : This method is used for editing price of a service for
	 * location
	 * 
	 * @Parameters : serviceName
	 * 
	 * @Return : NA
	 */
	public void locationBased_editFinalPrice(String serviceName, String price) throws Exception {
		Browser.clickOnTheElementByXpath("//div[@class='layout formFieldsRow servicesList row wrap']/div[contains(., '"
				+ serviceName + "')]/following-sibling::div/div[contains(., 'Final Price')]");
		for (int i = 0; i < 5; i++) {
			driver.findElement(By.xpath("//div[@class='layout formFieldsRow servicesList row wrap']/div[contains(., '"
					+ serviceName + "')]/following-sibling::div/div[contains(., 'Final Price')]/div/input"))
					.sendKeys(Keys.BACK_SPACE);
		}
		Browser.enterTextByXpath("//div[@class='layout formFieldsRow servicesList row wrap']/div[contains(., '"
				+ serviceName + "')]/following-sibling::div/div[contains(., 'Final Price')]/div/input", price);
		Browser.clickOnTheElementByXpath("//div[@class='servicesBlock']/div/div/label[text()='" + serviceName + "']");
		Browser.clickOnTheElementByXpath("//div[@class='btn__content' and text()='Save']");
		Browser.waitFortheElementXpath("//p[contains(., 'updated successfully')]");
	}
	
	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : Affilates login method
	 * 
	 * @Parameters : mobileNumber, Password
	 * 
	 * @Return : NA
	 */
	public void affiliates_login(String userID, String password) throws Exception {
		Browser.waitFortheElementXpath(Elements_Admin.affiliates_SignInHeader);
		Browser.enterTextByID(Elements_Admin.affiliates_SignInMobile, userID);
		Browser.enterTextByName(Elements_Admin.affiliates_SignInPassword, password);
		Browser.clickOnTheElementByXpath(Elements_Admin.affiliates_LoginBtn);
		Browser.waitFortheElementXpath(Elements_Admin.affiliates_appointmentHeadr);
		System.out.println("Affiliates login suucess.");
	}
	
	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : Affilates customer info details
	 * 
	 * @Parameters : mobileNumber, firstName, LastName, EmailId, Age, gender, city,
	 * date, address, Landmark
	 * 
	 * @Return : slotTime
	 */
	public String affiliates_customerInfo(String mobileNumber, String firstName, String LastName, String EmailId,
			String Age, String gender, String city, String date, String address, String Landmark) throws Exception {
		String slotTime = null;
		Browser.enterTextByID("custMobile", mobileNumber);
		Browser.enterTextByID("firstName", firstName);
		Browser.enterTextByID("LastName", LastName);
		Browser.enterTextByID("EmailId", EmailId);
		Browser.enterTextByID("Age", Age);
		Browser.selectXpathByText("//select[@data-vv-name='gender']", gender);
		Browser.selectXpathByText("//select[@data-vv-name='city']", city);
		Browser.waitFortheElementXpath("//select[@data-vv-name='pincode']/option[@item-value='pinCodeValue'][1]"); // pinCodeLaod
		Browser.clickOnTheElementByXpath("//select[@data-vv-name='pincode']"); // pinCodeSelect
		Browser.clickOnTheElementByXpath("//select[@data-vv-name='pincode']/option[@item-value='pinCodeValue'][1]"); // firstPinCode
		Browser.waitFortheElementXpath("//select[@data-vv-name='branch']/option[@item-value='id'][1]"); // branchLoad
		Browser.clickOnTheElementByXpath("//select[@data-vv-name='branch']"); // branchSelect
		Browser.clickOnTheElementByXpath("//select[@data-vv-name='branch']/option[@item-value='id'][1]"); // firstBranch
		Browser.clickOnTheElementByXpath("//input[@name='slot date']"); // clickSelectDateDrp
		Browser.clickOnTheElementByXpath("(//span[text()='" + date + "'])[2]");
		Browser.waitFortheElementXpath("//select[@data-vv-name='Slot']/option[@item-value='slotCode'][1]"); // slotLoad
		Browser.clickOnTheElementByXpath("//select[@data-vv-name='Slot']"); // slotSelectDrp
		slotTime = Browser.getTextByXpath("//select[@data-vv-name='Slot']/option[@item-value='slotCode'][1]"); // getFirstTimeSlot
		Browser.clickOnTheElementByXpath("//select[@data-vv-name='Slot']/option[@item-value='slotCode'][1]"); // firstSlotClick
		Browser.enterTextByID("custAddress", address);
		Browser.enterTextByID("custLandmark", Landmark);
		return slotTime;
	}
	
	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : Affilates customer info details Save
	 * 
	 * @Parameters : NA
	 * 
	 * @Return : NA
	 */
	public void affiliates_custInfo_Save() {
		Browser.clickOnTheElementByXpath("//button[text()='Save']");
	}
	
	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : Affilates click add package Btn
	 * 
	 * @Parameters :
	 * 
	 * @Return :
	 */
	public void affiliates_clickAddPackage() {
		Browser.waitFortheElementXpath("//div[text()='Booking Details']"); // bookingHeader
		Browser.clickOnTheElementByXpath("//button[text()='Packages']"); // packageBtn
	}
	
	
	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : Affilates add package
	 * 
	 * @Parameters : packageName
	 * 
	 * @Return : packagePrice
	 */
	public String affiliates_addPackage(String packageName) throws Exception {
		String packagePrice = null;
		Browser.waitFortheElementXpath("//h5[contains(., 'Add Package')]"); // packagePopUpHeader
		Browser.clickOnTheElementByXpath("//i[text()='arrow_drop_down']"); // paginationDrp
		Browser.clickOnTheElementByXpath("//div[@class='v-list__tile__title' and text()='All']"); // AllDrp
		Browser.scrollbyxpath("//td[text()='" + packageName + "'][1]"); // searchList
		packagePrice = Browser.getTextByXpath("//td[text()='" + packageName + "'][1]/following-sibling::td[3]"); // searchListPrice
		Browser.clickOnTheElementByXpath("//td[text()='" + packageName + "'][1]/following-sibling::td/button"); // pkgAddBtn
		return packagePrice;
	}
	
	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : Affilates add test / profile
	 * 
	 * @Parameters : serviceName
	 * 
	 * @Return : serviceCost
	 */
	public String affiliates_addService(String serviceName) throws Exception {
		String serviceCost = null;
		Browser.clickOnTheElementByXpath("//div[text()='Booking Details']"); // bookingHeader
		Browser.clickOnTheElementByXpath("//button[text()='Tests/Profiles']"); // addTestProfileBtn
		Browser.waitFortheElementXpath("//h5[contains(., 'Add Test / Profile')]"); // PopUpHeader
		Browser.enterTextByXpath("(//input[@type='text'])[1]", serviceName); // search
		Browser.waitFortheElementXpath("//td[text()='" + serviceName + "'][1]"); // searchList
		serviceCost = Browser.getTextByXpath("//td[text()='" + serviceName + "'][1]/following-sibling::td[3]"); // searchListPrice
		Browser.clickOnTheElementByXpath("//td[text()='" + serviceName + "'][1]/following-sibling::td/button"); // pkgAddBtn
		Browser.clickOnTheElementByXpath("//button[text()='Close']"); // closeBtn
		return serviceCost;
	}
	
	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : Affilates add patient
	 * 
	 * @Parameters : number, firstName, LastName, email, age, gender
	 * 
	 * @Return :
	 */
	public void affiliates_addPatient(String number, String firstName, String LastName, String email, String age, String gender) throws Exception {
		Browser.waitFortheElementXpath("//h5[contains(., 'Add Patient')]"); // PopUpHeader
		Browser.enterTextByID("custMobileNumber", number);
		Browser.enterTextByID("custfirstName", firstName);
		Browser.enterTextByID("custlastName", LastName);
		Browser.enterTextByID("custemail", email);
		Browser.enterTextByID("custage", age);
		Browser.selectXpathByText("//select[@data-vv-name='Gender']", gender);
		Browser.clickOnTheElementByXpath("//button[text()='Save']"); // SaveBtn
	}
	
	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : Affilates verify added patients in listing
	 * 
	 * @Parameters : firstName
	 * 
	 * @Return :
	 */
	public void affiliates_addPatientList(String firstName) {
		Browser.waitFortheElementXpath("//tbody/tr[contains(., '" + firstName + "')]");
	}
	
	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : Affilates booking details patient list map
	 * 
	 * @Parameters : packageName
	 * 
	 * @Return :
	 */
	public void affiliates_bookingPatientList(String packageName, String patientName) {
		Browser.waitFortheElementXpath("//td[text()='" + packageName + "']"); // searchList
		Browser.clickOnTheElementByXpath("//td[text()='" + packageName + "']/following-sibling::td/div"); // patientListDrp
		Browser.clickOnTheElementByXpath(
				"//td[text()='" + packageName + "']/following-sibling::td/div/div/ul/li/span/span[text()='" + patientName + "']");
	}
	
	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : Affilates booking details list price of service
	 * 
	 * @Parameters : packageName
	 * 
	 * @Return : listPrice
	 */
	public String affiliates_bookingDetails_listPrice(String packageName) {
		String listPrice = null;
		listPrice = Browser.getTextByXpath("//td[text()='" + packageName + "']/following-sibling::td[4]");
		return listPrice;
	}
	
	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : Affilates booking details quantity of service
	 * 
	 * @Parameters : packageName
	 * 
	 * @Return : serviceQuantity
	 */
	public String affiliates_bookingDetails_quantity(String packageName) {
		String serviceQuantity = null;
		serviceQuantity = Browser.getTextByXpath("//td[text()='" + packageName + "']/following-sibling::td[3]");
		return serviceQuantity;
	}
	
	/*
	 * @Author : Sagar Sen
	 * 
	 * @Description : Affilates booking paymenttype (CASH , ONLINE)
	 * 
	 * @Parameters : payMode
	 * 
	 * @Return :
	 */
	public void affiliates_selectPayMode_ConfirmBooking(String payMode) throws Exception {
		Browser.clickOnTheElementByXpath("//input[@value='" + payMode + "']");
		Thread.sleep(1000);
		Browser.clickOnTheElementByXpath("//button[text()='BOOK']"); // BookBtn
	}
}
